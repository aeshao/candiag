      PROGRAM PRDCDOC 
C     PROGRAM PRDCDOC(IN1,      SRTDOC,      DOCPGM,      OUTPUT,       )       G2
C    1          TAPE1=IN1,TAPE2=SRTDOC,TAPE3=DOCPGM,TAPE6=OUTPUT) 
C     -----------------------------------------------------------               G2
C                                                                               G2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       G2
C     JAN 26/93 - F.MAJAESS (MODIFIED TO RUN ON UNIX)                           G2
C     JAN 23/91 - F.MAJAESS (ADD 'J' SECTION)                                   
C     AUG 08/88 - F.MAJAESS.
C                                                                               G2
CPRDCDOC - PRODUCES PROGRAM SOURCE LIBRARY DOCUMENTATIONS               2  1    G1
C                                                                               G3
CAUTHOR  - F.MAJAESS.                                                           G3
C                                                                               G3
CPURPOSE - PRODUCES PROGRAM SOURCE LIBRARY DOCUMENTATIONS.                      G3
C                                                                               G3
CINPUT FILES...                                                                 G3
C                                                                               G3
C      IN1    = INPUT FILE CONTAINING NECESSARY INFORMATIONS ABOUT THE SOURCE   G3
C               LIBRARY AS WELL AS THE VARIOUS DOCUMENTATION SECTIONS.          G3
C      SRTDOC = INPUT FILE CONTAINING THE SORTED PROGRAMS DOCUMENTATIONS WHICH  G3
C               WERE EXTRACTED FROM THE PROGRAM SOURCE LIBRARY BY "DCXTRCT"     G3
C               PROGRAM.                                                        G3
C                                                                               G3
COUTPUT FILE...                                                                 G3
C                                                                               G3
C      DOCPGM = CONTAINS THE SOURCE LIBRARY PROGRAM DOCUMENTATIONS.             G3
C                                                                               G3
C------------------------------------------------------------------------ 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NCHR(10)
C 
      CHARACTER*1 DIAG,DIAGL(10),DIAGX,SCTN,SECTION,SBSCTN,SUBSCTN, 
     1            DECK*8,DKNAME*8,ALINE*80,HEAD(10)*80, 
     2            BLANKL*80,EQUALL*80,MINUSL*80,WRKLN1*80,WRKLN2*80,
     3            DOLLRL*80,DOTTDL*80 
C 
      DATA DIAGL/'A','B','C','D','E','F','G','H','I','J'/ 
      DATA NCHR / 10*0/, NDSCTN /10/
C-------------------------------------------------------------- 
      NFF=4
      CALL JCLPNT(NFF,-1,-2,-3,6)
C 
      REWIND 1
      REWIND 2
      REWIND 3
C 
      DO 10 I=1,80
        BLANKL(I:I)=' ' 
        EQUALL(I:I)='=' 
        MINUSL(I:I)='-' 
        IF(MOD(I,10).EQ.0)THEN
          DOLLRL(I:I)='+' 
          DOTTDL(I:I)='+' 
        ELSE
          IF(MOD(I,5).EQ.0)THEN 
            DOLLRL(I:I)=':' 
            DOTTDL(I:I)=':' 
          ELSE
            DOLLRL(I:I)='$' 
            DOTTDL(I:I)='.' 
          ENDIF 
        ENDIF 
  10  CONTINUE
C 
C     * READ AND WRITE THE DATA FROM FILE "IN1". ALSO, READ THE HEADING 
C     * FOR EACH DOCUMENTATION SECTION. 
C 
 100  READ(1,1000,END=200)ALINE 
      IF(ALINE(2:3).EQ.') ')THEN
        DO 110 I=1,NDSCTN 
          IF(ALINE(1:1).EQ.DIAGL(I))THEN
            HEAD(I)=ALINE 
            DO 105 J=1,80 
              IF(ALINE(J:J).NE.' ')NCHR(I)=J
 105        CONTINUE
            GO TO 120 
          ENDIF 
 110    CONTINUE
      ENDIF 
 120  WRITE(3,1000)ALINE
      GO TO 100 
C 
C     * CHECK IF ALL THE SECTION HEADINGS HAVE BEEN DEFINED.
C 
 200  DO 205 I=1,NDSCTN 
        IF(NCHR(I).EQ.0)THEN
          HEAD(I)(1:1)=DIAGL(I) 
          HEAD(I)(2:4)=')  '
          NCHR(I)=4 
          GO TO 205 
        ENDIF 
        IF(NCHR(I).LT.4)NCHR(I)=4 
 205  CONTINUE
C 
C     * READ IN THE DOCUMENTATIONS LINES FROM FILE SRTDOC AND WRITE THEM OUT. 
C 
      DIAG='0'
      SECTION='*' 
      DKNAME='********' 
      SUBSCTN='*' 
      INDEX=0 
 210  READ(2,2000,END=900)ALINE,DIAGX,SCTN,DECK,SBSCTN
C 
C     * ADJUST FOR THE GAP BETWEEN TWO MAJOR DOCUMENTATION SECTIONS.
C 
      IF(DIAGX.NE.DIAG)THEN 
        DO 220 I=1,NDSCTN 
          IF(DIAGX.EQ.DIAGL(I))THEN 
            INDEX=I 
            GO TO 225 
          ENDIF 
 220    CONTINUE
        GO TO 210 
 225    IF(SUBSCTN.EQ.'5') WRITE(3,3080)DOTTDL
        WRITE(3,3020)EQUALL 
        DIAG=DIAGX
        SECTION=SCTN
        DKNAME=DECK 
        SUBSCTN=SBSCTN
        LSTRNG=NCHR(INDEX)-3
        WRKLN1=BLANKL 
        WRKLN1(1:77)=HEAD(INDEX)(4:80)
        WRKLN2=BLANKL 
        WRKLN2(1:LSTRNG)=MINUSL(1:LSTRNG) 
        WRITE(3,3030)DIAG,DIAG,WRKLN1,WRKLN2
      ENDIF 
C 
C     * ADJUST FOR THE GAP BETWEEN TWO MINOR DOCUMENTATION SECTIONS.
C 
      IF(SECTION.NE.SCTN)THEN 
        IF(DIAG.EQ.'0')THEN 
C         * IN "DIAGPGM"
          DO 230 I=1,NDSCTN 
            IF(SCTN.EQ.DIAGL(I))THEN
              INDEX=I 
              GO TO 240 
            ENDIF 
 230      CONTINUE
          GO TO 210 
 240      LSTRNG=NCHR(INDEX)
          WRKLN1=BLANKL 
          WRKLN1(1:LSTRNG)=MINUSL(1:LSTRNG) 
          WRITE(3,3040)HEAD(INDEX),WRKLN1 
        ELSE
C         * BETWEEN TABLE AND BODY OF A MAJOR SECTION.
          WRITE(3,3050)MINUSL 
        ENDIF 
        SECTION=SCTN
        DKNAME=DECK 
        SUBSCTN=SBSCTN
      ENDIF 
C 
C     * ADJUST FOR THE GAP BETWEEN PROGRAMS IN THE BODY OF A DOCUMENTATION
C     * SECTION.
C 
      IF((DECK.NE.DKNAME).AND.(DIAG.NE.'0').AND.(SECTION.EQ.'2'))THEN 
C       WRITE(3,3060)DOLLRL 
        WRITE(3,3060)DOTTDL 
        DKNAME=DECK 
        SUBSCTN=SBSCTN
      ENDIF 
C 
C     * ADJUST FOR THE GAP JUST BEFORE  "CARD(S) READ..." IN THE BODY 
C     * PROGRAM OF A DOCUMENTATION SECTION. 
C 
      IF((SUBSCTN.NE.SBSCTN).AND.(SBSCTN.EQ.'4'))THEN 
        WRITE(3,3070) 
        SUBSCTN=SBSCTN
      ENDIF 
C 
C     * WRITE OUT THE DOCUMENTATION LINE. 
C 
      SUBSCTN=SBSCTN
      IF(SUBSCTN.EQ.'2')THEN
C       * PROGRAM HEADING LINE
        WRKLN1=BLANKL 
        WRKLN1(1:74)=ALINE(7:80)
        WRITE(3,3090)WRKLN1,DIAG,DKNAME 
C       WRITE(3,1000)WRKLN1 
      ELSE
        IF(ALINE(1:1).EQ.'C')THEN 
C       * LINE STARTING WITH "C" IS SHIFTED 1 CHARACTER TO THE LEFT.
          WRKLN1=BLANKL 
          WRKLN1(1:79)=ALINE(2:80)
          IF(SUBSCTN.EQ.'0')THEN
            WRITE(3,1000)WRKLN1 
          ELSE
            WRITE(3,3090)WRKLN1,DIAG,DKNAME 
          ENDIF 
        ELSE
C       * LINE STARTING WITH OTHER THAN "C" ESPECIALLY THOSE LINES
C       * STARTING WITH "*" ARE WRITTEN WITH "*" REPLACED BY " ". 
          ALINE(1:1)=' '
          IF(SUBSCTN.EQ.'0')THEN
            WRITE(3,1000)ALINE
          ELSE
            WRITE(3,3090)ALINE,DIAG,DKNAME
          ENDIF 
        ENDIF 
      ENDIF 
      GO TO 210 
C 
C     * E-O-F ENCOUNTERED.
C 
 900  IF(SUBSCTN.EQ.'5') WRITE(3,3080)DOTTDL
      WRITE(3,3020)EQUALL 
      CALL                                         XIT('PRDCDOC',0) 
C-------------------------------------------------------------------- 
 1000 FORMAT(A80) 
 2000 FORMAT(A80,1X,2A1,A8,A1) 
 3020 FORMAT(//A80/'+WEOR') 
 3030 FORMAT('+DECK DIAG',A1/ 
     1       'DIAGNOSTICS PROGRAMS USER MANUAL - ',A1/
     2       '------------------------------------'// 
     3       A80/A80//'CONTENTS... '//) 
 3040 FORMAT(//A80/A80//) 
 3050 FORMAT(//A80//'DESCRIPTIONS... '//) 
 3060 FORMAT(/A80///) 
 3070 FORMAT(/'CARD(S) READ...'/) 
 3080 FORMAT(/A80/) 
 3090 FORMAT(A80,2X,A1,2X,A8) 
      END 
