      PROGRAM ZXINT 
C     PROGRAM ZXINT (ZXIN,       INPUT,       OUTPUT,                   )       A2
C    1         TAPE1=ZXIN, TAPE5=INPUT, TAPE6=OUTPUT) 
C     -----------------------------------------------                           A2
C                                                                               A2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       A2
C     JAN 15/93 - E. CHAN  (DECODE LEVELS IN 8-WORD LABEL)                      
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)             
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)               
C     JUN 13/89 - F.MAJAESS (CORRECT 6035 FORMAT STATEMENT)                     
C     NOV 29/83 - J.D.HENDERSON, B.DUGAS. 
C                                                                               A2
CZXINT   - GRAPHS LAT AVG AND PRES INTEGRAL OF A ONE CROSS-SECTION      1  0 C GA1
C                                                                               A3
CAUTHOR  - J.D.HENDERSON                                                        A3
C                                                                               A3
CPURPOSE - GRAPHS ONE CROSS-SECTION (FROM FILE ZXIN) WHICH IS AVERAGED          A3
C          IN LATITUDE AND INTEGRATED IN PRESSURE. THE VALUE OF THE             A3
C          GLOBAL INTEGRAL PER UNIT AREA IS ALSO PRINTED.                       A3
C          NOTE - AT INPUT THE CROSS-SECTION IS ORDERED FROM S TO N AND         A3
C                 TOP TO BOTTOM.                                                A3
C                 MAXIMUM LATITUDES IS $BJ$ AND MAXIMUM LEVELS IS $L$.          A3
C                                                                               A3
CINPUT FILE...                                                                  A3
C                                                                               A3
C      ZXIN = FILE CONTAINING ONE CROSS-SECTION.                                A3
C 
CINPUT PARAMETERS...
C                                                                               A5
C      CONST     = MULTIPLICATIVE CONSTANT                                      A5
C      GMIN,GMAX = MIN AND MAX LIMITS FOR THE GRAPH OF PRESSURE                 A5
C                  INTEGRAL VS. LATITUDE.                                       A5
C                  IF BOTH ARE ZERO THE LIMITS ARE CALCULATED.                  A5
C      LABEL     = 80 CHARACTER LABEL PRINTED ON THE GRAPH.                     A5
C                                                                               A5
CEXAMPLE OF INPUT CARD...                                                       A5
C                                                                               A5
C*   ZXINT        1.        0.        0.                                        A5
C* LABEL CARD GOES HERE                                                         A5
C---------------------------------------------------------------------------- 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMMON/BLANCK/XA($BJ$,$L$)
C 
      LOGICAL OK
      INTEGER LEV($L$),LABEL(10)
      REAL FL($L$)
      REAL PR($L$),PRH($LP1$) 
      REAL F($BJ$),G($BJ$)
      REAL*8 SL($BJ$),CL($BJ$),WL($BJ$),WOSSL($BJ$),RAD($BJ$)
      COMMON/ICOM/IBUF(8),IDAT($BJV$)
      DATA MAXX/$BJV$/, MAXL/$L$/, MAXJ/$BJ$/ 
C---------------------------------------------------------------------
      NFF=3 
      CALL JCLPNT(NFF,1,5,6)
      REWIND 1
      PIH=3.14159265E0/2.E0 
C 
C     * READ MULT. CONSTANT, GRAPH LIMITS, AND THE LABEL. 
C 
      READ(5,5010,END=901) CONST,GMIN,GMAX                                      A4
      READ(5,5012,END=902) LABEL                                                A4
      WRITE(6,6006) CONST 
C 
C     * GET THE CROSS-SECTION. STOP IF THE FILE IS EMPTY. 
C     * LEV WILL CONTAIN THE PRESSURE LEVEL VALUES IN MILLIBARS.
C 
      CALL GETZX2(1,XA,MAXJ,LEV,NLEV,IBUF,MAXX,OK) 
      IF(.NOT.OK) CALL                             XIT('ZXINT',-1)
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('ZXINT',-2)
      NLAT=IBUF(5)
C 
C     * COMPUTE PRESSURE HALF LEVELS FOR INTEGRAL IN P. 
C     * BOTTOM BOUNDARY IS CHOSEN TO BE 1013.3 MB.
C     * PR,PRH ARE SET TO N/M**2. 
C 
      CALL LVDCODE(PR,LEV,NLEV)
      DO 120 L=1,NLEV 
  120 PR(L)=PR(L)*100.E0
      PRH(1)=PR(1)*0.5E0 
      DO 130 L=2,NLEV 
  130 PRH(L)=0.5E0*(PR(L-1)+PR(L)) 
      PRH(NLEV+1)=1013.3E0*100.E0 
C 
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR 
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).
C 
      ILATH=NLAT/2
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL) 
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL) 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * AVERAGE EACH LEVEL IN LATITUDE. PLOT ON A GRAPH.
C 
      WRITE(6,6001) 
      DO 280 L=1,NLEV 
      FL(L)=0.E0
      DO 240 J=1,NLAT 
      WLJ=WL(J)*0.5E0
      FL(L)=FL(L)+XA(J,L)*WLJ 
  240 CONTINUE
      WRITE(6,6028) LEV(L),FL(L)
  280 CONTINUE
      CALL SPLAT(FL,NLEV,1,NLEV,1,0.E0,0.E0)
C 
C     * THEN INTEGRATE THIS IN PRESSURE.
C 
      VAL=0.E0
      DO 305 L=1,NLEV 
  305 VAL=VAL+FL(L)*(PRH(L+1)-PRH(L)) 
      VAL=VAL*CONST 
      WRITE(6,6030) VAL 
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
C     * INTEGRATE EACH LATITUDE IN PRESSURE. PLOT ON A GRAPH (N TO S).
C 
      DO 410 J=1,NLAT 
      F(J)=0.E0 
      DO 405 L=1,NLEV 
  405 F(J)=F(J)+XA(J,L)*(PRH(L+1)-PRH(L)) 
      F(J)=F(J)*CONST 
      K=NLAT+1-J
      G(K)=F(J) 
  410 CONTINUE
      CALL SPLAT(G,NLAT,1,NLAT,1,GMIN,GMAX) 
C 
C     * AVERAGE THIS IN LATITUDE (S.HEM THEN N.HEM).
C 
      NLATH=NLAT/2
      NLATH1=NLATH+1
      VAL=F(1)*WL(1)*0.5E0 
      DO 450 J=2,NLATH
  450 VAL=VAL+0.5E0*F(J)*WL(J) 
      VALS=2.E0*VAL 
      VAL=0.E0
      DO 452 J=NLATH1,NLAT
  452 VAL=VAL+0.5E0*F(J)*WL(J) 
      VALN=2.E0*VAL 
      VAL=(VALS+VALN)*0.5E0
      WRITE(6,6035) VAL,VALS,VALN 
      WRITE(6,6040) LABEL 
C 
      CALL                                         XIT('ZXINT',0) 
C 
C     * E.O.F. ON INPUT.
C 
  901 CALL                                         XIT('ZXINT',-3)
  902 CALL                                         XIT('ZXINT',-4)
C---------------------------------------------------------------------
 5010 FORMAT(10X,3E10.0)                                                        A4
 5012 FORMAT(10A8)                                                              A4
 6001 FORMAT('1')
 6006 FORMAT(' ZXINT CONST =',E12.4)
 6028 FORMAT('0 LATITUDE AVERAGE AT LEVEL',I5,1PE15.6)
 6030 FORMAT('0  GLOBAL INTEGRAL/UNIT AREA =',1PE15.6)
 6035 FORMAT('0 INTEGRAL/UNIT AREA..GLOBAL =',1PE15.6,
     1          '   S.HEM =',1PE15.6,'   N.HEM =',1PE15.6)
 6040 FORMAT('0',30X,10A8)
      END 
