      PROGRAM SPLINV
C     PROGRAM SPLINV (SPIN,       SPOUT,       OUTPUT,                  )       E2
C    1          TAPE1=SPIN, TAPE2=SPOUT, TAPE6=OUTPUT)
C     ------------------------------------------------                          E2
C                                                                               E2
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                
C     SEP 13/85 - B. DUGAS, S. LAMBERT.                                        
C                                                                               E2
CSPLINV  - MULTIPLY SPECTRAL FILE BY A**2/(N*(N+1))                     1  1   GE1
C                                                                               E3
CAUTHOR  - S.LAMBERT                                                            E3
C                                                                               E3
CPURPOSE - COMPUTES THE INVERSE LAPLACIAN OF A GLOBAL SPECTRAL FILE             E3
C          BY MULTIPLYING EACH COEFF BY A*A/(N*(N+1))                           E3
C          WHERE, A IS THE RADIUS OF THE EARTH                                  E3
C          AND    N IS THE DEGREE OF THE ASSOCIATED LEGENDRE POLYNOMIAL.        E3
C          NOTE - ALL THE INPUT RECORDS ARE SUPPOSED TO HAVE THE SAME           E3
C                 SPECTRAL DIMENSIONS.                                          E3
C                                                                               E3
CINPUT FILE...                                                                  E3
C                                                                               E3
C      SPIN  = GLOBAL SPECTRAL FIELDS                                           E3
C                                                                               E3
COUTPUT FILE...                                                                 E3
C                                                                               E3
C      SPOUT = CORRESPONDING FILE OF INVERSE LAPLACIAN OF SPIN.                 E3
C-----------------------------------------------------------------------------
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      COMPLEX F($RPM$)
      DIMENSION LSR(2,$MP1$),SCAL($RPM$)
  
      COMMON /ICOM/IBUF(8),IDAT($2RPMV$) 
      DATA MAXX/$2RPMV$/
  
C-----------------------------------------------------------------------
      NF   = 3
      CALL JCLPNT(NF,1,2,6) 
      REWIND 1
      REWIND 2
  
      A      = 6371000.E0 
      ASQ    = A*A
      NRECS  = 0
  
C     * READ IN DATA. 
  
100   CALL GETFLD2(1,F,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)
          IF (.NOT.OK)                                         THEN 
              IF (NRECS.EQ.0) CALL                 XIT('SPLINV',-1) 
              CALL PRTLAB (IBUF)
              WRITE(6,6020) NRECS 
              CALL                                 XIT('SPLINV',0)
          ENDIF 
  
C         * CALCULATE MULTIPLICATIVE CONSTANTS ONCE AND FOR ALL.
  
          IF (NRECS.EQ.0)                                      THEN 
              CALL PRTLAB (IBUF)
              CALL DIMGT(LSR,LA,LR,LM,KTR,IBUF(7))
  
              DO 200 M=1,LM 
                  NN                     = LSR(1,M+1)-LSR(1,M)
                  DO 200 J=1,NN 
                      N                  = M+J-2
                      DEN                = FLOAT(N*(N+1)) 
                      SCAL(LSR(1,M)-1+J) = DEN/ASQ
  200         CONTINUE
  
              DO 250 L=2,LA 
                  SCAL(L) = 1.E0/SCAL(L)
  250         CONTINUE
          ENDIF 
  
C         * DO THE CONVERSION.
  
          F(1) = CMPLX(0.0E0,0.0E0) 
          DO 300 L=2,LA 
              F(L) = CMPLX( REAL(F(L))*SCAL(L),  IMAG(F(L))*SCAL(L))
  300     CONTINUE
  
C         * SAVE RECORD UNDER THE NAME LINV.
  
          IBUF(3) = NC4TO8("LINV")
          CALL PUTFLD2 (2,F,IBUF,MAXX) 
          NRECS   = NRECS+1 
  
      GOTO 100
C-----------------------------------------------------------------------
6020  FORMAT('0SPLINV COMPUTED ',I5,' RECORDS')
      END 
