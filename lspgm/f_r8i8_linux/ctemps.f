      PROGRAM CTEMPS                                                                                                                
C     PROGRAM CTEMPS (SSPHIS,       SSPHI,        SSTEMP,       INPUT,          E2                                                  
C    1                                                          OUTPUT, )       E2                                                  
C    2          TAPE1=SSPHIS, TAPE2=SSPHI , TAPE3=SSTEMP, TAPE5=INPUT,                                                              
C    3                                                    TAPE6=OUTPUT)                                                             
C     -----------------------------------------------------------------         E2                                                  
C                                                                               E2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2                                                  
C     FEB 15/94 - F.MAJAESS (REVISE FOR TRUNCATIONS > 99)                                                                           
C     JAN 12/93 - E. CHAN  (DECODE LEVEL INFORMATION IN IBUF)                                                                       
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 06/83 - R.LAPRISE.                                                                                                        
C     AUG 25/81 - J.D.HENDERSON.                                                                                                    
C                                                                               E2                                                  
CCTEMPS  - CONVERTS SPECTRAL PHI FILE TO SPECTRAL TEMPERATURES          2  1    E1                                                  
C                                                                               E3                                                  
CAUTHOR  - J.D.HENDERSON                                                        E3                                                  
C                                                                               E3                                                  
CPURPOSE - CONVERTS SPECTRAL GEOPOTENTIALS (PHI) ON MODEL FULL LEVELS           E3                                                  
C          TO SPECTRAL TEMPERATURES ON MODEL HALF LEVELS.                       E3                                                  
C          EACH SET CONTAINS SIGMA LEVELS 1 TO ILEV.                            E3                                                  
C                                                                               E3                                                  
CINPUT FILES...                                                                 E3                                                  
C                                                                               E3                                                  
C      SSPHIS = INPUT FILE OF SPECTRAL MOUNTAIN FIELDS.                         E3                                                  
C      SSPHI  = INPUT FILE OF SPECTRAL GEOPOTENTIALS (PHI) ON FULL              E3                                                  
C                             SIGMA LEVELS.                                     E3                                                  
C                                                                               E3                                                  
COUTPUT FILE...                                                                 E3                                                  
C                                                                               E3                                                  
C      SSTEMP = OUTPUT FILE OF SPECTRAL TEMPERATURES ON HALF LEVELS.            E3                                                  
C---------------------------------------------------------------------------                                                        
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMPLEX SP                                                                                                                    
      COMMON/BLANCK/SP(216645)                                                                                                      
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      INTEGER KBUF(8)                                                                                                               
      INTEGER LEV(100),LEVH(100)                                                                                                    
      INTEGER LSR(2,66)                                                                                                             
      REAL S(100),SF(100)                                                                                                           
      COMMON/ICOM/IBUF(8),IDAT(4290)                                                                                                
      DATA MAXX/4290/, MAXL/100/                                                                                                    
C---------------------------------------------------------------------                                                              
      NFF=4                                                                                                                         
      CALL JCLPNT(NFF,1,2,3,6)                                                                                                      
      DO 110 N=1,3                                                                                                                  
  110 REWIND N                                                                                                                      
C                                                                                                                                   
C     * GET SIGMA VALUES FROM PHI FILE.                                                                                             
C                                                                                                                                   
      CALL FILEV(LEV,NLEV,IBUF,2)                                                                                                   
      IF((NLEV.LT.2).OR.(NLEV.GT.MAXL)) CALL       XIT('CTEMPS',-1)                                                                 
      WRITE(6,6005) (LEV(L),L=1,NLEV)                                                                                               
      CALL PRTLAB (IBUF)                                                                                                            
      DO 130 I=1,8                                                                                                                  
  130 KBUF(I)=IBUF(I)                                                                                                               
      NWDS=2*IBUF(5)*IBUF(6)                                                                                                        
      NZT=NWDS+1                                                                                                                    
C                                                                                                                                   
C     * SET LEVH TO SIGMA HALF LEVELS FOR OUTPUT LABELS.                                                                            
C                                                                                                                                   
      CALL LVDCODE(S,LEV,NLEV)                                                                                                      
      DO 220 L=1,NLEV                                                                                                               
  220 S(L)=S(L)*0.001E0                                                                                                             
      NLEVM=NLEV-1                                                                                                                  
      DO 230 L=1,NLEVM                                                                                                              
  230 LEVH(L)=INT(1000.E0*SQRT(S(L)*S(L+1))+0.5E0)                                                                                  
      LEVH(NLEV)=INT(1000.E0*SQRT(S(NLEV))+0.5E0)                                                                                   
      IF(NSETS.EQ.0) WRITE(6,6005)(LEVH(L),L=1,NLEV)                                                                                
C                                                                                                                                   
C     * COMPUTE SF TO BE USED IN TFGZ.                                                                                              
C                                                                                                                                   
      DO 240 L=1,NLEVM                                                                                                              
  240 SF(L)=LOG(S(L+1)/S(L))                                                                                                        
      SF(NLEV)=LOG(1.E0/S(NLEV))                                                                                                    
C                                                                                                                                   
C     * GET THE MOUNTAIN FIELD.                                                                                                     
C                                                                                                                                   
      CALL GETFLD2(1,SP,NC4TO8("SPEC"),-1,NC4TO8("PHIS"),1,IBUF,MAXX,OK)                                                            
      IF(.NOT.OK) CALL                             XIT('CTEMPS',-2)                                                                 
      CALL PRTLAB (IBUF)                                                                                                            
      CALL CMPLBL(0,KBUF,0,IBUF,OK)                                                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        CALL PRTLAB (KBUF)                                                                                                          
        CALL PRTLAB (IBUF)                                                                                                          
        CALL                                       XIT('CTEMPS',-3)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * COMPUTE FIELD SIZES. LABEL WORD 7 CONTAINS TRUNCATION TYPE.                                                                 
C                                                                                                                                   
      LRLMT=IBUF(7)                                                                                                                 
      CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)                                                                                            
      CALL PRLRLMT (LA,LR,LM,KTR,LRLMT)                                                                                             
C---------------------------------------------------------------------                                                              
C     * GET THE NEXT SET OF PHI.                                                                                                    
C                                                                                                                                   
      NSETS=0                                                                                                                       
  250 CALL GETSET2(2,SP(NZT),LEV,NLEV,IBUF,MAXX,OK)                                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6010) NSETS                                                                                                         
        CALL                                       XIT('CTEMPS',0)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CHECK THE LABEL.                                                                                                            
C                                                                                                                                   
      CALL CMPLBL(0,KBUF,0,IBUF,OK)                                                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        CALL PRTLAB (KBUF)                                                                                                          
        CALL PRTLAB (IBUF)                                                                                                          
        CALL                                       XIT('CTEMPS',-4)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CONVERT TO TEMPERATURES AND WRITE ONTO FILE 1.                                                                              
      RGAS=287.04E0                                                                                                                 
      CALL TFGZ(SP(NZT),SP(NZT),SP(1),LA,NLEV,SF,RGAS)                                                                              
C                                                                                                                                   
      IBUF(3)=NC4TO8("TEMP")                                                                                                        
      CALL PUTSET2(3,SP(NZT),LEVH,NLEV,IBUF,MAXX)                                                                                   
      IF(NSETS.EQ.0) CALL PRTLAB (IBUF)                                                                                             
      NSETS=NSETS+1                                                                                                                 
      GO TO 250                                                                                                                     
C---------------------------------------------------------------------                                                              
 5010 FORMAT(10X,I5,13F5.1/16F5.1)                                                                                                  
 6005 FORMAT('0SIGMA LEVELS =',20I5/(15X,20I5))                                                                                     
 6010 FORMAT(' CTEMPS CONVERTS',I4,'  SETS OF GZ TO TEMP')                                                                          
      END                                                                                                                           
