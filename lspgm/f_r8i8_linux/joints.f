      PROGRAM JOINTS                                                                                                                
C     PROGRAM JOINTS (JOIN,         IN11,...        IN70,        OUTPUT,)       B2                                                  
C    1          TAPE10=JOIN, TAPE21=IN11,...,TAPE50=IN70, TAPE6 =OUTPUT)                                                            
C     ------------------------------------------------------------              B2                                                  
C                                                                               B2                                                  
C     NOV 25/09 - S.KHARIN (REMOVE "KIND"/"IBUF(1)" MATCH RESTRICTION CHECK)    B2                                                  
C     NOV 23/09 - S.KHARIN.                                                                                                         
C                                                                               B2                                                  
CJOINTS  - JOINS UP TO 60 FILES AND SAVE THEM AS TIME SERIES           60  1    B1                                                  
C                                                                               B3                                                  
CAUTHOR  - S.KHARIN                                                             B3                                                  
C                                                                               B3                                                  
CPURPOSE - JOINS TOGETHER UP TO 60 FILES INTO ONE TIME SERIES.                  B3                                                  
C                                                                               B3                                                  
CINPUT FILE(S)...                                                               B3                                                  
C                                                                               B3                                                  
C      IN11,... ,IN70 = INPUT FILES TO BE JOINED                                B3                                                  
C             KIND=SPEC AND KIND FOUR ARE NOT PERMITTED FOR INPUT FILES.        B3                                                  
C             ALL REMAINING RECORD TYPES (KIND=TIME, KIND=GRID, KIND=ZONL) ARE  B3                                                  
C             TREATED AS ONE-DIMENSIONAL TIME SERIES.                           B3                                                  
C                                                                               B3                                                  
COUTPUT FILE...                                                                 B3                                                  
C                                                                               B3                                                  
C      JOIN = FILE CONTAINING ALL OF THE ABOVE FILES JOINED.                    B3                                                  
C             LABL/CHAR RECORDS ARE RETAINED FROM THE LAST JOINED FILE.         B3                                                  
C             RECORD TYPE OF REMAINING RECORDS IS KIND=TIME.                    B3                                                  
C             THE LENGTH OF TIME SERIES IS THE SUM OF RECORD LENGTHS IN ALL     B3                                                  
C             INPUT FILES.                                                                                                          
C-------------------------------------------------------------------------                                                          
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      REAL F(65341)                                                                                                                 
      COMMON/ICOM/IBUF(65341)                                                                                                       
      LOGICAL OK                                                                                                                    
      DATA MAXX/65341/                                                                                                              
C                                                                                                                                   
C--------------------------------------------------------------------                                                               
      NF=62                                                                                                                         
      CALL JCLPNT(NF,10,11,12,13,14,15,16,17,18,19,20,                                                                              
     1                  21,22,23,24,25,26,27,28,29,30,                                                                              
     2                  31,32,33,34,35,36,37,38,39,40,                                                                              
     3                  41,42,43,44,45,46,47,48,49,50,                                                                              
     4                  51,52,53,54,55,56,57,58,59,60,                                                                              
     5                  61,62,63,64,65,66,67,68,69,70,6)                                                                            
      NF=NF-2                                                                                                                       
      REWIND 10                                                                                                                     
      DO N=11,10+NF                                                                                                                 
        REWIND N                                                                                                                    
      ENDDO                                                                                                                         
C                                                                                                                                   
      NR=0                                                                                                                          
 150  CONTINUE                                                                                                                      
C                                                                                                                                   
C     * READ RECORD FROM THE FIRST INPUT FILE                                                                                       
C                                                                                                                                   
      CALL GETFLD2(11,F,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                   
      IF (.NOT.OK)THEN                                                                                                              
        IF (NR.EQ.0) CALL                          XIT('JOINTS',-1)                                                                 
        WRITE(6,*)' JOINED ',NR,' RECORDS.'                                                                                         
        CALL                                       XIT('JOINTS',0)                                                                  
      ENDIF                                                                                                                         
      KIND=IBUF(1)                                                                                                                  
C                                                                                                                                   
C     * SPEC/FOUR ARE NOT ALLOWED                                                                                                   
C                                                                                                                                   
      IF (KIND.EQ.NC4TO8("SPEC").OR.                                                                                                
     +    KIND.EQ.NC4TO8("FOUR"))CALL              XIT('JOINTS',-2)                                                                 
      IF (KIND.EQ.NC4TO8("LABL").OR.KIND.EQ.NC4TO8("CHAR")) THEN                                                                    
C                                                                                                                                   
C       * SUPERLABELS/CHAR RECORDS MUST EXIST IN ALL INPUT FILE                                                                     
C                                                                                                                                   
        DO N=12,10+NF                                                                                                               
          CALL GETFLD2(N,F,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                
          IF (.NOT.OK) CALL                        XIT('JOINTS',-3)                                                                 
          IF(KIND.NE.IBUF(1)) CALL                 XIT('JOINTS',-4)                                                                 
        ENDDO                                                                                                                       
C                                                                                                                                   
C       * SAVE SUPERLABELS/CHAR RECORDS FROM THE LAST INPUT FILE                                                                    
C                                                                                                                                   
        CALL PUTFLD2(10,F,IBUF,MAXX)                                                                                                
      ELSE                                                                                                                          
C                                                                                                                                   
C       * MERGE ALL OTHER RECORD KINDS INTO A SINGLE TIME SERIES                                                                    
C                                                                                                                                   
        NWDS=IBUF(5)*IBUF(6)                                                                                                        
        DO N=12,10+NF                                                                                                               
          IF (IBUF(1).EQ.NC4TO8("SPEC").OR.                                                                                         
     +        IBUF(1).EQ.NC4TO8("FOUR")) CALL      XIT('JOINTS',-5)                                                                 
          CALL GETFLD2(N,F(NWDS+1),-1,-1,-1,-1,IBUF,MAXX,OK)                                                                        
          IF (.NOT.OK) CALL                        XIT('JOINTS',-6)                                                                 
          NWDS=NWDS+IBUF(5)*IBUF(6)                                                                                                 
        ENDDO                                                                                                                       
C                                                                                                                                   
C       * SAVED THE MERGED RECORD AS KIND=TIME                                                                                      
C                                                                                                                                   
        IBUF(1)=NC4TO8("TIME")                                                                                                      
        IBUF(5)=NWDS                                                                                                                
        IBUF(6)=1                                                                                                                   
        CALL PUTFLD2(10,F,IBUF,MAXX)                                                                                                
      ENDIF                                                                                                                         
      NR=NR+1                                                                                                                       
      GO TO 150                                                                                                                     
C----------------------------------------------------------------                                                                   
      END                                                                                                                           
                                                                                                                                    
