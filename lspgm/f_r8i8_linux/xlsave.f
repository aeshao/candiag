      PROGRAM XLSAVE                                                                                                                
C     PROGRAM XLSAVE (OLD,        ADD,        NEW,       INPUT,                 B2                                                  
C    1                                                   OUTPUT,        )       B2                                                  
C    2         TAPE11=OLD, TAPE12=ADD, TAPE13=NEW, TAPE5=INPUT,                                                                     
C    3                                             TAPE6=OUTPUT)                                                                    
C     ----------------------------------------------------------                B2                                                  
C                                                                               B2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2                                                  
C     FEB 21/92 - E. CHAN  (TREAT HYPERLABELS AS ASCII INSTEAD OF AS                                                                
C                           HOLLERITH LITERALS)                                                                                     
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAR 06/89 - F.MAJAESS (INCREASE FIELD DIMENSION TO 66000)                                                                     
C     JAN 04/84 - R.LAPRISE.                                                                                                        
C                                                                               B2                                                  
CXLSAVE  - ADD A HYPERLABELLED SET TO A FILE.                           2  1 C  B1                                                  
C                                                                               B3                                                  
CAUTHOR  - R.LAPRISE.                                                           B3                                                  
C                                                                               B3                                                  
CPURPOSE - ADD (OR REPLACE) A HYPERLABELLED SET FROM FILE "ADD" TO CONTENT      B3                                                  
C          OF FILE "OLD". "NEW" IS THE RESULTING FILE.                          B3                                                  
C          THE HYPERLABEL IS READ FROM AN INPUT CARD.                           B3                                                  
C                                                                               B3                                                  
CINPUT FILES...                                                                 B3                                                  
C                                                                               B3                                                  
C      OLD = EXISTING HYPERLABELLED FILE.                                       B3                                                  
C      ADD = SET OF RECORDS TO BE ADDED, PRECEEDED BY HYPERLABEL                B3                                                  
C            ON INPUT CARD.                                                     B3                                                  
C                                                                               B3                                                  
COUTPUT FILE...                                                                 B3                                                  
C                                                                               B3                                                  
C      NEW = RESULTING FILE.                                                    B3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               B5                                                  
C      LABEL = 80 CHARACTER HYPERLABEL TO BE USED.                              B5                                                  
C                                                                               B5                                                  
CEXAMPLE OF INPUT CARD...                                                       B5                                                  
C                                                                               B5                                                  
C*ARD007 M1A11GS                                                                B5                                                  
C------------------------------------------------------------------------------                                                     
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      CHARACTER LABEL*80,HLABL*80                                                                                                   
      COMMON /LCOM/ ILBL(8),LABEL                                                                                                   
      COMMON /ICOM/ IBUF(8),IDAT(65341)                                                                                             
      EQUIVALENCE (RLABL,LABEL), (HLABL,IDAT)                                                                                       
C                                                                                                                                   
      LOGICAL OK,LCOPY                                                                                                              
      COMMON /MACHTYP/ MACHINE,INTSIZE                                                                                              
C                                                                                                                                   
      DATA MAXX/65341/                                                                                                              
C     DATA ILBL/4HLABL,0,4HFILE,0,10,1,0,1/                                                                                         
C------------------------------------------------------------------------                                                           
      CALL SETLAB(ILBL,NC4TO8("LABL"),0,NC4TO8("FILE"),0,10,1,0,1)                                                                  
      NFF=5                                                                                                                         
      CALL JCLPNT(NFF,11,12,13,5,6)                                                                                                 
      REWIND 11                                                                                                                     
      REWIND 12                                                                                                                     
      REWIND 13                                                                                                                     
C                                                                                                                                   
C     * READ-IN HYPERLABEL CARD.                                                                                                    
C                                                                                                                                   
      READ(5,5010,END=901)LABEL                                                 B4                                                  
      WRITE(6,6030)ILBL                                                                                                             
      WRITE(6,6010)LABEL                                                                                                            
C                                                                                                                                   
C     * SCAN FILE OLD AND ADD TO NEW, UNLESS SAME HYPERLABEL ENCOUNTERED.                                                           
C     * IN THAT CASE THE HYPER SET IS SKIPPED.                                                                                      
C                                                                                                                                   
      LCOPY=.TRUE.                                                                                                                  
      NC=0                                                                                                                          
      NS=0                                                                                                                          
  100 CALL RECGET(11,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                      
      IF(.NOT.OK) GO TO 200                                                                                                         
      IF(IBUF(1).EQ.NC4TO8("LABL") .AND. IBUF(3).EQ.NC4TO8("FILE")) THEN                                                            
         IF( HLABL .EQ. LABEL ) LCOPY = .FALSE.                                                                                     
      ENDIF                                                                                                                         
C                                                                                                                                   
      IF(LCOPY) THEN                                                                                                                
         CALL RECPUT(13,IBUF)                                                                                                       
         NC=NC+1                                                                                                                    
      ELSE                                                                                                                          
         NS=NS+1                                                                                                                    
      ENDIF                                                                                                                         
C                                                                                                                                   
      GO TO 100                                                                                                                     
C                                                                                                                                   
C     * ADD THE CONTENT OF ADD TO NEW, PRECEEDED BY HYPERLABEL.                                                                     
C                                                                                                                                   
  200 CALL FBUFOUT(13,ILBL,10*MACHINE+8,K)                                                                                          
      NA=0                                                                                                                          
  250 CALL RECGET(12,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                      
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6030)IBUF                                                                                                           
        WRITE(6,6020)NC,NS,NA                                                                                                       
        IF(NA.EQ.0)THEN                                                                                                             
          CALL                                     XIT('XLSAVE',-1)                                                                 
        ELSE                                                                                                                        
          CALL                                     XIT('XLSAVE',0)                                                                  
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
      IF(IBUF(1).EQ.NC4TO8("LABL").AND.IBUF(3).EQ.NC4TO8("FILE"))                                                                   
     1   CALL                                      XIT('XLSAVE',-2)                                                                 
      CALL RECPUT(13,IBUF)                                                                                                          
      NA=NA+1                                                                                                                       
      IF(NA.EQ.1)WRITE(6,6030)IBUF                                                                                                  
      GO TO 250                                                                                                                     
C                                                                                                                                   
C     * E.O.F. ON INPUT.                                                                                                            
C                                                                                                                                   
  901 CALL                                         XIT('XLSAVE',-3)                                                                 
C-----------------------------------------------------------------------                                                            
 5010 FORMAT(A80)                                                               B4                                                  
 6010 FORMAT('+',50X,'=',A80)                                                                                                       
 6020 FORMAT('0 XLSAVE COPIED',I5,' RECORDS, SKIPPED',I5,                                                                           
     1      ', AND ADDED',I5, ' RECORDS.')                                                                                          
 6030 FORMAT(1X,A4,I10,1X,A4,I10,4I6)                                                                                               
      END                                                                                                                           
