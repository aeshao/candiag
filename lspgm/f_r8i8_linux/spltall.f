      PROGRAM SPLTALL                                                                                                               
C     PROGRAM SPLTALL (IN1,...,      IN3,      IN4,...,       IN31,             B2                                                  
C    1                                                         OUTPUT,  )       B2                                                  
C    2           TAPE1=IN1,...,TAPE3=IN3,TAPE7=IN4,...,TAPE34=IN31,                                                                 
C    3                                                   TAPE6=OUTPUT)                                                              
C     ----------------------------------------------------------------          B2                                                  
C                                                                               B2                                                  
C     NOV 19/13 - F.MAJAESS (ADD VERIFICATION CHECK BASED ON TOTAL BYTES SUM OF B2                                                  
C                            WRITTEN DATA RECORDS)                              B2                                                  
C     NOV 17/08 - S.KHARIN (INCREASE MAXUN TO 1024)                                                                                 
C     SEP 25/06 - F.MAJAESS (PHASE OUT VARIABLE NAME MAPPING TO UPPER CASE                                                          
C                            FILENAME)                                                                                              
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)                                                           
C     FEB 14/02 - S. KHARIN (ENSURE LEVEL CHECK IS DONE ON CONVERTED VALUES)                                                        
C     FEB 04/02 - S. KHARIN (SKIP SUPERLABELS)                                                                                      
C     JUN 06/01 - F. MAJAESS (INCREASE NUMBER OF INPUT FILES TO 31 FROM 3)                                                          
C     OCT 17/00 - S. KHARIN (WRITE OUT THE VARIABLE TABLE).                                                                         
C     JUL 26/00 - S. KHARIN.                                                                                                        
C                                                                               B2                                                  
CSPLTALL - SPLITS UP TO 31 MODEL TYPE FILES INTO ONE FILE PER VARIABLE 31       B1                                                  
C                                                                               B3                                                  
CAUTHORS - S.KHARIN AND F.MAJAESS.                                              B3                                                  
C                                                                               B3                                                  
CPURPOSE - SPLITS UP TO 31 MODEL TYPE FILES INTO ONE FILE PER VARIABLE.         B3                                                  
C          NOTE: THE TOTAL NUMBER OF UNITS AVAILABLE FOR WRITING OUT THE        B3                                                  
C                VARIABLES IS REDUCED BY THE NUMBER OF UNITS ASSIGNED FOR INPUT.B3                                                  
C                UNITS 5 AND 6 ARE RESERVED FOR THE NORMAL USE, AND UNIT 4 IS   B3                                                  
C                RESERVED FOR WRITING THE ".VARTABLE" (SEE BELOW) OUTPUT FILE.  B3                                                  
C                UNITS 100-102 RESERVED UNDER IRIX.                             B3                                                  
C                                                                                                                                   
C                                                                               B3                                                  
CINPUT FILE...                                                                  B3                                                  
C                                                                               B3                                                  
C      IN1,...,IN31 = FILES CONTAINING FIELDS TO BE SPLIT UP.                   B3                                                  
C                     FILES IN2, ..., IN3 ARE OPTIONAL.                         B3                                                  
C                     NO "SUPERLABEL" RECORDS IN THE INPUT FILES ARE ALLOWED.   B3                                                  
C                                                                               B3                                                  
COUTPUT FILES (INTERNALLY DEFINED)...                                           B3                                                  
C                                                                               B3                                                  
C     'X' = ALL RECORDS FOR VARIABLE '   X'.                                    B3                                                  
C           LEADING BLANKS ARE STRIPPED OFF FROM THE UPPER CASE FILE NAME.      B3                                                  
C           FOR EXAMPLE, RECORDS WITH THE NAME '  ST' ARE STORED IN A FILE 'ST'.B3                                                  
C '.VARTABLE' = AN ASCII TABLE WITH TIME STEP AND LEVEL INFORMATION FOR EACH    B3                                                  
C               VARIABLE. EACH LINE IN THIS FILE CONTAINS THE VARIABLE NAME     B3                                                  
C               FOLLOWED BY TIME STEP RANGE AND THE LEVEL RANGE.                B3                                                  
C----------------------------------------------------------------------------                                                       
C                                                                                                                                   
C     MAXVAR = MAXIMAL NUMBER OF VARIABLES IN INPUT FILES.                                                                          
C     MAXUN  = MAXIMAL NUMBER OF ALLOWED I/O UNITS.                                                                                 
C              (PREVIOUSLY SET TO "99" DUE TO THE LIMIT ON NEC SX[4,5] PLATFORMS)                                                   
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      PARAMETER ( MAXVAR=2000)                                                                                                      
      PARAMETER ( MAXUN=1024)                                                                                                       
                                                                                                                                    
      LOGICAL OK                                                                                                                    
      CHARACTER*32 CMD                                                                                                              
      INTEGER*8 NBYTES                                                                                                              
C                                                                                                                                   
C     INAM  = TABLE FOR VARIABLE NAME, TIME STEP RANGE AND LEVEL RANGE                                                              
C     IUOUT = ARRAY TO HOLD THE UNIT NUMBERS AVAILABLE FOR OUTPUT                                                                   
C                                                                                                                                   
      INTEGER INAM(5,MAXVAR), IUOUT(MAXUN)                                                                                          
      CHARACTER*4 NAME                                                                                                              
C                                                                                                                                   
      COMMON /MACHTYP/ MACHINE,INTSIZE                                                                                              
      COMMON/ICOM/IBUF(8),IDAT(65341)                                                                                               
      DATA MAXX/65341/                                                                                                              
                                                                                                                                    
C---------------------------------------------------------------------                                                              
      NF=32                                                                                                                         
      CALL JCLPNT(NF,1,2,3,7,8,9,10,11,12,13,14,15,16,17,18,19,20,                                                                  
     1                21,22,23,24,25,26,27,28,29,30,31,32,33,34,6)                                                                  
      NF=NF-1                                                                                                                       
      WRITE(6,6000) NF                                                                                                              
      IF (NF.LT.1) CALL                            XIT('SPLTALL',-1)                                                                
      LHEAD = 8 * MACHINE                                                                                                           
                                                                                                                                    
C     * SETUP...                                                                                                                    
C     * MAXOUT = MAXIMAL NUMBER OF OUTPUT FILES THAT CAN BE OPENED AT                                                               
C              ONCE.                                                                                                                
                                                                                                                                    
      MAXOUT = MAXUN-(NF+3)                                                                                                         
                                                                                                                                    
C     * IF MORE THAN 3 INPUT FILES TO BE PROCESSED, THEN ADJUST "NF"                                                                
C     * VALUE FOR UNITS 4-6 SINCE IT WILL USED AS A COUNTER LIMIT                                                                   
C     * FOR THE INPUT UNIT NUMBERS.                                                                                                 
                                                                                                                                    
      IF (NF.GT.3) NF=NF+3                                                                                                          
                                                                                                                                    
C     * ... AND INITIALIZE "IUOUT" ARRAY (EXCLUDE UNITS 4-6,100-102).                                                               
                                                                                                                                    
      IU=NF                                                                                                                         
      DO I=1,MAXOUT                                                                                                                 
       IU=IU+1                                                                                                                      
       IF(IU.GE.4.AND.IU.LE.6) IU=7                                                                                                 
       IF(IU.GE.100.AND.IU.LE.102) IU=103                                                                                           
       IUOUT(I)=IU                                                                                                                  
      ENDDO                                                                                                                         
                                                                                                                                    
C                                                                                                                                   
C     * NRECIN  = TOTAL NUMBER OF RECORDS READ FROM ALL INPUT FILES.                                                                
C     * NRECOUT = TOTAL NUMBER OF RECORDS WRITTEN TO OUTPUT FILES.                                                                  
C     * NVAR    = CURRENT NUMBER OF VARIABLE NAMES FOUND IN INPUT FILES.                                                            
C     * NVARP   = NUMBER OF VARIABLE NAMES FOUND DURING THE PREVIOUS SCAN.                                                          
C     * NVARM   = MAX NUMBER OF VARIABLE NAMES THAT CAN BE PROCESSED                                                                
C                 DURING THE CURRENT SCAN.                                                                                          
C     * NBYTES  = ACCUMULATED BYTES SUM OF WRITTEN DATA RECORDS.                                                                    
C                                                                                                                                   
      NRECIN=0                                                                                                                      
      NRECOUT=0                                                                                                                     
      NVAR=0                                                                                                                        
      NVARP=0                                                                                                                       
      NVARM=MAXOUT                                                                                                                  
      NSCAN=1                                                                                                                       
      NBYTES=0                                                                                                                      
C                                                                                                                                   
C     * SCAN INPUT FILES SEVERAL TIMES, IF NECESSARY.                                                                               
C                                                                                                                                   
 90   CONTINUE                                                                                                                      
      WRITE(6,6005) NSCAN                                                                                                           
C                                                                                                                                   
C     * DO FOR ALL INPUT FILES                                                                                                      
C                                                                                                                                   
      DO 130 N=1,NF                                                                                                                 
                                                                                                                                    
C       * SKIPPING UNITS 4-6 IF THEY ARE WITHIN "NF" RANGE.                                                                         
                                                                                                                                    
        IF(N.GE.4.AND.N.LE.6) GO TO 130                                                                                             
                                                                                                                                    
        REWIND N                                                                                                                    
        NREC=0                                                                                                                      
        WRITE(6,6010) N                                                                                                             
 100    CALL RECGET(N,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                     
        IF (.NOT.OK) THEN                                                                                                           
          IF (NREC.EQ.0) THEN                                                                                                       
            WRITE(6,6015) N                                                                                                         
            CALL                                   XIT('SPLTALL',-2)                                                                
          ENDIF                                                                                                                     
          IF (NSCAN.EQ.1) THEN                                                                                                      
            WRITE(6,6020) N,NREC                                                                                                    
            NRECIN=NRECIN+NREC                                                                                                      
          ENDIF                                                                                                                     
          IF (N.LT.NF) GOTO 130                                                                                                     
          IF (NVAR.LT.NVARM) THEN                                                                                                   
C                                                                                                                                   
C           * THE TOTAL NUMBER OF RECORDS PROCESSED IN INPUT FILES                                                                  
C           * SHOULD BE EQUAL TO THE TOTAL NUMBER OF RECORDS                                                                        
C           * WRITTEN OUT.                                                                                                          
C                                                                                                                                   
            IF (NRECIN.NE.NRECOUT) THEN                                                                                             
              WRITE(6,6025) NRECIN,NRECOUT                                                                                          
              CALL                                 XIT('SPLTALL',-3)                                                                
            ENDIF                                                                                                                   
C                                                                                                                                   
C           * WRITE OUT THE TABLE                                                                                                   
C                                                                                                                                   
            OPEN(4,FILE='.VARTABLE',FORM='FORMATTED',ERR=900)                                                                       
            REWIND 4                                                                                                                
            DO I=1,NVAR                                                                                                             
              WRITE(4,9000)(INAM(J,I),J=1,5)                                                                                        
            ENDDO                                                                                                                   
            CLOSE(4)                                                                                                                
C                                                                                                                                   
C           * NORMAL EXIT.                                                                                                          
C                                                                                                                                   
            WRITE(6,6030) NRECIN,NVAR                                                                                               
            WRITE(6,6070) NBYTES                                                                                                    
                                                                                                                                    
C           * CLOSE THE OUTPUT FILES TO ENSURE WRITING OF ANY                                                                       
C           * DATA LEFT IN THE ASSOCIATED BUFFERS.                                                                                  
                                                                                                                                    
            DO I=1,MAXOUT                                                                                                           
              CLOSE(IUOUT(I))                                                                                                       
            ENDDO                                                                                                                   
                                                                                                                                    
C           * FINAL TASK:                                                                                                           
                                                                                                                                    
            WRITE(6,'(A,A)')                                                                                                        
     1       ' VERIFYING TOTAL SIZE OF WRITTEN FILES IN',                                                                           
     2       ' THE LOCAL SUBDIRECTORY:'                                                                                             
            CALL FLUSH(6)                                                                                                           
            WRITE (CMD,'(I12)') NBYTES                                                                                              
            CALL SYSTEM('sumflsz '//CMD//' ; echo $? > .XsTs')                                                                      
            OPEN(54,FILE='.XsTs',ERR=910)                                                                                           
            REWIND 54                                                                                                               
            ISTS=-9                                                                                                                 
            READ(54,*,ERR=915) ISTS                                                                                                 
            CLOSE (54)                                                                                                              
            CALL SYSTEM('\rm .XsTs')                                                                                                
                                                                                                                                    
            IF ( ISTS .NE. 0 ) THEN                                                                                                 
             CALL                                  XIT('SPLTALL',-4)                                                                
            ELSE                                                                                                                    
             CALL                                  XIT('SPLTALL',0)                                                                 
            ENDIF                                                                                                                   
                                                                                                                                    
          ELSE                                                                                                                      
C                                                                                                                                   
C           * THERE MAY STILL BE SOME VARIABLES LEFT.                                                                               
C           * CLOSE THE OUTPUT FILES, GO BACK AND SCAN AGAIN.                                                                       
C                                                                                                                                   
            DO I=1,MAXOUT                                                                                                           
              CLOSE(IUOUT(I))                                                                                                       
            ENDDO                                                                                                                   
            NVARP=NVARP+MAXOUT                                                                                                      
            NVARM=NVARM+MAXOUT                                                                                                      
            NSCAN=NSCAN+1                                                                                                           
            GOTO 90                                                                                                                 
          ENDIF                                                                                                                     
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * SKIP SUPERLABELS                                                                                                          
C                                                                                                                                   
        IF (IBUF(1).EQ.NC4TO8("LABL")) GO TO 100                                                                                    
        NREC=NREC+1                                                                                                                 
                                                                                                                                    
        IF (NVAR.GT.0) THEN                                                                                                         
C                                                                                                                                   
C         * SKIP THE VARIABLE NAME VERIFICATION IF IT IS UNCHANGED.                                                                 
C                                                                                                                                   
          IF (IBUF(3).EQ.INAM(1,IV)) GO TO 120                                                                                      
C                                                                                                                                   
C         * CHECK IF THE VARIABLE NAME IS ALREADY IN THE VARIABLE                                                                   
C         * TABLE.                                                                                                                  
C                                                                                                                                   
          DO I=1,NVAR                                                                                                               
            IF (IBUF(3).EQ.INAM(1,I)) THEN                                                                                          
              IV=I                                                                                                                  
              GOTO 120                                                                                                              
            ENDIF                                                                                                                   
          ENDDO                                                                                                                     
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * SKIP THE RECORD IF MAXIMUM VARIABLE NUMBER THAT CAN BE                                                                    
C       * PROCESSED IN THIS SCAN IS REACHED.                                                                                        
C                                                                                                                                   
        IF (NVAR.EQ.NVARM) GOTO 100                                                                                                 
C                                                                                                                                   
C       * ABORT IF A NEW VARIABLE IS NOT PRESENT IN THE 1ST FILE.                                                                   
C                                                                                                                                   
        IF (N.GT.1) THEN                                                                                                            
          WRITE(6,6035) N,IBUF(3)                                                                                                   
          CALL                                     XIT('SPLTALL',-5)                                                                
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * ADD NEW NAME TO THE VARIABLE LIST.                                                                                        
C                                                                                                                                   
        NVAR=NVAR+1                                                                                                                 
        IF (NVAR.GT.MAXVAR) THEN                                                                                                    
          WRITE(6,6050) MAXVAR                                                                                                      
          CALL                                     XIT('SPLTALL',-6)                                                                
        ENDIF                                                                                                                       
        IV=NVAR                                                                                                                     
        INAM(1,NVAR)=IBUF(3)                                                                                                        
        INAM(2,NVAR)=IBUF(2)                                                                                                        
        INAM(3,NVAR)=IBUF(2)                                                                                                        
        INAM(4,NVAR)=IBUF(4)                                                                                                        
        INAM(5,NVAR)=IBUF(4)                                                                                                        
        WRITE(NAME,'(A4)')INAM(1,NVAR)                                                                                              
C                                                                                                                                   
C       * CHECK IF THE VARIABLE NAME IS RIGHT JUSTIFIED.                                                                            
C                                                                                                                                   
        IF(NAME(4:4).EQ.' ') THEN                                                                                                   
          WRITE(6,6060) NAME                                                                                                        
          CALL                                     XIT('SPLTALL',-7)                                                                
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * FIND THE FIRST NON BLANK CHARACTER IN THE VARIABLE NAME                                                                   
C       * BY SCANNING CHARACTERS FROM LEFT TO RIGHT.                                                                                
C                                                                                                                                   
        DO I=1,4                                                                                                                    
          I1=I                                                                                                                      
          IF(NAME(I1:I1).NE.' ') GOTO 110                                                                                           
        ENDDO                                                                                                                       
 110    CONTINUE                                                                                                                    
C                                                                                                                                   
C       * BLANKS INSIDE OF THE VARIABLE NAME ARE NOT PERMITTED.                                                                     
C                                                                                                                                   
        IF (I1+1.LE.3) THEN                                                                                                         
         DO I=I1+1,3                                                                                                                
           IF(NAME(I:I).EQ.' ') THEN                                                                                                
             WRITE(6,6060) NAME                                                                                                     
             CALL                                  XIT('SPLTALL',-8)                                                                
           ENDIF                                                                                                                    
         ENDDO                                                                                                                      
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * OPEN NEW FILE.                                                                                                            
C                                                                                                                                   
C       * NOTE: TO LIMIT OUTPUT OF VARIABLE LINES JUST TO THE                                                                       
C       *       FIRST, ENABLE NEXT "NVAR" IF BLOCK.                                                                                 
C                                                                                                                                   
C       IF (NVAR.EQ.1) THEN                                                                                                         
          IF (I1.EQ.1) THEN                                                                                                         
           WRITE(6,6040) NAME(I1:4),NAME,IUOUT(IV-NVARP)                                                                            
          ELSE                                                                                                                      
           WRITE(6,6045) NAME(1:(I1-1)),NAME(I1:4),NAME,                                                                            
     1                                   IUOUT(IV-NVARP)                                                                            
          ENDIF                                                                                                                     
C       ENDIF                                                                                                                       
        OPEN(IUOUT(IV-NVARP),FILE=NAME(I1:4),FORM='UNFORMATTED',                                                                    
     1                                                  ERR=905)                                                                    
        REWIND IUOUT(IV-NVARP)                                                                                                      
                                                                                                                                    
 120    CONTINUE                                                                                                                    
C                                                                                                                                   
C       * SKIP THE NAME IF IT IS ALREADY PROCESSED IN THE PREVIOUS                                                                  
C       * SCAN.                                                                                                                     
C                                                                                                                                   
        IF (IV.LE.NVARP) GOTO 100                                                                                                   
C                                                                                                                                   
C       * CHECK THE TIME STEPS AND (CONVERTED) LEVELS                                                                               
C                                                                                                                                   
        IF (IBUF(2).LT.INAM(2,IV)) INAM(2,IV)=IBUF(2)                                                                               
        IF (IBUF(2).GT.INAM(3,IV)) INAM(3,IV)=IBUF(2)                                                                               
                                                                                                                                    
        CALL LVACODE(IBUF4,IBUF(4),1)                                                                                               
        CALL LVACODE(INAM4,INAM(4,IV),1)                                                                                            
        CALL LVACODE(INAM5,INAM(5,IV),1)                                                                                            
        IF (IBUF4.LT.INAM4) INAM(4,IV)=IBUF(4)                                                                                      
        IF (IBUF4.GT.INAM5) INAM(5,IV)=IBUF(4)                                                                                      
C                                                                                                                                   
C       * WRITE OUT THE RECORD INTO THE APPROPRIATE FILE...                                                                         
C                                                                                                                                   
        NRECOUT=NRECOUT+1                                                                                                           
        CALL RECPUT(IUOUT(IV-NVARP),IBUF)                                                                                           
        CALL LBLCHK(LR,NWDS,NPACK,IBUF)                                                                                             
        LF=(LR-LHEAD)/MACHINE                                                                                                       
        IF ( INTSIZE .EQ. 1 ) THEN                                                                                                  
         NBYTES=NBYTES+(LR*(8*INTSIZE/MACHINE))+16                                                                                  
        ELSE                                                                                                                        
         NBYTES=NBYTES+(LR*(8/INTSIZE))+16                                                                                          
        ENDIF                                                                                                                       
        GOTO 100                                                                                                                    
 130  CONTINUE                                                                                                                      
                                                                                                                                    
 900  CALL                                         XIT('SPLTALL',-9)                                                                
 905  CALL                                         XIT('SPLTALL',-10)                                                               
 910  CALL                                         XIT('SPLTALL',-11)                                                               
 915  CALL                                         XIT('SPLTALL',-12)                                                               
C---------------------------------------------------------------------                                                              
 6000 FORMAT('0 FOUND',I5,' INPUT FILE(S).')                                                                                        
 6005 FORMAT('0 SCAN NUMBER =',I5,'.')                                                                                              
 6010 FORMAT('0 PROCESSING INPUT FILE ASSOCIATED WITH UNIT ',I5,                                                                    
     1       '...')                                                                                                                 
 6015 FORMAT('0 INPUT FILE ASSOCIATED WITH UNIT ',I5,' IS EMPTY.')                                                                  
 6020 FORMAT('0 INPUT FILE ASSOCIATED WITH UNIT ',I5,' CONTAINED ',                                                                 
     1       I10,' RECORDS.')                                                                                                       
 6025 FORMAT('0 THE NUMBER OF RECORDS READ IN NRECIN=',I10,                                                                         
     1       ' IS NOT EQUAL TO THE NUMBER OF RECORDS WRITTEN OUT',                                                                  
     2       ' NRECOUT=',I10)                                                                                                       
 6030 FORMAT('0 PROCESSED A TOTAL OF ',I10,' RECORDS IN ALL INPUT',                                                                 
     1       ' FILES,',/,'  AND CREATED',15X,I5,' OUTPUT FILES.')                                                                   
 6035 FORMAT('0 INPUT FILE ASSOCIATED WITH UNIT',I5,' CONTAINS ',                                                                   
     1       'VARIABLE "',A4,'"',/,                                                                                                 
     2       '  THAT DOES NOT EXIST IN THE FIRST INPUT FILE.')                                                                      
 6040 FORMAT('0 OPEN OUTPUT FILE "',     A,'" FOR VARIABLE "',                                                                      
     1       A4,'" USING UNIT NUMBER ',I4,'.')                                                                                      
 6045 FORMAT('0 OPEN OUTPUT FILE ',A,'"',A,'" FOR VARIABLE "',                                                                      
     1       A4,'" USING UNIT NUMBER ',I4,'.')                                                                                      
 6050 FORMAT('0 TOO MANY VARIABLES IN INPUT FILES.',/,                                                                              
     1       ' CURRENT LIMIT IS MAXVAR=',I5,' VARIABLES.')                                                                          
 6060 FORMAT('0 ILLEGAL VARIABLE NAME "',A4,'".')                                                                                   
 6070 FORMAT('0 ESTIMATED TOTAL SIZE OF WRITTEN DATA RECORDS',                                                                      
     1          ' (IN BYTES):',I12)                                                                                                 
 9000 FORMAT(A4,4(1X,I10))                                                                                                          
      END                                                                                                                           
