      PROGRAM SPREIM                                                                                                                
C     PROGRAM SPREIM (X,       Y,       Z,       OUTPUT,                )       E2                                                  
C    1          TAPE1=X, TAPE2=Y, TAPE3=Z, TAPE6=OUTPUT)                                                                            
C     --------------------------------------------------                        E2                                                  
C                                                                               E2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2                                                  
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     APR   /85 - G.J.BOER                                                                                                          
C                                                                               E2                                                  
CSPREIM  - SEPARATES REAL AND IMAGINARY PARTS OF A COMPLEX ARRAY        1  2    E1                                                  
C                                                                               E3                                                  
CAUTHOR  - G.J. BOER                                                            E3                                                  
C                                                                               E3                                                  
CPURPOSE - FILE PROGRAM Y = REAL (X) AND Z = IMAG (X).                          E3                                                  
C          NOTE - X MUST BE COMPLEX.                                            E3                                                  
C                 Y AND Z ARE RETURNED AS REAL PARTS OF COMPLEX FIELDS          E3                                                  
C                 HAVING THE SAME SIZE AS X.                                    E3                                                  
C                                                                               E3                                                  
CINPUT FILE...                                                                  E3                                                  
C                                                                               E3                                                  
C      X = ANY COMPLEX ARRAY                                                    E3                                                  
C                                                                               E3                                                  
COUTPUT FILES...                                                                E3                                                  
C                                                                               E3                                                  
C      Y = COMPLEX FIELD CONTAINING (REAL(X),0.)                                E3                                                  
C      Z = COMPLEX FIELD CONTAINING (IMAG(X),0.)                                E3                                                  
C------------------------------------------------------------------------------                                                     
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMPLEX A,B,C                                                                                                                 
      COMMON/BLANCK/A(6336),B(6336),C(6336)                                                                                         
C                                                                                                                                   
      LOGICAL OK,SPEC                                                                                                               
      COMMON/ICOM/IBUF(8),IDAT(12672)                                                                                               
      DATA MAXX/12672/                                                                                                              
C-----------------------------------------------------------------------                                                            
      NFF=4                                                                                                                         
      CALL JCLPNT(NFF,1,2,3,6)                                                                                                      
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      REWIND 3                                                                                                                      
C                                                                                                                                   
C     * READ THE NEXT FIELD.                                                                                                        
C                                                                                                                                   
      NR=0                                                                                                                          
  140 CALL GETFLD2(1,A, -1 ,0,0,0,IBUF,MAXX,OK)                                                                                     
      IF(.NOT.OK)THEN                                                                                                               
        IF(NR.EQ.0) CALL                           XIT('SPREIM',-1)                                                                 
        WRITE(6,6020) NR                                                                                                            
        CALL                                       XIT('SPREIM',0)                                                                  
      ENDIF                                                                                                                         
      IF(NR.EQ.0) WRITE(6,6030) IBUF                                                                                                
C                                                                                                                                   
C     * MAKE SURE THAT THE FIELD IS COMPLEX.                                                                                        
C                                                                                                                                   
      KIND=IBUF(1)                                                                                                                  
      SPEC=(KIND.EQ.NC4TO8("SPEC") .OR. KIND.EQ.NC4TO8("FOUR"))                                                                     
      IF(.NOT.SPEC) CALL                           XIT('SPREIM',-2)                                                                 
C                                                                                                                                   
C     * GET REAL AND IMAG PARTS.                                                                                                    
C                                                                                                                                   
      NWDS=IBUF(5)*IBUF(6)                                                                                                          
      DO 200 I=1,NWDS                                                                                                               
      B(I)=REAL(A(I))                                                                                                               
  200 C(I)= IMAG(A(I))                                                                                                              
C                                                                                                                                   
C     * SAVE THE RESULT.                                                                                                            
C                                                                                                                                   
      CALL PUTFLD2(2,B,IBUF,MAXX)                                                                                                   
      CALL PUTFLD2(3,C,IBUF,MAXX)                                                                                                   
      IF(NR.EQ.0) WRITE(6,6030) IBUF                                                                                                
      NR=NR+1                                                                                                                       
      GO TO 140                                                                                                                     
C-----------------------------------------------------------------------                                                            
 6020 FORMAT('0SPREIM READ',I6,' RECORDS')                                                                                          
 6030 FORMAT(' ',A4,I10,2X,A4,I10,4I6)                                                                                              
      END                                                                                                                           
