      PROGRAM ZXDLAT                                                                                                                
C     PROGRAM ZXDLAT (ZXIN,       ZXOUT,       INPUT,       OUTPUT,     )       F2                                                  
C    1          TAPE1=ZXIN, TAPE2=ZXOUT, TAPE5=INPUT, TAPE6=OUTPUT)                                                                 
C     -------------------------------------------------------------             F2                                                  
C                                                                               F2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2                                                  
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)                                                                 
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 14/83 - R.LAPRISE.                                                                                                        
C     NOV  7/80 - J.D.HENDERSON                                                                                                     
C                                                                               F2                                                  
CZXDLAT  - CROSS-SECTION LATITUDE DERIVATIVE - LEGENDRE POLYNOMIALS     1  1 C GF1                                                  
C                                                                               F3                                                  
CAUTHOR  - J.D.HENDERSON                                                        F3                                                  
C                                                                               F3                                                  
CPURPOSE - COMPUTES IN ZXOUT THE LATITUDE DERIVATIVE OF CROSS-SECTIONS          F3                                                  
C          IN ZXIN USING LEGENDRE POLYNOMIALS EXPANSION.                        F3                                                  
C                                                                               F3                                                  
CINPUT FILE...                                                                  F3                                                  
C                                                                               F3                                                  
C      ZXIN = CROSS-SECTIONS                                                    F3                                                  
C             (MAX LATITUDES IS $J$)                                            F3                                                  
C                                                                               F3                                                  
COUTPUT FILE...                                                                 F3                                                  
C                                                                               F3                                                  
C      ZXOUT = LAT DERIVATIVES                                                  F3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               F5                                                  
C      NLP = NUMBER OF LEGENDRE POLYNOMIALS TO USE IN THE EXPANSION.            F5                                                  
C            (MAXIMUM OF (100,$J$))                                             F5                                                  
C                                                                               F5                                                  
CEXAMPLE OF INPUT CARD...                                                       F5                                                  
C                                                                               F5                                                  
C*  ZXDLAT   26                                                                 F5                                                  
C------------------------------------------------------------------------------                                                     
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/BLANCK/D(96),DLAT(96)                                                                                                  
      COMMON/BLANCK/ALP,DALP,EPSI                                                                                                   
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      REAL F(96),G(100)                                                                                                             
      REAL*8 SL(96),CL(96),WL(96),WOSSL(96),RAD(96)                                                                                 
      REAL*8 ALP(9600),DALP(9600),EPSI(100)                                                                                         
      COMMON/ICOM/IBUF(8),IDAT(96)                                                                                                  
      DATA MAXX/96/                                                                                                                 
C---------------------------------------------------------------------                                                              
      NFF=4                                                                                                                         
      CALL JCLPNT(NFF,1,2,5,6)                                                                                                      
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
C                                                                                                                                   
C     * READ THE NUMBER OF LEGENDRE COMPONENTS.                                                                                     
C                                                                                                                                   
      READ(5,5010,END=901) NLP                                                  F4                                                  
      WRITE(6,6005) NLP                                                                                                             
C                                                                                                                                   
C    * GET ONE LEVEL AT A TIME.                                                                                                     
C                                                                                                                                   
      NR=0                                                                                                                          
  110 CALL GETFLD2(1,D,NC4TO8("ZONL"),-1,-1,-1,IBUF,MAXX,OK)                                                                        
      IF(.NOT.OK.AND.NR.EQ.0) CALL                 XIT('ZXDLAT',-1)                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6010) NR                                                                                                            
        CALL                                       XIT('ZXDLAT',0)                                                                  
      ENDIF                                                                                                                         
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -                                                               
C     * FIRST TIME ONLY...                                                                                                          
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR                                                               
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).                                                                        
C                                                                                                                                   
      IF(NR.GT.0) GO TO 190                                                                                                         
      NLAT=IBUF(5)                                                                                                                  
      ILATH=NLAT/2                                                                                                                  
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
C                                                                                                                                   
C     * ALPN2 COMPUTES LEGENDRE POLYNOMIALS FOR EACH LATITUDE.                                                                      
C     * ALPND2 COMPUTES THEIR DERIVATIVES.                                                                                          
C     * THE NUMBER OF COMPONENTS IS NLP.                                                                                            
C                                                                                                                                   
      CALL ALPN2(ALP,NLP,NLAT,SL,EPSI)                                                                                              
      CALL ALPND2(DALP,ALP,NLP,NLAT,EPSI)                                                                                           
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -                                                               
C     * COMPUTE COS(LAT)*DG/DLAT.                                                                                                   
C                                                                                                                                   
  190 IF(IBUF(5).NE.NLAT) CALL                     XIT('ZXDLAT',-2)                                                                 
      DO 200 I=1,NLAT                                                                                                               
  200 F(I)=D(I)                                                                                                                     
      CALL GALP(G,NLP,F,WL,NLAT,ALP)                                                                                                
      CALL LPAG(F,NLAT,G,NLP,DALP)                                                                                                  
C                                                                                                                                   
C     * TO GET THE TRUE DERIVATIVE, DIVIDE BY COS(LAT).                                                                             
C                                                                                                                                   
      DO 210 I=1,NLAT                                                                                                               
  210 DLAT(I)=F(I)/CL(I)                                                                                                            
C                                                                                                                                   
C     * PUT THE RESULT ON FILE ZXOUT.                                                                                               
C                                                                                                                                   
      CALL PUTFLD2(2,DLAT,IBUF,MAXX)                                                                                                
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
      NR=NR+1                                                                                                                       
      GO TO 110                                                                                                                     
C                                                                                                                                   
C     * E.O.F. ON INPUT.                                                                                                            
C                                                                                                                                   
  901 CALL                                         XIT('ZXDLAT',-3)                                                                 
C---------------------------------------------------------------------                                                              
 5010 FORMAT(10X,I5)                                                            F4                                                  
 6005 FORMAT(' ZXDLAT LEG. COEFF.=',I6)                                                                                             
 6010 FORMAT(' ',I6,' RECORDS READ')                                                                                                
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)                                                                                              
      END                                                                                                                           
