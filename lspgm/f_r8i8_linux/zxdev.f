      PROGRAM ZXDEV                                                                                                                 
C     PROGRAM ZXDEV (ZXIN,       ZXOUT,       INPUT,       OUTPUT,      )       F2                                                  
C    1         TAPE1=ZXIN, TAPE2=ZXOUT, TAPE5=INPUT, TAPE6=OUTPUT)                                                                  
C     ------------------------------------------------------------              F2                                                  
C                                                                               F2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       F2                                                  
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)                                                                 
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 14/83 - R.LAPRISE.                                                                                                        
C     NOV 05/80 - J.D.HENDERSON                                                                                                     
C                                                                               F2                                                  
CZXDEV   - CROSS-SECTION DEVIATION FROM MERIDIONAL MEAN                 1  1   GF1                                                  
C                                                                               F3                                                  
CAUTHOR  - J.D.HENDERSON                                                        F3                                                  
C                                                                               F3                                                  
CPURPOSE - READS CROSS-SECTIONS FROM FILE ZXIN, COMPUTES DEVIATIONS AT          F3                                                  
C          EACH LEVEL FROM THE MERIDIONAL MEAN AND PUTS THE RESULT ON           F3                                                  
C          FILE ZXOUT.                                                          F3                                                  
C          NOTE - MAX LATITUDES IS $J$, MAX LEVELS IS $L$.                      F3                                                  
C                                                                               F3                                                  
CINPUT FILE...                                                                  F3                                                  
C                                                                               F3                                                  
C      ZXIN  = CROSS-SECTIONS                                                   F3                                                  
C                                                                               F3                                                  
COUTPUT FILE...                                                                 F3                                                  
C                                                                               F3                                                  
C      ZXOUT = DEVIATION CROSS-SECTIONS                                         F3                                                  
C------------------------------------------------------------------------------                                                     
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/XS/XA(193,100),XB(193,100)                                                                                             
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      INTEGER LEV(100)                                                                                                              
      REAL F(193)                                                                                                                   
      REAL*8 SL(96),CL(96),WL(96),WOSSL(96),RAD(96)                                                                                 
      COMMON/ICOM/IBUF(8),IDAT(193)                                                                                                 
      DATA MAXX/193/, MAXL/100/, MAXJ/193/                                                                                          
C---------------------------------------------------------------------                                                              
      NFF=3                                                                                                                         
      CALL JCLPNT(NFF,1,2,6)                                                                                                        
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      PIH=3.14159265E0/2.E0                                                                                                         
C                                                                                                                                   
C     * READ THE NEXT CROSS-SECTION. STOP AT EOF.                                                                                   
C     * LEV WILL CONTAIN THE PRESSURE LEVEL VALUES IN MILLIBARS.                                                                    
C                                                                                                                                   
      NSETS=0                                                                                                                       
  150 CALL GETZX2(1,XA,MAXJ,LEV,NLEV,IBUF,MAXX,OK)                                                                                  
      IF(.NOT.OK)THEN                                                                                                               
        IF(NSETS.EQ.0) CALL                        XIT(' ZXDEV',-1)                                                                 
        WRITE(6,6010) NSETS                                                                                                         
        CALL                                       XIT(' ZXDEV',0)                                                                  
      ENDIF                                                                                                                         
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF                                                                                             
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT(' ZXDEV',-2)                                                                 
      NLAT=IBUF(5)                                                                                                                  
C                                                                                                                                   
C     * GAUSSG COMPUTES THE VALUE OF THE GAUSSIAN LATITUDES AND THEIR                                                               
C     * SINES AND COSINES. TRIGL MAKES THEM GLOBAL (S TO N).                                                                        
C                                                                                                                                   
      ILATH=NLAT/2                                                                                                                  
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
C                                                                                                                                   
C     * INTEGRATE IN LATITUDE TO COMPUTE THE MEAN.                                                                                  
C     * SUBTRACT THE MEAN TO GET THE DEVIATIONS.                                                                                    
C                                                                                                                                   
      DO 280 L=1,NLEV                                                                                                               
      DO 240 J=1,NLAT                                                                                                               
  240 F(J)=XA(J,L)* CL(J)                                                                                                           
      VAL=F(1)*(RAD(1)-(-PIH))                                                                                                      
      DO 250 J=2,NLAT                                                                                                               
  250 VAL=VAL+0.5E0*(F(J)+F(J-1))*(RAD(J)-RAD(J-1))                                                                                 
      VAL=VAL+F(NLAT)*(PIH-RAD(NLAT))                                                                                               
      FMEAN=VAL*0.5E0                                                                                                               
      DO 260 J=1,NLAT                                                                                                               
  260 XB(J,L)=XA(J,L)-FMEAN                                                                                                         
  280 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * PUT THE RESULT ON FILE ZXOUT.                                                                                               
C                                                                                                                                   
      DO 440 L=1,NLEV                                                                                                               
      IBUF(4)=LEV(L)                                                                                                                
      CALL PUTFLD2(2,XB(1,L),IBUF,MAXX)                                                                                             
      IF(NSETS.EQ.0) WRITE(6,6025) IBUF                                                                                             
  440 CONTINUE                                                                                                                      
C                                                                                                                                   
      NSETS=NSETS+1                                                                                                                 
      GO TO 150                                                                                                                     
C---------------------------------------------------------------------                                                              
 6010 FORMAT(' ',I6,' SETS PROCESSED')                                                                                              
 6025 FORMAT(' ',A4,I10,1X,A4,I10,4I6)                                                                                              
      END                                                                                                                           
