      PROGRAM CRVPLOT                                                                                                               
C     PROGRAM CRVPLOT (YAXIS,       XAXIS,       INPUT,       OUTPUT,   )       A2                                                  
C    1           TAPE1=YAXIS, TAPE2=XAXIS, TAPE5=INPUT, TAPE6=OUTPUT,                                                               
C    2                                                  TAPE8       )                                                               
C     ---------------------------------------------------------------           A2                                                  
C                                                                               A2                                                  
C     FEB 02/10 - S.KHARIN (ADD "NCURRCOLRESET" FOR RESETTING CURVE PATTERNS)   A2                                                  
C     DEC 24/09 - S.KHARIN (FIX A BUG IN READING "ICURVCOL" ARRAY ELEMENTS)                                                         
C     NOV 30/09 - F.MAJAESS (HARDCODE CURVE VECTOR LENGTH TO 99999 ELEMENTS,AND                                                     
C                            REVISE THE LOGIC FOR HANDLING "NPNTS=0/99999")                                                         
C     MAR 18/09 - F.MAJAESS (ENSURE "SPVAL" TESTS ARE DONE ONLY ON INITIALIZED                                                      
C                            ELEMENTS)                                                                                              
C     FEB 25/09 - F.MAJAESS (REVISED TO FIX SCATTER PLOT COLOUR OPTION)                                                             
C     AUG 23/07 - B.MIVILLE (CAN SPECIFY DASH PATTERN FOR EACH CURVE,                                                               
C                             NEW FONT, MORE COLOR LINES AND SYMBOLS.)                                                              
C     SEP 18/06 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)                                                                       
C     MAR 11/04 - M.BERKLEY (MODIFIED for NCAR V3.2)                                                                                
C     APR 10/02 - F.MAJAESS (REVISED FOR "CHAR" KIND RECORDS.)                                                                      
C     AUG 21/01 - S.KHARIN (Recognize special value 1.E+38)                                                                         
C     JUL 27/00 - F.MAJAESS (Change "6000 FORMAT" setting from "I8" to "I5,3X",                                                     
C                            since "SHORTN" ignores the last 3 characters.                                                          
C                            Added "EXTERNAL" block.)                                                                               
C     MAR 06/00 - S.KHARIN (Use NCAR smoothing flag "IOFFS" to switch off                                                           
C                             smoothing)                                                                                            
C     DEC 31/97 - R. TAYLOR (CLEAN UP GGPLOT UPGRADE, FIX BUGS, TEST, TEST, TEST)                                                   
C     AUG 31/96 - T. GUI (UPDATE TO NCAR GRAPHICS V3.2)                                                                             
C     AUG 13/96 - F.MAJAESS (Correct the order of calls in implementing user                                                        
C                            requested axes labelling, increase MINPTS to 1000)                                                     
C     MAR 20/96 - B. DENIS (Modify NSKIP input parameter capability to                                                              
C                           allow skipping end of curves)  (NSKIP < 0).                                                             
C                           The skipping is now done AFTER reversing.                                                               
C     MAR 11/96 - F.MAJAESS (ISSUE WARNING WHEN PACKING DENSITY <= 0)                                                               
C     JUN 19/95 - M. BERKLEY (Change .LT. to .LE. on FXLFT test vs. FXRIT in                                                        
C                              setting the x-axis range)                                                                            
C     NOV 19/93 - F.MAJAESS (USE DEFAULT TENSION, POSSIBLY INTERPOLATE)                                                             
C     SEP 21/93 - M. BERKLEY (changed Holleriths to strings)                                                                        
C     SEP 20/93 - M. BERKLEY (changed to save data files instead of                                                                 
C                             generating meta code.)                                                                                
C     SEP 08/93 - J.FYFE (ALLOW REWINDING JUST "XAXIS" FILE)                                                                        
C     SEP 01/92 - T. JANG   (add format to write to unit 44)                                                                        
C     JUL 20/92 - T. JANG   (changed variable "MAX" to "MAXX" so not to                                                             
C                            conflict with the generic function "MAX")                                                              
C     DEC 11/91 - T. JANG   (DELETED OBSOLETE CALLS TO BURNF)                                                                       
C     JUL 15/91 - S. WILSON (FIX BUG FOR X-AXIS ORIENTATION)                                                                        
C     JUL  4/91 - S. WILSON (ADD LABEL FOR SCIENTIFIC NOTATION AND FIX                                                              
C                            OTHER BUGS)                                                                                            
C     APR 15/91 - F. MAJAESS (FIX BUGS, ADD ZERO LINE DRAWING...)                                                                   
C     FEB 15/91 - F. ZWIERS (MINOR BUGS)                                                                                            
C     NOV 08/90 - S. WILSON (CONVERT TO AUTOGRAPH PLOTTING ROUTINES.)                                                               
C     JUN 15/90 - F.ZWIERS (ADD INTERNAL CONTROL OF INTERPOLATING SPLINE                                                            
C                           TENSION)                                                                                                
C     MAY 09/90 - F.ZWIERS (IMPROVE LABELS)                                                                                         
C     FEB 22/90 - C. BERGSETIN (SET UP FT44 FOR OVERLAY CONTROLING)                                                                 
C     JUN 21/89 - H. REYNOLDS (UPDATE TO NCAR GRAPHICS V2.0)                                                                        
C     SEP 14/88 - F.ZWIERS  (FIX A PROBLEM IN CHOOSING UPPER AND LOWER                                                              
C                            AXES LIMITS)                                                                                           
C     MAR 31/88 - F.MAJAESS (CHANGE ABORT EXIT TO WARNING EXIT FOR THE                                                              
C                            CASE WHERE THERE IS PROBLEM IN READING THE                                                             
C                            INPUT CARD(S))                                                                                         
C     MAR 09/88 - F.MAJAESS (ALLOW VARIABLE LENGTH CURVES TO BE PLOTTED                                                             
C                            ON THE SAME PLOT. ALSO, ADD AN OPTION WHICH                                                            
C                            ALLOWS PRODUCING CURVE PATTERNS PLOT)                                                                  
C     OCT 22/87 - M.LAZARE (MODIFY DIMENSIONS TO HANDLE UP TO 5 SETS OF                                                             
C                           5-YEAR PLOTS)                                                                                           
C     MAY 07/87 - M.LAZARE (CHANGE BUG IN SECTION WHERE REVERSE ORDER                                                               
C                           OF ARRAYS FOR MULTI-CURVE PLOT)                                                                         
C     JUL 17/86 - F.ZWIERS (INCREASE MAX NUMBER OF CURVES PER PLOT                                                                  
C                 TO 26, ALLOW UP TO 4 PLOTS PER PAGE)                                                                              
C     FEB 21/86 - R.LAPRISE, B.DUGAS, F.ZWIERS. (X VS Y OR Y ONLY AND                                                               
C                                                SCATTER PLOT OPTION)                                                               
C                                                                               A2                                                  
CCRVPLOT - PLOT CURVES IN THE X - Y PLANE                               2  1 C  A1                                                  
C                                                                               A3                                                  
CAUTHORS - F.ZWIERS, R.LAPRISE                                                  A3                                                  
C                                                                               A3                                                  
CPURPOSE - READ GROUPS OF MULTI-VALUED FUNCTIONS FROM FILE YAXIS AND            A3                                                  
C          PLOTS THEM AGAINST FUNCTION FROM FILE XAXIS (IF PRESENT).            A3                                                  
C          NOTE - YAXIS AND XAXIS MUST HAVE PROPER CCRN 8-WORD LABEL            A3                                                  
C                 (MEANING THAT IT IS EITHER A ZONAL OR A TIME SERIES           A3                                                  
C                  ARRAY).                                                      A3                                                  
C                 DEFAULT IS TO REWIND FILE(S) YAXIS (AND XAXIS)                A3                                                  
C                 BETWEEN EACH SET OF INPUT CARDS.                              A3                                                  
C                                                                               A3                                                  
CINPUT FILES...                                                                 A3                                                  
C                                                                               A3                                                  
C      YAXIS = FUNCTIONS TO BE PLOTTED ON YAXIS                                 A3                                                  
C      XAXIS = FUNCTIONS TO BE PLOTTED ON XAXIS                                 A3                                                  
C                                                                               A3                                                  
CCARDS READ...                                                                  A5                                                  
C                                                                               A5                                                  
C      UP TO FIVE INPUT CARDS ARE READ.                                         A5                                                  
C      EVERY SET OF INPUT CARDS DEFINES THE NUMBER OF CURVES ON THAT PLOT.      A5                                                  
C                                                                               A5                                                  
C      CARD 1:                                                                  A5                                                  
C      -------                                                                  A5                                                  
C      **** TAKE NOTE OF ISAMPL BELOW ****                                      A5                                                  
C                                                                               A5                                                  
C      READ (5,5010) NT,NAME,LVL,FYLO,FYHI,NSKIP,NPNTS,                         A5                                                  
C     1              NC,PTYPE,LTYPE,MLTRX,LSPC,                                 A5                                                  
C     2              NFRM,IRWND,IXAXS,ISCAT,ISAMPL                              A5                                                  
C 5010 FORMAT(10X,I10,1X,A4,I5,2E10.0,3I5,1X,2I2,I3,I2,5I1)                     A5                                                  
C                                                                               A5                                                  
C11-20:NT    = TIMESTEP NUMBER                                                  A5                                                  
C22-25:NAME  = NAME OF VARIABLE                                                 A5                                                  
C26-30:LVL   = LEVEL NUMBER                                                     A5                                                  
C31-40:FYLO  = LOWER VALUE OF Y-AXIS                                            A5                                                  
C41-50:FYHI  = UPPER VALUE OF Y-AXIS                                            A5                                                  
C              IF YLO.EQ.YHI THEN AUTOMATIC SCALING IS DONE                     A5                                                  
C              ON THE Y-AXIS                                                    A5                                                  
C51-55:NSKIP = NO OF POINTS TO BE OMITTED FROM THE LEFT OR RIGHT HAND END       A5                                                  
C              OF EACH CURVE.                                                   A5                                                  
C              USE NSKIP>0 TO OMIT NSKIP POINTS FROM THE LEFT END               A5                                                  
C                          OF EACH CURVE.                                       A5                                                  
C              USE NSKIP<0 TO OMIT NSKIP POINTS FROM THE RIGHT END              A5                                                  
C                          OF EACH CURVE.                                       A5                                                  
C              ** NOTE**: THE SKIPPING OPERATION IS PERFORMED *AFTER* THE       A5                                                  
C              DATA HAS BEEN ARRANGED AS SPECIFIED BY LSPC (SEE LSPC BELOW).    A5                                                  
C56-60:NPNTS = MAX. NO. OF POINTS PER CURVE.                                    A5                                                  
C              0 OR 99999 LEADS TO ALL VALID POINTS BASED ON THE FIRST RECORD   A5                                                  
C                         VECTOR LENGTH.                                        A5                                                  
C61-65:NC    = MAX. NO. OF CURVES PER PLOT (NC <= 26)                           A5                                                  
C67-68:PTYPE = NOT USED ANYMORE, ALL TICK MARKS ARE HANDLED AUTOMATICALLY.      A5                                                  
C              HOWEVER, IF PTYPE .NE. 0, AN ADDITIONAL CARD WILL BE READ        A5                                                  
C              IN FOR THE SAKE OF COMPATABILITY.                                A5                                                  
C69-70:LTYPE = (1,2,3,4) FOR (LIN-LIN), (LIN-LOG), (LOG-LIN), (LOG-LOG)         A5                                                  
C71-73:MLTRX = 0, THE NC Y-VECTORS ARE CONSIDERED TO BE OF FIXED LENGTH.        A5                                                  
C                 (IF XAXIS IS PRESENT THEN THE X-VECTOR IS READ ONLY           A5                                                  
C                                       ONCE FOR THE SET OF NC CURVES).         A5                                                  
C            = 1, THE NC Y-VECTORS ARE CONSIDERED TO BE OF VARIABLE LENGTH.     A5                                                  
C                 (IF XAXIS IS PRESENT THEN READ AN X-VECTOR FOR EVERY          A5                                                  
C                  Y-VECTOR; I.E. READ NC X-VECTORS)                            A5                                                  
C74-75:LSPC  = (1,0) FOR REVERSED OR NOT ORDER OF VECTOR IN DISPLAY             A5                                                  
C              N.B.: THE REVERSING IS DONE BEFORE THE SKIPPING (SEE NSKIP)      A5                                                  
C              USE LSPC=1 IF PROCESSING THE OUTPUT OF SPCSUM.                   A5                                                  
C76-76:NFRM  = (1,2,3,4) TO DETERMINE NUMBER OF FRAMES PER PAGE.                A5                                                  
C            = 1, ONE PLOT PER PAGE, (DEFAULT).                                 A5                                                  
C            = 2, TWO PLOTS PER PAGE ORIENTED HORIZONALLY.                      A5                                                  
C            = 3, TWO PLOTS PER PAGE ORIENTED VERTICALLY.                       A5                                                  
C            = 4, FOUR PLOTS PER PAGE                                           A5                                                  
C77-77:IRWND = 0, REWIND FILE YAXIS (AND XAXIS) AFTER A SET OF CARDS            A5                                                  
C                  HAS BEEN READ.                                               A5                                                  
C            = 1, DO NOT REWIND FILE YAXIS (AND XAXIS) AFTER A SET OF           A5                                                  
C                  CARDS HAS BEEN READ.                                         A5                                                  
C            = 2, ONLY REWIND FILE YAXIS AFTER READING A SET OF CARDS.          A5                                                  
C            = 3, ONLY REWIND FILE XAXIS AFTER READING A SET OF CARDS.          A5                                                  
C78-78:IXAXS = (1,0) TO READ OR NOT READ THE X-AXIS SCALE FROM A THIRD          A5                                                  
C              INPUT CARD.                                                      A5                                                  
C79-79:ISCAT = (0,1) FOR CONTINUOUS PLOT OR DISCRETE POINTS.                    A5                                                  
C80-80:ISAMPL= 9, PRODUCE A CURVE PATTERNS PLOT SKIPPING TO THE NEXT            A5                                                  
C                 EOR OR EOF MARK.                                              A5                                                  
C            = 8, DRAW CURVES OR POINTS IN COLOUR, USING SHADES ON CARD 5.      A5                                                  
C            = 7, DRAW CURVES OR POINTS IN COLOUR, USING SHADES ON CARD 5, AND  A5                                                  
C                 PRODUCE A CURVE PATTERNS PLOT (AS IN ISAMPL=9).               A5                                                  
C            = 0,BLANK  DRAW CURVES IN BLACK WITH DIFFERENT DASH PATTERN FOR    A5                                                  
C                       EACH CURVE                                              A5                                                  
C                                                                               A5                                                  
C      CARD 2:                                                                  A5                                                  
C      -------                                                                  A5                                                  
C      READ (5,5020,END=902) (IAX(I),I=1,20),(JAX(I),I=1,20),                   A5                                                  
C     1                      (TITLE(I),I=1,40)                                  A5                                                  
C 5020 FORMAT(2(20A1),40A1)                                                     A5                                                  
C                                                                               A5                                                  
C      IAX   = 20 CHAR. LABEL OF X-AXIS                                         A5                                                  
C      JAX   = 20 CHAR. LABEL OF Y-AXIS                                         A5                                                  
C      TITLE = 40 CHAR. LABEL OF PLOT                                           A5                                                  
C                                                                               A5                                                  
C      NOTE - ALL LEADING AND TRAILING SPACES TO THE LABELS ARE STRIPPED        A5                                                  
C             AND THEY ARE CENTERED AUTOMATICALLY.                              A5                                                  
C                                                                               A5                                                  
C      CARD 3                                                                   A5                                                  
C      ------                                                                   A5                                                  
C                                                                               A5                                                  
C   1)-IF (IXAXS=0) (USE DEFAULT X-AXIS SCALE, CONTROL AXIS LABELLING)          A5                                                  
C                                                                               A5                                                  
C   2)-IF (IXAXS=1 AND PTYPE=0) READ (5,5030,END=903) FXLFT,FXRIT               A5                                                  
C 5030 FORMAT(30X,2E10.0)                                                       A5                                                  
C                                                                               A5                                                  
C31-40 FXLFT  = LOWER VALUE OF X-AXIS                                           A5                                                  
C41-50 FXRIT  = UPPER VALUE OF X-AXIS                                           A5                                                  
C                                                                               A5                                                  
C      NOTE -  IF THE INPUT FILE XAXIS IS PRESENT AND FXLFT.EQ.FXRIT THEN       A5                                                  
C              AUTOMATIC SCALING IS DONE ON THE X-AXIS.                         A5                                                  
C              IF THE INPUT FILE XAXIS IS MISSING AND FXLFT.NE.FXRIT THEN       A5                                                  
C              ORDINATE VALUES ARE PLOTTED AGAINST ABSCISSA VALUES              A5                                                  
C              X(I)=FXLFT + (I-1)*(FXRIT-FXLFT)/(LNT-1).                        A5                                                  
C                                                                               A5                                                  
C   3)-IF (IXAXS=0 AND PTYPE=1) READ(5,5040) MGRX,MINRX,MGRY,MINRY              A5                                                  
C 5040 FORMAT(10X,4I5)                                                          A5                                                  
C      (I.E., SPECIFY X-AXIS SCALE AND CONTROL AXIS LABELLING)                  A5                                                  
C                                                                               A5                                                  
C      FOR LINEAR X-AXIS:                                                       A5                                                  
C      MGRX  = NUMBER OF MAJOR DIVISIONS ALONG THE X-AXIS (RESULTING IN         A5                                                  
C              MGRX+1 LABELS ON THE X-AXIS)                                     A5                                                  
C      MINRX = NUMBER OF MINOR DIVISIONS WITHIN EACH MAJOR DIVISIONS            A5                                                  
C              (RESULTING IN MINRX-1 TICS)                                      A5                                                  
C                                                                               A5                                                  
C      FOR LOG    X-AXIS:                                                       A5                                                  
C      MGRX  = EXPONENT OF 10 BETWEEN SUCCESSIVE MAJOR DIVISIONS                A5                                                  
C              (MGRX=1 RESULTS IN ORDINARY LOG GRAPH PAPER)                     A5                                                  
C      MINRX = CONTROL OVER PLOTTING OF SUBCYCLE DIVISIONS                      A5                                                  
C              MGRX=1 AND MINRX<=10 ==> 8 NORMAL SUBCYCLE DIVISIONS             A5                                                  
C                                         ARE PLOTTED                           A5                                                  
C              MGRX>1 OR  MINRX >10 ==> SUBCYCLE DIVISIONS SUPPRESSED           A5                                                  
C      MGRY  = LIKE MGRX,  EXCEPT FOR Y-AXIS                                    A5                                                  
C      MINRT = LIKE MINRX, EXCEPT FOR Y-AXIS                                    A5                                                  
C                                                                               A5                                                  
C      CARD 4                                                                   A5                                                  
C      ------                                                                   A5                                                  
C                                                                               A5                                                  
C      IF (IXAXS=1 AND PTYPE=1) READ(5,5040) MGRX,MINRX,MGRY,MINRY              A5                                                  
C 5040 FORMAT(10X,4I5)                                                          A5                                                  
C      (SPECIFY X-AXIS SCALE AND CONTROL AXIS LABELLING)                        A5                                                  
C                                                                               A5                                                  
C      FOR LINEAR X-AXIS:                                                       A5                                                  
C      MGRX  = NUMBER OF MAJOR DIVISIONS ALONG THE X-AXIS (RESULTING IN         A5                                                  
C              MGRX+1 LABELS ON THE X-AXIS)                                     A5                                                  
C      MINRX = NUMBER OF MINOR DIVISIONS WITHIN EACH MAJOR DIVISIONS            A5                                                  
C              (RESULTING IN MINRX-1 TICS)                                      A5                                                  
C                                                                               A5                                                  
C      FOR LOG    X-AXIS:                                                       A5                                                  
C      MGRX  = EXPONENT OF 10 BETWEEN SUCCESSIVE MAJOR DIVISIONS                A5                                                  
C              (MGRX=1 RESULTS IN ORDINARY LOG GRAPH PAPER)                     A5                                                  
C      MINRX = CONTROL OVER PLOTTING OF SUBCYCLE DIVISIONS                      A5                                                  
C              MGRX=1 AND MINRX<=10 ==> 8 NORMAL SUBCYCLE DIVISIONS             A5                                                  
C                                         ARE PLOTTED                           A5                                                  
C              MGRX>1 OR  MINRX >10 ==> SUBCYCLE DIVISIONS SUPPRESSED           A5                                                  
C      MGRY  = LIKE MGRX,  EXCEPT FOR Y-AXIS                                    A5                                                  
C      MINRT = LIKE MINRX, EXCEPT FOR Y-AXIS                                    A5                                                  
C                                                                               A5                                                  
C      CARD 5                                                                   A5                                                  
C      ------                                                                   A5                                                  
C                                                                               A5                                                  
C      IF CARD 5 IS PROVIDED, THEN CURVES WILL BE COLOURED, WITHOUT             A5                                                  
C      DASH PATTERNS.  IF THERE ARE MORE CURVES (NC) THAN PROVIDED NUMBER OF    A5                                                  
C      SHADES (NCURVCOL), THEN CRVPLOT WILL START OVER WITH SHADE 1, USING      A5                                                  
C      A DIFFERENT DASH PATTERN FOR THE SECOND SET OF CURVES, ETC.              A5                                                  
C      UP TO 26 COLORS.                                                         A5                                                  
C      YOU CAN ALSO SPECIFY WHICH DASH PATTERN TO USE FOR EACH INDIVIDUAL CURVE A5                                                  
C      BY HAVING AN EXTRA NUMBER IN FRONT OF THE IPAT COLOR:                    A5                                                  
C      FOR EXAMPLE: IPAT=106 IS YELLOW AND IPAT=5106 IS A YELLOW DASH LINE      A5                                                  
C      USING DASH PATTERN NUMBER 5. YOU CAN USE ANY OF THE 9 FIRST DASH PATTERN A5                                                  
C      PROVIDED. USE OPTION ISAMPLE=9 TO SEE WHICH PATTERNS ARE AVAILABLE.      A5                                                  
C                                                                               A5                                                  
C      READ(5,5050) NPAT, (IPAT(I),I=1,7)                                       A5                                                  
C 5050 FORMAT(10X,1X,I4,7I5)                                                    A5                                                  
C      ...                                                                      A5                                                  
C      READ(5,5052) (IPAT(I),I=21,NPAT)                                         A5                                                  
C 5052 FORMAT(15X,7I5)                                                          A5                                                  
C                                                                               A5                                                  
CEXAMPLE OF INPUT CARDS...                                                      A5                                                  
C                                                                               A5                                                  
C* CRVPLOT        -1 NEXT    1    40000.    60000.    0   32    1  1 1    12118 A5                                                  
C* LATITUDE         HEIGHT (GPM)        FJ - Z(500) - 2W                        A5                                                  
C*                                 -90.0      90.0                              A5                                                  
C*            4    2   10    2                                                  A5                                                  
C*            7   52    0   48    0   48    0   52                              A5                                                  
C                                                                               A5                                                  
CEXAMPLE WITH SPECIFIC DASH PATTERNS                                            A5                                                  
C                                                                               A5                                                  
C                                                                               A5                                                  
C* CRVPLOT        -1 NEXT  250       -2.       -2.    0   40    2  0 1  0 11008 A5                                                  
C* LATITUDE         HEIGHT (GPM)        FJ - Z(500) - 2W                        A5                                                  
C*            2 5120 2100                                                       A5                                                  
C                                                                               A5                                                  
C-----------------------------------------------------------------------------                                                      
C                                                                                                                                   
                                                                                                                                    
C     * VARIABLES:                                                                                                                  
C     * EQUIVALENCE(AIAX, IAX) - LABEL FOR X-AXIS                                                                                   
C     * EQUIVALENCE(AJAX, JAX) - LABEL FOR Y-AXIS                                                                                   
C     * EQUIVALENCE(ATITLE, TITLE) - TITLE OF GRAPH                                                                                 
C     * PAT - ARRAY OF 26 DASHED-LINE PATTERNS                                                                                      
C     * NGWND - AN ARRAY THAT HOLDS THE NUMBER OF GRAPHS PER PAGE FOR EACH                                                          
C     *         VALUE OF NFRM.                                                                                                      
C     * GWND - A 3D ARRAY CONTAINING THE GRAPH WINDOW LOCATIONS FOR EACH                                                            
C     *        VALUE OF NFRM.  E.G. FOR GWND(I,J,K), I=(1, 2, 3, 4) FOR THE                                                         
C     *        LEFT, RIGHT, BOTTOM & TOP CORNERS OF THE WINDOW RESPECTIVELY;                                                        
C     *        J=(1, 2, ... , NGWND(NFRM)) SPECIFIES WHICH WINDOW; AND                                                              
C     *        K=(1, 2, ... , MAXFRM) FOR WHATEVER NFRM IS.                                                                         
C     * NPLOT - KEEPS TRACK OF HOW MANY PLOTS STILL HAVE TO BE DRAWN ON                                                             
C     *         A PAGE                                                                                                              
C     * NPLT - THE NUMBER OF PLOTS THAT HAVE BEEN DRAWN                                                                             
C     * X - HOLDS THE X VALUES TO BE PLOTTED (I.E. PASSED TO 'EZMXY')                                                               
C     * Y - HOLDS THE Y VALUES TO BE PLOTTED (I.E. PASSED TO 'EZMXY')                                                               
C     * GX - IS A TEMPORARY ARRAY WHICH X VALUES ARE READ INTO                                                                      
C     * GY - IS A TEMPORARY ARRAY WHICH Y VALUES ARE READ INTO                                                                      
C     * NMLBDT - HOLDS DATA ABOUT NUMERIC LABELS ON LEFT AXIS                                                                       
C     * TCKDAT - HOLDS DATA ABOUT TICK MARKS ON THE LEFT AXIS                                                                       
                                                                                                                                    
                                                                                                                                    
      INTEGER MAXWND, MAXFRM                                                                                                        
      PARAMETER (MAXWND = 4, MAXFRM = 4)                                                                                            
C     *  MAXVL - MAXIMUM CURVE VECTOR LENGTH                                                                                        
C     * MAXWDS - MAXIMUM NUMBER OF CURVES TIMES "MAXVL"                                                                             
      INTEGER MAXVL,MAXVLV,MAXWDS                                                                                                   
      PARAMETER (MAXVL=99999,MAXVLV=MAXVL*2,MAXWDS=26*MAXVL)                                                                        
                                                                                                                                    
      LOGICAL OK,XAXS,LAT,YFLIP,SKPBGNG                                                                                             
      REAL X(MAXWDS), Y(MAXWDS)                                                                                                     
      REAL FXLFT, FXRIT, FYLO, FYHI, YLO, YHI                                                                                       
      REAL UCOORD(4), NMLBDT(11), TCKDAT(3), FSCALE                                                                                 
      INTEGER IBUF(8),PTYPE,CBASE,CWDS,LNT(26),NGWND(4)                                                                             
      INTEGER NPLOT, NPLT, ILLL                                                                                                     
      INTEGER IDASH(26), ISDASH, IDIV                                                                                               
      REAL GWND(4,MAXWND,MAXFRM), XINC                                                                                              
                                                                                                                                    
C     SHADES FOR COLOUR CURVES                                                                                                      
      LOGICAL COLOURCURVES                                                                                                          
      INTEGER NCURVCOL,MAXNCURVCOL,ICURVCOL(26)                                                                                     
      INTEGER IPATNE,NCURRCOLRESET                                                                                                  
      COMMON /CCCAGCHCU/ COLOURCURVES,NCURVCOL,MAXNCURVCOL,ICURVCOL,                                                                
     1     PAT,IPATNE,NCURRCOLRESET                                                                                                 
                                                                                                                                    
C     Array to pass user defined colours to dfclrs - unused in CRVPLOT                                                              
      REAL HSVV(3, 26)                                                                                                              
                                                                                                                                    
      CHARACTER AIAX*23,AJAX*23,ATITLE*43,PAT(26)*16                                                                                
      CHARACTER*1 IAX(23),JAX(23),TITLE(43),ICHR(26),IPLUS,PLTCHR                                                                   
      CHARACTER STRING*24, TMP*8, PLTNAM*8                                                                                          
      CHARACTER CDASH*17                                                                                                            
      EQUIVALENCE (IAX,AIAX),(JAX,AJAX),(TITLE,ATITLE)                                                                              
                                                                                                                                    
      EQUIVALENCE (UCOORD(1),FXLFT),(UCOORD(2),FXRIT)                                                                               
      EQUIVALENCE (UCOORD(3),FYLO),(UCOORD(4),FYHI)                                                                                 
                                                                                                                                    
C     logical unit numbers for saving card images, data, etc.                                                                       
      INTEGER LUNCARD,UNIQNUM,LUNDATAFILES(2)                                                                                       
      PARAMETER (LUNOFFSET=10,                                                                                                      
     1     LUNFIELD1=LUNOFFSET+1,LUNFIELD2=LUNOFFSET+2)                                                                             
                                                                                                                                    
      COMMON /ICOM/ IBUFX(8),IDATX(MAXVLV)                                                                                          
      COMMON /JCOM/ IBUFY(8),IDATY(MAXVLV)                                                                                          
                                                                                                                                    
      COMMON/BLANCK/ GX(MAXVL),GY(MAXVL)                                                                                            
                                                                                                                                    
      COMMON /INTPR/ZZZZZZ(2),TENSN,ZZZZZY(7)                                                                                       
      COMMON /PUSER/ MODE                                                                                                           
                                                                                                                                    
C                                                                                                                                   
C     * DECLARE NCAR PLUGINS EXTERNAL, SO CORRECT VERSION ON SGI                                                                    
C                                                                                                                                   
      EXTERNAL AGCHCU                                                                                                               
      EXTERNAL AGCHNL                                                                                                               
      EXTERNAL AGPWRT                                                                                                               
                                                                                                                                    
C                                                                                                                                   
C     * DECLARE BLOCK DATAS EXTERNAL, SO INITIALIZATION HAPPENS                                                                     
C                                                                                                                                   
      EXTERNAL AGCHCU_BD                                                                                                            
      EXTERNAL FILLCBBD                                                                                                             
      EXTERNAL FILLPAT_BD                                                                                                           
                                                                                                                                    
C                                                                                                                                   
C     * COMMON BLOCK TO TELL AGCHNL WHICH PROGRAM IS CALLING IT AND                                                                 
C     * WHAT TYPE OF X-AXIS (ZONAL/MERIDIONAL) IS BEING DRAWN.                                                                      
C                                                                                                                                   
      COMMON / AGNMLB / PLTNAM, LAT                                                                                                 
                                                                                                                                    
C     * COMMON BLOCK TO CONTROL GRAPH SMOOTHING.                                                                                    
                                                                                                                                    
      COMMON /SMFLAG/ IOFFS                                                                                                         
                                                                                                                                    
      DATA MAXX/MAXVLV/                                                                                                             
      DATA ICHR/'A','B','C','D','E','F','G','H','I','J',                                                                            
     1          'K','L','M','N','O','P','Q','R','S','T',                                                                            
     2          'U','V','W','X','Y','Z'/                                                                                            
      DATA IPLUS/'+'/                                                                                                               
                                                                                                                                    
      DATA NGWND / 1, 2, 2, 4 /                                                                                                     
                                                                                                                                    
      DATA (GWND(I,1,1),I=1,4) / 0.01, 0.99, 0.01, 0.99 /                                                                           
                                                                                                                                    
      DATA (GWND(I,2,2),I=1,4) / 0.01, 0.99, 0.51, 0.99 /                                                                           
      DATA (GWND(I,1,2),I=1,4) / 0.01, 0.99, 0.01, 0.49 /                                                                           
                                                                                                                                    
      DATA (GWND(I,2,3),I=1,4) / 0.01, 0.49, 0.01, 0.99 /                                                                           
      DATA (GWND(I,1,3),I=1,4) / 0.51, 0.99, 0.01, 0.99 /                                                                           
                                                                                                                                    
      DATA (GWND(I,4,4),I=1,4) / 0.01, 0.49, 0.51, 0.99 /                                                                           
      DATA (GWND(I,3,4),I=1,4) / 0.51, 0.99, 0.51, 0.99 /                                                                           
      DATA (GWND(I,2,4),I=1,4) / 0.01, 0.49, 0.01, 0.49 /                                                                           
      DATA (GWND(I,1,4),I=1,4) / 0.51, 0.99, 0.01, 0.49 /                                                                           
                                                                                                                                    
      DATA SPVAL/1.E+38/                                                                                                            
C-----------------------------------------------------------------------                                                            
                                                                                                                                    
C     * OPEN ALL FILES                                                                                                              
      NF=4                                                                                                                          
      CALL JCLPNT(NF,LUNFIELD1,LUNFIELD2,5,6)                                                                                       
                                                                                                                                    
C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.                                                                   
                                                                                                                                    
      SPVALT=1.E-6*SPVAL                                                                                                            
C     * SET NCAR TENSION                                                                                                            
C     TENSN=9.0                                                                                                                     
C                                                                                                                                   
C                                                                                                                                   
C     * SET THRESHOLD FOR THE MINIMUM NUMBER OF POINTS                                                                              
C     * PASSED TO NCAR PLOTTING ROUTINES.                                                                                           
C                                                                                                                                   
C      MINPTS=144                                                                                                                   
C      MINPTS=129                                                                                                                   
C      MINPTS=1000                                                                                                                  
                                                                                                                                    
                                                                                                                                    
C     * START UP NCAR GRAPHICS PACKAGE                                                                                              
      CALL PSTART                                                                                                                   
      CALL GSFAIS (1)                                                                                                               
      CALL BFCRDF(0)                                                                                                                
                                                                                                                                    
      NF=NF-2                                                                                                                       
                                                                                                                                    
      XAXS=.FALSE.                                                                                                                  
      IF(NF.EQ.2) XAXS=.TRUE.                                                                                                       
                                                                                                                                    
      REWIND LUNFIELD1                                                                                                              
      IF(XAXS) REWIND LUNFIELD2                                                                                                     
                                                                                                                                    
C                                                                                                                                   
C     * SET THE PLOT NAME FOR AGCHNL AND SET THE LINE THICKNESSES FOR                                                               
C     * AGCHCU.                                                                                                                     
C                                                                                                                                   
      PLTNAM = 'CRVPLOT'                                                                                                            
      CALL GSLWSC(2.0)                                                                                                              
      CALL GSMKSC(2.0)                                                                                                              
                                                                                                                                    
C     * READ CONTROL INFORMATION AND LABELS ON 2,3 OR 4 INPUT CARDS.                                                                
                                                                                                                                    
      NPLOT=0                                                                                                                       
      NPLT=0                                                                                                                        
      NFOLD=1                                                                                                                       
                                                                                                                                    
C     * TURN OFF SMOOTHING.                                                                                                         
                                                                                                                                    
      IOFFS=1                                                                                                                       
                                                                                                                                    
C     * GET BASIC PLOT PARAMETERS INCLUDING THE RANGE                                                                               
C     * AND SIMULATE A 'WHILE NOT(EOF)' LOOP.                                                                                       
C                                                                                                                                   
C     * INITIALIZE "NSKIP" TO ZERO (NO VALUES TO BE SKIPPED) AND "SKPBGNG" TO                                                       
C     * "TRUE" (OMIT FROM THE BEGINNING AND NOT FROM THE END; IF APPLICABLE)                                                        
C                                                                                                                                   
      NSKIP=0                                                                                                                       
      SKPBGNG=.TRUE.                                                                                                                
C                                                                                                                                   
C     * INITIALIZE COLOURCURVES TO FALSE                                                                                            
      COLOURCURVES=.FALSE.                                                                                                          
C                                                                                                                                   
C    * READ IN INPUT PARAMETERS... ** CARD 1 **                                                                                     
                                                                                                                                    
  110 READ (5,5010,END=901) NT,NAME,LVL,FYLO,FYHI,NSKIP,NPNTS,                                                                      
     1                      NC,PTYPE,LTYPE,MLTRX,LSPC,                                                                              
     2                      NFRM,IRWND,IXAXS,ISCAT,ISAMPL                                                                           
      WRITE(6,6110)         NT,NAME,LVL,FYLO,FYHI,NSKIP,NPNTS,                                                                      
     1                      NC,PTYPE,LTYPE,MLTRX,LSPC,                                                                              
     2                      NFRM,IRWND,IXAXS,ISCAT,ISAMPL                                                                           
      WRITE(6,6010)         NT,NAME,LVL,FYLO,FYHI,NSKIP,NPNTS,                                                                      
     1                      NC,PTYPE,LTYPE,MLTRX,LSPC,                                                                              
     2                      NFRM,IRWND,IXAXS,ISCAT,ISAMPL                                                                           
                                                                                                                                    
C     * SET NPNTS TO A VERY BIG NUMBER IF ZERO                                                                                      
C     IF(NPNTS.EQ.0)NPNTS=999999999                                                                                                 
                                                                                                                                    
      CSIZE=1.                                                                                                                      
      IF(NFRM.GT.1)CSIZE=1.35                                                                                                       
                                                                                                                                    
C     * TELL PCHIQU TO USE THE DUPLEX CHARACTER FONT                                                                                
      CALL PCSETI('FN',21)                                                                                                          
      CALL PCSETI('OF', 1)                                                                                                          
      CALL PCSETI('OL', 1)                                                                                                          
                                                                                                                                    
C     * CHANGE THE LINE-END CHARACTER TO '!'.                                                                                       
      CALL AGSETC('LINE/END.', '!')                                                                                                 
                                                                                                                                    
C     * SET THE LENGTH OF THE DASH-PATTERN STRINGS & THE SIZE OF THE                                                                
C     * CHARACTERS DRAWN IN THE DASHED-LINES.                                                                                       
      CALL AGSETF('DASH/LENGTH.', 16.0)                                                                                             
      CALL AGSETF('DASH/CHARACTER.', 0.02)                                                                                          
                                                                                                                                    
C     * MAKE THE NUMERIC LABELS BIGGER.                                                                                             
      CALL AGSETF('LEFT/WIDTH/MANTISSA.', 0.020*CSIZE)                                                                              
      CALL AGSETF('LEFT/WIDTH/EXPONENT.', 0.013333*CSIZE)                                                                           
      CALL AGSETF('BOTTOM/WIDTH/MANTISSA.', 0.020*CSIZE)                                                                            
      CALL AGSETF('BOTTOM/WIDTH/EXPONENT.', 0.013333*CSIZE)                                                                         
                                                                                                                                    
C     * MAKE THE TOP LABEL BIGGER.                                                                                                  
      CALL AGSETC('LABEL/NAME.', 'T')                                                                                               
      CALL AGSETF('LINE/NUMBER.', 100.0)                                                                                            
      CALL AGSETF('LINE/CHARACTER.', 0.026666*CSIZE)                                                                                
                                                                                                                                    
C     * MAKE THE LEFT LABEL BIGGER.                                                                                                 
      CALL AGSETC('LABEL/NAME.', 'L')                                                                                               
      CALL AGSETF('LINE/NUMBER.', 100.0)                                                                                            
      CALL AGSETF('LINE/CHARACTER.', 0.020*CSIZE)                                                                                   
                                                                                                                                    
C     * MAKE THE BOTTOM LABEL BIGGER.                                                                                               
      CALL AGSETC('LABEL/NAME.', 'B')                                                                                               
      CALL AGSETF('LINE/NUMBER.', -100.0)                                                                                           
      CALL AGSETF('LINE/CHARACTER.', 0.020*CSIZE)                                                                                   
                                                                                                                                    
C     * SET UP THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR                                                                      
C     * SCIENTIFIC NOTATION.                                                                                                        
      CALL AGSETC('LABEL/NAME.', 'SCALE')                                                                                           
      CALL AGSETF('LABEL/BASEPOINT/X.', 0.0)                                                                                        
      CALL AGSETF('LABEL/BASEPOINT/Y.', 0.9)                                                                                        
      CALL AGSETF('LABEL/OFFSET/X.', -0.015)                                                                                        
      CALL AGSETF('LABEL/ANGLE.', 90.0)                                                                                             
                                                                                                                                    
C     * MAKE THE SCALING LABEL BIGGER.                                                                                              
      CALL AGSETF('LINE/NUMBER.', 100.0)                                                                                            
      CALL AGSETF('LINE/CHARACTER.', 0.020*CSIZE)                                                                                   
                                                                                                                                    
C                                                                                                                                   
C     * SHUT OFF INFORMATION LABEL SCALING.  AUTOGRAPH WILL ONLY BE ABLE TO                                                         
C     * PREVENT INFORMATION LABELS FROM OVERLAPPING BY MOVING THEM.                                                                 
C                                                                                                                                   
      CALL AGSETF('LABEL/CONTROL.', 1.0)                                                                                            
                                                                                                                                    
                                                                                                                                    
C          * CHECK IF A COLOURED CURVES ARE TO BE PRODUCED.                                                                         
           IF((ISAMPL.EQ.8).OR.(ISAMPL.EQ.7)) THEN                                                                                  
              COLOURCURVES=.TRUE.                                                                                                   
C              IF(ISAMPL.EQ.7) THEN                                                                                                 
C                 ISAMPL = 9                                                                                                        
C              ENDIF                                                                                                                
           ENDIF                                                                                                                    
                                                                                                                                    
C          * CHECK IF A SAMPLE CURVE PATTERNS PLOT IS TO BE PRODUCED                                                                
C          * (ISAMPL.EQ.9)                                                                                                          
                                                                                                                                    
           IF (ISAMPL.EQ.9) THEN                                                                                                    
             IF (NPLOT .NE. 0) THEN                                                                                                 
                  CALL FRAME                                                                                                        
                  CALL RESET                                                                                                        
             ENDIF                                                                                                                  
                                                                                                                                    
             CALL AGSETF('X/LOGARITHMIC.', 0.)                                                                                      
             CALL AGSETF('Y/LOGARITHMIC.', 0.)                                                                                      
             CALL AGSETP('GRAPH WINDOW.', GWND(1, 1, 1), 4)                                                                         
             CALL AGSETC('LABEL/NAME.', 'SCALE')                                                                                    
             CALL AGSETF('LABEL/SUPPRESSION.', 1.0)                                                                                 
                                                                                                                                    
             CALL PLOTLGD(PAT,IPATNE)                                                                                               
             CALL                                  PXIT('CRVPLOT',0)                                                                
           ENDIF                                                                                                                    
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
C          * GET THE LABELS  ** CARD 2 **                                                                                           
           READ (5,5020,END=902) (IAX(I),I=1,20),(JAX(I),I=1,20),                                                                   
     1                           (TITLE(I),I=1,40)                                                                                  
           WRITE(6,6120)         (IAX(I),I=1,20),(JAX(I),I=1,20),                                                                   
     1                           (TITLE(I),I=1,40)                                                                                  
                                                                                                                                    
C          * SET THE X & Y LABELS AND THE DASHED LINE PATTERNS                                                                      
           CALL SHORTN(IAX, 23, .TRUE., .TRUE., ILEN)                                                                               
           CALL SHORTN(JAX, 23, .TRUE., .TRUE., JLEN)                                                                               
           IF(COLOURCURVES) THEN                                                                                                    
              CALL ANOTAT(AIAX(4:ILEN)//'!', AJAX(4:JLEN)//'!',                                                                     
     1             0,0,0,PAT)                                                                                                       
           ELSE                                                                                                                     
              CALL ANOTAT(AIAX(4:ILEN)//'!', AJAX(4:JLEN)//'!',                                                                     
     1             0,0,26,PAT)                                                                                                      
           ENDIF                                                                                                                    
                                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
C          * THE DEFAULT SETTING IS TO HAVE AUTOGRAPH AUTOMATICALLY CALCULATE                                                       
C          * THE X AND Y RANGES, NO WINDOWING (I.E. THE WHOLE CURVE IS ON THE                                                       
C          * GRAPH, AND 'NICE' TICK MARKS (ENDPOINTS ARE FORCED TO HAVE MAJOR                                                       
C          * TICK MARKS.                                                                                                            
C                                                                                                                                   
           FXLFT = 1.E36                                                                                                            
           FXRIT = 1.E36                                                                                                            
           IF (FYHI.LE.FYLO) THEN                                                                                                   
              FYLO = 1.E36                                                                                                          
              FYHI = 1.E36                                                                                                          
           ENDIF                                                                                                                    
           CALL AGSETI('WINDOWING.', 0)                                                                                             
           CALL AGSETF('X/NICE.', -1.0)                                                                                             
           CALL AGSETF('Y/NICE.', -1.0)                                                                                             
                                                                                                                                    
C                                                                                                                                   
C          * SET THE DEFAULT VALUES FOR YFLIP AND XORDER.                                                                           
C                                                                                                                                   
           YFLIP = (LSPC .EQ. 1)                                                                                                    
           XORDER = 0.0                                                                                                             
C                                                                                                                                   
C          * IF FXRIT AND FXLFT ARE SPECIFIED, THEN WE MUST MODIFY THE DEFAULTS.                                                    
C                                                                                                                                   
           IF(IXAXS.NE.0)THEN                                                                                                       
C               ** CARD 3 **                                                                                                        
                READ (5,5030,END=903) FXLFT,FXRIT                                                                                   
                WRITE(6,6130)         FXLFT,FXRIT                                                                                   
C                                                                                                                                   
C               * IF FXLFT=FXRIT THEN SETUP TO USE THE DEFAULT X-AXIS SETTINGS.                                                     
C                                                                                                                                   
                IF (FXLFT .EQ. FXRIT) IXAXS = 0                                                                                     
           ENDIF                                                                                                                    
C                                                                                                                                   
C          * GET AXIS LABELLING INFORMATION IF NECESSARY                                                                            
C                                                                                                                                   
           IF(PTYPE.NE.0)THEN                                                                                                       
C               ** CARD 4 **                                                                                                        
              READ (5,5040,END=904) MGRX,MINRX,MGRY,MINRY                                                                           
              WRITE(6,6140)         MGRX,MINRX,MGRY,MINRY                                                                           
           ENDIF                                                                                                                    
C                                                                                                                                   
           IF(LTYPE.GT.4 .OR. LTYPE.LT.1) LTYPE=1                                                                                   
                                                                                                                                    
C               ** CARD 5 **                                                                                                        
           IF(COLOURCURVES) THEN                                                                                                    
              READ(5,5050) NCURVCOL,(ICURVCOL(I),I=1,7)                                                                             
            IF(NCURVCOL.GT.7.AND.NCURVCOL.LE.14) THEN                                                                               
              READ(5,5052) (ICURVCOL(I),I=8,NCURVCOL)                                                                               
            ELSEIF(NCURVCOL.GT.14.AND.NCURVCOL.LE.21) THEN                                                                          
              READ(5,5052) (ICURVCOL(I),I=8,14)                                                                                     
              READ(5,5052) (ICURVCOL(I),I=15,NCURVCOL)                                                                              
            ELSEIF(NCURVCOL.GT.21.AND.NCURVCOL.LE.26) THEN                                                                          
              READ(5,5052) (ICURVCOL(I),I=8,14)                                                                                     
              READ(5,5052) (ICURVCOL(I),I=15,21)                                                                                    
              READ(5,5052) (ICURVCOL(I),I=22,NCURVCOL)                                                                              
           ENDIF                                                                                                                    
C                                                                                                                                   
C       * VERIFY IF SPECIAL DASH PATTERNS WAS REQUESTED                                                                             
C                                                                                                                                   
           ISDASH=0                                                                                                                 
           DO I=1,26                                                                                                                
              IDASH(I)=0                                                                                                            
           ENDDO                                                                                                                    
           DO I=1,NCURVCOL                                                                                                          
              IF(ICURVCOL(I).GT.999) THEN                                                                                           
                 IDIV=ICURVCOL(I)/1000                                                                                              
                 IF (IDIV.GT.9) CALL               PXIT('CRVPLOT',-101)                                                             
                 ICURVCOL(I)=ICURVCOL(I)-(IDIV*1000)                                                                                
                 IDASH(I)=IDIV                                                                                                      
                 ISDASH=1                                                                                                           
              ENDIF                                                                                                                 
           ENDDO                                                                                                                    
C                                                                                                                                   
              CALL DFCLRS(-NCURVCOL,HSVV,ICURVCOL)                                                                                  
           ENDIF                                                                                                                    
                                                                                                                                    
                                                                                                                                    
           IF(IXAXS.NE.0) WRITE(6,6030) FXLFT,FXRIT                                                                                 
           IF(PTYPE.NE.0) WRITE(6,6040) MGRX,MINRX,MGRY,MINRY                                                                       
                                                                                                                                    
           IF(XAXS .AND. IXAXS.GT.0 .AND. NSKIP.NE.0)THEN                                                                           
                WRITE(6,6050)                                                                                                       
                CALL                               PXIT('CRVPLOT',-1)                                                               
           ENDIF                                                                                                                    
                                                                                                                                    
C          * CHECK IF A SAMPLE CURVE PATTERNS PLOT IS TO BE PRODUCED                                                                
C          * (ISAMPL.EQ.7)                                                                                                          
                                                                                                                                    
           IF (ISAMPL.EQ.7) THEN                                                                                                    
             IF (NPLOT .NE. 0) THEN                                                                                                 
                  CALL FRAME                                                                                                        
                  CALL RESET                                                                                                        
             ENDIF                                                                                                                  
                                                                                                                                    
             CALL AGSETF('X/LOGARITHMIC.', 0.)                                                                                      
             CALL AGSETF('Y/LOGARITHMIC.', 0.)                                                                                      
             CALL AGSETP('GRAPH WINDOW.', GWND(1, 1, 1), 4)                                                                         
             CALL AGSETC('LABEL/NAME.', 'SCALE')                                                                                    
             CALL AGSETF('LABEL/SUPPRESSION.', 1.0)                                                                                 
                                                                                                                                    
             CALL PLOTLGD(PAT,IPATNE)                                                                                               
           ENDIF                                                                                                                    
                                                                                                                                    
C          * VERIFY IF SKIPPING TO BE DONE FROM THE END INSTEAD OF                                                                  
C          * THE BEGINNING.                                                                                                         
C                                                                                                                                   
           IF(NSKIP.LT.0) SKPBGNG=.FALSE.                                                                                           
           NSKIP=MAX(ABS(NSKIP),0)                                                                                                  
                                                                                                                                    
C                                                                                                                                   
C          * THIS SECTION READS IN THE X,Y DATA FOR THE PLOTS                                                                       
C-----------------------------------------------------------------------                                                            
                                                                                                                                    
C          * REWIND INPUT FILES IF NECESSARY                                                                                        
                                                                                                                                    
           NCURV=0                                                                                                                  
           IF (IRWND.EQ.0 .OR. IRWND.EQ.2) REWIND LUNFIELD1                                                                         
           IF ((XAXS).AND.(IRWND.EQ.0 .OR. IRWND.EQ.3)) REWIND LUNFIELD2                                                            
                                                                                                                                    
C          * READ UP TO NC CURVES FROM FILE CURV BASED ON INPUT CARD INFO.                                                          
                                                                                                                                    
           CWDS = 0                                                                                                                 
                                                                                                                                    
C          * SIMULATE 'REPEAT-UNTIL' LOOP                                                                                           
  200      CONTINUE                                                                                                                 
              CALL GETFLD2(LUNFIELD1,GY,-1,NT,NAME,LVL,IBUFY,MAXX,OK)                                                               
              IF(.NOT.OK .AND. NCURV.EQ.0) THEN                                                                                     
                   CALL                            PXIT('CRVPLOT',-102)                                                             
              ENDIF                                                                                                                 
              IF(OK)THEN                                                                                                            
                 IF(IBUFY(1).EQ.NC4TO8("SPEC")                                                                                      
     1                .OR. IBUFY(1).EQ.NC4TO8("FOUR")                                                                               
     2                .OR.IBUFY(1).EQ.NC4TO8("LABL")                                                                                
     3                .OR.IBUFY(1).EQ.NC4TO8("CHAR")                                                                                
     4                .OR. IBUFY(6).NE.1) THEN                                                                                      
                      CALL                         PXIT('CRVPLOT',-2)                                                               
                 ENDIF                                                                                                              
                 IF(IBUFY(8).LE.0) WRITE(6,6090) IBUFY(8)                                                                           
                 NCURV=NCURV+1                                                                                                      
                                                                                                                                    
                 IF((NCURV.EQ.1).AND.(MLTRX.EQ.0)) THEN                                                                             
                    DO 210 I=1,8                                                                                                    
                       IBUF(I)=IBUFY(I)                                                                                             
  210               CONTINUE                                                                                                        
                 ENDIF                                                                                                              
                                                                                                                                    
                 IF ((NCURV.GT.1).AND.(MLTRX.EQ.0).AND.                                                                             
     1               (IBUFY(5).NE.IBUF(5))) CALL   PXIT('CRVPLOT',-3)                                                               
                                                                                                                                    
C                * SET LENGTH OF CURVE BASED ON FIRST RECORD.                                                                       
                                                                                                                                    
                 NWDS=IBUFY(5)*IBUFY(6)                                                                                             
                 IF(NCURV.EQ.1.AND.(NPNTS.EQ.0.OR.NPNTS.EQ.99999)) THEN                                                             
                   IF (MLTRX.EQ.0) THEN                                                                                             
                    NPNTS=NWDS                                                                                                      
                   ELSE                                                                                                             
                    NPNTS=MAX(MAXVL,NWDS)                                                                                           
                   ENDIF                                                                                                            
                   WRITE(6,6160) NPNTS                                                                                              
                 ENDIF                                                                                                              
                 LNT(NCURV)=MIN(NWDS-NSKIP,NPNTS)                                                                                   
                 IF(LNT(NCURV).LT.2) CALL          PXIT('CRVPLOT',-4)                                                               
                                                                                                                                    
                                                                                                                                    
C                IF(CWDS+NPNTS .GT. MAXWDS) GOTO 300                                                                                
                 IF(CWDS+NPNTS .GT. MAXWDS) CALL   PXIT('CRVPLOT',-5)                                                               
                                                                                                                                    
C                                                                                                                                   
C                * ACCUMULATE CURVE VALUES IN MULTI-CURVE ARRAY.                                                                    
C                                                                                                                                   
                 CBASE = CWDS                                                                                                       
                 CWDS = CWDS + NPNTS                                                                                                
                 LMIN = MIN(NWDS, NPNTS)                                                                                            
                                                                                                                                    
                 DO 219 L = 1, NSKIP                                                                                                
                      Y(CBASE + L) = 1.E36                                                                                          
  219            CONTINUE                                                                                                           
                                                                                                                                    
C                * THE TASK OF THE FOLLOWING LOOP IS TO FIRST REVERSE                                                               
C                * THE ORDER OF THE ELEMENTS IF "YFLIP=.TRUE.", THEN                                                                
C                * TO SKIP FROM THE BEGINNING OR THE END (IF APPICABLE).                                                            
C                * ALSO,REPLACE SPVAL BY 1.E36, THE MISSING VALUE IN NCAR.                                                          
C                * NOTE: THE Y-ARRAY IS POSSIBLY REDUCED IN LENGTH                                                                  
C                *       TO LMIN-NSKIP ELEMENTS IN LOOP 335 BELOW.                                                                  
                                                                                                                                    
                                                                                                                                    
                 DO 220 L = NSKIP+1, LMIN                                                                                           
                  IF (YFLIP) THEN                                                                                                   
                    IF (SKPBGNG) THEN                                                                                               
                      INDXGY=LMIN-L+1                                                                                               
                    ELSE                                                                                                            
                      INDXGY=LMIN-L+1+NSKIP                                                                                         
                    ENDIF                                                                                                           
                  ELSE                                                                                                              
                    IF (SKPBGNG) THEN                                                                                               
                      INDXGY=L                                                                                                      
                    ELSE                                                                                                            
                      INDXGY=L-NSKIP                                                                                                
                    ENDIF                                                                                                           
                  ENDIF                                                                                                             
                  IF (ABS(GY(INDXGY)-SPVAL).LE.SPVALT) GY(INDXGY)=1.E36                                                             
                  Y(CBASE + L) = GY(INDXGY)                                                                                         
 220             CONTINUE                                                                                                           
C                                                                                                                                   
                                                                                                                                    
                 DO 221 L = LMIN+1, NPNTS                                                                                           
                      Y(CBASE + L) = 1.E36                                                                                          
  221            CONTINUE                                                                                                           
                                                                                                                                    
                                                                                                                                    
C             * READ X-AXIS IF NECESSARY                                                                                            
                                                                                                                                    
               IF (XAXS) THEN                                                                                                       
                IF (( MLTRX.NE.0).OR.                                                                                               
     1              ((MLTRX.EQ.0).AND.(NCURV.EQ.1))) THEN                                                                           
                   CALL GETFLD2(LUNFIELD2,                                                                                          
     1                GX,-1,NT,NAME,LVL,IBUFX,MAXX,OK)                                                                              
                   IF((IBUFX(1).NE.IBUFY(1) .AND. IBUFX(5).NE.IBUFY(5)                                                              
     2                 .AND. IBUFX(6).NE.IBUFY(6)) .OR.  .NOT.OK)                                                                   
     1               CALL                          PXIT('CRVPLOT',-6)                                                               
                                                                                                                                    
                   IF(IBUFX(8).LE.0) WRITE(6,6090) IBUFX(8)                                                                         
                                                                                                                                    
                ENDIF                                                                                                               
                                                                                                                                    
C               * ACCUMULATE X-VALUES                                                                                               
C               * REPLACE SPVAL BY 1.E36, THE MISSING VALUE IN NCAR.                                                                
                                                                                                                                    
                DO 230 L=1,NPNTS                                                                                                    
                     IF (L .GT. NSKIP .AND. L .LE. NWDS) THEN                                                                       
                          IF (ABS(GX(L)-SPVAL).LE.SPVALT) GX(L)=1.E36                                                               
                          X(CBASE+L) = GX(L)                                                                                        
                     ELSE                                                                                                           
                          X(CBASE + L) = 1.E36                                                                                      
                     ENDIF                                                                                                          
  230           CONTINUE                                                                                                            
                                                                                                                                    
               ENDIF                                                                                                                
                                                                                                                                    
              ELSE                                                                                                                  
                                                                                                                                    
                 NC=NCURV                                                                                                           
                 GO TO 300                                                                                                          
                                                                                                                                    
              ENDIF                                                                                                                 
                                                                                                                                    
C             * GO BACK AND REPEAT LOOP UNTIL NCURV .LT. NC                                                                         
              IF (NCURV.LT.NC) GO TO 200                                                                                            
  300      CONTINUE                                                                                                                 
                                                                                                                                    
                                                                                                                                    
C          * FIND THE MOST NUMBER OF POINTS THE CURVES WILL HAVE.                                                                   
           LMAX = 0                                                                                                                 
           DO 310 I=1,NCURV                                                                                                         
              IF(LNT(I).GT.LMAX) LMAX=LNT(I)                                                                                        
  310      CONTINUE                                                                                                                 
           WRITE(6,6998) NCURV,LMAX,IXAXS,XAXS                                                                                      
 6998      FORMAT(1X,'NCURV,LMAX,IXAXS,XAXS:',3I10,L10)                                                                             
                                                                                                                                    
                                                                                                                                    
C          * CHECK IF THERE IS NO FILE FOR X DATA                                                                                   
           IF (.NOT. XAXS) THEN                                                                                                     
                                                                                                                                    
C               * IF NO X DATA EXISTS AND FXLFT, FXRIT HAVEN'T BEEN SPECIFIED                                                       
C               * THEN THE X DATA WILL JUST BE THE POINT NUMBERS.                                                                   
                IF (IXAXS .EQ. 0) THEN                                                                                              
                     XINC = 1.0                                                                                                     
                     FXLFT = 1.0                                                                                                    
                     FXRIT = FLOAT(LMAX)                                                                                            
                                                                                                                                    
C               * IF WE GET HERE, THEN FXLFT AND FXRIT MUST HAVE BEEN SPECIFIED                                                     
C               * BUT THERE IS NO X DATA.  THE X DATA IS LINEARLY INTERPOLATED                                                      
C               * BETWEEN FXLFT AND FXRIT.                                                                                          
                ELSE                                                                                                                
                     XINC=(FXRIT-FXLFT)/FLOAT(LMAX-1)                                                                               
                ENDIF                                                                                                               
           ENDIF                                                                                                                    
                                                                                                                                    
                                                                                                                                    
C          * REPACK THE ARRAYS AND SET UP XAXIS IF NECESSARY                                                                        
                                                                                                                                    
           DO 335 I=1,NCURV                                                                                                         
              CBASE = (I-1)*NPNTS                                                                                                   
              NBASE = (I-1)*LMAX                                                                                                    
                                                                                                                                    
              DO 320 L=1,LMAX                                                                                                       
                   Y(NBASE + L)=Y(CBASE + NSKIP + L)                                                                                
  320         CONTINUE                                                                                                              
                                                                                                                                    
              IF(XAXS)THEN                                                                                                          
                 DO 325 L=1,LMAX                                                                                                    
                    X(NBASE+L)=X(CBASE+L+NSKIP)                                                                                     
  325            CONTINUE                                                                                                           
              ELSE                                                                                                                  
                 DO 330 L=1,LMAX                                                                                                    
                    IF(L.LE.LNT(I))THEN                                                                                             
                       X(NBASE+L)=FXLFT+XINC*FLOAT(L-1)                                                                             
                    ELSE                                                                                                            
                       X(NBASE+L)=1.E36                                                                                             
                    ENDIF                                                                                                           
  330            CONTINUE                                                                                                           
              ENDIF                                                                                                                 
  335      CONTINUE                                                                                                                 
                                                                                                                                    
C-----------------------------------------------------------------------                                                            
                                                                                                                                    
C          * SET THE AXIS RANGES                                                                                                    
                                                                                                                                    
           IF(IXAXS.EQ.0 .AND. .NOT.XAXS) FXRIT=FLOAT(LMAX)                                                                         
           WRITE(6,6999) LMAX,FXLFT,FXRIT,FYLO,FYHI                                                                                 
 6999      FORMAT(1X,'LMAX,FXLFT,FXRIT,FYLO,FYHI:',I10,4E12.4)                                                                      
                                                                                                                                    
           IF(IXAXS.NE.0) WRITE(6,6030) FXLFT,FXRIT                                                                                 
                                                                                                                                    
                                                                                                                                    
CC-----------------------------------------------------------------------                                                           
CC                                                                                                                                  
CC        *  DOUBLE RESOLUTION BY INTERPOLATION IF THE NUMBER OF                                                                    
CC        *  POINTS TO PLOT PER CURVE IS LESS THAN MINPTS.                                                                          
CC                                                                                                                                  
C         ISUBST=0                                                                                                                  
C  350    IF ( LMAX.GE.MINPTS .OR. ISCAT.EQ.1 ) GO TO 390                                                                           
C                                                                                                                                   
C         NLMAX=2*LMAX-1                                                                                                            
C         IF (NLMAX.GT.MAXWDS) THEN                                                                                                 
C           WRITE(6,6100)                                                                                                           
C           CALL                                   PXIT('CRVPLOT',-7)                                                               
C         ENDIF                                                                                                                     
C         DO 385 I = NCURV,1,-1                                                                                                     
C          NBASE=(I-1)*LMAX                                                                                                         
C          NNBASE=(I-1)*NLMAX                                                                                                       
C                                                                                                                                   
C          Y(NNBASE+NLMAX)=Y(NBASE+LMAX)                                                                                            
C          X(NNBASE+NLMAX)=X(NBASE+LMAX)                                                                                            
CC                                                                                                                                  
C          DO 380 J = LMAX-1, 1, -1                                                                                                 
C           IF (Y(NBASE+J).NE.1.E36.AND.Y(NBASE+J+1).NE.1.E36) THEN                                                                 
C             IF(LTYPE.NE.2.AND.LTYPE.NE.4)THEN                                                                                     
C              Y(NNBASE+2*J)=0.5*(Y(NBASE+J)+Y(NBASE+J+1))                                                                          
C             ELSE                                                                                                                  
C              IF(Y(NBASE+J).EQ.0.0.AND.Y(NBASE+J+1).EQ.0.0) THEN                                                                   
C               Y(NNBASE+2*J)=0.0                                                                                                   
C              ELSE                                                                                                                 
C               IF(Y(NBASE+J).EQ.0.0) THEN                                                                                          
C                Y(NNBASE+2*J)=1.E36                                                                                                
C                ISUBST=1                                                                                                           
C               ELSE                                                                                                                
C                IF(Y(NBASE+J+1).EQ.0.0) THEN                                                                                       
C                 Y(NNBASE+2*J)=1.E36                                                                                               
C                 ISUBST=1                                                                                                          
C                ELSE                                                                                                               
C                 Y(NNBASE+2*J)=10.**(0.5*(LOG10(Y(NBASE+J))+                                                                       
C     1                                    LOG10(Y(NBASE+J+1))))                                                                    
C                ENDIF                                                                                                              
C               ENDIF                                                                                                               
C              ENDIF                                                                                                                
C             ENDIF                                                                                                                 
C           ELSE                                                                                                                    
C             Y(NNBASE+2*J)   = 1.E36                                                                                               
C           ENDIF                                                                                                                   
C           Y(NNBASE+2*J-1) = Y(NBASE+J)                                                                                            
CC                                                                                                                                  
C           IF (X(NBASE+J).NE.1.E36.AND.X(NBASE+J+1).NE.1.E36) THEN                                                                 
C             IF(LTYPE.NE.3.AND.LTYPE.NE.4)THEN                                                                                     
C              X(NNBASE+2*J)=0.5*(X(NBASE+J)+X(NBASE+J+1))                                                                          
C             ELSE                                                                                                                  
C              IF(X(NBASE+J).EQ.0.0.AND.X(NBASE+J+1).EQ.0.0) THEN                                                                   
C               X(NNBASE+2*J)=0.0                                                                                                   
C              ELSE                                                                                                                 
C               IF(X(NBASE+J).EQ.0.0) THEN                                                                                          
C                X(NNBASE+2*J)=1.E36                                                                                                
C                ISUBST=1                                                                                                           
C               ELSE                                                                                                                
C                IF(X(NBASE+J+1).EQ.0.0) THEN                                                                                       
C                 X(NNBASE+2*J)=1.E36                                                                                               
C                 ISUBST=1                                                                                                          
C                ELSE                                                                                                               
C                 X(NNBASE+2*J)=10.**(0.5*(LOG10(X(NBASE+J))+                                                                       
C     1                                    LOG10(X(NBASE+J+1))))                                                                    
C                ENDIF                                                                                                              
C               ENDIF                                                                                                               
C              ENDIF                                                                                                                
C             ENDIF                                                                                                                 
C           ELSE                                                                                                                    
C             X(NNBASE+2*J)   = 1.E36                                                                                               
C           ENDIF                                                                                                                   
C           X(NNBASE+2*J-1) = X(NBASE+J)                                                                                            
C                                                                                                                                   
C  380     CONTINUE                                                                                                                 
C          LNT(I)=NLMAX                                                                                                             
C  385    CONTINUE                                                                                                                  
C         LMAX=NLMAX                                                                                                                
C         GO TO 350                                                                                                                 
C                                                                                                                                   
                                                                                                                                    
C  390    CONTINUE                                                                                                                  
C         IF ( ISUBST .NE. 0 ) WRITE(6,6070)                                                                                        
                                                                                                                                    
                                                                                                                                    
C-----------------------------------------------------------------------                                                            
C                                                                                                                                   
C          * IF THIS PLOT IS GOING ON A NEW PAGE THEN SET THE NUMBER OF                                                             
C          * PLOTS LEFT TO DRAW ON THIS PAGE AND STORE THE OLD VALUE OF                                                             
C          * NFRM IN NFOLD.                                                                                                         
C                                                                                                                                   
           IF (NPLOT .EQ. 0) THEN                                                                                                   
                IF (NFRM .EQ. 0) NFRM = 1                                                                                           
                NPLOT = NGWND(NFRM)                                                                                                 
                NFOLD = NFRM                                                                                                        
                                                                                                                                    
C                                                                                                                                   
C          * IF NFRM DOES NOT AGREE WITH THE OLD VALUE OF NFRM (NFOLD)                                                              
C          * THEN START ON A NEW PAGE AND RESET THE VALUES FOR NPLOT                                                                
C          * AND NFOLD AS ABOVE.                                                                                                    
C                                                                                                                                   
           ELSEIF (NFRM .NE. NFOLD) THEN                                                                                            
                CALL FRAME                                                                                                          
                CALL RESET                                                                                                          
                                                                                                                                    
                IF (NFRM .EQ. 0) NFRM = 1                                                                                           
                NPLOT = NGWND(NFRM)                                                                                                 
                NFOLD = NFRM                                                                                                        
           ENDIF                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
C          * SET THE WINDOW IN WHICH THE GRAPH IS TO BE DRAWN                                                                       
C                                                                                                                                   
           CALL AGSETP('GRAPH WINDOW.', GWND(1, NPLOT, NFOLD), 4)                                                                   
                                                                                                                                    
C                                                                                                                                   
C          * SUPPRESS THE FRAME ADVANCE, SET THE X ARRAY DIMENSION TO                                                               
C          * ONE OR TWO DIMENSIONS, WHATEVER MLTRX SPECIFIES, AND SET                                                               
C          * THE LINEAR OR LOGARITHMIC NATURE OF THE PLOT.                                                                          
C                                                                                                                                   
           CALL DISPLA(2, MLTRX + 1, LTYPE)                                                                                         
                                                                                                                                    
           IF(PTYPE.NE.0) WRITE(6,6040) MGRX,MINRX,MGRY,MINRY                                                                       
                                                                                                                                    
C          * IF A SCATTER PLOT IS TO BE DRAWN, SUPPRESS THE DRAWING                                                                 
C          * OF CURVES.                                                                                                             
           IF (ISCAT .EQ. 1) THEN                                                                                                   
                CALL AGSETF('SET.', -1.0)                                                                                           
           ENDIF                                                                                                                    
C                                                                                                                                   
C          * IF FXRIT AND FXLFT WERE SPECIFIED, THEN WE MUST MODIFY THE DEFAULTS.                                                   
C                                                                                                                                   
           IF(IXAXS.NE.0) THEN                                                                                                      
C                                                                                                                                   
C            * TURN WINDOWING ON SO THAT ONLY THE PORTION OF THE                                                                    
C            * CURVE IN THE X-AXIS RANGE IS DRAWN. MAJOR TICK MARKS                                                                 
C            * DON'T HAVE TO BE AT THE ENDPOINTS OF THE X-AXIS.                                                                     
                                                                                                                                    
C             CALL AGSETI('WINDOWING.', 1)                                                                                          
             CALL AGSETF('X/NICE.', 0.0)                                                                                            
           ENDIF                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
C          * DO THE SAME AS ABOVE FOR THE Y-AXIS IF FYLO AND FYHI HAVE BEEN                                                         
C          * SPECIFIED.                                                                                                             
C                                                                                                                                   
           IF (FYHI .GT. FYLO) THEN                                                                                                 
                CALL AGSETI('WINDOWING.', 1)                                                                                        
                CALL AGSETF('Y/NICE.', 0.0)                                                                                         
           ENDIF                                                                                                                    
                                                                                                                                    
           IF (FXLFT .LE. FXRIT) THEN                                                                                               
                CALL AGSETF('X/MINIMUM.', FXLFT)                                                                                    
                CALL AGSETF('X/MAXIMUM.', FXRIT)                                                                                    
                CALL AGSETF('X/ORDER.', 0.0)                                                                                        
           ELSE                                                                                                                     
                CALL AGSETF('X/MINIMUM.', FXRIT)                                                                                    
                CALL AGSETF('X/MAXIMUM.', FXLFT)                                                                                    
                CALL AGSETF('X/ORDER.', 1.0)                                                                                        
           ENDIF                                                                                                                    
           CALL AGSETF('Y/MINIMUM.', FYLO)                                                                                          
           CALL AGSETF('Y/MAXIMUM.', FYHI)                                                                                          
                                                                                                                                    
C                                                                                                                                   
C          * RUN AGSTUP TO MAKE AUTOGRAPH CALCULATE THE NUMERIC LABEL                                                               
C          * SETTINGS.  WE CAN CHECK THE 'SECONDARY' PARAMETERS TO MAKE                                                             
C          * SURE THE THE LABELS WILL NOT USE SCIENTIFIC NOTATION OR HAVE                                                           
C          * LABELS OF THE FORM 0.00001, 0.00002, 0.00003, ETC.  WHERE THERE                                                        
C          * ARE TOO MANY ZEROS.                                                                                                    
C                                                                                                                                   
           CALL AGSTUP(X,NCURV,LMAX,LMAX,1,Y,NCURV,LMAX,LMAX,1)                                                                     
                                                                                                                                    
C                                                                                                                                   
C          * GET THE NUMERIC LABEL INFORMATION ON THE Y AXIS.                                                                       
C                                                                                                                                   
           CALL AGGETP('SECONDARY/LEFT/NUMERIC.', NMLBDT, 11)                                                                       
           CALL AGGETP('SECONDARY/LEFT/TICKS.', TCKDAT, 3)                                                                          
                                                                                                                                    
           YLO = FYLO                                                                                                               
           YHI = FYHI                                                                                                               
                                                                                                                                    
C                                                                                                                                   
C          * GET THE USER COORDINATES OF THE GRID WINDOW.                                                                           
C                                                                                                                                   
           CALL AGGETP('SECONDARY/USER.', UCOORD, 4)                                                                                
                                                                                                                                    
C                                                                                                                                   
C          * TAKE THE ABSOLUTE VALUES OF FYLO AND FYHI SET FSCALE EQUAL                                                             
C          * TO THE LOG OF WHICH EVER ONE IS HIGHER.                                                                                
C                                                                                                                                   
           IF (FYLO * FYHI .NE. 0) THEN                                                                                             
                FSCALE = MAX(ANINT(LOG10(ABS(FYLO))),                                                                               
     1                         ANINT(LOG10(ABS(FYHI))))                                                                             
           ELSE                                                                                                                     
                FSCALE = ANINT(LOG10(MAX(ABS(FYLO), ABS(FYHI))))                                                                    
           ENDIF                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
C          * IF (THE NUMERIC LABEL USES SCIENTIFIC NOTATION) OR (THE                                                                
C          * NUMERIC LABEL *DOESN'T* USE SCIENTIFIC NOTATION *AND*                                                                  
C          * THE LARGEST ABSOLUTE VALUE IS LESS THAN 0.01) THEN                                                                     
C          * SCALE THE DATA ACCORDINGLY AND SHOW THE SCALING LABEL                                                                  
C          * ALONG THE Y AXIS.                                                                                                      
C                                                                                                                                   
           IF ((NMLBDT(1) .EQ. 2.0 .AND. TCKDAT(1) .EQ. 1.0) .OR.                                                                   
     1         (NMLBDT(1) .EQ. 3.0 .AND. ((FSCALE .LT. -2.0).OR.                                                                    
     2                                    (FSCALE .GT.  4.0)))) THEN                                                                
                                                                                                                                    
C                                                                                                                                   
C               * IF SCIENTIFIC NOTATION WAS USED, THE LOG OF THE                                                                   
C               * SCALING FACTOR IS STORED IN NMLBDT(2) (WHICH IS ACTUALLY                                                          
C               * THE VALUE OF 'LEFT/EXPONENT.')                                                                                    
C                                                                                                                                   
                IF (NMLBDT(1) .EQ. 2 .AND. TCKDAT(1) .EQ. 1) THEN                                                                   
                     WRITE(TMP, 6000) INT(NMLBDT(2))                                                                                
                     FSCALE = 10.0**(-1*NMLBDT(2))                                                                                  
                                                                                                                                    
C                                                                                                                                   
C               * IF SCIENTIFIC NOTATION WASN'T USED, THEN WE'VE ALREADY                                                            
C               * CALCULATED THE LOG OF THE SCALING FACTOR IN FSCALE.                                                               
C                                                                                                                                   
                ELSE                                                                                                                
                     WRITE(TMP, 6000) INT(FSCALE)                                                                                   
                     FSCALE = 10.0**(-1*FSCALE)                                                                                     
                ENDIF                                                                                                               
                                                                                                                                    
C                                                                                                                                   
C               * HERE IS WHERE WE SCALE THE DATA.                                                                                  
C                                                                                                                                   
                DO 400 I = 1, NCURV*LMAX                                                                                            
                     IF (Y(I) .NE. 1.E36) THEN                                                                                      
                          Y(I) = Y(I) * FSCALE                                                                                      
                     ENDIF                                                                                                          
  400           CONTINUE                                                                                                            
                                                                                                                                    
C                                                                                                                                   
C               * CHANGE THE YHI AND YLO SETTINGS IF THEY WERE SPECIFIED                                                            
C               * BY THE USER                                                                                                       
C                                                                                                                                   
                IF (YLO .NE. 1.E36) THEN                                                                                            
                     CALL AGSETF('Y/MAXIMUM.', YHI*FSCALE)                                                                          
                     CALL AGSETF('Y/MINIMUM.', YLO*FSCALE)                                                                          
                ENDIF                                                                                                               
                                                                                                                                    
C                                                                                                                                   
C               * SET NUMERIC LABEL TYPE TO 'NO-EXPONENT' NOTATION.                                                                 
C                                                                                                                                   
                CALL AGSETF('LEFT/TYPE.', 3.)                                                                                       
                                                                                                                                    
C                                                                                                                                   
C               * TURN ON THE SCALE LABEL IN THE TOP LEFT HAND CORNER FOR                                                           
C               * SCIENTIFIC NOTATION.                                                                                              
C                                                                                                                                   
                CALL AGSETC('LABEL/NAME.', 'SCALE')                                                                                 
                CALL AGSETF('LABEL/SUPPRESSION.', 0.0)                                                                              
                                                                                                                                    
C                                                                                                                                   
C               * SET THE TEXT OF THE LABEL.                                                                                        
C                                                                                                                                   
                CALL AGSETF('LINE/NUMBER.', 100.0)                                                                                  
                CALL SHORTN(TMP, 11, .TRUE., .TRUE., ILEN)                                                                          
C                STRING='''L1''X10''H-7'''//TMP(4:ILEN)//'''H7''!'                                                                  
                STRING='X10:S:'//TMP(4:ILEN)//':E:'                                                                                 
                CALL AGSETC('LINE/TEXT.', STRING)                                                                                   
                                                                                                                                    
                                                                                                                                    
                WRITE(6, *) 'SCALING LABEL SHOULD BE DRAWN.'                                                                        
           ELSE                                                                                                                     
                                                                                                                                    
C                                                                                                                                   
C               * MAKE SURE THE SCALE LABEL IS TURNED OFF.                                                                          
C                                                                                                                                   
                CALL AGSETC('LABEL/NAME.', 'SCALE')                                                                                 
                CALL AGSETF('LABEL/SUPPRESSION.', 1.0)                                                                              
                                                                                                                                    
                WRITE (6, *) 'NO SCALING LABEL NEEDED.'                                                                             
           ENDIF                                                                                                                    
                                                                                                                                    
           WRITE(6, 6997) NMLBDT                                                                                                    
           WRITE(6, 6996) TCKDAT                                                                                                    
 6997      FORMAT(1X,'NMLBDT:', 11E11.4)                                                                                            
 6996      FORMAT(1X,'TCKDAT:',  3E11.4)                                                                                            
                                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
C          * FIX UP THE TITLE                                                                                                       
C                                                                                                                                   
           CALL SHORTN(TITLE, 43, .TRUE., .TRUE., ILEN)                                                                             
C                                                                                                                                   
C          * IF SPECIFIC DASH PATTERN REQUESTED                                                                                     
C                                                                                                                                   
           IF (ISDASH.EQ.1) THEN                                                                                                    
                                                                                                                                    
           CALL AGSETI('DASH/SELECTOR.',NCURVCOL)                                                                                   
C                                                                                                                                   
              DO I=1,NC                                                                                                             
                 IF((I/NCURVCOL).GT.1) THEN                                                                                         
                    J=I/NCURVCOL                                                                                                    
                 ELSE                                                                                                               
                    J=IDASH(I)+1                                                                                                    
                 ENDIF                                                                                                              
                 IF(I.GT.9) THEN                                                                                                    
                    WRITE(CDASH,7001) I                                                                                             
                 ELSE                                                                                                               
                    WRITE(CDASH,7000) I                                                                                             
                 ENDIF                                                                                                              
                 CALL AGSETC(CDASH,PAT(J))                                                                                          
              ENDDO                                                                                                                 
                                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
 7000      FORMAT('DASH/PATTERNS/',I1,'.')                                                                                          
 7001      FORMAT('DASH/PATTERNS/',I2,'.')                                                                                          
C                                                                                                                                   
           ENDIF                                                                                                                    
C                                                                                                                                   
C          * DRAW THE GRAPH                                                                                                         
C                                                                                                                                   
           CALL EZMXY(X, Y, LMAX, NCURV, LMAX, ATITLE(4:ILEN)//'!')                                                                 
                                                                                                                                    
C          * SETUP FOR RESETTING THE CURVE PATTERNS                                                                                 
                                                                                                                                    
           NCURRCOLRESET=1                                                                                                          
                                                                                                                                    
C                                                                                                                                   
C          * DRAW Y=0 LINE IF 0 IS IN THE RANGE                                                                                     
C                                                                                                                                   
           IF(FYLO*FYHI .LT. 0.0) THEN                                                                                              
             CALL LINE (FXLFT, 0.0, FXRIT, 0.0)                                                                                     
           ENDIF                                                                                                                    
                                                                                                                                    
C                                                                                                                                   
C          * IF THE GRAPH IS A SCATTER PLOT, PLOT THE POINTS.                                                                       
C                                                                                                                                   
           IF (ISCAT .EQ. 1) THEN                                                                                                   
              DO 450 J = 1, NCURV                                                                                                   
C!!!!!        * RE-ENABLED TO GET COLOUR SCATTER PLOT.                                                                              
C!!!!!        * ADDED "COLOURCURVES" AND "NCURVCOL" CONDITIONALS.                                                                   
                IF(COLOURCURVES) THEN                                                                                               
                 IF(J.LE.NCURVCOL) THEN                                                                                             
                  MOM=1                                                                                                             
                  IF(ICURVCOL(J).GE.100.AND.ICURVCOL(J).LE.199)                                                                     
     1                 MOM=ICURVCOL(J)-98                                                                                           
                  IF(ICURVCOL(J).GE.350.AND.ICURVCOL(J).LE.419)                                                                     
     1                 MOM=ICURVCOL(J)-248                                                                                          
                    CALL GSTXCI(MOM)                                                                                                
                    CALL PCSETI('CC',MOM)                                                                                           
                    CALL PCSETI('OC', MOM)                                                                                          
                 ELSE                                                                                                               
                  WRITE(6,6150) J                                                                                                   
                 ENDIF                                                                                                              
                ENDIF                                                                                                               
C!!!!!                                                                                                                              
                   IF (NCURV .EQ. 1) THEN                                                                                           
                        PLTCHR = '+'                                                                                                
                   ELSE                                                                                                             
                        PLTCHR = ICHR(J)                                                                                            
                   ENDIF                                                                                                            
                                                                                                                                    
                   DO 440 I = NSKIP + 1, LNT(J)                                                                                     
                        CALL AGPWRT(X(I + (J - 1) * NPNTS),                                                                         
     1                              Y(I + (J - 1) * NPNTS),                                                                         
     2                              PLTCHR, 1, 12, 0, 0)                                                                            
  440              CONTINUE                                                                                                         
  450         CONTINUE                                                                                                              
              CALL AGSETF('SET.', 1.0)                                                                                              
           ENDIF                                                                                                                    
                                                                                                                                    
           NPLOT = NPLOT - 1                                                                                                        
           NPLT = NPLT + 1                                                                                                          
           IF (NPLOT .EQ. 0) THEN                                                                                                   
                CALL FRAME                                                                                                          
                CALL RESET                                                                                                          
           ENDIF                                                                                                                    
                                                                                                                                    
           WRITE(6,6060) NCURV,NPLT                                                                                                 
                                                                                                                                    
C     * END 'WHILE NOT(EOF)' LOOP                                                                                                   
      GO TO 110                                                                                                                     
                                                                                                                                    
C     * E.O.F. ON INPUT.                                                                                                            
                                                                                                                                    
  901 IF (NPLT .EQ. 0) THEN                                                                                                         
         CALL                                      PXIT('CRVPLOT',-8)                                                               
      ELSE                                                                                                                          
         IF (NPLOT .NE. 0) THEN                                                                                                     
            CALL FRAME                                                                                                              
         ENDIF                                                                                                                      
         CALL                                      PXIT('CRVPLOT',0)                                                                
      ENDIF                                                                                                                         
  902 IF (NPLT .EQ. 0) THEN                                                                                                         
         CALL                                      PXIT('CRVPLOT',-9)                                                               
      ELSE                                                                                                                          
         IF (NPLOT .NE. 0) THEN                                                                                                     
            CALL FRAME                                                                                                              
         ENDIF                                                                                                                      
         CALL                                      PXIT('CRVPLOT',0)                                                                
      ENDIF                                                                                                                         
  903 IF (NPLT .EQ. 0) THEN                                                                                                         
         CALL                                      PXIT('CRVPLOT',-10)                                                              
      ELSE                                                                                                                          
         IF (NPLOT .NE. 0) THEN                                                                                                     
            CALL FRAME                                                                                                              
         ENDIF                                                                                                                      
         CALL                                      PXIT('CRVPLOT',0)                                                                
      ENDIF                                                                                                                         
  904 IF (NPLT .EQ. 0) THEN                                                                                                         
         CALL                                      PXIT('CRVPLOT',-11)                                                              
      ELSE                                                                                                                          
         IF (NPLOT .NE. 0) THEN                                                                                                     
            CALL FRAME                                                                                                              
         ENDIF                                                                                                                      
         CALL                                      PXIT('CRVPLOT',0)                                                                
      ENDIF                                                                                                                         
                                                                                                                                    
C-----------------------------------------------------------------------                                                            
                                                                                                                                    
 5010 FORMAT(10X,I10,1X,A4,I5,2E10.0,3I5,1X,2I2,I3,I2,5I1)                                                                          
 5020 FORMAT(2(20A1),40A1)                                                                                                          
 5030 FORMAT(30X,2E10.0)                                                                                                            
 5040 FORMAT(10X,4I5)                                                                                                               
 5050 FORMAT(10X,1X,I4,7I5)                                                                                                         
 5052 FORMAT(15X,7I5)                                                                                                               
C                                                                                                                                   
 6000 FORMAT(I5,3X)                                                                                                                 
 6010 FORMAT(' NT=',I10,' NAME=',A4,' LVL=',I5,' FYLO=',E12.4,                                                                      
     1     ' FYHI=',E12.4,' NSKIP=',I5,' NPNTS=',I5,' NC=',I5,/                                                                     
     2     ' PTYPE=',I2,' LTYPE=',I2,' MLTRX=',I3,' LSPC=',I2,                                                                      
     3     ' NFRM=',I1,' IRWND=',I1,' IXAXS=',I1,' ISCAT=',I1,                                                                      
     4     ' ISAMPL=',I1)                                                                                                           
 6020 FORMAT(1X,2A20,A40)                                                                                                           
 6030 FORMAT(31X,2G11.3)                                                                                                            
 6040 FORMAT(11X,4I5)                                                                                                               
 6050 FORMAT('0NSKIP SHOULD BE ZERO IF IXAXS<0 AND XAXIS VALUES ARE',                                                               
     1       ' READ FROM A FILE.')                                                                                                  
 6060 FORMAT(I5,' CURVES ON PLOT NO',I5)                                                                                            
 6070 FORMAT(/,' *** WARNING: MISSING VALUE(S) SUBSTITUTED IN ',                                                                    
     1         'INTERPOLATING LOGARITHMIC DATA ***',/)                                                                              
 6090 FORMAT(/,' *** WARNING: PACKING DENSITY = ', I4,' ***',/)                                                                     
 6100 FORMAT(/,' *** SORRY, THE ARRAY DIMENSIONS NOT LARGE',                                                                        
     1         ' ENOUGH TO PERFORM LINEAR INTERPOLATION ***',/)                                                                     
 6110 FORMAT('CRVPLOT   ',I10,1X,A4,I5,2G10.4,3I5,1X,2I2,I3,I2,5I1)                                                                 
 6120 FORMAT(2(20A1),40A1)                                                                                                          
 6130 FORMAT(30X,2G10.4)                                                                                                            
 6140 FORMAT(10X,4I5)                                                                                                               
 6150 FORMAT(' NO COLOUR PATTERN DEFINED FOR CURVE NUMBER ',I3,'!')                                                                 
 6160 FORMAT(' RESET NPNTS=',I8)                                                                                                    
                                                                                                                                    
      END                                                                                                                           
