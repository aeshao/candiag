      PROGRAM SPGDEV                                                                                                                
C     PROGRAM SPGDEV (SPIN,       SPOUT,       INPUT,       OUTPUT,     )       E2                                                  
C    1          TAPE1=SPIN, TAPE2=SPOUT, TAPE5=INPUT, TAPE6=OUTPUT)                                                                 
C     -------------------------------------------------------------             E2                                                  
C                                                                               E2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       E2                                                  
C     JUN 24/97 - D. LIU (KEEP IBUF(3) THE SAME AS THAT OF INPUT)                                                                   
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 13/83 - R.LAPRISE.                                                                                                        
C     DEC 03/80 - J.D.HENDERSON                                                                                                     
C     SEP   /79 - S. LAMBERT                                                                                                        
C                                                                               E2                                                  
CSPGDEV  - SETS SPECTRAL MEANS TO ZERO                                  1  1    E1                                                  
C                                                                               E3                                                  
CAUTHOR  - S.LAMBERT                                                            E3                                                  
C                                                                               E3                                                  
CPURPOSE - SETS THE MEAN OF EACH FIELD IN A SPECTRAL FILE TO ZERO               E3                                                  
C          LEAVING THE DEVIATION FROM THE GLOBAL MEAN.                          E3                                                  
C                                                                               E3                                                  
CINPUT FILE...                                                                  E3                                                  
C                                                                               E3                                                  
C      SPIN  = GLOBAL SPECTRAL FIELDS                                           E3                                                  
C                                                                               E3                                                  
COUTPUT FILE...                                                                 E3                                                  
C                                                                               E3                                                  
C      SPOUT = DEVIATION OF SPECTRAL FIELDS                                     E3                                                  
C                                                                               E3                                                  
C-----------------------------------------------------------------------------                                                      
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/BLANCK/F(4420)                                                                                                         
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      COMMON/ICOM/IBUF(8),IDAT(4420)                                                                                                
      DATA MAXX/4420/                                                                                                               
C-----------------------------------------------------------------------                                                            
      NFF=3                                                                                                                         
      CALL JCLPNT(NFF,1,2,6)                                                                                                        
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      NRECS=0                                                                                                                       
  100 CALL GETFLD2(1,F,NC4TO8("SPEC"),-1,-1,-1,IBUF,MAXX,OK)                                                                        
      IF(.NOT.OK)THEN                                                                                                               
        IF(NRECS.EQ.0) CALL                        XIT('SPGDEV',-1)                                                                 
        WRITE(6,6020) NRECS                                                                                                         
        CALL                                       XIT('SPGDEV',0)                                                                  
      ENDIF                                                                                                                         
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF                                                                                             
      F(1)=0.E0                                                                                                                     
C     IBUF(3)=4HGDEV                                                                                                                
      CALL PUTFLD2(2,F,IBUF,MAXX)                                                                                                   
      IF(NRECS.EQ.0) WRITE(6,6030) IBUF                                                                                             
      NRECS=NRECS+1                                                                                                                 
      GO TO 100                                                                                                                     
C-----------------------------------------------------------------------                                                            
6020  FORMAT('0SPGDEV READ',I6,' RECORDS')                                                                                          
6030  FORMAT(1X,A4,I10,2X,A4,I10,4I6)                                                                                               
      END                                                                                                                           
