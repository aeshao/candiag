      PROGRAM EVALPM                                                                                                                
C     PROGRAM EVALPM (STATS,       INPUT,       OUTPUT,                 )       H2                                                  
C    1          TAPE1=STATS, TAPE5=INPUT, TAPE6=OUTPUT)                                                                             
C     -------------------------------------------------                         H2                                                  
C                                                                               H2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2                                                  
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     DEC 10/84 - F. ZWIERS                                                                                                         
C                                                                               H2                                                  
CEVALPM  - LAST STEP OF PERMUTATION TEST PROCESS, RANKING TEST STAT.    1       H1                                                  
C                                                                               H3                                                  
CAUTHOR  - F. ZWIERS                                                            H3                                                  
C                                                                               H3                                                  
CPURPOSE - STEP 3 OF THE PERMUTAION TEST PROCESS.  THIS PROGRAM RANKS THE       H3                                                  
C          OBSERVED TEST STATISTIC AMONGST  THOSE GENERATED  BY PERMUTING       H3                                                  
C          THE LABELS ON THE OBSERVED FIELDS.                                   H3                                                  
C                                                                               H3                                                  
C          SEE THE DOCUMENTATION FOR PROGRAM PERMUT FOR A COMPLETE              H3                                                  
C          DESCRIPTION                                                          H3                                                  
C                                                                               H3                                                  
CINPUT FILE...                                                                  H3                                                  
C                                                                               H3                                                  
C       STATS =  A FILE OF TEST STATISTICS -- ONE REALIZATION OF THE TEST       H3                                                  
C                STATISTIC PER RECORD. THE FIRST REALIZATION REPRESENTS         H3                                                  
C                THE RESULT OF THE EXPERIMENT AS IT WAS CONDUCTED. THE          H3                                                  
C                REMAINDER OF THE REALIZATIONS ARE THE RESULT OF PERMUTING      H3                                                  
C                OBSERVATIONS WITHIN AND BETWEEN SAMPLES.                       H3                                                  
C------------------------------------------------------------------------                                                           
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      DIMENSION IBUF(8),IDAT(64),C(32),Z(18528)                                                                                     
      LOGICAL OK                                                                                                                    
      COMMON/ICOM/IBUF,IDAT                                                                                                         
      DATA MAXX/64/, MAXNPT/18528/                                                                                                  
C                                                                                                                                   
C---------------------------------------------------------------------------                                                        
C                                                                                                                                   
      NF=3                                                                                                                          
      CALL JCLPNT(NF,1,5,6)                                                                                                         
      REWIND 1                                                                                                                      
C                                                                                                                                   
C-----------------------------------------------------------------------                                                            
C                                                                                                                                   
C     * GET THE OBSERVED VALUE OF THE TEST STATISTIC.                                                                               
C                                                                                                                                   
      CALL GETFLD2(1,C,-1,0,0,0,IBUF,MAXX,OK)                                                                                       
      IF(.NOT.OK)THEN                                                                                                               
         WRITE(6,6030)                                                                                                              
         CALL                                      XIT('EVALPM',-1)                                                                 
      ENDIF                                                                                                                         
      P=C(1)                                                                                                                        
      Z(1)=C(1)                                                                                                                     
C                                                                                                                                   
C     * GET THE REMAINING NPERM-1 DIFFERENCES                                                                                       
C                                                                                                                                   
      NPERM=1                                                                                                                       
   40 CONTINUE                                                                                                                      
      CALL GETFLD2(1,C,-1,0,0,0,IBUF,MAXX,OK)                                                                                       
      IF(.NOT.OK) GOTO 50                                                                                                           
      NPERM=NPERM+1                                                                                                                 
      IF(NPERM.GT.MAXNPT) CALL                     XIT('EVALPM',-2)                                                                 
      Z(NPERM)=C(1)                                                                                                                 
      GOTO 40                                                                                                                       
                                                                                                                                    
   50 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * SORT THE STATISTICS USING AN IMSL SORT ROUTINE                                                                              
C                                                                                                                                   
      CALL VSRTA(Z,NPERM)                                                                                                           
      WRITE(6,6040)                                                                                                                 
      NP50=NPERM/50                                                                                                                 
      DO 60 II=1,NP50                                                                                                               
         IL=(II-1)*50 + 1                                                                                                           
         IU=IL + 49                                                                                                                 
         WRITE(6,6050)                                                                                                              
         WRITE(6,6060) (I,(Z(I+J-1),J=1,5),I=IL,IU,5)                                                                               
   60 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * COMPUTE THE PERCENTILE RANK OF THE OBSERVED VALUE                                                                           
C                                                                                                                                   
      DO 70 I=1,NPERM                                                                                                               
         IF(Z(I).EQ.P) GOTO 80                                                                                                      
   70 CONTINUE                                                                                                                      
      I=NPERM                                                                                                                       
   80 CONTINUE                                                                                                                      
      PERCNT=((FLOAT(I)-0.5E0)/FLOAT(NPERM))*100.0E0                                                                                
      WRITE(6,6070) P,PERCNT                                                                                                        
      CALL                                         XIT('EVALPM',0)                                                                  
C                                                                                                                                   
C-------------------------------------------------------------------------                                                          
C                                                                                                                                   
 6030 FORMAT('0THE FILE OF TEST STATISTICS IS EMPTY.')                                                                              
 6040 FORMAT('1RANKED VALUES OF TEST STATISTIC')                                                                                    
 6050 FORMAT(' ')                                                                                                                   
 6060 FORMAT(10(' ',I5,5E12.4/))                                                                                                    
 6070 FORMAT('0THE OBSERVED VALUE OF THE TEST STATISTIC IS ',E12.4/                                                                 
     1       '0THE PERCENTILE RANK OF THIS VALUE IS ',F6.2)                                                                         
      END                                                                                                                           
