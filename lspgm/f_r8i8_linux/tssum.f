      PROGRAM TSSUM                                                                                                                 
C     PROGRAM TSSUM (X,       XB,        OUTPUT,             )                  H2                                                  
C    1         TAPE1=X, TAPE2=XB,  TAPE6=OUTPUT)                                                                                    
C     -----------------------------------------------------                     H2                                                  
C                                                                               H2                                                  
C     FEB 18/10 - G.J.BOER                                                      H2                                                  
C                                                                               H2                                                  
CTSSUM    - SIMPLE TIMSUM BUT ON TIMESERIES FILES                       1  1 C  H1                                                  
C                                                                               H3                                                  
CAUTHOR  - G.J.BOER                                                             H3                                                  
C                                                                               H3                                                  
CPURPOSE - SIMPLE CUMULATIVE SUM                                                H3                                                  
C          NOTE -  X MUST BE TIME SERIES (EXCEPT FOR LABL AND CHAR)             H3                                                  
C                                                                               H3                                                  
CINPUT FILES...                                                                 H3                                                  
C                                                                               H3                                                  
C      X  = INPUT TIMESERIES                                                    H3                                                  
C                                                                               H3                                                  
COUTPUT FILES...                                                                H3                                                  
C                                                                               H3                                                  
C      XB = OUTPUT TIMESERIES CUMULATIVE SUM OF INPUT VALUES                    H3                                                  
C                                                                                                                                   
C----------------------------------------------------------------------------                                                       
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      PARAMETER (MAXT=18528, MAXTV=MAXT*1)                                                                                          
      REAL X(MAXT)                                                                                                                  
C                                                                                                                                   
      LOGICAL OK,SPEC                                                                                                               
      COMMON/ICOM/IBUF(8),IDAT(MAXTV)                                                                                               
      DATA MAXX/MAXTV/                                                                                                              
C---------------------------------------------------------------------                                                              
      NFF=3                                                                                                                         
      CALL JCLPNT(NFF,1,2,6)                                                                                                        
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
C                                                                                                                                   
C     * READ INPUT FILE                                                                                                             
C                                                                                                                                   
      NR=0                                                                                                                          
100   CALL GETFLD2(1,X,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                    
      IF (.NOT.OK)THEN                                                                                                              
        IF (NR.EQ.0)CALL                           XIT('TSSUM',-1)                                                                  
        CALL PRTLAB(IBUF)                                                                                                           
        WRITE(6,'(A,I5)') ' PROCESSED RECORDS: ',NR                                                                                 
        CALL                                       XIT('TSSUM',0)                                                                   
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * PROPAGATE SUPERLABELS/CHAR RECORDS                                                                                          
C                                                                                                                                   
      IF (IBUF(1).EQ.NC4TO8("LABL").OR.                                                                                             
     1    IBUF(1).EQ.NC4TO8("CHAR")) GOTO 210                                                                                       
C                                                                                                                                   
C     * ABORT IF NOT TIME SERIES                                                                                                    
C                                                                                                                                   
      IF(IBUF(1).NE.NC4TO8("TIME")) CALL           XIT('TSSUM',-2)                                                                  
      NR=NR+1                                                                                                                       
      NWDS=IBUF(5)                                                                                                                  
C                                                                                                                                   
C     * CUMULATIVE SUM OF VALUES                                                                                                    
C                                                                                                                                   
      DO J=2,NWDS                                                                                                                   
        X(J)=X(J-1)+X(J)                                                                                                            
      ENDDO                                                                                                                         
C                                                                                                                                   
C     * SAVE THE RESULT ON FILE XB                                                                                                  
C                                                                                                                                   
 210  CALL PUTFLD2(2,X,IBUF,MAXX)                                                                                                   
      GOTO 100                                                                                                                      
      END                                                                                                                           
