      PROGRAM HYDFLDS                                                                                                               
C     PROGRAM HYDFLDS (PVEG,       SVEG,       SOIL,       GC,                  C2                                                  
C    1                 WCAP,       ESLP,       SMSK,       OUTPUT,      )       C2                                                  
C    2           TAPE1=PVEG, TAPE2=SVEG, TAPE3=SOIL, TAPE4=GC,                                                                      
C    3           TAPE7=WCAP, TAPE8=ESLP, TAPE9=SMSK, TAPE6=OUTPUT)                                                                  
C     ------------------------------------------------------------              C2                                                  
C                                                                               C2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       C2                                                  
C     NOV 06/95 - F.MAJAESS (REVISE DEFAULT PACKING DENSITY VALUE)                                                                  
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     JUN 22/88 - M.LAZARE.                                                                                                         
C                                                                               C2                                                  
CHYDFLDS - PRODUCES MODEL INTERNAL HYDROLOGICAL FIELDS                  4  3    C1                                                  
C                                                                               C3                                                  
CAUTHOR  - M.LAZARE                                                             C3                                                  
C                                                                               C3                                                  
CPURPOSE - PRODUCE MODEL INTERNAL HYDROLOGICAL FIELDS DERIVED FROM LOOK-UP      C3                                                  
C          TABLE BASED ON GROUND COVER, SOIL AND PRIMARY/SECONDARY VEGETATION.  C3                                                  
C                                                                               C3                                                  
CINPUT FILES...                                                                 C3                                                  
C                                                                               C3                                                  
C      PVEG = CONTAINS PRIMARY   VEGETATION FIELD                               C3                                                  
C      SVEG = CONTAINS SECONDARY VEGETATION FIELD                               C3                                                  
C      SOIL = CONTAINS SOIL                 FIELD                               C3                                                  
C      GC   = CONTAINS GROUND COVER         FIELD                               C3                                                  
C                                                                               C3                                                  
COUTPUT FILES...                                                                C3                                                  
C                                                                               C3                                                  
C      WCAP = GROUND WATER HOLDING CAPACITY FIELD (KG/M**2)                     C3                                                  
C      ESLP = EVAPOTRANSPIRATION SLOPE                                          C3                                                  
C      SMSK = SNOW MASKING DEPTH (METRES)                                       C3                                                  
C-----------------------------------------------------------------------------                                                      
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/BLANCK/PVEG(18528),SVEG(18528),                                                                                        
     1              SOIL(18528),GC(18528)                                                                                           
      COMMON/OUTFLD/WCAP(18528),ESLP(18528),                                                                                        
     1              SMSK(18528)                                                                                                     
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      COMMON/ICOM/IBUF(8),IDAT(18528)                                                                                               
      COMMON/JCOM/JBUF(8),JDAT(18528)                                                                                               
      COMMON/KCOM/KBUF(8),KDAT(18528)                                                                                               
      COMMON/LCOM/LBUF(8),LDAT(18528)                                                                                               
C                                                                                                                                   
C     * PARAMETERS IN COMMON WITH SUBROUTINE HYDTABL...                                                                             
C                                                                                                                                   
      COMMON /HYD/ HYDFACT(3,24)                                                                                                    
      COMMON /WEIGHT/ WEIGHTP,WEIGHTS                                                                                               
C                                                                                                                                   
      DATA MAXX/18528/                                                                                                              
C---------------------------------------------------------------------                                                              
      NFF=8                                                                                                                         
      CALL JCLPNT(NFF,1,2,3,4,7,8,9,6)                                                                                              
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      REWIND 3                                                                                                                      
      REWIND 4                                                                                                                      
C                                                                                                                                   
C     * READ THE NEXT SET OF FIELDS.                                                                                                
C                                                                                                                                   
      NR=0                                                                                                                          
  140 CALL GETFLD2(1,PVEG, -1 ,0,0,0,IBUF,MAXX,OK)                                                                                  
      IF(.NOT.OK)THEN                                                                                                               
        IF(NR.EQ.0) CALL                           XIT('HYDFLDS',-1)                                                                
        WRITE(6,6010) NR                                                                                                            
        CALL                                       XIT('HYDFLDS',0)                                                                 
      ENDIF                                                                                                                         
      IF(NR.EQ.0) THEN                                                                                                              
        WRITE(6,6025) IBUF                                                                                                          
        NPACK=MIN(2,IBUF(8))                                                                                                        
      ENDIF                                                                                                                         
C                                                                                                                                   
      CALL GETFLD2(2,SVEG, -1 ,0,0,0,JBUF,MAXX,OK)                                                                                  
      IF(.NOT.OK) CALL                             XIT('HYDFLDS',-2)                                                                
      IF(NR.EQ.0) THEN                                                                                                              
        WRITE(6,6025) JBUF                                                                                                          
        NPACK=MIN(NPACK,JBUF(8))                                                                                                    
      ENDIF                                                                                                                         
C                                                                                                                                   
      CALL GETFLD2(3,SOIL, -1 ,0,0,0,KBUF,MAXX,OK)                                                                                  
      IF(.NOT.OK) CALL                             XIT('HYDFLDS',-3)                                                                
      IF(NR.EQ.0) THEN                                                                                                              
        WRITE(6,6025) KBUF                                                                                                          
        NPACK=MIN(NPACK,KBUF(8))                                                                                                    
      ENDIF                                                                                                                         
C                                                                                                                                   
      CALL GETFLD2(4,GC, -1 ,0,0,0,LBUF,MAXX,OK)                                                                                    
      IF(.NOT.OK) CALL                             XIT('HYDFLDS',-4)                                                                
      IF(NR.EQ.0) WRITE(6,6025) LBUF                                                                                                
C                                                                                                                                   
C     * MAKE SURE THAT THE FIELDS ARE THE SAME KIND AND SIZE.                                                                       
C                                                                                                                                   
      CALL CMPLBL(0,IBUF,0,JBUF,OK)                                                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6025) IBUF,JBUF                                                                                                     
        CALL                                       XIT('HYDFLDS',-5)                                                                
      ENDIF                                                                                                                         
C                                                                                                                                   
      CALL CMPLBL(0,IBUF,0,KBUF,OK)                                                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6025) IBUF,KBUF                                                                                                     
        CALL                                       XIT('HYDFLDS',-6)                                                                
      ENDIF                                                                                                                         
C                                                                                                                                   
      CALL CMPLBL(0,IBUF,0,LBUF,OK)                                                                                                 
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6025) IBUF,LBUF                                                                                                     
        CALL                                       XIT('HYDFLDS',-7)                                                                
      ENDIF                                                                                                                         
C                                                                                                                                   
      NWDS=IBUF(5)*IBUF(6)                                                                                                          
      CALL GCROUND(GC,1,NWDS)                                                                                                       
C                                                                                                                                   
C     * OBTAIN FIELDS FROM LOOK-UP TABLE.                                                                                           
C                                                                                                                                   
      CALL HYDTABL(PVEG,SVEG,SOIL,GC,WCAP,ESLP,SMSK,NWDS)                                                                           
C                                                                                                                                   
C     * SAVE THE RESULTS.                                                                                                           
C                                                                                                                                   
      IBUF(8)=NPACK                                                                                                                 
      IBUF(3)=NC4TO8("WCAP")                                                                                                        
      CALL PUTFLD2(7,WCAP,IBUF,MAXX)                                                                                                
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
C                                                                                                                                   
      IBUF(3)=NC4TO8("ESLP")                                                                                                        
      CALL PUTFLD2(8,ESLP,IBUF,MAXX)                                                                                                
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
C                                                                                                                                   
      IBUF(3)=NC4TO8("SMSK")                                                                                                        
      CALL PUTFLD2(9,SMSK,IBUF,MAXX)                                                                                                
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
C                                                                                                                                   
      NR=NR+1                                                                                                                       
      GO TO 140                                                                                                                     
C---------------------------------------------------------------------                                                              
 6010 FORMAT('0',I6,'  SETS OF RECORDS PROCESSED')                                                                                  
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)                                                                                              
      END                                                                                                                           
