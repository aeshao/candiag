      PROGRAM UNPAKRS                                                                                                               
C     PROGRAM UNPAKRS (OLDRS,       NEWRS,       OUTPUT,                    )   J2                                                  
C    1           TAPE1=OLDRS, TAPE2=NEWRS, TAPE6=OUTPUT)                                                                            
C     ------------------------------------------------------                    J2                                                  
C                                                                               J2                                                  
C     SEP 25/06 - F. MAJAESS (SKIP CONVERTING RECORDS WITH KIND "CHAR" )        J2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)                                                           
C     JUL 13/92 - E. CHAN.                                                                                                          
C                                                                               J2                                                  
CUNPAKRS - CONVERTS FIELDS WITH PACKING DENSITY 1 TO NATIVE MACHINE     1  1    J1                                                  
C          FORMAT.                                                              J1                                                  
C                                                                               J3                                                  
CAUTHOR  - E. CHAN                                                              J3                                                  
C                                                                               J3                                                  
CPURPOSE - CONVERTS FIELDS PACKED WITH IBUF(8)=1 TO NATIVE MACHINE FORMAT       J3                                                  
C          (I.E. IBUF(8)=0).  PORTABLE MODEL RESTART FILES (I.E. DATA PACKED    J3                                                  
C          IN 64-BIT IEEE FORMAT) MUST HAVE FIELDS WITH PACKING DENSITY 1       J3                                                  
C          CONVERTED BACK TO NATIVE MACHINE FORMAT BEFORE BEING READ IN BY      J3                                                  
C          THE MODEL.  INTERNAL PACKING DENSITIES OF 1 ARE NOT PERMITTED        J3                                                  
C          IN THE MODEL.                                                        J3                                                  
C                                                                               J3                                                  
CINPUT FILE(S)...                                                               J3                                                  
C                                                                               J3                                                  
C      OLDRS        = FILE CONTAINING DATA TO RESTART THE MODEL                 J3                                                  
C                                                                               J3                                                  
COUTPUT FILES...                                                                J3                                                  
C                                                                               J3                                                  
C      NEWRS        = CONVERTED RESTART FILE WITH FIELDS PACKED USING           J3                                                  
C                     PACKING DENSITIES OTHER THAN 1.                           J3                                                  
C--------------------------------------------------------------------------                                                         
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      LOGICAL OK                                                                                                                    
                                                                                                                                    
      COMMON/BLANCK/ F(271600)                                                                                                      
      COMMON/BUFCOM/ IBUF(8),IDAT(271600)                                                                                           
                                                                                                                                    
      DATA MAXX/271600/                                                                                                             
C----------------------------------------------------------------------                                                             
      NFIL=3                                                                                                                        
      CALL JCLPNT (NFIL,1,2,6)                                                                                                      
C                                                                                                                                   
C     * ENSURE THAT ALL FIELDS IN RESTART FILE WITH PACKING DENSITY 1                                                               
C     * ARE CONVERTED TO NATIVE MACHINE FORMAT.                                                                                     
C                                                                                                                                   
      NR=0                                                                                                                          
      NCR=0                                                                                                                         
  100 CALL GETFLD2 (1,F,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                   
      IF ( .NOT.OK )  THEN                                                                                                          
        IF( NR.EQ.0 ) THEN                                                                                                          
          CALL                                     XIT('UNPAKRS',-1)                                                                
        ELSE                                                                                                                        
          WRITE(6,6000) IBUF                                                                                                        
          WRITE(6,6020) NR,NCR                                                                                                      
          CALL                                     XIT('UNPAKRS',0)                                                                 
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
      IF ( IBUF(8).EQ.1 .AND. IBUF(1).NE.NC4TO8("CHAR") ) THEN                                                                      
        IBUF(8)=0                                                                                                                   
        NCR=NCR+1                                                                                                                   
      ENDIF                                                                                                                         
      CALL PUTFLD2 (2,F,IBUF,MAXX)                                                                                                  
      IF ( NR.EQ.0 ) WRITE(6,6000) IBUF                                                                                             
      NR=NR+1                                                                                                                       
      GOTO 100                                                                                                                      
C                                                                                                                                   
C     * ERROR EXIT.                                                                                                                 
C                                                                                                                                   
  901 CALL                                         XIT('UNPAKRS',-2)                                                                
C---------------------------------------------------------------------                                                              
 6000 FORMAT(1X,A4,I10,1X,A4,5I10)                                                                                                  
 6020 FORMAT ('  RECORDS READ      =',I6,                                                                                           
     1      /,'  RECORDS CONVERTED =',I6)                                                                                           
      END                                                                                                                           
