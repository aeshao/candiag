      PROGRAM LLAGG                                                                                                                 
C     PROGRAM LLAGG (GL,       VL,       GG,       VG,       FG,                D2                                                  
C                                               INPUT,       OUTPUT,    )       D2                                                  
C    1        TAPE11=GL,TAPE12=VL,TAPE13=GG,TAPE14=VG,TAPE15=FG,                                                                    
C                                         TAPE5=INPUT, TAPE6=OUTPUT)                                                                
C     --------------------------------------------------------------            D2                                                  
C                                                                               D2                                                  
C     MAR 25/13 - S.KHARIN (ADD AREA-WEIGHTED INTERPOLATION.                    D2                                                  
C                           IMPROVE INTERPOLATION NEAR THE POLES.               D2                                                  
C                           FIX CYCLIC LONGITUDE TREATMENT FOR SHIFTED GRIDS.   D2                                                  
C                           CORRECT TREATMENT OF VECTOR FIELDS.                 D2                                                  
C                           ADD OPTION FOR INPUT GAUSSIAN GRIDS.                D2                                                  
C                           USE PACKING DENSITY OF INPUT FIELD AS DEFAULT.      D2                                                  
C                           USE BILINEAR INTERPOLATION AS DEFAULT.              D2                                                  
C                           TREAT SPECIAL VALUES 1E38 AS MISSING WHEN KIND>0.   D2                                                  
C                           USE MORE ACCURATE PI. HARMONIZE CODE WITH GGALL).   D2                                                  
C     OCT 13/04 - S.KHARIN (FIX A BUG AFFECTING DIMENSIONS WHEN KIND<0)                                                             
C     OCT 06/04 - S.KHARIN (FIX A BUG AFFECTING ZONAL AVERAGES INTERPOLATION)                                                       
C     SEP 20/04 - S.KHARIN (ADD INTERPOLATION FOR ZONAL AVERAGES)                                                                   
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)                                                           
C     APR 12/99 - F.MAJAESS (DISABLE CHANGING "0" LEVEL VALUE TO "1")                                                               
C     JAN 15/97 - F.MAJAESS (REVISED TO ALLOW WRITING OUT GRID SUPERLABELS)                                                         
C     MAR 03/93 - F.MAJAESS (REVISED TO HANDLE MASKED/SHIFTED LAT-LON GRID)                                                         
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)                                                                 
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 12/83 - R.LAPRISE.                                                                                                        
C     MAR 23/81 - J.D.HENDERSON                                                                                                     
C                                                                               D2                                                  
CLLAGG   - CONVERT LAT-LON OR GAUSSIAN GRID/ZONL TO GAUSSIAN GRID/ZONL  2  2 C GD1                                                  
C                                                                               D3                                                  
CAUTHOR  - J.D.HENDERSON                                                        D3                                                  
C                                                                               D3                                                  
CPURPOSE - CONVERTS GLOBAL LAT-LON OR GAUSSIAN GRIDS OR ZONAL AVERAGES TO       D3                                                  
C          GLOBAL GAUSSIAN GRIDS OR ZONAL AVERAGES BY BILINEAR, CUBIC, OR       D3                                                  
C          AREA-WEIGTED INTERPOLATION.                                          D3                                                  
C          NOTE - INPUT LAT-LON GRIDS MAY BE SHIFTED AND UNSHIFTED.             D3                                                  
C                 LAT-UNSHIFTED GRID INCLUDES THE POLES.                        D3                                                  
C                 LON-UNSHIFTED GRID INCLUDES AN EXTRA CYCLIC LONGITUDE.        D3                                                  
C                 SPVAL=1.E38 VALUES ARE TREATED AS MISSING.                    D3                                                  
C                                                                               D3                                                  
CINPUT FILES...                                                                 D3                                                  
C                                                                               D3                                                  
C      GL = GLOBAL LAT-LON GRIDS OR ZONAL AVERAGES OF A SCALAR FIELD            D3                                                  
C           OR U- OR V-COMPONENT OF A VECTOR FIELD.                             D3                                                  
C      VL = (OPTIONAL) GLOBAL LAT-LON GRIDS OR ZONAL AVERAGES OF THE            D3                                                  
C           V-COMPONENT OF A VECTOR FIELD (IFLD=2).                             D3                                                  
C                                                                               D3                                                  
COUTPUT FILES...                                                                D3                                                  
C                                                                               D3                                                  
C      GG = GLOBAL GAUSSIAN OR LAT/LON GRIDS OR ZONAL AVERAGES OF A SCALAR      D3                                                  
C           FIELD OR U- OR V-COMPONENT OF A VECTOR FIELD.                       D3                                                  
C      VG = (OPTIONAL) GLOBAL GAUSSIAN OR LAT/LON GRIDS OR ZONAL AVERAGES OF    D3                                                  
C           THE V-COMPONENT OF A VECTOR FIELD.                                  D3                                                  
C           MUST BE PRESENT IF INPUT FILE VG IS PRESENT (IFLD=2).               D3                                                  
C      FG = (OPTIONAL) AREA FRACTION (0..1) OF OUTPUT GRID CELLS WHERE INPUT    D3                                                  
C           DATA ARE AVAILABLE. THIS ARGUMENT CAN BE USED ONLY WITH             D3                                                  
C           AREA-WEIGHTED INTEPOLATION (KIND=5 OR KIND=-5).                     D3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               D5                                                  
C11-15 NLG   = NUMBER OF LONGITUDES IN THE OUTPUT GRID EXCLUDING THE CYCLIC     D5                                                  
C              LONGITUDE. IGNORED FOR INPUT FIELDS OF KIND 'ZONL'.              D5                                                  
C              IF NLG<0 AND NLAT>0 THEN OUTPUT GRID IS UNSHIFTED LAT-LON GRID   D5                                                  
C                ABS(NLG) BY NLAT THAT INCLUDES THE POLES AND CYCLIC LONGITUDE. D5                                                  
C16-20 NLAT  = NUMBER OF LATITUDES IN THE OUTPUT GRID.                          D5                                                  
C              IF NLAT<0 AND NLG>0, OUTPUT GRID IS SHIFTED LAT-LON GRID         D5                                                  
C                NLG BY ABS(NLAT). OUTPUT GRID SHIFTS SHFTLTO/SHFTLGO ARE READ  D5                                                  
C                FROM EXTRA INPUT CARD.                                         D5                                                  
C              IF BOTH NLG<0 AND NLAT<0 THEN OUTPUT GRID IS IRREGULAR LATS/LONS.D5                                                  
C                OUTPUT GRID SHIFTS SHFTLTO/SHFTLGO ARE READ FROM EXTRA CARD.   D5                                                  
C                THEN VALUES OF LATS (DEGREES) ARE READ FROM ANOTHER INPUT CARD.D5                                                  
C                IF SHFTLTO<0 (SEE BELOW) THEN THE VALUES OF LONS (DEGREES) ARE D5                                                  
C                READ FROM AN EXTRA INPUT CARD.                                 D5                                                  
C21-22 NOCYC = 1 SUPPRESS CYCLIC LONGITUDE IN OUTPUT GRID (FOR EXAMPLE, IN      D5                                                  
C                GAUSSIAN OR UNSHIFTED LAT/LON GRIDS).                          D5                                                  
C                OTHERWISE, STANDARD GRID WITH CYCLIC LONGITUDE.                D5                                                  
C23-25 KIND  = INPUT GRID AND INTERPOLATION TYPE:                               D5                                                  
C            = 1 BILINEAR INTERPOLATION FROM UNSHIFTED LAT-LON GRID THAT        D5                                                  
C                INCLUDES THE POLES AND CYCLIC LONGITUDE.                       D5                                                  
C            = 2 BILINEAR INTERPOLATION FROM GAUSSIAN GRID THAT                 D5                                                  
C                INCLUDES CYCLIC LONGITUDE.                                     D5                                                  
C            = 3 CUBIC  INTERPOLATION FROM UNSHIFTED LAT-LON GRID THAT          D5                                                  
C                INCLUDES THE POLES AND CYCLIC LONGITUDE.                       D5                                                  
C            = 4 CUBIC  INTERPOLATION FROM GAUSSIAN GRID THAT                   D5                                                  
C                INCLUDES CYCLIC LONGITUDE.                                     D5                                                  
C            = 5 AREA-WEIGHTED INTERPOLATION FROM UNSHIFTED LAT-LON GRID THAT   D5                                                  
C                INCLUDES THE POLES AND CYCLIC LONGITUDE.                       D5                                                  
C            = 6 AREA-WEIGHTED INTERPOLATION FROM GAUSSIAN GRID THAT            D5                                                  
C                INCLUDES CYCLIC LONGITUDE.                                     D5                                                  
C            =-1 BILINEAR INTERPOLATION FROM SHIFTED LAT-LON GRID.              D5                                                  
C            =-3 CUBIC  INTERPOLATION FROM SHIFTED LAT-LON GRID.                D5                                                  
C            =-5 AREA-WEIGHTED INTERPOLATION FROM SHIFTED LAT-LON GRID.         D5                                                  
C                NOTE:                                                          D5                                                  
C                LONG-UNSHIFTED GRID (SHFTLG=0) INCLUDES CYCLIC LONGITUDE.      D5                                                  
C                LONG-SHIFTED (SHFTLG!=0) GRID DOESN'T INCLUDE CYCLIC LONGITUDE.D5                                                  
C            = 0 DEFAULTS TO 1.                                                 D5                                                  
C26-30 NPKGG = OUTPUT PACKING DENSITY.                                          D5                                                  
C            = 0 USE PACKING DENSITY OF INPUT FIELDS, EXCEPT WHEN THEY PACKED   D5                                                  
C                WITH PACKING DENSITY 0 IN WHICH CASE NPKGG IS SET TO 1.        D5                                                  
C31-40 SPVAL =   SPECIAL VALUE TO INDICATE MISSING DATA. USED ONLY WHEN KIND<0. D5                                                  
C                OTHERWISE SPVAL=1.E+38.                                        D5                                                  
C41-50 SHFTLT=   GRID SHIFT ALONG LATITUDE FROM THE POLES IN DEGREES.           D5                                                  
C51-60 SHFTLG=   GRID SHIFT ALONG LONGITUDE FROM THE GREENWICH IN DEGREES.      D5                                                  
C61-65 ISLBL = 1 PROPAGATE SUPERLABELS FROM INPUT FILE INTO OUTPUT FILE.        D5                                                  
C                OTHERWISE DO NOT OUTPUT THEM.                                  D5                                                  
C66-70 IFLD  = INPUT FIELD TYPE:                                                D5                                                  
C            = 0 INPUT FILE CONTAINS SCALAR FIELDS.                             D5                                                  
C            = 1 INPUT FILE CONTAINS U- OR V-VECTOR COMPONENTS.                 D5                                                  
C            = 2 TWO INPUT FILES CONTAIN U- AND V-VECTOR COMPONENTS.            D5                                                  
C                THE RESULTS WILL BE SAVED IN TWO OUTPUT FILES.                 D5                                                  
C                                                                               D5                                                  
C 2ND INPUT CARD (OPTIONAL. USED ONLY IF NLAT<0):                               D5                                                  
C                                                                               D5                                                  
C11-20 SHFTLTO=  OUTPUT GRID SHIFT ALONG LATITUDE FROM THE POLES IN DEGREES.    D5                                                  
C21-30 SHFTLGO=  OUTPUT GRID SHIFT ALONG LONGITUDE FROM GREENWICH IN DEGREES    D5                                                  
C31-35 IGLOB  =  0 OUTPUT GRID IS GLOBAL.                                       D5                                                  
C                  OTHERWESE OUTPUT GRID IS A SUBAREA OF A GLOBAL GRID.         D5                                                  
C                  USED ONLY FOR CUSTOM LAT/LON OUTPUT GRIDS.                   D5                                                  
C                                                                               D5                                                  
C 3RD INPUT CARD (OPTIONAL. USED ONLY IF NLAT<0 AND NLG<0.)                     D5                                                  
C                                                                               D5                                                  
C11-...  DLAT = LATITUDES IN DEGREES OF OUTPUT GRID (10 CHARACTERS PER VALUE).  D5                                                  
C                                                                               D5                                                  
C 4TH INPUT CARD (OPTIONAL. USED ONLY IF NLAT<0, NLG<0 AND SHFTLTO<0):          D5                                                  
C                                                                               D5                                                  
C11-...  DLON = LONGITUDES IN DEGREES OF OUTPUT GRID (10 CHARACTERS PER VALUE). D5                                                  
C                                                                               D5                                                  
C      NOTE:   INPUT PARAMETERS SPVAL,SHFTLT,SHFTLG ARE USED ONLY WHEN KIND<0.  D5                                                  
C              SPVAL=1.E38 IS USED WHEN KIND>0.                                 D5                                                  
C                                                                               D5                                                  
CEXAMPLES OF INPUT CARD...                                                      D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64    1                                                       D5                                                  
C(BILINEAR INTERPOLATION OF SCALAR FIELDS FROM UNSHIFTED LAT-LON GRID TO        D5                                                  
C128X64 GAUSSIAN GRID. INPUT GRIDS MUST INLCUDE POLES AND CYCLIC LONGITUDE.     D5                                                  
C OUTPUT PACKING DENSITY IS OF INPUT FIELDS. SKIP SUPERLABELS.)                 D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -1    0     1.E38        0.        0.                    D5                                                  
C(THIS CARD IS EQUIVALENT TO THE CARD ABOVE.)                                   D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -1    1     1.E38       2.5       2.5                    D5                                                  
C(BILINEAR INTERPOLATION TO128X64 GAUSSIAN GRID FROM SHIFTED 72X36 LAT-LON GRID.D5                                                  
C THE SHIFTED INPUT GRID DOES NOT HAVE CYCLIC LONGITUDE. PACKING IS SET TO 1)   D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -1    1     1.E38      1.25      1.25                    D5                                                  
C(LINEAR INTERPOLATION TO128X64 GAUSSIAN GRID FROM SHIFTED 144X72 LAT-LON GRID) D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -5    1     1.E38      1.25      1.25                    D5                                                  
C(WEIGHT.INTERPOLATION TO128X64 GAUSSIAN GRID FROM SHIFTED 144X72 LAT-LON GRID) D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -1    1     1.E38        1.        1.                    D5                                                  
C(LINEAR INTERPOLATION TO 128X64 GAUSSIAN GRID FROM 180X90 SHIFTED LAT-LON GRID)D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -1    0    309.99       0.5       0.5                    D5                                                  
C(LINEAR INTERPOLATION TO128X64 GAUSSIAN GRID FROM 360X180 SHIFTED LAT-LON GRID.D5                                                  
C SPECIAL VALUE IS 309.99. USE PACKING DENSITY OF INPUT FIELDS.)                D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64   -5    0     1.E38       0.5       0.5    1               D5                                                  
C(AREA-WEIGHTED INTERPOLATION TO 128X64 GAUSSIAN GRID FROM 360X180 SHIFTED      D5                                                  
C LAT-LON GRID. PROPAGATE SUPERLABELS FROM INPUT FILE INTO OUTPUT FILE.         D5                                                  
C USE PACKING DENSITY OF INPUT FIELDS.)                                         D5                                                  
C                                                                               D5                                                  
C* LLAGG    128   64    6    0                                  1               D5                                                  
C(AREA-WEIGHTED INTERPOLATION TO 128X64 GAUSSIAN GRID FROM GAUSSIAN INPUT       D5                                                  
C GRID. PROPAGATE SUPERLABELS FROM INPUT FILE INTO OUTPUT FILE.                 D5                                                  
C USE PACKING DENSITY OF INPUT FIELDS.)                                         D5                                                  
C                                                                               D5                                                  
C* LLAGG   -144   73    5    0     1.E38        1.        1.    1               D5                                                  
C(AREA-WEIGHTED INTERPOLATION TO 145X73 UNSHIFTED LAT-LON GRID FROM 180X90      D5                                                  
C SHIFTED LAT-LON GRID. PROPAGATE SUPERLABELS FROM INPUT FILE INTO OUTPUT FILE. D5                                                  
C USE PACKING DENSITY OF INPUT FIELDS.)                                         D5                                                  
C                                                                               D5                                                  
C* LLAGG    -57  -48   -1    0     1.E38       0.5       0.5    1               D5                                                  
C* LLAGG         -1.       0.0    1                                             D5                                                  
C* LATS      46.1771   46.5312   46.8854   47.2396   47.5938   47.9479   48.3021D5                                                  
C* LATS      48.6563   49.0104   49.3646   49.7188   50.0729   50.4271   50.7813D5                                                  
C* LATS      51.1354   51.4896   51.8438   52.1979   52.5521   52.9063   53.2604D5                                                  
C* LATS      53.6146   53.9688   54.3229   54.6771   55.0313   55.3854   55.7396D5                                                  
C* LATS      56.0938   56.4480   56.8021   57.1563   57.5105   57.8646   58.2188D5                                                  
C* LATS      58.5730   58.9271   59.2813   59.6355   59.9896   60.3438   60.6980D5                                                  
C* LATS      61.0521   61.4063   61.7605   62.1146   62.4688   62.8230          D5                                                  
C* LONS      215.307   215.921   216.535   217.149   217.763   218.377   218.991D5                                                  
C* LONS      219.605   220.219   220.833   221.447   222.061   222.675   223.289D5                                                  
C* LONS      223.903   224.517   225.131   225.746   226.360   226.974   227.588D5                                                  
C* LONS      228.202   228.816   229.430   230.044   230.658   231.272   231.886D5                                                  
C* LONS      232.500   233.114   233.728   234.342   234.956   235.570   236.184D5                                                  
C* LONS      236.798   237.412   238.026   238.640   239.254   239.868   240.482D5                                                  
C* LONS      241.096   241.710   242.324   242.938   243.552   244.166   244.780D5                                                  
C* LONS      245.394   246.008   246.622   247.237   247.851   248.465   249.079D5                                                  
C* LONS      249.693                                                            D5                                                  
C(BILINEAR INTERPOLATION TO CUSTOM 57X48 GRID FROM 360X180 SHIFTED LAT-LON GRID.D5                                                  
C THEN LATS ARE READ. SINCE SHFTLTO=-1 IS NEGATIVE, LONS ARE ALSO READ-IN.      D5                                                  
C PROPAGATE SUPERLABELS FROM INPUT FILE INTO OUTPUT FILE.                       D5                                                  
C USE PACKING DENSITY OF INPUT FIELDS.)                                         D5                                                  
C----------------------------------------------------------------------------                                                       
C                                                                                                                                   
C      IMPLICIT REAL (A-H,O-Z),                                                                                                     
C     +INTEGER (I-N)                                                                                                                
      IMPLICIT NONE                                                                                                                 
      INTEGER NLTX,NLGX,NWDSX,NWDSX2                                                                                                
      PARAMETER(NLTX=181,NLGX=361,NWDSX=NLTX*NLGX,NWDSX2=1*NWDSX)                                                                   
      REAL GG(NWDSX),VG(NWDSX),GL(NWDSX),VL(NWDSX),FG(NWDSX)                                                                        
      LOGICAL OK,LWGHT                                                                                                              
      REAL SL(NLTX),CL(NLTX),WL(NLTX),WOSSL(NLTX),RAD(NLTX)                                                                         
      REAL SLAT (NLTX),SLON (NLGX),DLAT (NLTX),DLON (NLGX),                                                                         
     1     SLATH(NLTX),SLONH(NLGX),DLATH(NLTX),DLONH(NLGX)                                                                          
      INTEGER I,J,ILG,ILG1,ILAT,ILAT1,ILG1OLD,ILATOLD,                                                                              
     1     NLG,NLG1,NLAT,NLAT1,NOCYC,IGLOB,                                                                                         
     2     KIND,NPKGG,ISLBL,INTERP,IFLD,ICYC,NR,NFF,NOUT,NOUTW,LLI,                                                                 
     3     ICYCO,LLO                                                                                                                
      REAL DX,DY,DXH,DYH,SHFTLG,SHFTLT,R2D,D2R,WLT,ASIN,                                                                            
     1     SHFTLGO,SHFTLTO                                                                                                          
C                                                                                                                                   
      INTEGER IBUF,IDAT,MAXX,NC4TO8                                                                                                 
      COMMON/ICOM/IBUF(8),IDAT(NWDSX2)                                                                                              
      INTEGER JBUF(8)                                                                                                               
      DATA MAXX/NWDSX2/                                                                                                             
      REAL PI,SPVAL,SPVALC,SPVALT,SMALL                                                                                             
      DATA PI/3.14159265358979E0/,SPVAL/1.E+38/,SPVALT/1.E+32/,                                                                     
     1     SMALL/1.E-6/                                                                                                             
C----------------------------------------------------------------------                                                             
                                                                                                                                    
      R2D=180.E0/PI                                                                                                                 
      D2R=PI/180.E0                                                                                                                 
                                                                                                                                    
      NFF=7                                                                                                                         
      CALL JCLPNT(NFF,11,12,13,14,15,5,6)                                                                                           
      NFF=NFF-2                                                                                                                     
      REWIND 11                                                                                                                     
      REWIND 12                                                                                                                     
C                                                                                                                                   
C     * READ INPUT CARD                                                                                                             
C                                                                                                                                   
      READ(5,5010,END=900) NLG,NLAT,NOCYC,KIND,NPKGG,SPVALC,                    D4                                                  
     1     SHFTLT,SHFTLG,ISLBL,IFLD                                             D4                                                  
C                                                                                                                                   
C     * CHECK INTERPOLATION TYPE: 1=BILINEAR (DEFAULT), 3=CUBIC, 5=AREA-WEIGHTED                                                    
C                                                                                                                                   
      IF (KIND.EQ.0)KIND=1                                                                                                          
      IF (IABS(KIND).EQ.3.OR.KIND.EQ.4) THEN                                                                                        
        INTERP=3                                                                                                                    
        WRITE(6,'(A)')'CUBIC INTERPOLATION.'                                                                                        
      ELSE IF (IABS(KIND).EQ.5.OR.KIND.EQ.6) THEN                                                                                   
        INTERP=5                                                                                                                    
        WRITE(6,'(A)')'AREA-WEIGHTED INTERPOLATION.'                                                                                
      ELSE IF (IABS(KIND).EQ.1.OR.KIND.EQ.2) THEN                                                                                   
        INTERP=1                                                                                                                    
        WRITE(6,'(A)')'BILINEAR INTERPOLATION.'                                                                                     
      ELSE                                                                                                                          
        WRITE(6,'(A)')'***ERROR: INVALID INTERPOLATION KIND.'                                                                       
        CALL                                       XIT('LLAGG',-1)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * PRINT OUT THE CARD                                                                                                          
C                                                                                                                                   
      WRITE(6,6010) NLG,NLAT,NOCYC,INTERP,NPKGG,SPVALC,                                                                             
     1     SHFTLT,SHFTLG,ISLBL,IFLD                                                                                                 
C                                                                                                                                   
C     * CHECK OUTPUT GRID DIMENSIONS                                                                                                
C                                                                                                                                   
      IF(NLG.GT.NLGX.OR.ABS(NLAT).GT.NLTX)THEN                                                                                      
        WRITE(6,*)'***ERROR: OUTPUT DIMENSIONS ARE TOO LARGE.'                                                                      
        WRITE(6,*)'NLG=',NLG,' NLGX=',NLGX                                                                                          
        WRITE(6,*)'NLAT=',NLAT,' NLTX=',NLTX                                                                                        
        CALL                                       XIT('LLAGG',-2)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CHECK INPUT GRID TYPE                                                                                                       
C                                                                                                                                   
      IF(KIND.EQ.2.OR.KIND.EQ.4.OR.KIND.EQ.6)THEN                                                                                   
C                                                                                                                                   
C       * GAUSSIAN INPUT GRIDS                                                                                                      
C                                                                                                                                   
        WRITE(6,'(A)')'INPUT GRID IS GAUSSIAN.'                                                                                     
        LLI=2                                                                                                                       
        SPVALC=SPVAL                                                                                                                
        ICYC=1                                                                                                                      
        SHFTLG=0.E0                                                                                                                 
      ELSE IF(KIND.EQ.1.OR.KIND.EQ.3.OR.KIND.EQ.5)THEN                                                                              
C                                                                                                                                   
C       * UNSHIFTED LAT/LON INPUT GRIDS                                                                                             
C                                                                                                                                   
        WRITE(6,'(A)')'INPUT GRID IS UNSHIFTED LAT/LON.'                                                                            
        LLI=1                                                                                                                       
        SPVALC=SPVAL                                                                                                                
        ICYC=1                                                                                                                      
        SHFTLT=0.E0                                                                                                                 
        SHFTLG=0.E0                                                                                                                 
      ELSE IF(KIND.EQ.-1.OR.KIND.EQ.-3.OR.KIND.EQ.-5)THEN                                                                           
C                                                                                                                                   
C       * SHIFTED LAT/LON INPUT GRIDS                                                                                               
C                                                                                                                                   
        WRITE(6,'(A)')'INPUT GRID IS SHIFTED LAT/LON.'                                                                              
        LLI=1                                                                                                                       
        IF(SHFTLG.EQ.0.E0) THEN                                                                                                     
          ICYC=1                                                                                                                    
        ELSE                                                                                                                        
          ICYC=0                                                                                                                    
        ENDIF                                                                                                                       
      ELSE                                                                                                                          
        WRITE(6,'(A)')'UNKNOWN INPUT GRID.'                                                                                         
        CALL                                       XIT('LLAGG',-3)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CYCLICITY IN INPUT GRID                                                                                                     
C                                                                                                                                   
      IF(ICYC.EQ.1) THEN                                                                                                            
        WRITE(6,'(A)')                                                                                                              
     1       'INPUT GRID IS ASSUMED TO HAVE A CYCLIC LONGITUDE.'                                                                    
      ELSE                                                                                                                          
        WRITE(6,'(A)')                                                                                                              
     1       'INPUT GRID IS ASSUMED TO HAVE NO CYCLIC LONGITUDE.'                                                                   
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CHECK THE OUTPUT GRID TYPE                                                                                                  
C                                                                                                                                   
      IF(NLG.GE.0.AND.NLAT.GE.0)THEN                                                                                                
        LLO=0                                                                                                                       
        ICYCO=1                                                                                                                     
        SHFTLGO=0.E0                                                                                                                
        WRITE(6,'(A)')'OUTPUT GRID IS GAUSSIAN.'                                                                                    
        IF(MOD(NLAT,2).NE.0)THEN                                                                                                    
          WRITE(6,'(A)')'***ERROR: INVALID GAUSSIAN GRID.'                                                                          
          CALL                                     XIT('LLAGG',-4)                                                                  
        ENDIF                                                                                                                       
      ELSE IF(NLG.LT.0.AND.NLAT.GT.0)THEN                                                                                           
        NLG=-NLG                                                                                                                    
        LLO=1                                                                                                                       
        ICYCO=1                                                                                                                     
        SHFTLTO=0.E0                                                                                                                
        SHFTLGO=0.E0                                                                                                                
        WRITE(6,'(A)')'OUTPUT GRID IS UNSHIFTED LAT/LON.'                                                                           
      ELSE IF(NLAT.LT.0.AND.NLG.GT.0)THEN                                                                                           
        NLAT=-NLAT                                                                                                                  
        LLO=1                                                                                                                       
C                                                                                                                                   
C       * READ INPUT CARD 2 FOR OUTPUT LAT/LON SHIFTS                                                                               
C                                                                                                                                   
        READ(5,5015,END=905) SHFTLTO,SHFTLGO                                    D4                                                  
        IF(SHFTLGO.EQ.0.E0) THEN                                                                                                    
          ICYCO=1                                                                                                                   
        ELSE                                                                                                                        
          ICYCO=0                                                                                                                   
        ENDIF                                                                                                                       
        WRITE(6,'(A)')'OUTPUT GRID IS SHIFTED LAT/LON.'                                                                             
      ELSE IF(NLAT.LT.0.AND.NLG.LT.0)THEN                                                                                           
        NLG=-NLG                                                                                                                    
        NLAT=-NLAT                                                                                                                  
C                                                                                                                                   
C       * READ INPUT CARD 2 FOR OUTPUT LAT/LON SHIFTS                                                                               
C                                                                                                                                   
        READ(5,5015,END=905) SHFTLTO,SHFTLGO,IGLOB                              D4                                                  
C                                                                                                                                   
C       * READ LATITUDES (DEGREES)                                                                                                  
C                                                                                                                                   
        READ(5,5016,END=905) (DLAT(J),J=1,NLAT)                                 D4                                                  
        IF(SHFTLTO.LT.-1.E-6)THEN                                                                                                   
          LLO=2                                                                                                                     
C                                                                                                                                   
C         * IF SHFTLTO<0 -> ASSUME CUSTOM BOTH LONS AND LATS                                                                        
C                                                                                                                                   
          WRITE(6,'(A)')'OUTPUT GRID IS CUSTOM LONS/LATS'                                                                           
C                                                                                                                                   
C         * READ LONGITUDES (DEGREES)                                                                                               
C                                                                                                                                   
          READ(5,5016,END=905) (DLON(J),J=1,NLG)                                D4                                                  
          ICYCO=0                                                                                                                   
        ELSE                                                                                                                        
C                                                                                                                                   
C         * OTHERWISE ASSUME REGULAR LONS BUT CUSTOM LATS                                                                           
C                                                                                                                                   
          WRITE(6,'(A)')'OUTPUT GRID IS CUSTOM LATS/REGULAR LONS'                                                                   
          LLO=3                                                                                                                     
          IF(SHFTLGO.EQ.0.E0) THEN                                                                                                  
            ICYCO=1                                                                                                                 
          ELSE                                                                                                                      
            ICYCO=0                                                                                                                 
          ENDIF                                                                                                                     
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CYCLICITY IN OUTPUT GRID                                                                                                    
C                                                                                                                                   
      IF(NOCYC.EQ.1) THEN                                                                                                           
        ICYCO=0                                                                                                                     
      ENDIF                                                                                                                         
      IF(ICYCO.EQ.1) THEN                                                                                                           
        WRITE(6,'(A)')                                                                                                              
     1       'OUTPUT GRID WILL HAVE A CYCLIC LONGITUDE.'                                                                            
      ELSE                                                                                                                          
        WRITE(6,'(A)')                                                                                                              
     1       'OUTPUT GRID WILL NOT HAVE A CYCLIC LONGITUDE.'                                                                        
      ENDIF                                                                                                                         
      NLG1=NLG+ICYCO                                                                                                                
C                                                                                                                                   
C     * SUPERLABEL TREATMENTS                                                                                                       
C                                                                                                                                   
      IF(ISLBL.EQ.1)THEN                                                                                                            
        WRITE(6,'(A)')                                                                                                              
     1       'SUPERLABELS FROM INPUT FILE WILL BE WRITTEN OUT.'                                                                     
      ELSE                                                                                                                          
        WRITE(6,'(A)')                                                                                                              
     1       'SUPERLABELS FROM INPUT FILE WON''T BE WRITTEN OUT.'                                                                   
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * INPUT FIELD TYPE PARAMETER                                                                                                  
C                                                                                                                                   
      IF(IFLD.EQ.2) THEN                                                                                                            
        WRITE(6,'(A)')'INTERPOLATE A VECTOR FIELD (U,V).'                                                                           
      ELSE IF(IFLD.EQ.1) THEN                                                                                                       
        WRITE(6,'(A)')'INTERPOLATE A VECTOR COMPONENT U OR V.'                                                                      
      ELSE IF(IFLD.EQ.0) THEN                                                                                                       
        WRITE(6,'(A)')'INTERPOLATE A SCALAR FIELD.'                                                                                 
      ELSE                                                                                                                          
        WRITE(6,'(A)')'***ERROR: INVALID INPUT FIELD TYPE IFLD.'                                                                    
        CALL                                       XIT('LLAGG',-5)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * PRINT OUT OFFSETS                                                                                                           
C                                                                                                                                   
      IF(LLO.NE.0)WRITE(6,6011) SHFTLTO,SHFTLGO                                                                                     
C                                                                                                                                   
C     * CHECK IF IFLD IS INPUT FIELD PARAMETER IS CONSISTENT WITH                                                                   
C     * THE NUMBER OF ARGUMENTS                                                                                                     
C                                                                                                                                   
      LWGHT=.FALSE.                                                                                                                 
      IF(IFLD.EQ.2)THEN                                                                                                             
        NOUT=13                                                                                                                     
        IF(NFF.EQ.4)THEN                                                                                                            
          REWIND 13                                                                                                                 
          REWIND 14                                                                                                                 
        ELSE IF(NFF.EQ.5)THEN                                                                                                       
          WRITE(6,'(A)')                                                                                                            
     1         'SAVE FILE WITH AREA FRACTION OF NON-MISSING VALUES.'                                                                
          LWGHT=.TRUE.                                                                                                              
          NOUTW=15                                                                                                                  
          REWIND 15                                                                                                                 
        ELSE                                                                                                                        
          WRITE(6,'(A)')                                                                                                            
     1         '***ERROR: WRONG NUMBER OF ARGUMENTS FOR IFLD=2.'                                                                    
          CALL                                     XIT('LLAGG',-6)                                                                  
        ENDIF                                                                                                                       
      ELSE                                                                                                                          
        NOUT=12                                                                                                                     
        IF(NFF.EQ.2)THEN                                                                                                            
          REWIND 12                                                                                                                 
        ELSE IF(NFF.EQ.3)THEN                                                                                                       
          WRITE(6,'(A)')                                                                                                            
     1         'SAVE FILE WITH AREA FRACTION OF NON-MISSING VALUES.'                                                                
          LWGHT=.TRUE.                                                                                                              
          NOUTW=13                                                                                                                  
          REWIND 13                                                                                                                 
        ELSE                                                                                                                        
          WRITE(6,'(A)')                                                                                                            
     1         '***ERROR: WRONG NUMBER OF ARGUMENTS FOR IFLD=1.'                                                                    
          CALL                                     XIT('LLAGG',-7)                                                                  
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
      IF(LWGHT.AND.INTERP.NE.5)THEN                                                                                                 
        WRITE(6,'(2A)')'***ERROR: OUTPUT FILE WITH AREA FRACTIONS',                                                                 
     1       ' CAN BE USED ONLY WITH AREA-WEIGHTED INTERPOLATION.'                                                                  
        CALL                                       XIT('LLAGG',-8)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * COMPUTE OUTPUT LATITUDES IN DEGREES FROM THE EQUATOR                                                                        
C                                                                                                                                   
      IF(LLO.EQ.0)THEN                                                                                                              
        CALL GAUSSG(NLAT/2,SL,WL,CL,RAD,WOSSL)                                                                                      
        CALL TRIGL (NLAT/2,SL,WL,CL,RAD,WOSSL)                                                                                      
        DO J=1,NLAT                                                                                                                 
          DLAT(J)=RAD(J)*R2D                                                                                                        
        ENDDO                                                                                                                       
        DY=DLAT(NLAT/2+1)-DLAT(NLAT/2)                                                                                              
      ELSE IF(LLO.EQ.1)THEN                                                                                                         
        DY=(180.E0-2.E0*SHFTLTO)/FLOAT(NLAT-1)                                                                                      
        DO J=1,(NLAT+1)/2                                                                                                           
          DLAT(J)=-90.E0+FLOAT(J-1)*DY+SHFTLTO                                                                                      
          DLAT(NLAT-J+1)=-DLAT(J)                                                                                                   
        ENDDO                                                                                                                       
      ELSE IF(LLO.EQ.2.OR.LLO.EQ.3)THEN                                                                                             
C                                                                                                                                   
C       * CUSTOM LATS: COMPUTE DY FROM THE MIDDLE OF THE GRID                                                                       
C                                                                                                                                   
        DY=DLAT(NLAT/2+1)-DLAT(NLAT/2)                                                                                              
      ENDIF                                                                                                                         
      WRITE(6,'(A,F10.5)')'DY=',DY                                                                                                  
      WRITE(6,'(A/1000(10F10.5/))')'OUTPUT LAT=',                                                                                   
     1     (DLAT(J),J=1,NLAT)                                                                                                       
C                                                                                                                                   
C     * COMPUTE OUTPUT LONGITUDES IN DEGREES FROM THE GREENWICH.                                                                    
C                                                                                                                                   
      IF(LLO.EQ.2)THEN                                                                                                              
C                                                                                                                                   
C       * CUSTOM LONS: COMPUTE DX FROM THE MIDDLE OF THE GRID                                                                       
C                                                                                                                                   
        DX=DLON(NLG/2+1)-DLON(NLG/2)                                                                                                
      ELSE                                                                                                                          
C                                                                                                                                   
C       * REGULAR LONS                                                                                                              
C                                                                                                                                   
        DX=360.E0/FLOAT(NLG)                                                                                                        
        DO I=1,NLG1                                                                                                                 
          DLON(I)=FLOAT(I-1)*DX+SHFTLGO                                                                                             
        ENDDO                                                                                                                       
      ENDIF                                                                                                                         
      WRITE(6,'(A,F10.5)')'DX=',DX                                                                                                  
      WRITE(6,'(A/1000(10F10.5/))')'OUTPUT LON=',                                                                                   
     1     (DLON(I),I=1,NLG1)                                                                                                       
      IF(INTERP.EQ.5)THEN                                                                                                           
        IF(LLO.EQ.0)THEN                                                                                                            
C                                                                                                                                   
C         * HALF LATITUDES OF OUTPUT GAUSSIAN GRID                                                                                  
C                                                                                                                                   
          NLAT1=NLAT+1                                                                                                              
          DLATH(1)=-90.E0                                                                                                           
          DLATH(NLAT/2+1)=0.E0                                                                                                      
          WLT=-1.E0                                                                                                                 
          DO J=2,NLAT/2                                                                                                             
            WLT=WLT+WL(J-1)                                                                                                         
            DLATH(J)=ASIN(WLT)*R2D                                                                                                  
            DLATH(NLAT1-J+1)=-DLATH(J)                                                                                              
          ENDDO                                                                                                                     
          DLATH(NLAT+1)=90.E0                                                                                                       
        ELSE IF(LLO.EQ.1)THEN                                                                                                       
C                                                                                                                                   
C         * HALF LATITUDES OF OUTPUT LAT/LON GRID                                                                                   
C                                                                                                                                   
          DYH=DY/2.E0                                                                                                               
          DO J=1,NLAT                                                                                                               
            DLATH(J)=MAX(-90.E0,DLAT(J)-DYH)                                                                                        
          ENDDO                                                                                                                     
          DLATH(NLAT+1)=MIN(90.E0,DLAT(NLAT)+DYH)                                                                                   
        ELSE IF(LLO.EQ.2.OR.LLO.EQ.3)THEN                                                                                           
C                                                                                                                                   
C         * HALF LATITUDES OF NON-UNIFORM LAT/LON GRID                                                                              
C                                                                                                                                   
          DO J=2,NLAT                                                                                                               
            DLATH(J)=(DLAT(J)+DLAT(J-1))/2.E0                                                                                       
          ENDDO                                                                                                                     
          IF(IGLOB.EQ.0)THEN                                                                                                        
            DLATH(1)=-90.E0                                                                                                         
            DLATH(NLAT+1)=90.E0                                                                                                     
          ELSE                                                                                                                      
            DYH=DY/2.E0                                                                                                             
            DLATH(1)=MAX(-90.E0,DLAT(1)-DYH)                                                                                        
            DLATH(NLAT+1)=MIN(90.E0,DLAT(NLAT)+DYH)                                                                                 
          ENDIF                                                                                                                     
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * HALF LONGITUDES OF OUTPUT GRID                                                                                            
C                                                                                                                                   
        DXH=DX/2.E0                                                                                                                 
        DO I=1,NLG1                                                                                                                 
          DLONH(I)=DLON(I)-DXH                                                                                                      
        ENDDO                                                                                                                       
        DLONH(NLG1+1)=DLON(NLG1)+DXH                                                                                                
                                                                                                                                    
        WRITE(6,'(A/1000(10F10.5/))')'OUTPUT LAT BOUNDS=',                                                                          
     1       (DLATH(J),J=1,NLAT+1)                                                                                                  
        WRITE(6,'(A/1000(10F10.5/))')'OUTPUT LON BOUNDS=',                                                                          
     1       (DLONH(I),I=1,NLG1+1)                                                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * READ NEXT GRID/ZONL                                                                                                         
C                                                                                                                                   
      ILG1OLD=-1                                                                                                                    
      ILATOLD=-1                                                                                                                    
      NR=0                                                                                                                          
 100  CALL GETFLD2(11,GL,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                  
      IF(.NOT.OK)THEN                                                                                                               
        IF(NR.EQ.0) CALL                           XIT('LLAGG',-9)                                                                  
        CALL PRTLAB(IBUF)                                                                                                           
        WRITE(6,6020) NR                                                                                                            
        CALL                                       XIT('LLAGG',0)                                                                   
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * (OPTIONAL) READ V-COMPONENT                                                                                                 
C                                                                                                                                   
      IF(IFLD.EQ.2)THEN                                                                                                             
        DO I=1,8                                                                                                                    
          JBUF(I)=IBUF(I)                                                                                                           
        ENDDO                                                                                                                       
        CALL GETFLD2(12,VL,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                
        IF(.NOT.OK) CALL                           XIT('LLAGG',-10)                                                                 
C                                                                                                                                   
C       * MAKE SURE THAT ALL RECORDS HAVE THE SAME KIND AND DIMENSIONS                                                              
C                                                                                                                                   
        CALL CMPLBL(0,IBUF,0,JBUF,OK)                                                                                               
        IF (.NOT.OK) THEN                                                                                                           
          CALL PRTLAB (IBUF)                                                                                                        
          CALL PRTLAB (JBUF)                                                                                                        
          CALL                                     XIT('LLAGG',-11)                                                                 
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CHECK INPUT GRID DIMENSIONS                                                                                                 
C                                                                                                                                   
      IF(IBUF(5).GT.NLGX.OR.IBUF(6).GT.NLTX)THEN                                                                                    
        WRITE(6,*)'***ERROR: INPUT DIMENSIONS ARE TOO LARGE.'                                                                       
        WRITE(6,*)'IBUF(5)=',IBUF(5),' NLGX=',NLGX                                                                                  
        WRITE(6,*)'IBUF(6)=',IBUF(6),' NLTX=',NLTX                                                                                  
        CALL                                       XIT('LLAGG',-12)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * GET INPUT GRID DIMENSIONS                                                                                                   
C                                                                                                                                   
      IF(IBUF(1).EQ.NC4TO8("GRID"))THEN                                                                                             
        ILG1=IBUF(5)                                                                                                                
        ILG=ILG1-ICYC                                                                                                               
        ILAT=IBUF(6)                                                                                                                
      ELSE IF(IBUF(1).EQ.NC4TO8("ZONL"))THEN                                                                                        
C                                                                                                                                   
C       * MAKE GRID FROM ZONL                                                                                                       
C                                                                                                                                   
        ILG=1                                                                                                                       
        ILG1=2                                                                                                                      
        ILAT=IBUF(5)                                                                                                                
        DO J=ILAT,1,-1                                                                                                              
          GL(J*2  )=GL(J)                                                                                                           
          GL(J*2-1)=GL(J)                                                                                                           
        ENDDO                                                                                                                       
        IF(IFLD.EQ.2) THEN                                                                                                          
          DO J=ILAT,1,-1                                                                                                            
            VL(J*2  )=VL(J)                                                                                                         
            VL(J*2-1)=VL(J)                                                                                                         
          ENDDO                                                                                                                     
        ENDIF                                                                                                                       
        NLG=1                                                                                                                       
        NLG1=1                                                                                                                      
      ELSE                                                                                                                          
        IF(IBUF(1).EQ.NC4TO8("LABL").AND.ISLBL.EQ.1)THEN                                                                            
C                                                                                                                                   
C         * SAVE SUPERLABELS, IF REQUESTED                                                                                          
C                                                                                                                                   
          CALL PUTFLD2(NOUT,GL,IBUF,MAXX)                                                                                           
          IF(LWGHT)THEN                                                                                                             
            CALL PUTFLD2(NOUTW,GL,IBUF,MAXX)                                                                                        
          ENDIF                                                                                                                     
          IF(IFLD.EQ.2)THEN                                                                                                         
            CALL PUTFLD2(NOUT+1,VL,IBUF,MAXX)                                                                                       
          ENDIF                                                                                                                     
        ENDIF                                                                                                                       
C                                                                                                                                   
C       * SKIP ALL OTHER RECORD KINDS                                                                                               
C                                                                                                                                   
        GOTO 100                                                                                                                    
      ENDIF                                                                                                                         
      IF(NR.EQ.0) THEN                                                                                                              
        IF(IFLD.EQ.2)CALL PRTLAB(JBUF)                                                                                              
        CALL PRTLAB(IBUF)                                                                                                           
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * COMPUTE LATITUDES OF INPUT GRID IN DEGREES FROM THE EQUATOR                                                                 
C                                                                                                                                   
      IF (ILAT.NE.ILATOLD) THEN                                                                                                     
C                                                                                                                                   
C       * ABORT IF ILAT DIMENSION HAS CHANGED FOR LAT-SHIFTED GRIDS                                                                 
C                                                                                                                                   
        IF(SHFTLT.NE.0.E0.AND.ILATOLD.NE.-1)CALL   XIT('LLAGG',-13)                                                                 
        IF(LLI.EQ.1)THEN                                                                                                            
C                                                                                                                                   
C         * REGULAR INPUT LATITUDES                                                                                                 
C                                                                                                                                   
          DY=(180.E0-2.E0*SHFTLT)/FLOAT(ILAT-1)                                                                                     
          DO J=1,(ILAT+1)/2                                                                                                         
            SLAT(J)=-90.E0+FLOAT(J-1)*DY+SHFTLT                                                                                     
            SLAT(ILAT-J+1)=-SLAT(J)                                                                                                 
          ENDDO                                                                                                                     
C                                                                                                                                   
C         * HALF LATITUDES OF REGULAR INPUT GRID                                                                                    
C                                                                                                                                   
          IF(INTERP.EQ.5)THEN                                                                                                       
            DYH=DY/2.E0                                                                                                             
            DO J=1,ILAT                                                                                                             
              SLATH(J)=MAX(-90.E0,SLAT(J)-DYH)                                                                                      
            ENDDO                                                                                                                   
            SLATH(ILAT+1)=MIN(90.E0,SLAT(ILAT)+DYH)                                                                                 
          ENDIF                                                                                                                     
        ELSE IF(LLI.EQ.2)THEN                                                                                                       
C                                                                                                                                   
C         * GAUSSIAN LATITUDES                                                                                                      
C                                                                                                                                   
          CALL GAUSSG(ILAT/2,SL,WL,CL,RAD,WOSSL)                                                                                    
          CALL TRIGL (ILAT/2,SL,WL,CL,RAD,WOSSL)                                                                                    
          DO J=1,ILAT                                                                                                               
            SLAT(J)=RAD(J)*R2D                                                                                                      
          ENDDO                                                                                                                     
C                                                                                                                                   
C         * HALF LATITUDES OF GAUSSIAN INPUT GRID                                                                                   
C                                                                                                                                   
          IF(INTERP.EQ.5)THEN                                                                                                       
            ILAT1=ILAT+1                                                                                                            
            SLATH(1)=-90.E0                                                                                                         
            SLATH(ILAT/2+1)=0.E0                                                                                                    
            WLT=-1.E0                                                                                                               
            DO J=2,ILAT/2                                                                                                           
              WLT=WLT+WL(J-1)                                                                                                       
              SLATH(J)=ASIN(WLT)*R2D                                                                                                
              SLATH(ILAT1-J+1)=-SLATH(J)                                                                                            
            ENDDO                                                                                                                   
            SLATH(ILAT+1)=90.E0                                                                                                     
          ENDIF                                                                                                                     
        ENDIF                                                                                                                       
        ILATOLD=ILAT                                                                                                                
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * COMPUTE INPUT LONGITUDES IN DEGREES FROM THE GREENWICH.                                                                     
C                                                                                                                                   
      IF (ILG1.NE.ILG1OLD) THEN                                                                                                     
C                                                                                                                                   
C       * ABORT IF ILG DIMENSION HAS CHANGED FOR LONG-SHIFTED GRIDS                                                                 
C                                                                                                                                   
        IF(SHFTLG.NE.0.E0.AND.ILG1OLD.NE.-1)CALL   XIT('LLAGG',-14)                                                                 
        DX=360.E0/FLOAT(ILG)                                                                                                        
        DO I=1,ILG1                                                                                                                 
          SLON(I)=FLOAT(I-1)*DX+SHFTLG                                                                                              
        ENDDO                                                                                                                       
C                                                                                                                                   
C       * HALF LONGITUDES OF INPUT GRID                                                                                             
C                                                                                                                                   
        IF(INTERP.EQ.5)THEN                                                                                                         
          DXH=DX/2.E0                                                                                                               
          DO I=1,ILG1                                                                                                               
            SLONH(I)=SLON(I)-DXH                                                                                                    
          ENDDO                                                                                                                     
          SLONH(ILG1+1)=SLON(ILG1)+DXH                                                                                              
        ENDIF                                                                                                                       
        ILG1OLD=ILG1                                                                                                                
        WRITE(6,'(A/1000(10F10.5/))')'INPUT LAT=',                                                                                  
     1       (SLAT(J),J=1,ILAT)                                                                                                     
        WRITE(6,'(A/1000(10F10.5/))')'INPUT LON=',                                                                                  
     1       (SLON(I),I=1,ILG1)                                                                                                     
        IF(INTERP.EQ.5)THEN                                                                                                         
          WRITE(6,'(A/1000(10F10.5/))')'INPUT LAT BOUNDS=',                                                                         
     1         (SLATH(J),J=1,ILAT+1)                                                                                                
          WRITE(6,'(A/1000(10F10.5/))')'INPUT LON BOUNDS=',                                                                         
     1         (SLONH(I),I=1,ILG1+1)                                                                                                
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * RESET INPUT CARD SPECIAL VALUE TO STANDARD SPVAL=1.E+38                                                                     
C                                                                                                                                   
      IF(KIND.LT.0)THEN                                                                                                             
        DO I=1,ILG1*ILAT                                                                                                            
          IF(ABS(GL(I)-SPVALC).LT.SMALL*MAX(1.,ABS(SPVALC)))GL(I)=SPVAL                                                             
        ENDDO                                                                                                                       
        IF(IFLD.EQ.2)THEN                                                                                                           
          DO I=1,ILG1*ILAT                                                                                                          
           IF(ABS(VL(I)-SPVALC).LT.SMALL*MAX(1.,ABS(SPVALC)))VL(I)=SPVAL                                                            
          ENDDO                                                                                                                     
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CONVERT FROM INPUT GLOBAL GRID TO OUTPUT GLOBAL GRID                                                                        
C                                                                                                                                   
      IF(INTERP.EQ.5) THEN                                                                                                          
        CALL GGIGGW(GL,VL,ILG1,ILG,ILAT,SLON,SLAT,SLONH,SLATH,                                                                      
     1              GG,VG,NLG1,NLG,NLAT,DLON,DLAT,DLONH,DLATH,                                                                      
     2              FG,INTERP,IFLD)                                                                                                 
      ELSE                                                                                                                          
        CALL GGIGG (GL,VL,ILG1,ILG,ILAT,SLON,SLAT,                                                                                  
     1              GG,VG,NLG1,NLG,NLAT,DLON,DLAT,                                                                                  
     2              INTERP,IFLD)                                                                                                    
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * RESET DEFAULT SPVAL=1.E+38 BACK TO SPECIAL VALUE IN INPUT CARD                                                              
C                                                                                                                                   
      IF(KIND.LT.0)THEN                                                                                                             
        DO I=1,NLG1*NLAT                                                                                                            
          IF(ABS(GG(I)-SPVAL).LT.SPVALT)GG(I)=SPVALC                                                                                
        ENDDO                                                                                                                       
        IF(IFLD.EQ.2)THEN                                                                                                           
          DO I=1,NLG1*NLAT                                                                                                          
            IF(ABS(VG(I)-SPVAL).LT.SPVALT)VG(I)=SPVALC                                                                              
          ENDDO                                                                                                                     
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * SAVE OUTPUT GRID/ZONL                                                                                                       
C                                                                                                                                   
      IF(IBUF(1).EQ.NC4TO8("GRID"))THEN                                                                                             
        IBUF(5)=NLG1                                                                                                                
        IBUF(6)=NLAT                                                                                                                
      ELSE                                                                                                                          
        IBUF(5)=NLAT                                                                                                                
        IBUF(6)=1                                                                                                                   
      ENDIF                                                                                                                         
      IF(NPKGG.GT.0)THEN                                                                                                            
        IBUF(8)=NPKGG                                                                                                               
      ELSE                                                                                                                          
        IF(IBUF(8).EQ.0)IBUF(8)=1                                                                                                   
      ENDIF                                                                                                                         
      IF(IFLD.EQ.2)THEN                                                                                                             
        CALL PUTFLD2(NOUT+1,VG,IBUF,MAXX)                                                                                           
        IBUF(3)=JBUF(3)                                                                                                             
      ENDIF                                                                                                                         
      CALL PUTFLD2(NOUT,GG,IBUF,MAXX)                                                                                               
C                                                                                                                                   
C     * SAVE AREA FRACTION                                                                                                          
C                                                                                                                                   
      IF(LWGHT)THEN                                                                                                                 
C        IBUF(3)=NC4TO8("WGHT")                                                                                                     
        CALL PUTFLD2(NOUTW,FG,IBUF,MAXX)                                                                                            
      ENDIF                                                                                                                         
      NR=NR+1                                                                                                                       
      GO TO 100                                                                                                                     
C                                                                                                                                   
C     * E.O.F. ON INPUT.                                                                                                            
C                                                                                                                                   
  900 CALL                                         XIT('LLAGG',-15)                                                                 
  905 CALL                                         XIT('LLAGG',-16)                                                                 
C-----------------------------------------------------------------------                                                            
 5010 FORMAT(10X,2I5,I2,I3,I5,3E10.0,2I5)                                       D4                                                  
 5015 FORMAT(10X,2E10.0,I5)                                                     D4                                                  
 5016 FORMAT(100(10X,7E10.0/))                                                  D4                                                  
 6010 FORMAT('GRID SIZE',2I6,' NOCYC=',I2,' INTERP=',I2,                                                                            
     1     ' NPKGG=',I2,' SPVAL=',1PE12.5,                                                                                          
     2     ' INPUT LAT/LON SHIFTS=',0P2F10.7,                                                                                       
     3     ' ISLBL=',I2,' IFLD=',I2)                                                                                                
 6011 FORMAT('OUTPUT LAT/LON SHIFTS=',0P2F10.7)                                                                                     
 6020 FORMAT(I6,' RECORDS PROCESSED.')                                                                                              
      END                                                                                                                           
