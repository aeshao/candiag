      PROGRAM GGDEV                                                                                                                 
C     PROGRAM GGDEV (GGIN,       GGOUT,       OUTPUT,                   )       D2                                                  
C    1         TAPE1=GGIN, TAPE2=GGOUT, TAPE6=OUTPUT)                                                                               
C     -----------------------------------------------                           D2                                                  
C                                                                               D2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2                                                  
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)                                                                 
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 20/83 - R.LAPRISE.                                                                                                        
C     NOV 13/80 - J.D.HENDERSON                                                                                                     
C                                                                               D2                                                  
CGGDEV   - GRID SET DEVIATION FROM GLOBAL MEAN                          1  1   GD1                                                  
C                                                                               D3                                                  
CAUTHOR  - J.D.HENDERSON                                                        D3                                                  
C                                                                               D3                                                  
CPURPOSE - COMPUTES DEVIATIONS FROM THE GLOBAL MEAN OF GLOBAL GAUSSIAN          D3                                                  
C          GRIDS IN FILE GGIN AND STORES THE RESULT ON FILE GGOUT.              D3                                                  
C                                                                               D3                                                  
CINPUT FILE...                                                                  D3                                                  
C                                                                               D3                                                  
C      GGIN  = GLOBAL GAUSSIAN GRIDS                                            D3                                                  
C                                                                               D3                                                  
COUTPUT FILE...                                                                 D3                                                  
C                                                                               D3                                                  
C      GGOUT = DEVIATIONS FROM THE GLOBAL MEANS.                                D3                                                  
C-------------------------------------------------------------------------                                                          
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/BLANCK/F(18528)                                                                                                        
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      REAL ZM(96)                                                                                                                   
      REAL*8 SL(96),CL(96),WL(96),WOSSL(96),RAD(96)                                                                                 
      COMMON/ICOM/ IBUF(8),IDAT(18528)                                                                                              
      DATA MAXX/18528/                                                                                                              
C---------------------------------------------------------------------                                                              
      NFF=3                                                                                                                         
      CALL JCLPNT(NFF,1,2,6)                                                                                                        
      PIH=3.14159E0/2.E0                                                                                                            
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
C                                                                                                                                   
C     * READ THE NEXT GRID FROM FILE GGIN.                                                                                          
C                                                                                                                                   
      NR=0                                                                                                                          
  140 CALL GETFLD2(1,F,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)                                                                        
      IF(.NOT.OK)THEN                                                                                                               
        IF(NR.EQ.0)THEN                                                                                                             
          CALL                                     XIT('GGDEV ',-1)                                                                 
        ELSE                                                                                                                        
          WRITE(6,6010) NR                                                                                                          
          CALL                                     XIT('GGDEV',0)                                                                   
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --                                                              
C     * FIRST RECORD ONLY...                                                                                                        
C     * GAUSSG COMPUTES GAUSSIAN LATITUDES, COSINES, ETC.(HEMISPHERIC).                                                             
C     * TRIGL CONVERTS THESE VECTORS TO GLOBAL (S TO N).                                                                            
C                                                                                                                                   
      IF(NR.GT.0) GO TO 190                                                                                                         
      NLON=IBUF(5)                                                                                                                  
      NLAT=IBUF(6)                                                                                                                  
      NLONM=NLON-1                                                                                                                  
      NWDS=NLAT*NLON                                                                                                                
      ILATH=NLAT/2                                                                                                                  
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --                                                              
C     * COMPUTE ZONAL MEAN TIMES COS(LAT) IN ZM.                                                                                    
C                                                                                                                                   
  190 IF(IBUF(5).NE.NLON) CALL                     XIT('GGDEV',-2)                                                                  
      IF(IBUF(6).NE.NLAT) CALL                     XIT('GGDEV',-3)                                                                  
      DO 240 J=1,NLAT                                                                                                               
      JR=(J-1)*NLON                                                                                                                 
      AVG=0.E0                                                                                                                      
      DO 230 I=1,NLONM                                                                                                              
  230 AVG=AVG+F(I+JR)                                                                                                               
      ZM(J)=AVG/FLOAT(NLONM)*CL(J)                                                                                                  
  240 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * COMPUTE GLOBAL MEAN.                                                                                                        
C                                                                                                                                   
      GM=ZM(1)*(RAD(1)-(-PIH))                                                                                                      
      DO 250 J=2,NLAT                                                                                                               
  250 GM=GM+0.5E0*(ZM(J)+ZM(J-1))*(RAD(J)-RAD(J-1))                                                                                 
      GM=GM+ZM(NLAT)*(PIH-RAD(NLAT))                                                                                                
      GM=0.5E0*GM                                                                                                                   
      WRITE(6,6030) GM                                                                                                              
C                                                                                                                                   
C     * SUBTRACT GLOBAL MEAN FROM THE GRID.                                                                                         
C                                                                                                                                   
      DO 270 I=1,NWDS                                                                                                               
  270 F(I)=F(I)-GM                                                                                                                  
C                                                                                                                                   
C     * SAVE ON FILE GGOUT.                                                                                                         
C                                                                                                                                   
      CALL PUTFLD2(2,F,IBUF,MAXX)                                                                                                   
      WRITE(6,6025) IBUF                                                                                                            
      NR=NR+1                                                                                                                       
      GO TO 140                                                                                                                     
C---------------------------------------------------------------------                                                              
 6010 FORMAT(' ',I6,' RECORDS READ')                                                                                                
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)                                                                                              
 6030 FORMAT('+',60X,' GGDEV GLOBAL MEAN = ',E15.7)                                                                                 
      END                                                                                                                           
