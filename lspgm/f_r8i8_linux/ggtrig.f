      PROGRAM GGTRIG                                                                                                                
C     PROGRAM GGTRIG (GGIN,       GGOUT,       INPUT,       OUTPUT,     )       D2                                                  
C    1          TAPE1=GGIN, TAPE2=GGOUT, TAPE5=INPUT, TAPE6=OUTPUT)                                                                 
C     -------------------------------------------------------------             D2                                                  
C                                                                               D2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       D2                                                  
C     JUL 13/92 - E. CHAN  (DIMENSION SELECTED VARIABLES AS REAL*8)                                                                 
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 11/83 - R.LAPRISE.                                                                                                        
C     MAY 25/81 - J.D.HENDERSON                                                                                                     
C                                                                               D2                                                  
CGGTRIG  - GRID SET MULTIPLIED BY  CONST*TRIG(LAT)**N                   1  1 C GD1                                                  
C                                                                               D3                                                  
CAUTHOR  - J.D.HENDERSON                                                        D3                                                  
C                                                                               D3                                                  
CPURPOSE - MULTIPLIES GLOBAL GAUSSIAN GRID FILE GGIN BY CONST*TRIG(LAT)**N,     D3                                                  
C          WHERE TRIG CAN BE SIN,COS OR TAN.                                    D3                                                  
C          THE RESULT IS STORED ON FILE GGOUT.                                  D3                                                  
C                                                                               D3                                                  
CINPUT FILE...                                                                  D3                                                  
C                                                                               D3                                                  
C      GGIN  = GLOBAL GAUSSIAN GRIDS                                            D3                                                  
C                                                                               D3                                                  
COUTPUT FILE...                                                                 D3                                                  
C                                                                               D3                                                  
C      GGOUT = GGIN MULTIPLIED AS REQUESTED.                                    D3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               D5                                                  
C      CONST = A MULTIPLIER VALUE                                               D5                                                  
C      TRIG  = CAN BE SIN, COS OR TAN                                           D5                                                  
C      N     = IS THE EXPONENT OF TRIG                                          D5                                                  
C                                                                               D5                                                  
CEXAMPLE OF INPUT CARD...                                                       D5                                                  
C                                                                               D5                                                  
C*  GGTRIG      1.E0  COS    2                                                  D5                                                  
C----------------------------------------------------------------------------                                                       
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/BLANCK/F(18528)                                                                                                        
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      REAL TRIG(194)                                                                                                                
      REAL*8 SL(96),CL(96),WL(96),WOSSL(96),RAD(96)                                                                                 
      CHARACTER*3 KTR                                                                                                               
      COMMON/ICOM/ IBUF(8),IDAT(18528)                                                                                              
      DATA MAXX/18528/                                                                                                              
C---------------------------------------------------------------------                                                              
      NFF=4                                                                                                                         
      CALL JCLPNT(NFF,1,2,5,6)                                                                                                      
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
C                                                                                                                                   
C     * READ CONST, KTR, N FROM A CARD.                                                                                             
C     * KTR IS THE KIND OF TRIG FUNCTION ('SIN','COS','TAN').                                                                       
C                                                                                                                                   
      READ(5,5010,END=906) CONST,KTR,N                                          D4                                                  
      WRITE(6,6005)CONST,KTR,N                                                                                                      
      IF((KTR.EQ.'SIN').OR.(KTR.EQ.'COS').OR.(KTR.EQ.'TAN'))GO TO 120                                                               
      WRITE(6,6901) KTR                                                                                                             
      CALL                                         XIT('GGTRIG',-1)                                                                 
  120 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * READ THE NEXT GRID FROM FILE GGIN.                                                                                          
C                                                                                                                                   
      NR=0                                                                                                                          
  140 CALL GETFLD2(1,F,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)                                                                        
      IF(.NOT.OK)THEN                                                                                                               
        IF(NR.EQ.0)THEN                                                                                                             
          WRITE(6,6902) KTR                                                                                                         
          CALL                                     XIT('GGTRIG',-2)                                                                 
        ELSE                                                                                                                        
          WRITE(6,6010) NR                                                                                                          
          CALL                                     XIT('GGTRIG',0)                                                                  
        ENDIF                                                                                                                       
      ENDIF                                                                                                                         
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --                                                              
C     * FIRST RECORD ONLY...                                                                                                        
C     * GAUSSG COMPUTES GAUSSIAN LATITUDES, COSINES, ETC.(HEMISPHERIC).                                                             
C     * TRIGL CONVERTS THESE VECTORS TO GLOBAL (S TO N).                                                                            
C                                                                                                                                   
      IF(NR.GT.0) GO TO 190                                                                                                         
      NLON=IBUF(5)                                                                                                                  
      NLAT=IBUF(6)                                                                                                                  
      ILATH=NLAT/2                                                                                                                  
      CALL GAUSSG(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
      CALL  TRIGL(ILATH,SL,WL,CL,RAD,WOSSL)                                                                                         
C                                                                                                                                   
C     * SET TRIG TO THE REQUESTED TRIG FUNCTION.                                                                                    
C                                                                                                                                   
      DO 320 J=1,NLAT                                                                                                               
      IF(KTR.EQ.'SIN') TRIG(J)= SL(J)**N                                                                                            
      IF(KTR.EQ.'COS') TRIG(J)= CL(J)**N                                                                                            
      IF(KTR.EQ.'TAN') TRIG(J)=( SL(J)/ CL(J))**N                                                                                   
  320 CONTINUE                                                                                                                      
C - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --                                                              
C     * MULTIPLY THE GRID BY CONST*TRIG(LAT)**N                                                                                     
C                                                                                                                                   
  190 IF(IBUF(5).NE.NLON) CALL                     XIT('GGTRIG',-3)                                                                 
      IF(IBUF(6).NE.NLAT) CALL                     XIT('GGTRIG',-4)                                                                 
      DO 420 J=1,NLAT                                                                                                               
      JR=(J-1)*NLON                                                                                                                 
      DO 420 I=1,NLON                                                                                                               
      K=JR+I                                                                                                                        
  420 F(K)=CONST*TRIG(J)*F(K)                                                                                                       
C                                                                                                                                   
C     * SAVE ON FILE GGOUT.                                                                                                         
C                                                                                                                                   
      CALL PUTFLD2(2,F,IBUF,MAXX)                                                                                                   
      IF(NR.EQ.0) WRITE(6,6025) IBUF                                                                                                
      NR=NR+1                                                                                                                       
      GO TO 140                                                                                                                     
C                                                                                                                                   
C     * E.O.F. ON INPUT.                                                                                                            
C                                                                                                                                   
  906 CALL                                         XIT('GGTRIG',-5)                                                                 
C---------------------------------------------------------------------                                                              
 5010 FORMAT(10X,E10.0,2X,A3,I5)                                                D4                                                  
 6005 FORMAT(' GGTRIG CONST,KTR,N =',1PE12.4,3X,A4,I5)                                                                              
 6010 FORMAT(' ',I6,' RECORDS READ')                                                                                                
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)                                                                                              
 6090 FORMAT(' ',I5,' RECORDS READ BY GGTRIG')                                                                                      
 6901 FORMAT('0KTR MUST BE SIN,COS OR TAN.  KTR=',A3)                                                                               
 6902 FORMAT('0 FILE EMPTY')                                                                                                        
      END                                                                                                                           
