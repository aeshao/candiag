      PROGRAM GETSTEP                                                                                                               
C     PROGRAM GETSTEP (RESTART,      TIMSTEP,      OUTPUT,              )       J2                                                  
C    1           TAPE1=RESTART,TAPE2=TIMSTEP,TAPE6=OUTPUT)                                                                          
C     ---------------------------------------------------------------           J2                                                  
C                                                                               J2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       J2                                                  
C     APR 26/94 - E. CHAN (ADJUST FBUFFIN CALL)                                 J2                                                  
C     JUL 16/92 - E. CHAN                                                                                                           
C                                                                               J2                                                  
CGETSTEP - EXTRACT TIMESTEP NUMBER FROM MODEL RESTART FILE.             1  1    J1                                                  
C                                                                               J3                                                  
CAUTHOR  - E.CHAN                                                               J3                                                  
C                                                                               J3                                                  
CPURPOSE - GENERATE A FILE CONTAINING THE TIMESTEP NUMBER EXTRACTED             J3                                                  
C          FROM THE MODEL RESTART FILE.                                         J3                                                  
C                                                                               J3                                                  
CINPUT FILE...                                                                  J3                                                  
C                                                                               J3                                                  
C      RESTART = LAST RESTART DATASET                                           J3                                                  
C                                                                               J3                                                  
COUTPUT FILE...                                                                 J3                                                  
C                                                                               J3                                                  
C      TIMSTEP = ONE LINE CHARACTER FILE CONTAINING THE LATEST TIMESTEP         J3                                                  
C----------------------------------------------------------------------------                                                       
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      DIMENSION IBUF(8)                                                                                                             
C                                                                                                                                   
C----------------------------------------------------------------------------                                                       
C                                                                                                                                   
      NFF=3                                                                                                                         
      CALL JCLPNT(NFF,1,-2,6)                                                                                                       
C                                                                                                                                   
C     * GET TIMESTEP NUMBER FROM LAST RESTART DATASET.                                                                              
C                                                                                                                                   
      REWIND 1                                                                                                                      
      CALL FBUFFIN(-1,IBUF,-8,K,LEN)                                                                                                
      IF (K.GE.0) GOTO 901                                                                                                          
      KOUNT=IBUF(2)                                                                                                                 
C                                                                                                                                   
      WRITE(2,2000) KOUNT                                                                                                           
      WRITE(6,6000) KOUNT                                                                                                           
C                                                                                                                                   
      CALL                                         XIT('GETSTEP',0)                                                                 
  901 CALL                                         XIT('GETSTEP',-1)                                                                
C------------------------------------------------- -----------------                                                                
 2000 FORMAT(I20)                                                                                                                   
 6000 FORMAT(' KOUNT = ',I20)                                                                                                       
      END                                                                                                                           
