      PROGRAM GSCOPY                                                                                                                
C     PROGRAM GSCOPY (IN,       OUT,       INPUT,       OUTPUT,         )       B2                                                  
C    1          TAPE1=IN, TAPE2=OUT, TAPE5=INPUT, TAPE6=OUTPUT)                                                                     
C     ---------------------------------------------------------                 B2                                                  
C                                                                               B2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       B2                                                  
C     JAN 29/92 - E. CHAN  (CONVERT HOLLERITH LITERALS TO ASCII)                                                                    
C     MAY 12/83 - R.LAPRISE.                                                                                                        
C     SEP 30/81 - J.D.HENDERSON                                                                                                     
C     JUL 21/78 - TED SHEPHERD                                                                                                      
C                                                                               B2                                                  
CGSCOPY  - CONVERTS ONE LEVEL GRID FILE TO MANY LEVELS                  1  1 C  B1                                                  
C                                                                               B3                                                  
CAUTHOR  - J.D.HENDERSON                                                        B3                                                  
C                                                                               B3                                                  
CPURPOSE - CONVERTS A ONE-LEVEL FILE OF ANY TYPE TO A FILE CONTAINING           B3                                                  
C          AN  EQUAL NUMBER OF MULTI-LEVEL SETS  WITH EACH SET HAVING           B3                                                  
C          ALL FIELDS AS COPIES OF THE ORIGINAL ONE LEVEL FIELD.                B3                                                  
C          THE LEVELS INSERTED IN THE FOURTH LABEL WORD ARE READ FROM           B3                                                  
C          A CARD.                                                              B3                                                  
C                                                                               B3                                                  
CINPUT FILE...                                                                  B3                                                  
C                                                                               B3                                                  
C      IN  = FILE OF ONE LEVEL FIELDS                                           B3                                                  
C                                                                               B3                                                  
COUTPUT FILE...                                                                 B3                                                  
C                                                                               B3                                                  
C      OUT = FILE OF MULTI-LEVEL SETS MADE FROM INPUT FILE IN                   B3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               B5                                                  
C      NLEV = NUMBER OF LEVELS IN EACH SET (MAX $L$)                            B5                                                  
C      LEV  = LEVEL VALUES (WORD 4 OF THE LABEL)                                B5                                                  
C                                                                               B5                                                  
CEXAMPLE OF INPUT CARD...                                                       B5                                                  
C                                                                               B5                                                  
C*  GSCOPY    6  100  200  500  700  850  900                                   B5                                                  
C------------------------------------------------------------------------                                                           
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      COMMON/BLANCK/F(65341)                                                                                                        
C                                                                                                                                   
      LOGICAL OK                                                                                                                    
      INTEGER LEV(100)                                                                                                              
      COMMON/ICOM/IBUF(8),IDAT(65341)                                                                                               
      DATA MAXX/65341/, MAXL/100/                                                                                                   
C-----------------------------------------------------------------------                                                            
      NFF=4                                                                                                                         
      CALL JCLPNT(NFF,1,2,5,6)                                                                                                      
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      NRECS=0                                                                                                                       
C                                                                                                                                   
C     * READ IN THE DESIRED LEVELS.                                                                                                 
C     * NLEV = NO. OF LEVELS, LEV(I) ARE THE IBUF LEVEL INDICES.                                                                    
C                                                                                                                                   
      READ(5,5010,END=901) NLEV,(LEV(I),I=1,NLEV)                               B4                                                  
      IF((NLEV.LT.1).OR.(NLEV.GT.MAXL)) CALL       XIT('GSCOPY',-1)                                                                 
C                                                                                                                                   
C     * GET THE NEXT FIELD .                                                                                                        
C                                                                                                                                   
  100 CALL GETFLD2(1,F,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                                    
      IF(.NOT.OK)THEN                                                                                                               
        WRITE(6,6010) NRECS,NLEV                                                                                                    
        CALL                                       XIT('GSCOPY',0)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CONSTRUCT A MULTI-LEVEL SET ON FILE 2 BY COPYING THE FIELD                                                                  
C     * JUST READ WITH THE NEW LEVELS IN IBUF(4).                                                                                   
C                                                                                                                                   
      DO 200 J=1,NLEV                                                                                                               
      IBUF(4)=LEV(J)                                                                                                                
      IF(NRECS.EQ.0) WRITE(6,6020) IBUF                                                                                             
  200 CALL PUTFLD2(2,F,IBUF,MAXX)                                                                                                   
      NRECS=NRECS+1                                                                                                                 
      GO TO 100                                                                                                                     
C                                                                                                                                   
C     * E.O.F. ON INPUT.                                                                                                            
C                                                                                                                                   
  901 CALL                                         XIT('GSCOPY',-2)                                                                 
C-----------------------------------------------------------------------                                                            
 5010 FORMAT(10X,14I5)                                                          B4                                                  
 6010 FORMAT('0GSCOPY COPIED',I6,' RECORDS TO',I3,' LEVELS')                                                                        
 6020 FORMAT('0',A4,I10,2X,A4,I10,4I6)                                                                                              
      END                                                                                                                           
