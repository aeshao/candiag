      PROGRAM TTESTA                                                                                                                
C     PROGRAM TTESTA (XBAR,        VX,            YBAR,          VY,            H2                                                  
C    1                MDIF,        TVALUE,        KVALUE,        DVALUE,        H2                                                  
C    2                             MASK,          INPUT,         OUTPUT,)       H2                                                  
C    3          TAPE1=XBAR,  TAPE2=VX,      TAPE3=YBAR,    TAPE4=VY,                                                                
C    4         TAPE10=MDIF, TAPE11=TVALUE, TAPE12=KVALUE, TAPE13=DVALUE,                                                            
C    5                      TAPE14=MASK,    TAPE5=INPUT,   TAPE6=OUTPUT)                                                            
C     ------------------------------------------------------------------        H2                                                  
C                                                                               H2                                                  
C     JUL 03/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)       H2                                                  
C     AUG 21/00 - F.MAJAESS (ENSURE FIELD DIMENSION CHECK VERSUS "MAXRSZ")                                                          
C     JUN 05/94 - B.DENIS  (MODIFY CALCULATION OF XSCALE/YSCALE)                                                                    
C     MAR 07/94 - F.MAJAESS (MODIFY DATA SECTION)                                                                                   
C     AUG 04/92 - E. CHAN   (MODIFY EXTRACTION/CALCULATION OF XMAX, XMIN,                                                           
C                            AND XSCALE DUE TO IMPLEMENTATION OF NEW PACKER)                                                        
C     JAN 29/92 - E. CHAN   (CONVERT HOLLERITH LITERALS TO ASCII)                                                                   
C     OCT 10/89 - F.MAJAESS (FIX ERROR IN CALCULATING PACKING NOISE VALUE)                                                          
C     APR 29/88 - F.MAJAESS (REPLACE ABORT EXIT BY WARNING EXIT FOR                                                                 
C                            UNMATCHED MEAN FIELD NAMES)                                                                            
C     NOV 06/87 - F.MAJAESS (ALLOW SUPPLYING VARIANCE OR UNBIASED                                                                   
C                            ESTIMATES OF THE STANDARD DEVIATION AND                                                                
C                            IMPLEMENT COMPUTATIONS OF K AND D-VALUES).                                                             
C     JAN 25/85 - F. ZWIERS                                                                                                         
C                                                                               H2                                                  
CTTESTA  - CONDUCT AN "APPROXIMATE" DIFFERENCE OF MEANS TEST.           4  5 C  H1                                                  
C                                                                               H3                                                  
CAUTHORS - F. ZWIERS, F. MAJAESS.                                               H3                                                  
C                                                                               H3                                                  
CPURPOSE - CONDUCT A  DIFFERENCE OF MEANS TEST.  THE TEST WHICH IS CONDUCTED    H3                                                  
C          IS THE 'APPROXIMATE T-TEST' WHICH MAKES  NO ASSUMPTION  ABOUT THE    H3                                                  
C          EQUALITY OF VARIANCES. HOWEVER, ALL THE OTHER ASSUMPTIONS USUALLY    H3                                                  
C          MADE WHEN CONDUCTING A T-TEST (I.E., INDEPEDENT OBSERVATIONS FROM    H3                                                  
C          NORMAL POPULATIONS IN EACH SAMPLE, INDEPENDENT SAMPLES) MUST STILL   H3                                                  
C          BE SATISFIED. UPON RETURN, THE PROGRAM REPORTS THE COMPUTED T-TEST   H3                                                  
C          VALUES  IN  TVALUE  AS WELL AS  RETURNING THE  K-VALUE AND D-VALUE   H3                                                  
C          WHICH ARE RESPECTIVELY THE TRANSFORMED SIGNIFICANCE LEVELS AND THE   H3                                                  
C          ACCEPTANCE/REJECTION FLAG.                                           H3                                                  
C          NOTE - TESTS ARE NOT PERFORMED WHEN THE "POOLED VARIANCE" IS         H3                                                  
C                 APPROXIMATELY EQUAL TO THE NOISE INDUCED BY THE PACKING       H3                                                  
C                 ALGORITHM.                                                    H3                                                  
C                 INTERPRETATION OF VX AND VY FILES IS CONTROLED BY INPUT       H3                                                  
C                 PARAMETER IVAR.                                               H3                                                  
C                 ALSO, MASK FILE NEED NOT BE SPECIFIED IN THE PROGRAM          H3                                                  
C                 CALLING SEQUENCE. (SEE OUTPUT FILES SECTION BELOW).           H3                                                  
C                                                                               H3                                                  
CINPUT FILES...                                                                 H3                                                  
C                                                                               H3                                                  
C      XBAR   = FIELDS OF MEANS OF THE 'X-CONTROL' VARIABLE.                    H3                                                  
C                                                                               H3                                                  
C    IF (IVAR.EQ.0) THEN                                                        H3                                                  
C      VX     = FIELDS OF THE VARIANCE OF X.                                    H3                                                  
C               SUM(X(I)-XBAR)**2/NX, I=1,...,NX.                               H3                                                  
C    OTHERWISE,                                                                 H3                                                  
C      VX     = FIELDS OF THE STANDARD DEVIATION OF X.                          H3                                                  
C               SQRT(SUM(X(I)-XBAR)**2/(NX-1)), I=1,...,NX.                     H3                                                  
C                                                                               H3                                                  
C      YBAR   = SAME AS XBAR EXCEPT FOR THE 'Y-EXPERIMENT' VARIABLE.            H3                                                  
C      VY     = SAME AS VX   EXCEPT FOR THE 'Y-EXPERIMENT' VARIABLE.            H3                                                  
C                                                                               H3                                                  
COUTPUT FILES...                                                                H3                                                  
C                                                                               H3                                                  
C      MDIF   = FIELDS OF MEAN DIFFERENCES COMPUTED AS D=(YBAR-XBAR).           H3                                                  
C                                                                               H3                                                  
C      TVALUE = FIELDS OF 'T-VALUES' GIVEN BY                                   H3                                                  
C                                                                               H3                                                  
C                         T=(YBAR-XBAR)/SDF                                     H3                                                  
C                                                                               H3                                                  
C                         SDF=SQRT(AXT+AYT)                                     H3                                                  
C                                                                               H3                                                  
C                         AXT=VARX/(NX-1)                                       H3                                                  
C                                                                               H3                                                  
C                         AYT=VARY/(NY-1)                                       H3                                                  
C                                                                               H3                                                  
C              WHERE, VARX AND VARY ARE THE VARIANCES OF X AND Y                H3                                                  
C              VARIABLES RESPECTIVELY.                                          H3                                                  
C              NOTE - THE APPROPRIATE NUMBER OF DEGREES OF FREEDOM, (NDF),      H3                                                  
C                     ARE ESTIMATED FROM THE OBSERVATIONS AS FOLLOWS:           H3                                                  
C                     (SEE FOR EXAMPLE, PROB AND STATS FOR ENGINEERS            H3                                                  
C                      AND SCIENTISTS BY WALPOLE AND MYERS).                    H3                                                  
C                                                                               H3                                                  
C                         NDF=MAX(1,INT(ANUM/ADEN+0.5))                         H3                                                  
C                                                                               H3                                                  
C                         ANUM=(AXT+AYT)**2                                     H3                                                  
C                                                                               H3                                                  
C                         ADEN=AXT**2/(NX-1)+AYT**2/(NY-1)                      H3                                                  
C                                                                               H3                                                  
C                     WHERE, AXT AND AYT ARE AS DEFINED ABOVE.                  H3                                                  
C                                                                               H3                                                  
C      KVALUE = FIELDS OF 'K-VALUES' OF TRANSFORMED SIGNIFICANCE LEVELS.        H3                                                  
C               COMPUTED AS K IN:                                               H3                                                  
C                                                                               H3                                                  
C                          P=ALPHA/(5**(K-1))                                   H3                                                  
C                                                                               H3                                                  
C               OR         K=LOG (ALPHA/P)+1                                    H3                                                  
C                               5                                               H3                                                  
C                                                                               H3                                                  
C               WHICH INDICATES THAT THE OBSERVED STATISTIC IS SIGNIFICANT      H3                                                  
C               AT THE  "ALPHA/(5**(K-1))"  SIGNIFICANCE LEVEL.                 H3                                                  
C               THUS K=1 INDICATES THAT THE OBSERVED STATISTIC IS JUST          H3                                                  
C               SIGNIFICANT AT THE ALPHA SIGNIFICANCE LEVEL.                    H3                                                  
C               IF PLOTTED WITH UNIT CONTOUR INTERVALS, SUCCESSIVE              H3                                                  
C               CONTOURS WILL ENCLOSE REGIONS WHERE LOCALLY IT IS               H3                                                  
C               FIVE TIMES AS UNLIKELY THAT VALUES OF THE OBSERVED              H3                                                  
C               STATISTICS ARE CONSISTENT WITH THE NULL HYPOTHESIS              H3                                                  
C               THAN IN REGIONS OUTSIDE THE NEXT LOWER CONTOUR.                 H3                                                  
C                                                                               H3                                                  
C      DVALUE = FIELDS OF 'D-VALUES' COMPUTED AS:                               H3                                                  
C                       __                                                      H3                                                  
C                       ! 0  IF THE NULL HYPOTHESIS IS ACCEPTED                 H3                                                  
C                   D = !                                                       H3                                                  
C                       ! 1  IF THE NULL HYPOTHESIS IS REJECTED                 H3                                                  
C                       --                                                      H3                                                  
C                                                                               H3                                                  
C      MASK   = FIELDS OF 'MASK-VALUES' COMPUTED AS:                            H3                                                  
C                       __                                                      H3                                                  
C                       ! 1  IF THE TEST WAS     CONDUCTED                      H3                                                  
C                MASK = !                                                       H3                                                  
C                       ! 0  IF THE TEST WAS NOT CONDUCTED                      H3                                                  
C                       --                                                      H3                                                  
C                                                                               H3                                                  
C               NOTE - THE MASK FILE IS NOT RETURNED IF THE PROGRAM IS          H3                                                  
C                      NOT CALLED WITH OUTPUT FILE MASK.                        H3                                                  
C                                                                                                                                   
CINPUT PARAMETERS...                                                                                                                
C                                                                               H5                                                  
C      NX=INT(ANX) & NY=INT(ANY)                                                H5                                                  
C                                                                               H5                                                  
C    WHERE,                                                                     H5                                                  
C                                                                               H5                                                  
C      ALPHA = THE SIGNIFICANCE LEVEL OF THE TEST.                              H5                                                  
C      NX    = THE NUMBER OF OBSERVATIONS IN THE CONTROL    DATA SET,           H5                                                  
C      NY    = THE NUMBER OF OBSERVATIONS IN THE EXPERIMENT DATA SET.           H5                                                  
C              IF NX > 1 AND NY > 1 THEN                                        H5                                                  
C                 NX AND NY READ FROM THE INPUT CARD ARE USED IN THE            H5                                                  
C                 COMPUTATION OF THE NUMBER OF DEGREES OF FREEDOM.              H5                                                  
C              OTHERWISE,                                                       H5                                                  
C                 THE NX AND NY VALUES NEEDED IN THE COMPUTATION ARE            H5                                                  
C                 OBTAINED FROM THE LABELS OF THE FILES XBAR AND YBAR.          H5                                                  
C      IKIND = -1, REJECT THE NULL HYPOTHESIS IF P > 1 - ALPHA                  H5                                                  
C                  (IE - CONDUCT A ONE TAILED TEST AND REJECT IF                H5                                                  
C                        THE TEST STATISTIC IS UNUSUALLY SMALL).                H5                                                  
C            =  0, INDICATES A TWO TAILED TEST (REJECT IF                       H5                                                  
C                  P > 1 - ALPHA/2   OR IF  P < ALPHA/2)                        H5                                                  
C                  (IE - REJECT IF THE TEST STATISTIC IS                        H5                                                  
C                        UNUSUALLY SMALL OR LARGE).                             H5                                                  
C            =  1, REJECT THE NULL HYPOTHESIS IF P < ALPHA                      H5                                                  
C                  (IE - CONDUCT A ONE TAILED TEST AND REJECT IF                H5                                                  
C                        THE TEST STATISTIC IS UNUSUALLY LARGE).                H5                                                  
C      IVAR  = A FLAG USED TO DETERMINE THE CONTENTS OF VX AND VY FILES.        H5                                                  
C              THAT IS :                                                        H5                                                  
C              IF (IVAR.EQ.0) THEN                                              H5                                                  
C                VX AND VY FILES CONTAIN THE VARIANCES OF X AND Y               H5                                                  
C                RESPECTIVELY.                                                  H5                                                  
C              OTHERWISE,                                                       H5                                                  
C                VX AND VY FILES CONTAIN THE UNBIASED ESTIMATES OF              H5                                                  
C                THE STANDARD DEVIATIONS OF X AND Y RESPECTIVELY.               H5                                                  
C                                                                               H5                                                  
CEXAMPLE OF INPUT CARD...                                                       H5                                                  
C                                                                               H5                                                  
C*TTESTA.      5.E-2  10.   5.    0    1                                        H5                                                  
C-----------------------------------------------------------------------------                                                      
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      DIMENSION IBUFX(8),IBUFY(8),XBAR(18528),YBAR(18528),                                                                          
     1          VX(18528),VY(18528),T(18528),P(18528),                                                                              
     2          DIF(18528),D(18528),AXT(18528),AYT(18528),                                                                          
     3          FMASK(18528)                                                                                                        
      REAL K(18528),WK1,WK2,WK3,WK4                                                                                                 
      COMMON/BLANCK/DIF,T,P,K,D,XBAR,YBAR,FMASK                                                                                     
      COMMON/MACHTYP/MACHINE,INTSIZE                                                                                                
      COMMON/ICOM/IBUF(8),IDAT(18528)                                                                                               
      EQUIVALENCE (AXT(1),XBAR(1)),(AYT(1),YBAR(1)),                                                                                
     1            (VX(1),K(1)),(VY(1),D(1))                                                                                         
      LOGICAL OK,LNXNY,VAR,NRMLXIT,MASK,MATCH                                                                                       
      DATA NBW/64/, MAXX/18528/                                                                                                     
      DATA MAXRSZ /18528/                                                                                                           
C     DATA NGRID/4HGRID/,NZONL/4HZONL/,NDIF/4HMDIF/,NT/4H   T/                                                                      
C     DATA NK   /4H   K/,ND   /4H   D/,NMASK/4HMASK/                                                                                
C-----------------------------------------------------------------------------                                                      
C                                                                                                                                   
C                                                                                                                                   
      NGRID=NC4TO8("GRID")                                                                                                          
      NZONL=NC4TO8("ZONL")                                                                                                          
      NDIF=NC4TO8("MDIF")                                                                                                           
      NT=NC4TO8("   T")                                                                                                             
      NK   =NC4TO8("   K")                                                                                                          
      ND   =NC4TO8("   D")                                                                                                          
      NMASK=NC4TO8("MASK")                                                                                                          
      NRMLXIT=.TRUE.                                                                                                                
      MATCH=.TRUE.                                                                                                                  
      NF=11                                                                                                                         
      CALL JCLPNT(NF,1,2,3,4,10,11,12,13,14,5,6)                                                                                    
      IF(NF.GT.10)THEN                                                                                                              
        MASK=.TRUE.                                                                                                                 
      ELSE                                                                                                                          
        MASK=.FALSE.                                                                                                                
      ENDIF                                                                                                                         
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      REWIND 3                                                                                                                      
      REWIND 4                                                                                                                      
C                                                                                                                                   
C-----------------------------------------------------------------------                                                            
C     * GET ALPHA, ANX, ANY, IKIND AND IVAR.                                                                                        
C                                                                                                                                   
      READ(5,5010,END=8000)ALPHA,ANX,ANY,IKIND,IVAR                             H4                                                  
      GO TO 8010                                                                                                                    
C                                                                                                                                   
C-----------------------------------------------------------------------                                                            
C     * EOF ON UNIT #5.                                                                                                             
C                                                                                                                                   
 8000 CONTINUE                                                                                                                      
      WRITE(6,6070)                                                                                                                 
      CALL                                         XIT('TTESTA',-1)                                                                 
 8010 CONTINUE                                                                                                                      
      WRITE(6,6110)ALPHA,ANX,ANY,IKIND,IVAR                                                                                         
      NX=INT(ANX+0.0001E0)                                                                                                          
      NY=INT(ANY+0.0001E0)                                                                                                          
C                                                                                                                                   
C     * CHECK NX, NY AND IVAR.                                                                                                      
C                                                                                                                                   
      IF((NX.LE.1).OR.(NY.LE.1))THEN                                                                                                
        LNXNY=.TRUE.                                                                                                                
      ELSE                                                                                                                          
        LNXNY=.FALSE.                                                                                                               
      ENDIF                                                                                                                         
      IF(IVAR.EQ.0)THEN                                                                                                             
        VAR=.TRUE.                                                                                                                  
      ELSE                                                                                                                          
        VAR=.FALSE.                                                                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C-----------------------------------------------------------------------                                                            
C                                                                                                                                   
      NREC=0                                                                                                                        
 1000 CONTINUE                                                                                                                      
      CALL GETFLD2(1,XBAR,-1,0,0,0,IBUF,MAXX,OK)                                                                                    
      IF(.NOT.OK)THEN                                                                                                               
         IF(NREC.EQ.0)THEN                                                                                                          
            WRITE(6,6010)                                                                                                           
            CALL                                   XIT('TTESTA',-2)                                                                 
         ENDIF                                                                                                                      
         WRITE(6,6060) NREC                                                                                                         
         IF(NRMLXIT.AND.MATCH)THEN                                                                                                  
           CALL                                    XIT('TTESTA',0)                                                                  
         ELSE                                                                                                                       
           CALL                                    XIT('TTESTA',-101)                                                               
         ENDIF                                                                                                                      
      ENDIF                                                                                                                         
      DO 1005 I=1,8                                                                                                                 
         IBUFX(I)=IBUF(I)                                                                                                           
 1005 CONTINUE                                                                                                                      
      IF(NREC.EQ.0)WRITE(6,6100)IBUFX                                                                                               
      IF(LNXNY)NX=IBUFX(2)                                                                                                          
      IF(NX.LE.1)THEN                                                                                                               
         WRITE(6,6080)NX                                                                                                            
         CALL                                      XIT('TTESTA',-3)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * READ IN VX.                                                                                                                 
C                                                                                                                                   
      CALL GETFLD2(2,VX,-1,0,0,0,IBUF,MAXX,OK)                                                                                      
      IF(.NOT.OK)THEN                                                                                                               
         WRITE(6,6020)                                                                                                              
         CALL                                      XIT('TTESTA',-4)                                                                 
      ENDIF                                                                                                                         
      IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                                
C     XMIN=DECODR(IDAT(1))                                                                                                          
C     XSCALE=1./DECODR(IDAT(2))                                                                                                     
C     XMAX=XMIN+XSCALE*(2**(NBW/IBUF(8))-1.0)                                                                                       
      CALL DECODR2(IBUF(9),XMIN)                                                                                                    
      CALL DECODR2(IBUF(8+MACHINE+1),XMAX)                                                                                          
      IF(.NOT.VAR)THEN                                                                                                              
        FACTX=(FLOAT(NX)-1.0E0)/FLOAT(NX)                                                                                           
        XMIN=FACTX*(XMIN**2)                                                                                                        
        XMAX=FACTX*(XMAX**2)                                                                                                        
      ENDIF                                                                                                                         
      XSCALE=(XMAX-XMIN)/(2.E0**(NBW/IBUF(8))-1.0E0)                                                                                
      IF(XMIN.LE.1.E-3*XMAX)THEN                                                                                                    
        EPSX=2.0E0*(XMIN+XSCALE)                                                                                                    
      ELSE                                                                                                                          
        EPSX=2.0E0*XSCALE                                                                                                           
      ENDIF                                                                                                                         
C                                                                                                                                   
C                                                                                                                                   
      CALL GETFLD2(3,YBAR,-1,0,0,0,IBUF,MAXX,OK)                                                                                    
      IF(.NOT.OK)THEN                                                                                                               
         WRITE(6,6030)                                                                                                              
         CALL                                      XIT('TTESTA',-5)                                                                 
      ENDIF                                                                                                                         
      DO 1015 I=1,8                                                                                                                 
         IBUFY(I)=IBUF(I)                                                                                                           
 1015 CONTINUE                                                                                                                      
      IF(NREC.EQ.0)WRITE(6,6100)IBUFY                                                                                               
      IF(LNXNY)NY=IBUFY(2)                                                                                                          
      IF(NY.LE.1)THEN                                                                                                               
         WRITE(6,6090)NY                                                                                                            
         CALL                                      XIT('TTESTA',-6)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * READ IN VY.                                                                                                                 
C                                                                                                                                   
      CALL GETFLD2(4,VY,-1,0,0,0,IBUF,MAXX,OK)                                                                                      
      IF(.NOT.OK)THEN                                                                                                               
         WRITE(6,6040)                                                                                                              
         CALL                                      XIT('TTESTA',-7)                                                                 
      ENDIF                                                                                                                         
      IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                                
C     YMIN=DECODR(IDAT(1))                                                                                                          
C     YSCALE=1./DECODR(IDAT(2))                                                                                                     
C     YMAX=YMIN+YSCALE*(2**(NBW/IBUF(8))-1.0)                                                                                       
      CALL DECODR2(IBUF(9),YMIN)                                                                                                    
      CALL DECODR2(IBUF(8+MACHINE+1),YMAX)                                                                                          
      IF(.NOT.VAR)THEN                                                                                                              
        FACTY=(FLOAT(NY)-1.0E0)/FLOAT(NY)                                                                                           
        YMIN=FACTY*(YMIN**2)                                                                                                        
        YMAX=FACTY*(YMAX**2)                                                                                                        
      ENDIF                                                                                                                         
      YSCALE=(YMAX-YMIN)/(2.E0**(NBW/IBUF(8))-1.0E0)                                                                                
      IF(YMIN.LE.1.E-3*YMAX)THEN                                                                                                    
        EPSY=2.0E0*(YMIN+YSCALE)                                                                                                    
      ELSE                                                                                                                          
        EPSY=2.0E0*YSCALE                                                                                                           
      ENDIF                                                                                                                         
      EPS=MIN(EPSX,EPSY)                                                                                                            
C                                                                                                                                   
C                                                                                                                                   
      IF( (IBUFX(1).NE.NGRID .AND. IBUFX(1).NE.NZONL) .OR.                                                                          
     1    (IBUFY(1).NE.NGRID .AND. IBUFY(1).NE.NZONL) .OR.                                                                          
     2    (IBUFX(1).NE.IBUFY(1)) .OR. (IBUFX(4).NE.IBUFY(4)) .OR.                                                                   
     3    (IBUFX(5).NE.IBUFY(5)) .OR. (IBUFX(6).NE.IBUFY(6)))THEN                                                                   
         WRITE(6,6050)                                                                                                              
         WRITE(6,6100)IBUFX                                                                                                         
         WRITE(6,6100)IBUFY                                                                                                         
         CALL                                      XIT('TTESTA',-8)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
      IF( (IBUFX(3).NE.IBUFY(3)) .AND. MATCH ) THEN                                                                                 
         MATCH=.FALSE.                                                                                                              
         WRITE(6,6140)                                                                                                              
      ENDIF                                                                                                                         
C                                                                                                                                   
      FNX=FLOAT(NX)                                                                                                                 
      FNY=FLOAT(NY)                                                                                                                 
      FNXM1=FNX-1.0E0                                                                                                               
      FNYM1=FNY-1.0E0                                                                                                               
      NWDS=IBUFX(5)*IBUFX(6)                                                                                                        
      IF(NWDS.GT.MAXRSZ)THEN                                                                                                        
         WRITE(6,6120)MAXRSZ,NWDS                                                                                                   
         CALL                                      XIT('TTESTA',-9)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * COMPUTE THE MEAN DIFFERENCES.                                                                                               
C                                                                                                                                   
      DO 1030 I=1,NWDS                                                                                                              
         DIF(I)=YBAR(I)-XBAR(I)                                                                                                     
 1030 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * CONVERT UNBIASED STANDARD DEVIATION ESTIMATES TO VARIANCES.                                                                 
C                                                                                                                                   
      IF(.NOT.VAR)THEN                                                                                                              
         FACTX=FNXM1/FNX                                                                                                            
         FACTY=FNYM1/FNY                                                                                                            
         DO 1040 I=1,NWDS                                                                                                           
           VX(I)=FACTX*(VX(I)**2)                                                                                                   
           VY(I)=FACTY*(VY(I)**2)                                                                                                   
 1040    CONTINUE                                                                                                                   
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * COMPUTE AXT AND AYT.                                                                                                        
C                                                                                                                                   
      DO 1045 I=1,NWDS                                                                                                              
         AXT(I)=VX(I)/FNXM1                                                                                                         
         AYT(I)=VY(I)/FNYM1                                                                                                         
 1045 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * COMPUTE THE T-STATISTICS AND CHECK # OF OCCURENCES OF VERY                                                                  
C       SMALL SDF.                                                                                                                  
C                                                                                                                                   
      ICNT=0                                                                                                                        
      DO 1050 I=1,NWDS                                                                                                              
         SDF=SQRT(AXT(I)+AYT(I))                                                                                                    
         IF(SDF.LE.EPS)THEN                                                                                                         
           FMASK(I)=0.0E0                                                                                                           
           T(I)=0.0E0                                                                                                               
           ICNT=ICNT+1                                                                                                              
         ELSE                                                                                                                       
           FMASK(I)=1.0E0                                                                                                           
           T(I)=DIF(I)/SDF                                                                                                          
         ENDIF                                                                                                                      
 1050 CONTINUE                                                                                                                      
      IF(ICNT.GE.1)THEN                                                                                                             
        NRMLXIT=.FALSE.                                                                                                             
        WRITE(6,6130)ICNT,NWDS,IBUFX(4)                                                                                             
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * WRITE OUT THE RESULTS                                                                                                       
C                                                                                                                                   
      DO 1060 I=1,8                                                                                                                 
         IBUF(I)=IBUFX(I)                                                                                                           
 1060 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * WRITE OUT MEAN DIFFERENCES.                                                                                                 
C                                                                                                                                   
      IBUF(3)=NDIF                                                                                                                  
      CALL PUTFLD2(10,DIF,IBUF,MAXX)                                                                                                
      IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                                
C                                                                                                                                   
C     * WRITE OUT T-VALUES.                                                                                                         
C                                                                                                                                   
      IBUF(3)=NT                                                                                                                    
      CALL PUTFLD2(11,T,IBUF,MAXX)                                                                                                  
      IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                                
C                                                                                                                                   
C     * COMPUTE THE P-VALUES AS                                                                                                     
C     *                                                                                                                             
C     *                     P=PROB(STUDENT'S T > OBSERVED T)                                                                        
C     *                                                                                                                             
C     *          IE. P IS THE PROB THAT T IS GREATER THAN THE                                                                       
C     *          OBSERVED VALUE ASSUMING THAT THE NULL HYPOTHESIS                                                                   
C     *          OF EQUALITY OF MEANS IS CORRECT.                                                                                   
C     *                                                                                                                             
C                                                                                                                                   
      DO 1070 I=1,NWDS                                                                                                              
         IF(FMASK(I).EQ.0.0E0)THEN                                                                                                  
           NDF=1                                                                                                                    
         ELSE                                                                                                                       
           ANUM=(AXT(I)+AYT(I))**2                                                                                                  
           ADEN=AXT(I)**2/FNXM1+AYT(I)**2/FNYM1                                                                                     
           NDF=MAX(1,INT(ANUM/ADEN+0.5E0))                                                                                          
         ENDIF                                                                                                                      
         CALL PROBT(T(I),NDF,1,P(I),WK1,WK2,WK3,WK4)                                                                                
 1070 CONTINUE                                                                                                                      
C                                                                                                                                   
C     * COMPUTE K AND D-VALUES.                                                                                                     
C                                                                                                                                   
      CALL HTEST(P,NWDS,ALPHA,IKIND,K,D)                                                                                            
C                                                                                                                                   
C     * WRITE OUT K-VALUES.                                                                                                         
C                                                                                                                                   
      IBUF(3)=NK                                                                                                                    
      CALL PUTFLD2(12,K,IBUF,MAXX)                                                                                                  
      IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                                
C                                                                                                                                   
C     * WRITE OUT D-VALUES.                                                                                                         
C                                                                                                                                   
      IBUF(3)=ND                                                                                                                    
      CALL PUTFLD2(13,D,IBUF,MAXX)                                                                                                  
      IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                                
C                                                                                                                                   
C     * WRITE OUT MASK-VALUES IF REQUIRED.                                                                                          
C                                                                                                                                   
      IF(MASK)THEN                                                                                                                  
        IBUF(3)=NMASK                                                                                                               
        CALL PUTFLD2(14,FMASK,IBUF,MAXX)                                                                                            
        IF(NREC.EQ.0)WRITE(6,6100)IBUF                                                                                              
      ENDIF                                                                                                                         
C                                                                                                                                   
      NREC=NREC+1                                                                                                                   
      GOTO 1000                                                                                                                     
C                                                                                                                                   
C-----------------------------------------------------------------------                                                            
C                                                                                                                                   
 5010 FORMAT(10X,E10.0,2F5.0,2I5)                                               H4                                                  
 6010 FORMAT('0FILE OF MEANS FOR VARIABLE X EMPTY.')                                                                                
 6020 FORMAT('0UNEXPECTED EOF ON FILE OF X VARIANCE.')                                                                              
 6030 FORMAT('0UNEXPECTED EOF ON FILE OF Y MEANS.')                                                                                 
 6040 FORMAT('0UNEXPECTED EOF ON FILE OF Y VARIANCE.')                                                                              
 6050 FORMAT('0LABELS ON MEAN FIELDS DO NOT MATCH.')                                                                                
 6060 FORMAT('0CONDUCTED T-TESTS ON ',I5,' PAIRS OF MEAN FIELDS.')                                                                  
 6070 FORMAT('0 THE TTESTA CARD IS MISSING.')                                                                                       
 6080 FORMAT('0ILLEGAL # OF CONTROL OBS. NX= ',I10)                                                                                 
 6090 FORMAT('0ILLEGAL # OF EXPERIMENTAL OBS. NY= ',I10)                                                                            
 6100 FORMAT(' ',A4,I10,2X,A4,I10,4I6)                                                                                              
 6110 FORMAT('0TTESTA.   ',E10.1,2F5.0,2I5)                                                                                         
 6120 FORMAT('0INSUFFICIENT ARRAY SIZE= ',I10,', FOR # PTS= ',I10)                                                                  
 6130 FORMAT('0WARNING ',I6,' DETECTED VERY SMALL SDF OUT OF ',I6,                                                                  
     1       ' COMPUTED VALUES AT LEVEL ',I6)                                                                                       
 6140 FORMAT('0** NOTE - UNMATCHED MEAN FIELD NAMES **.')                                                                           
C                                                                                                                                   
C-----------------------------------------------------------------------                                                            
C                                                                                                                                   
      END                                                                                                                           
