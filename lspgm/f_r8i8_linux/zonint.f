      PROGRAM ZONINT                                                                                                                
C     PROGRAM ZONINT(FIELD,       DX,       INTZO,       OUTPUT           )     D2                                                  
C    1         TAPE1=FIELD, TAPE2=DX, TAPE3=INTZO, TAPE6=OUTPUT)                                                                    
C     ----------------------------------------------------------                D2                                                  
C                                                                               D2                                                  
C     JUL 03/2003 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)     D2                                                  
C     MAY 10/2002 - B.MIVILLE (REVISED FOR NEW DATA DESCRIPTION FORMAT)         D2                                                  
C     MAR 05/2002 - F.MAJAESS (REVISED FOR "CHAR" KIND RECORDS)                                                                     
C     MAY 30/2001 - B.MIVILLE - ORIGINAL VERSION                                                                                    
C                                                                               D2                                                  
CZONINT  - ZONAL INTEGRAL OF OCEAN FIELD                                2  1    D1                                                  
C                                                                               D3                                                  
CAUTHOR  - B. MIVILLE                                                           D3                                                  
C                                                                               D3                                                  
CPURPOSE - ZONAL INTEGRAL OF OCEAN FIELD                                        D3                                                  
C                                                                               D3                                                  
CINPUT FILE...                                                                  D3                                                  
C                                                                               D3                                                  
C   FIELD = INPUT OCEAN FIELD                                                   D3                                                  
C   DX    = HORIZONTAL GRID CELL WIDTH                                          D3                                                  
C                                                                               D3                                                  
COUTPUT FILE...                                                                 D3                                                  
C                                                                               D3                                                  
C   INTZO = ZONAL INTEGRATION RESULTS                                           D3                                                  
C                                                                               D3                                                  
C-------------------------------------------------------------------------------                                                    
C                                                                                                                                   
C     * MAXIMUM 2D GRID SIZE IM*JM --> IJM                                                                                          
C                                                                                                                                   
      IMPLICIT REAL (A-H,O-Z),                                                                                                      
     +INTEGER (I-N)                                                                                                                 
      PARAMETER(IM=771,JM=386,IJM=IM*JM,IJMV=IJM*1)                                                                                 
C                                                                                                                                   
      LOGICAL OK,SPEC                                                                                                               
      REAL FIELD(IJM),DX(IJM),INTZO(IJM)                                                                                            
      INTEGER IBUF,IDAT,MAXX                                                                                                        
      INTEGER FLABEL                                                                                                                
C                                                                                                                                   
      COMMON/ICOM/IBUF(8),IDAT(IJMV)                                                                                                
      DATA MAXX/IJMV/                                                                                                               
C---------------------------------------------------------------------                                                              
C                                                                                                                                   
      NF=4                                                                                                                          
      CALL JCLPNT(NF,1,2,3,6)                                                                                                       
      REWIND 1                                                                                                                      
      REWIND 2                                                                                                                      
      REWIND 3                                                                                                                      
C                                                                                                                                   
C     * READ DX FOR ALL LEVEL                                                                                                       
C                                                                                                                                   
      CALL GETFLD2 (2,DX,NC4TO8("GRID"),-1,-1,-1,IBUF,MAXX,OK)                                                                      
      IF (.NOT.OK) CALL                            XIT('ZONINT',-1)                                                                 
C                                                                                                                                   
C     * THE DX ARE EVENLY SPACED IN THE X DIRECTION BUT NOT IN THE Y                                                                
C     * DIRECTION, THERE ARE NLAT DIFFERENT DX.                                                                                     
C                                                                                                                                   
      NLON=IBUF(5)                                                                                                                  
      NLAT=IBUF(6)                                                                                                                  
C                                                                                                                                   
      IF(NLON.GT.IM.OR.NLAT.GT.JM) CALL            XIT('ZONINT',-2)                                                                 
C                                                                                                                                   
      NR=0                                                                                                                          
C                                                                                                                                   
C     * READ OCEAN FIELD                                                                                                            
C                                                                                                                                   
 100  CALL GETFLD2 (1,FIELD,-1,-1,-1,-1,IBUF,MAXX,OK)                                                                               
      IF (.NOT.OK)THEN                                                                                                              
         IF (NR.EQ.0) CALL                         XIT('ZONINT',-3)                                                                 
         CALL                                      XIT('ZONINT',0)                                                                  
      ENDIF                                                                                                                         
C                                                                                                                                   
      IF(IBUF(5).GT.IM) CALL                       XIT('ZONINT',-4)                                                                 
C                                                                                                                                   
C     * VERIFY IF NLAT IS THE SAME AS IN DX                                                                                         
C                                                                                                                                   
      IF(IBUF(5).NE.NLON.OR.IBUF(6).NE.NLAT) THEN                                                                                   
         WRITE(6,6000)' DIMENSION NOT THE SAME BETWEEN DX AND FIELD '                                                               
         CALL                                      XIT('ZONINT',-5)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * VERIFY IF THERE IS ANY "LABL" OR "CHAR" KIND RECORDS.                                                                       
C     * PROGRAM WILL EXIT IF IT ENCOUNTERS SUCH A RECORD OR IT                                                                      
C     * IS NOT A GRID.                                                                                                              
C                                                                                                                                   
      IF(IBUF(1).NE.NC4TO8("GRID"))THEN                                                                                             
         IF(IBUF(1).EQ.NC4TO8("LABL").OR.IBUF(1).EQ.NC4TO8("CHAR"))THEN                                                             
            WRITE(6,6000)' *** LABL/CHAR RECORDS ARE NOT ALLOWED ***'                                                               
            CALL                                   XIT('ZONINT',-6)                                                                 
         ELSE                                                                                                                       
            WRITE(6,6000)' *** FIELD LABEL IS NOT GRID ***'                                                                         
            CALL                                   XIT('ZONINT',-7)                                                                 
         ENDIF                                                                                                                      
      ENDIF                                                                                                                         
C                                                                                                                                   
      IF(NR.EQ.0)THEN                                                                                                               
C                                                                                                                                   
C        * SAVE ORIGINAL FIELD LABEL                                                                                                
C                                                                                                                                   
         FLABEL=IBUF(3)                                                                                                             
         NLONM=NLON-1                                                                                                               
C                                                                                                                                   
      ELSE                                                                                                                          
C                                                                                                                                   
C        * CHECK IF FIELD CHANGED LABEL                                                                                             
C                                                                                                                                   
         IF(IBUF(3).NE.FLABEL) THEN                                                                                                 
            WRITE(6,6000)' FIELD NAME HAS CHANGED '                                                                                 
            CALL                                   XIT('ZONINT',-8)                                                                 
         ENDIF                                                                                                                      
C                                                                                                                                   
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * CHECK IF FIELD HAS SAME DIMENSION THROUGHT THE INPUT FILE                                                                   
C                                                                                                                                   
      IF(IBUF(5).NE.NLON.OR.IBUF(6).NE.NLAT)THEN                                                                                    
         WRITE(6,6000) ' * DIMENSION OF FIELD HAS CHANGED *'                                                                        
         CALL                                      XIT('ZONINT',-9)                                                                 
      ENDIF                                                                                                                         
C                                                                                                                                   
C     * INITIALIZE AT EACH LEVEL OR EACH TIME                                                                                       
C                                                                                                                                   
C     * INTEGRATE                                                                                                                   
C                                                                                                                                   
      DO 125 J=1,NLAT                                                                                                               
         INTZO(J) = 0.E0                                                                                                            
         DO 120 I=1,NLONM                                                                                                           
            IJ = (J - 1) * NLON + I                                                                                                 
            INTZO(J) = INTZO(J) + FIELD(IJ) * DX(IJ)                                                                                
 120     CONTINUE                                                                                                                   
 125  CONTINUE                                                                                                                      
C                                                                                                                                   
      NR=NR+1                                                                                                                       
C                                                                                                                                   
C     * WRITE RESULTS TO FILE                                                                                                       
C                                                                                                                                   
      IBUF(1)=NC4TO8("ZONL")                                                                                                        
      IBUF(5)=NLAT                                                                                                                  
      IBUF(6)=1                                                                                                                     
C                                                                                                                                   
      CALL PUTFLD2(3,INTZO,IBUF,MAXX)                                                                                               
C                                                                                                                                   
      GOTO 100                                                                                                                      
C                                                                                                                                   
C---------------------------------------------------------------------                                                              
 6000 FORMAT(A)                                                                                                                     
      END                                                                                                                           
