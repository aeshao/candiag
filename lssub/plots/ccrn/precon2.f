      SUBROUTINE PRECON2(FLO,HI,CINT,SCALE,A,NI,NJ,NC)
  
C **********************************************************************
C *   OBTAINS CONTOUR INTERVAL AND SCALING FACTOR FOR AN SCM ARRAY     *
C *   APPROPRIATE FOR INPUT TO CONREC.                                 *
C *   INPUT: A=ARRAY                                                   *
C *          NI,NJ=DIMENSIONS OF A                                     *
C *          NC=APPROXIMATE NUMBER OF CONTOUR INTERVALS                *
C *   OUTPUT:FLO  =MINIMUM CONTOUR CONSIDERED                          *
C *          HI   =MAXIMUM CONTOUR CONSIDERED                          *
C *          CINT =CONTOUR INTERVAL                                    *
C *          SCALE=SCALING FACTOR                                      *
C *   F.MAJAESS 1994-12-14 (REPLACE AMIN/AMAX CALLS BY AMNX/AMXX)      *
C *   JOHN L. WALMSLEY     1979-12-27  (MODIFIED 1985-01-15 - BD)      *
C **********************************************************************
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION A(NI,NJ)
C     ------------------------------------------------------------------
      AMX  =AMXX(A,NI,NJ,0) 
      AMN  =AMNX(A,NI,NJ,0) 
      RANGE=AMX-AMN 
      SCALE=FSCAL(AMX,AMN)
      CINT =CONT(RANGE,NC,SCALE)
      FLO  =FLOAT((INT(AMN*SCALE)/100+1)*100)
      HI   =FLOAT((INT(AMX*SCALE)/100)*100)
C     ------------------------------------------------------------------
      RETURN
      END 
