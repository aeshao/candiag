      SUBROUTINE READPLTINFO(LUN,DLAT1,DLON1,DLAT2,DLON2,
     +     DGRW,NHEM,NOUTYP,IOST)
C 
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for NCAR V3.2)
C     * AUG 11/97 - R. TAYLOR (back away from -180 and 180 longitudes)
C     * OCT 28/93 - F. MAJAESS (REVISED TO READ "PLTINFO" 
C     *                         DATA AS A STANDARD CCRN RECORD)
C     * SEP 24/93 - J. M. Berkley
C     * THIS ROUTINE IS USED TO HANDLE I/O CALLS FOR PLTINFO IN 
C     * ORDER TO PREVENT PROBLEMS WITH 64 VS 32 BIT. IT IS 
C     * SETUP TO USE REAL*8 AND INTEGER*4.
C 

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
      REAL RDATA(7)
      COMMON /PLTINFO/ IPBUF(8), IPDATA(22)
      DATA MAXP /22/
C--------------------------------------------------------------------
      IOST=0
      CALL GETFLD2(LUN,RDATA,NC4TO8("SUBA"),-1,NC4TO8("INFO"),-1,
     +                                             IPBUF,MAXP,OK)
      IF(.NOT.OK) CALL                   XIT('READPLTINFO',-1)
C
      DLAT1=RDATA(1)
      IF (RDATA(2).NE.180.) THEN
         DLON1=RDATA(2)
      ELSE
         DLON1=-179.89
      ENDIF
      DLAT2=RDATA(3)
      IF(RDATA(4).NE.180.) THEN
         DLON2=RDATA(4)
      ELSE
         DLON2=179.89
      ENDIF
      DGRW=RDATA(5)
      NHEM=INT(RDATA(6))
      NOUTYP=INT(RDATA(7))
C
      RETURN
      END
