      SUBROUTINE GNSQR(XCNTR, YCNTR, DELTA, XCOORD, YCOORD)
 
C     * AUG 23/07 - B.MIVILLE
C     * GENERATE A SQUARE MARKER
C

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL XCNTR, YCNTR, XCOORD(5), YCOORD(5)
C------------------------------------------------------------
C
       XCOORD(1) = XCNTR + DELTA
       YCOORD(1) = YCNTR + DELTA
       XCOORD(2) = XCNTR + DELTA
       YCOORD(2) = YCNTR - DELTA
       XCOORD(3) = XCNTR - DELTA
       YCOORD(3) = YCNTR - DELTA
       XCOORD(4) = XCNTR - DELTA
       YCOORD(4) = YCNTR + DELTA
       XCOORD(5) = XCNTR + DELTA
       YCOORD(5) = YCNTR + DELTA
C
      RETURN
      END
