      SUBROUTINE ARDBD1 (X1,Y1,X2,Y2,IL)

C     * APR 07/08 - F.MAJAESS (CORRECT A BUG)
C     * AUG 23/07 - B.MIVILLE
C
C     * The routine ARDBD1 is to draw an arrow from the point (X1,Y1) to the
C     * point (X2,Y2), in the fractional coordinate system. IL is budget value. 
C     * In order to prevent too many arrowheads from appearing, we keep track
C     * of the cumulative distance along edges being drawn (in DT).
C
      COMMON /ARCOM1/ DT
C
C     * Define variables required to write the budget values.
C
      REAL IL
      CHARACTER*10 CS 
      CHARACTER*6 CS0
      CHARACTER*1 IC
C
C     * Define some temporary X and Y coordinate arrays, to be used in marking
C     * the head and tail of the arrow.
C
      DIMENSION TX(8),TY(8)
C-----------------------------------------------------------------------------

C
C     * Plot line between two points.
C
      CALL GSCR(1,1,0.,0.,0.)
      CALL PLOTIF(X1,Y1,0)
      CALL PLOTIF(X2,Y2,1)
C
C     * Compute the length of the arrow.  If it's zero, quit.
C
      DX=X2-X1
      DY=Y2-Y1
      DP=SQRT(DX*DX+DY*DY)
C
      IF (DP.EQ.0.) RETURN
C
C     * Arrage the position beside arrow, where we will write numbers or characters.
C
      XC=.5*(X1+X2)
      YC=.5*(Y1+Y2)
C
C     * Write budget value in the arrow.
C
      IF(IL.GT.0..AND.IL.LT.1.) THEN
         WRITE(CS,1000) IL 
      ELSEIF(IL.GT.-1..AND.IL.LT.0.) THEN
         WRITE(CS,1001) IL*(-1.0)
      ELSEIF(IL.EQ.0.) THEN
         WRITE(CS0,1002)
      ELSE
         WRITE(CS,1003) IL 
      ENDIF
      IF(IL.EQ.0.) THEN
         CALL PLCHHQ (XC,YC,CS0,.014,0.,0.)
      ELSE
         CALL PLCHHQ (XC,YC,CS,.014,0.,0.)
      ENDIF
C
C
      DT=DT+DP
      IF(DT.LE..008) RETURN
C
C     * otherwise, zero the cumulative length ...
C
      DT=0.
C
C     * and draw an solid-filled arrowhead.
C
      B=(DP-.02)/DP
      A=1.0-B
      XT=A*X1+B*X2
      YT=A*Y1+B*Y2
C
      TX(1)=X2
      TY(1)=Y2
      TX(2)=XT-.01*DY/DP
      TY(2)=YT+.01*DX/DP
      TX(3)=XT
      TY(3)=YT
      TX(4)=XT+.01*DY/DP
      TY(4)=YT-.01*DX/DP
C
      CALL GSFACI (1)
      CALL GFA    (4,TX,TY)
C
C     * Done.
C
      RETURN
C
 1000 FORMAT(':F21:0',F3.2)
 1001 FORMAT(':F21:-0',F3.2)
 1002 FORMAT(':F21:0')
 1003 FORMAT(':F21:',F5.2)
C
      END
