      SUBROUTINE GGP_V_DATPRP(LX,LY,LXY,V,GG2,FLIP,VLO,VI,
     1           VSCAL,SPVAL,INCX,INCY,LYR,LXR) 

C     * SEP 18/2006 - F.MAJAESS (MODIFIED FOR TOLERANCE IN SPVAL)
C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C     This routine prepare data for V vector field.
      IMPLICIT NONE

      INTEGER LX,LY,LXY
      REAL V(*),GG2(*)
      LOGICAL FLIP
      REAL VLO,VI,VSCAL,SPVAL,SPVALT
C     Interval for vectors - along X/Y axis
      INTEGER INCX, INCY
      INTEGER LYR,LXR

      INTEGER ILGH,I,J,JR,NL,NR,NN,JJ,N,IPN

      PRINT*, "V_DATPRP"

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

C
C     * EXCHANGE TWO HALVES OF FIELD TO SHIFT LON 0 TO CENTRE
C
      IF (FLIP) THEN
        IF ( INT(LX/2.)*2 .EQ. LX ) THEN
          ILGH= LX/2
          DO 435 J=1,LY
            JR=(J-1)*LX
            DO 430 I=1,ILGH
              NL=JR+I
              NR=NL+ILGH
              V(NL)=GG2(NR)
              V(NR)=GG2(NL)
  430       CONTINUE
  435     CONTINUE
        ELSE
          ILGH=(LX-1)/2
          DO 445 J=1,LY
            JR=(J-1)*LX
            DO 440 I=1,ILGH
              NL=JR+I
              NR=NL+ILGH
              V(NL)=GG2(NR)
              V(NR)=GG2(NL)
  440       CONTINUE
            V(JR+LX)=V(JR+1)
  445     CONTINUE
        ENDIF
C       CALL MOVLEV(V,GG2,LXY)
        CALL SCOPY(LXY,V,1,GG2,1)
      ENDIF

C     * ZERO-OUT POINTS AND SCALE THE V FIELD.

      IF (INCX.LT.1 .OR. INCY.LT.1) THEN
        LXR=LX
        LYR=LY
        GO TO 480
      ELSE
        LXR=(LX-1)/INCX+1
        LYR=(LY-1)/INCY+1
      ENDIF

      IF (INCX.GT.1 .OR. INCY.GT.1)    THEN
        DO 460 I=1,LXR*LY
           IF(ABS(V(I)-SPVAL).GT.SPVALT) THEN
              V(I)=0.
           ENDIF
  460   CONTINUE
      ENDIF

      DO 470 J=1,LYR
        JJ=(J-1)*INCY+1
        DO 470 I=1,LXR
          N = I           + (JJ-1)*LXR
          NN=(I-1)*INCX+1 + (JJ-1)*LX
          IF(ABS(V(N)-   SPVAL).GT.SPVALT .AND.
     1       ABS(GG2(NN)-SPVAL).GT.SPVALT) THEN
             V(N)=GG2(NN)*VSCAL
          ELSE
             V(N)=SPVAL
          ENDIF
  470 CONTINUE

C     * ZERO-OUT THE POLE VALUES.

  480 IPN=(LY-1)*LXR

      END
