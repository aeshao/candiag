      SUBROUTINE ARDBD2 (X1,Y1,X2,Y2,IL,IR,IS) 

C     * AUG 23/07 - B.MIVILLE
C
C     * The routine ARDBD2 is to draw char and numbers besides the arrow 
C     * from (X1,Y1) to (X2,Y2), in the fractional coordinate system. 
C     * IL is budget value. IR,IS is the character value.
C
C     * Define variables required to write the area identifiers.
C
      REAL IL
      CHARACTER*11 CC 
      CHARACTER*10 CS
      CHARACTER*6 CS0
      CHARACTER*1 IC,IRC
C-----------------------------------------------------------------------------

C     
C     * Define some temporary X and Y coordinate arrays, to be used in 
C     * marking the head and tail of the arrow.
C
      CALL GSCR(1,0,0.,0.,0.)
C
C     * Compute the length of the arrow.  If it's zero, quit.
C
      DX=X2-X1
      DY=Y2-Y1
      DP=SQRT(DX*DX+DY*DY)
C     
      IF (DP.EQ.0.) RETURN 
C
C     * Arrage the position beside arrow, where we will write numbers.
C
      XC=.5*(X1+X2)
      YC=.5*(Y1+Y2)
C     
C     * Write budget value on the arrow.
C
      IF(IL.GT.0..AND.IL.LT.1.) THEN
         WRITE(CS,1000) IL 
      ELSEIF(IL.GT.-1..AND.IL.LT.0.) THEN
         WRITE(CS,1001) IL*(-1.0)
      ELSEIF(IL.EQ.0.) THEN
         WRITE(CS0,1002)
      ELSE
         WRITE(CS,1003) IL 
      ENDIF
      IF(IL.EQ.0.) THEN
         CALL PLCHHQ (XC,YC,CS0,.014,0.,0.)
      ELSE
         CALL PLCHHQ (XC,YC,CS,.014,0.,0.)
      ENDIF
      CALL PCSETI ('BF - BOX FLAG',0)
C
      WRITE(IRC,'(A1)') IR
      IF(IRC.EQ.'G') THEN
         WRITE (CC,1004) IR,IS
         CALL PLCHHQ (X1+0.05,Y1,CC,.020,0.,0.) 
      ELSE
         WRITE (CC,1004) IR,IS
         CALL PLCHHQ (X2-0.05,Y2,CC,.020,0.,0.) 
      ENDIF
C
C     * Done.
C
      RETURN
C
 1000 FORMAT(':F21:0',F3.2)
 1001 FORMAT(':F21:-0',F3.2)
 1002 FORMAT(':F21:0')
 1003 FORMAT(':F21:',F5.2)
 1004 FORMAT(':F21:',A1,':B1:',A1)
C     
      END
