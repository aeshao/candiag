      SUBROUTINE SETCBOX(RBM,IBC)

C     AUG 23/07 - B.MIVILLE
C
C     * SET BOX AROUND CHARACTER
C
C-----------------------------------------------------------------------------
      CALL PCSETI ('BF - BOX FLAG',7)
      CALL PCSETI ('BL - BOX LINE WIDTH',2)
      CALL PCSETR ('BM - BOX MARGIN',RBM)
      CALL PCSETR ('BX - BOX SHADOW X OFFSET',0.)
      CALL PCSETR ('BY - BOX SHADOW Y OFFSET',0.)
      CALL PCSETI ('BC(1) - BOX COLOR - BOX OUTLINE    ',IBC)
      CALL PCSETI ('BC(2) - BOX COLOR - BOX FILL       ',0)
      CALL PCSETI ('BC(3) - BOX COLOR - BOX SHADOW FILL',0)
C
      RETURN
      END
