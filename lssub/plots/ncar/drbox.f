      SUBROUTINE DRBOX (X,Y,SZX,SZY,ION)

C     * AUG 23/07 - B.MIVILLE
C     * Draw a color box with lower left corner (X,Y).
C     * Size of rectangular boxes are SZX and SZY.
C

      DIMENSION  A(5),B(5)
C-----------------------------------------------------------------------------
C
      CALL GSFAIS(1)
      A(1) = X
      B(1) = Y
      A(2) = X+SZX
      B(2) = Y
      A(3) = A(2)
      B(3) = Y+SZY
      A(4) = X
      B(4) = B(3)
      A(5) = A(1)
      B(5) = B(1)
      CALL GFA(4,A,B)
      CALL GSFAIS(0)
C     * DRAW LINE AROUND BAR (MAKING A BOX BAR)
      IF(ION.EQ.1) THEN
         CALL GSFACI(1)
         CALL GFA(4,A,B)
      ENDIF
C
      RETURN
      END
