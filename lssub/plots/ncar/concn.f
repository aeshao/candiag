      SUBROUTINE CONCN(FHIGHN, FLOWN, FINC, PB, DOLABEL)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

      IMPLICIT NONE

C CONtour CoNstruction routine - makes thick/thin lines

      REAL FHIGHN, FLOWN, FINC
      LOGICAL PB,DOLABEL

      INTEGER I, NCL, ICLU, NIPATS
      PARAMETER (NIPATS = 28)
      REAL FLVAL
      REAL TOL
      PARAMETER (TOL=1D-8)
      INTEGER NPATS,IARBI
      REAL ZLEVS(NIPATS),CWM
      COMMON /ARBI/ ZLEVS,NPATS,IARBI,CWM

C!!!      write(*,*)'dolabel: ',dolabel

C     Bigger contour labels?
C!!!      CALL CPSETR('LLS',0.04)

      IF(ABS(FINC).LT.TOL) THEN
         CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
         DO I=1,NCL
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
            CALL CPGETI('CLU - CONTOUR LEVEL USE FLAG', ICLU)
            IF(ICLU.EQ.3) THEN
               CALL CPSETR('CLL - CONTOUR LINE LINE WIDTH',2.)
            ELSE
               CALL CPSETR('CLL', 0.5)
            ENDIF
C           * Force one thickness if pub quality
            IF (PB) THEN
               CALL CPSETR('CLL', 2.)
            ENDIF
         ENDDO
      ELSE
         CALL CPGETI('NCL - NUMBER OF CONTOUR LEVELS', NCL)
         DO I=1,NCL
            CALL CPSETI('PAI - PARAMETER ARRAY INDEX', I)
            CALL CPGETR('CLV - CONTOUR LEVEL VALUES',FLVAL)
            IF(MOD(NINT(FLVAL/FINC),2).EQ.0.OR.IARBI.NE.0) THEN
               CALL CPSETR('CLL - CONTOUR LINE LINE WIDTH',2.)
               IF (DOLABEL) THEN
                  CALL CPSETI('CLU', 3)
               ELSE
                  CALL CPSETI('CLU', 1)
               ENDIF
            ELSE
                  CALL CPSETR('CLL', 0.5)
            ENDIF
C           * Force one thickness if pub quality
            IF (PB) THEN
               CALL CPSETR('CLL', 2.)
            ENDIF
         ENDDO
      ENDIF
      
      RETURN
      END
