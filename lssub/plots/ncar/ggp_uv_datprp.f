      SUBROUTINE GGP_UV_DATPRP(G,U,LX,LY,FLIP,VSCAL,SPVAL,INCX,INCY) 

C     * DEC 07/2010 - S.KHARIN (BASED ON GGP_U_DATPRP/GGP_V_DATPRP)

C     This routine prepare data for U/V vector field.
      IMPLICIT NONE

      INTEGER LX,LY
      REAL G(*),U(*)
      LOGICAL FLIP
      REAL VSCAL,SPVAL,SPVALT,OLD
C     Interval for vectors - along X/Y axis
      INTEGER INCX, INCY

      INTEGER ILGH,I,J,JR,NL,NR,IJ

      PRINT*, "UV_DATPRP"

C     * SETUP IN "SPVALT" THE TOLERANCE TO CHECK "SPVAL" AGAINST.

      SPVALT=1.E-6*SPVAL

      IF (FLIP) THEN
        IF ( MOD(LX,2).EQ.0 ) THEN

C         * LX=EVEN (NO CYCLIC MERIDIAN)

          ILGH= LX/2
          DO 235 J=1,LY
            JR=(J-1)*LX
            DO 230 I=1,ILGH
              NL=JR+I
              NR=NL+ILGH
              OLD=G(NL)
              G(NL)=G(NR)
              G(NR)=OLD
  230       CONTINUE
  235     CONTINUE
        ELSE

C         * LX=ODD (WITH CYCLIC MERIDIAN)

          ILGH=(LX-1)/2
          DO 245 J=1,LY
            JR=(J-1)*LX
            DO 240 I=1,ILGH
              NL=JR+I
              NR=NL+ILGH
              OLD=G(NL)
              G(NL)=G(NR)
              G(NR)=OLD
  240       CONTINUE
            G(JR+LX)=G(JR+1)
  245     CONTINUE
        ENDIF
      ENDIF

C     * ZERO-OUT POINTS AND SCALE THE U FIELD.

      DO 360 I=1,LX*LY
        U(I)=0.
  360 CONTINUE

      DO 370 J=1,LY,INCY
        DO 370 I=1,LX,INCX
          IJ=(J-1)*LX+I
          IF(ABS(G(IJ)-SPVAL).GT.SPVALT) THEN
             U(IJ)=G(IJ)*VSCAL
          ELSE
             U(IJ)=SPVAL
          ENDIF
  370 CONTINUE
      END
