      SUBROUTINE GGP_PSTPLT(PUB,MERPRLL,MAP)

C     * MAR 11/2004 - M.BERKLEY (MODIFIED for CCCMA)

C     Drawing continental outline, latitude and longtitude.
      IMPLICIT NONE

      LOGICAL PUB,MERPRLL,MAP
      
      INTEGER IWU,RWU
C Initialize Ezmap and add to area map

      IF (MAP) THEN
         IF (MERPRLL) THEN
            CALL MAPGRD
         ENDIF
         CALL MPSETI('LA', 0)
         CALL MAPLBL
         IF(PUB) THEN
            CALL GSLWSC(4.0)
         ELSE
            CALL GSLWSC(2.0)
         ENDIF
         CALL MAPLOT
      ENDIF
      
CDEBUG - useful to see if memory is running out.
C!!!      CALL CPGETI('IWU - Integer Workspace Usage',IWU)
C!!!      CALL CPGETI('RWU - Real Workspace Usage',RWU)
C!!!      WRITE(*,'("Integer workspace used: ",I8,
C!!!     1     " Real workspace used: ",I8)') IWU,RWU
CDEBUG

      RETURN
      END
