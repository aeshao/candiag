      SUBROUTINE BF1BI64(BF,IEEEF,NF)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * JUL 21/92 - J. STACEY - CORRECT SHIFT OF LEAST SIGNIFICANT BITS.
C     * MAY 22/92 - J. STACEY - REMOVE ASSUMPTION THAT INTEGER SIZE IS
C     *                         NECESSARILY THE SAME AS THE REAL SIZE.
C     * APR 15/92 - J. STACEY -
C
C     * CONVERT FLOATING POINT: IBM 64-BIT TO IEEE 64-BIT.
C
      IMPLICIT INTEGER (A-Z)
      INTEGER      BF(1),IEEEF(1)
      REAL*8       ZERO
      INTEGER      IZERO
      EQUIVALENCE (ZERO,IZERO)
      INTEGER      IEXP(15),IMAN(15)   !IEEE NORMALIZATION FACTORS.
      COMMON /MACHTYP/ MACHINE,INTSIZE
      DATA         ZERO/0.0E0/
      DATA         IEXP/-4,-3,-3,-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1/
      DATA         IMAN/ 0,-1,-1,-2,-2,-2,-2,-3,-3,-3,-3,-3,-3,-3,-3/
C------------------------------------------------------------------------
      MASK2 = IBITS(-1,0,7)          !7 BIT MASK FOR IBM EXPONENT.
      MASK4 = IBITS(-1,0,4)          !4 BIT MASK FOR FIRST HEX DIGIT.
      MASK5 = IBITS(-1,0,11)         !11 BIT MASK FOR IEEE EXPONENT.

      IF (INTSIZE .EQ. 1) THEN !64' INTEGERS/64' REALS.
      MBSE0 = 0
      MBSE63 = 63
      MASK1 = IBSET(MBSE0,MBSE63)    !1 BIT MASK FOR SIGN BIT.
      MBITM1 = -1
      MBIT0 = 0
      MBIT52 = 52
      MASK3 = IBITS(MBITM1,MBIT0,MBIT52) !52 BIT MASK FOR IEEE MANTISSA.
      DO 100 I=1,NF
        IF (BF(I) .NE. IZERO) THEN
            MSHFM52 = -52
            J = IAND(ISHFT(BF(I),MSHFM52),MASK4)
            MSHFM56 = -56
            MSHF52 = 52
            IEEEF(I)  = IOR(IOR(
     1        IAND(BF(I),MASK1),
     2        ISHFT(IAND(4*(IAND(ISHFT(BF(I),MSHFM56),MASK2)-64)
     3              +1023+IEXP(J),MASK5),MSHF52)
     4                         ),
     5        IAND(ISHFT(BF(I),IMAN(J)),MASK3)
     6                     )
        ELSE
            IEEEF(I) = 0
        END IF
  100 CONTINUE
      ELSE                    !32' INTEGERS/64' REALS.
      MBSE0 = 0
      MBSE31 = 31
      MASK1 = IBSET(MBSE0,MBSE31)      !1 BIT MASK FOR SIGN BIT.
      MASK3 = IBITS(-1,0,20)           !20 BIT MASK FOR IEEE MANTISSA.
      DO 101 I=1,NF*2,2
        IF (BF(I) .NE. IZERO) THEN
            MSHFM20 = -20
            J = IAND(ISHFT(BF(I),MSHFM20),MASK4)
            MSHFM24 = -24
            MSHF20 = 20
            IEEEF(I)  = IOR(IOR(
     1        IAND(BF(I),MASK1),
     2        ISHFT(IAND(4*(IAND(ISHFT(BF(I),MSHFM24),MASK2)-64)
     3              +1023+IEXP(J),MASK5),MSHF20)
     4                         ),
     5        IAND(ISHFT(BF(I),IMAN(J)),MASK3)
     6                     )
CBUG        IEEEF(I+1) = ISHFT(BF(I+1),IMAN(J))
            IEEEF(I+1) = IOR(
     1                       ISHFT(BF(I),32+IMAN(J)),
     2                       ISHFT(BF(I+1),IMAN(J))
     3                      ) 
        ELSE
            IEEEF(I) = 0
            IEEEF(I+1) = 0
        END IF
  101 CONTINUE
      END IF
      RETURN
      END
