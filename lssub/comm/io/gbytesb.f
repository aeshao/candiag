      SUBROUTINE GBYTESB(JPAK,J,NBITS,IDIM)
C
C     * JUL 07/03 - A.IRWIN,F.MAJAESS (REVISED FOR F90 AND LINUX PLATFORMS)
C     * JUN 16/92 - A.J. STACEY - MODIFIED TO HANDLE PARTIALLY PACKED WORDS
C     *                           WITHOUT OVERWRITING.
C     * APR 15/92 - A.J. STACEY - REWRITTEN GBYTESC ROUTINE UNPACKS
C     *                           INTEGERS.
C     *
C     * PURPOSE: GBYTESB UNPACKS INTEGERS FROM EITHER 32- OR 64-BIT
C     *          WORDS.
C     *          THIS ROUTINE IS COMPLETELY PORTABLE BETWEEN 32
C     *          AND 64-BIT MACHINES.
C     *
C     *          THIS ROUTINE IS THE INVERSE OF SBYTESB.
C     * 
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER J(1),JPAK(1)
      COMMON /MACHTYP/ MACHINE,INTSIZE
C-------------------------------------------------------------------------------
      NPACK32   = 32/NBITS
      MASK      = IBITS(-1,0,MIN(31,NBITS))
      IF (NPACK32 .LE. 0) THEN
         WRITE(6,6100)
 6100    FORMAT('0*** ERROR *** GBYTESB: NBITS > 32 IS ILLEGAL.')
         CALL ABORT
      END IF
      NPACK64        = 2*NPACK32
C
C     * THIS IS THE UNOPTIMIZED CODE.
C
C     JJ               = 1
C     II               = 1
C     DO 500 K=1,LEN64*MACHINE
C       DO 400 M=2/MACHINE-1,0,-1
C         DO 400 I=NPACK32-1,0,-1
C           IOFFSET       = M*32
C           J(II*INTSIZE) = IBITS(JPAK(JJ),I*NBITS+IOFFSET,NBITS)
C           II            = II + 1
C 400   CONTINUE
C       JJ             = JJ + 1
C 500 CONTINUE
C
C     * THIS IS THE OPTIMIZED CODE.  NOTE THE LOOP INVERSION.
C
      IRMDR1      = MOD(IDIM,NPACK64)
      IRMDR2      = MOD(IRMDR1,NPACK32)
      LEN64       = IDIM/NPACK64
      MLAST       = IRMDR1/NPACK32
      II          = 1
      INC         = NPACK32*2/MACHINE
      IF (MACHINE .EQ. 1) THEN            ! 64-BIT MACHINE.

        DO 400 M=1,0,-1
        DO 400 I=NPACK32-1,0,-1
          IPT = I*NBITS + M*32
          KPT = II
          DO 500 K=1,LEN64
C           J(KPT) = IBITS(JPAK(K),IPT,NBITS)!FAILS ON MIPS (SIGN BIT RETAINED)
            J(KPT) = IAND(ISHFT(JPAK(K),-IPT),MASK)
            KPT    = KPT + INC
  500     CONTINUE
          II = II + 1
  400   CONTINUE

        K   = LEN64 + 1
        IF (IRMDR1 .NE. 0) THEN          ! UNPACK PARTIAL WORD OF DATA.
          KPT = LEN64*NPACK64 + 1
          IF (MLAST .GT. 0) THEN         ! UNPACK NPACK32 PARCELS.
            DO 520 I=NPACK32-1,0,-1
             IPT    = I*NBITS + 32
C            J(KPT) = IBITS(JPAK(K),IPT,NBITS)!FAILS ON MIPS (SIGN BIT RETAINED)
             J(KPT) = IAND(ISHFT(JPAK(K),-IPT),MASK)
             KPT    = KPT + 1
  520       CONTINUE
          END IF
C
C         * UNPACK REMAINDER OF  DATA.
C
          MLEFT = 0
          IF (MLAST .EQ. 0) MLEFT = 1
          DO 540 I=NPACK32-1,NPACK32-IRMDR2,-1
            IPT    = I*NBITS + MLEFT*32
C           J(KPT) = IBITS(JPAK(K),IPT,NBITS)!FAILS ON MIPS (SIGN BIT RETAINED)
            J(KPT) = IAND(ISHFT(JPAK(K),-IPT),MASK)
            KPT    = KPT + 1
  540     CONTINUE
        END IF

      ELSE IF (MACHINE .EQ. 2) THEN        ! 32-BIT MACHINE.

        LEN32  = IDIM/NPACK32
        IRMDR2 = MOD(IDIM,NPACK32)
        INC2   = INC*INTSIZE
        DO 700 I=NPACK32-1,0,-1
          IPT  = I*NBITS
          KPT  = II*INTSIZE
          DO 800 K=1,LEN32
C           J(KPT) = IBITS(JPAK(K),IPT,NBITS)!FAILS ON MIPS (SIGN BIT RETAINED)
            J(KPT) = IAND(ISHFT(JPAK(K),-IPT),MASK)
            KPT    = KPT + INC2
  800     CONTINUE
          II = II + 1
  700   CONTINUE

        IF (IRMDR2 .NE. 0) THEN          ! UNPACK REMAINDER OF  DATA.
          K        = LEN32 + 1
          KPT      = (IDIM - IRMDR2 + 1)*INTSIZE
          DO 840 I=NPACK32-1,NPACK32-IRMDR2,-1
            IPT    = I*NBITS
C           J(KPT) = IBITS(JPAK(K),IPT,NBITS) !FAILS ON MIPS (SIGN BIT RETAINED)
            J(KPT) = IAND(ISHFT(JPAK(K),-IPT),MASK)
            KPT    = KPT + INTSIZE
  840     CONTINUE
        END IF

      ELSE

         WRITE(6,6200)
 6200    FORMAT('0*** ERROR *** GBYTESB: ILLEGAL MACHINE TYPE.')
         CALL ABORT

      END IF
C
      RETURN
      END
