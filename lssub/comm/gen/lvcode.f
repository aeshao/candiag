      SUBROUTINE LVCODE (IBUF4,ETA,NLEV) 
C 
C     * JAN 22/93 - E.CHAN. 
C     * 
C     * GENERATES CODED LEVEL INFORMATION TO BE INCLUDED IN CCRN
C     * STANDARD LABELS.
C     * 
C     * ETA = ETA COORDINATE OR PRESSURE IN MB DIVIDED BY 1000. 
C     * IBUF4 = CODED LABEL FOR LEVEL IN THE FOLLOWING FORMS: 
C     *   1)  -XAAA = A.AA E-X     IF ETA <  .010 
C     *   2)   AAAA = AAAA         IF ETA >= .010 
C     * 
C     * USE SUBROUTINE LVDCODE FOR REVERSE OPERATION. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION IBUF4(NLEV), ETA(NLEV) 
      CHARACTER STRING*9
C-----------------------------------------------------------------------
      DO 100 L = 1, NLEV
C
C       * CONVERT TO VERTICAL COORDINATE (PRESSURE IN MB OR ETA*1000).
C
        VC = 1000.E0 * ETA(L)
C
        IF (VC .GE. 10.E0) THEN 
C
C         * CASE 1: VC >= 10
C
          IBUF4(L) = INT(VC + 0.5E0) 
C 
        ELSEIF ( VC .GT. 1.E-9 ) THEN
C
C         * CASE 2: 10 > VC > 1E-9
C
C         * THE INTEGER EXPONENT IX AND MANTISSA IAAA ARE GENERATED FROM
C         * THE REAL NUMBER VC BY FIRST WRITING VC AS CHARACTER DATA INTO 
C         * STRING. IX AND IAAA ARE THEN EXTRACTED FROM THE APPROPRIATE
C         * LOCATIONS IN STRING.
C 
          WRITE (STRING,2000) VC 
          READ  (STRING,2010) IAAA, IXP1
          IX = IXP1 - 1 
          IBUF4(L) = IX*1000 - IAAA 
C 
        ELSE
C
C         CASE 3: 1E-9 >= VC >= 1E-11
C 
          IBUF4(L) = -9000 - INT( VC*1.E11 + 0.5E0 )
C 
        ENDIF 
C
  100 CONTINUE
C
      RETURN
C-----------------------------------------------------------------------
 2000 FORMAT (E9.3) 
 2010 FORMAT (2X,I3,1X,I3)
      END 
