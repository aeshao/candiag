      SUBROUTINE EPSCAL(EPSI,LSR,LM)
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * JUL  2/79 - J.D.HENDERSON 
C     * COMPUTES ARRAY EPSILON USED IN THE SPECTRAL MODEL.
C     * EPSILON(N,M)=SQRT((N**2-M**2)/(4*N**2-1)) 
C     * LSR CONTAINS ROW START INFO.
C 
C     * LEVEL 2,EPSI
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 EPSI(1)
      INTEGER LSR(2,1)
C---------------------------------------------------------------------
      DO 220 M=1,LM 
      MS=M-1
      KL=LSR(2,M) 
      KR=LSR(2,M+1)-1 
      K1=KL 
      IF(M.EQ.1) K1=2 
      DO 220 K=K1,KR
      NS=MS+(K-KL)
      FNUM=FLOAT(NS**2-MS**2) 
      FDEN=FLOAT(4*NS**2-1) 
  220 EPSI(K)=SQRT(FNUM/FDEN) 
      EPSI(1)=0.E0
C 
      RETURN
      END
