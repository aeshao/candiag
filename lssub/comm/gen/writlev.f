      SUBROUTINE WRITLEV(ALEV,NLEV,LABEL)
C 
C     * JAN 22/93 - E.CHAN.   
C
C     * PRINTS OUT LEVELS WITH FORMATS DEPENDING ON THE VALUE.
C     * 
C     * ALEV    = ARRAY OF LEVELS.
C     * NLEV    = NUMBER OF LEVELS.
C     * LABEL   = CHARACTER LABEL IDENTIFYING THE TYPE OF LEVEL
C     *           (E.G. LABL='ETA:').
C     * 
C     *          VALUE                      FORMAT
C     *          -----                      ------
C     * 
C     *           ALEV  <  .001              1PE9.2 
C     *     .01 > ALEV  >= .001                F9.5
C     *           ALEV  >= .01                 F7.3 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION ALEV(NLEV)
      CHARACTER*4 LABEL
C------------------------------------------------------------------
C
C     * DETERMINE NUMBER OF LEVELS IN EACH OF THREE CATEGORIES.
C
      NHI = 0
      NMID = 0
      DO 100 L=1,NLEV
        IF (ALEV(L) .LT. 0.001E0) THEN 
          NHI = NHI + 1
        ELSEIF (ALEV(L) .LT. 0.01E0) THEN
          NMID = NMID + 1
        ENDIF
  100 CONTINUE
      NLOW=NLEV-NHI-NMID
C
C     * PRINT OUT LEVELS.
C
      WRITE(6,6000) NLEV,LABEL,(ALEV(L),L=1,NHI)
      WRITE(6,6005) (ALEV(NHI+L),L=1,NMID)
      WRITE(6,6010) (ALEV(NHI+NMID+L),L=1,NLOW)
C
      RETURN
C------------------------------------------------------------------
 6000 FORMAT (I4,1X,A4,' LEVELS',/,(5X,10(1PE9.2)))
 6005 FORMAT (5X,10F9.5)
 6010 FORMAT (3X,10F9.3)
      END
