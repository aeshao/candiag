      SUBROUTINE SDET (COSSD,SINSD,MNTH,DAY)
C
C     * MAR 26/98 F.MAJAESS (REMOVE ZMONTH(12),AMONTH)
C     * APR 10/81 - J.D.HENDERSON 
C     THIS ROUTINE CALCULATES THE SOLAR DECLINATION FOR A GIVEN DAY.
C     ------------------------------------------------------------------
C 
C     D  I  C  T  I  O  N  A  R  Y     (REFER TO SDET AND SDET1). 
C 
C       COSSD - COSINE OF SOLAR DECLINATION. 
C       COSL - COSINE OF LATITUDE 
C       COSZ - COSINE OF SOLAR ZENITH ANGLE.
C       DAY - DAY COUNTER -1. 
C       DAYPYR - DAYS IN YEAR.
C       DEC - 23.5*PI/180*COS(2*PI*(DY-173)/365),SOLAR DECLINATION. 
C       DECMAX - 23.5*PI/180, MAXIMUM SOLAR DECLINATION(RADIANS). 
C       DLON - EAST/WEST GRID-POINT SEPARATION.(RADIANS)
C       DY - DAY COUNTER. 
C       EQNX - EQUINOX,22 JUNE (=173).
C       HACOS - SOLAR ZENITH ANGLE PARAMETER. 
C       IM - MAXIMUM NUMBER OF EAST/WEST GRID-POINTS. 
C       IX - LONGITUDINAL GRID-POINT INDEX. 
C       JDYACC - VARIABLE FOR DAY OF MONTH DETERMINATION. 
C       JY - LATITUDINAL GRID-POINT INDEX.
C       LVAL - MONTH INDEX. 
C       MAXDAY - MAXIMUM ALLOWED DAY IN YEAR. 
C       MNTHDY - IDENTIFICATION FOR DAY OF MONTH. 
C       MONTH - DAYS IN EACH MONTH, BEGINNING WITH JANUARY. 
C       MNTH - MONTH OF THE YEAR FOR GIVEN DAY.(JAN=1, FEB=2,...,DEC=12)
C       ROT - HOUR OF DAY (CONVERTED TO RADIANS). 
C       ROTPER - PERIOD OF SOLAR ROTATION.(=24) 
C       SDEDY - DAY COUNTER.
C       SDEYR - YEAR COUNTER. 
C       SEASON - (DY-173)/365, TIME PARAMETER IN SOLAR DECLINATION. 
C       SINSD - SINE OF SOLAR DECLINATION. 
C       SINL - SINE OF LATITUDE 
C       TOFDAY - TIME OF DAY COUNTER (GREENWICH HOURS). 
C 
C     ------------------------------------------------------------------
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER MONTH(12)
C 
C     ------------------------------------------------------------------
      DATA  MONTH/31,28,31,30,31,30,31,31,30,31,30,31/
C     ------------------------------------------------------------------
C 
      DATA DAYPYR/365.E0/,DECMAX/0.4101523743E0/,EQNX/173.E0/ 
      DATA PI/3.1415926535898E0/
C-------------------------------------------------------------------- 
      MAXDAY=DAYPYR + 1.0E-2
      JDY=DAY+1.0E-2
      JYR=JDY/MAXDAY
      JDY=JDY-JYR*MAXDAY
211   JDYACC=0
      DO 251 L=1,12 
      LVAL=L
      JDYACC=JDYACC+MONTH(L)
      IF(JDY.LE.JDYACC) GO TO 241 
  251 CONTINUE
  241 MNTHDY=MONTH(LVAL)-JDYACC+JDY 
      MNTH=LVAL 
      DY=JDY
      SEASON=(DY-EQNX)/DAYPYR 
C     ------------------------------------------------------------------
C     EQNX = JUNE 22
C     APIHELION = JULY 1
C     ------------------------------------------------------------------
      DEC=DECMAX*COS(2.0E0*PI*SEASON) 
      SINSD=SIN(DEC) 
      COSSD=COS(DEC) 
C 
      RETURN
      END 
