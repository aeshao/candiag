      SUBROUTINE PTOX 
     1           (NF1,NF2,KOX,IMO,X,Y,GZS,SP,LA,NPL,NSL,
     2            AG,BG,AH,BH,NPL1,FPCOL,DFDLNP,DLNP, 
     3            LP,LG,LH,SIG,PRLOG, IBUF,MAXP8)

C     * CONVERTS NPL PRESSURE LEVELS OF GAUSSIAN GRIDS (ILG1,ILAT)
C     * ON FILE NF1 TO NSL ETA LEVELS ON FILE NF2 FOR HYBRID MODEL. 
C     * PRESSURE LEVELS IN PR ARE ORDERED TOP DOWN. 

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      LOGICAL OK
  
      REAL X(LA,NPL), GZS(LA), SP(LA)
  
      REAL AG    (NSL),BG    (NSL),AH     (NSL) ,BH   (NSL) 
      REAL FPCOL (NPL ),DFDLNP (NPL1),DLNP (NPL) 
      REAL SIG   (NSL ),PRLOG (NPL )
      INTEGER LP (NPL ),LG    (NSL ),LH     (NSL )
      INTEGER IBUF(MAXP8) 
C-------------------------------------------------------------------- 
      MAXX = MAXP8-8 
C
      DO 410 L=1,NPL
          CALL GETFLD2(NF1,X(1,L),NC4TO8("GRID"),IMO,KOX,LP (L),
     1                                             IBUF,MAXX,OK)
          IF(.NOT.OK) CALL XIT('PTOX',-1) 
          WRITE(6,6025) (IBUF(IND),IND=1,8)
  410 CONTINUE
  
C     * INTERPOLATE TO ETA LEVELS. 

      RLUP = 0.E0 
      RLDN = 0.E0 
      CALL PAEL  (X,LA,SIG,NSL, X,PRLOG,NPL,SP,RLUP,RLDN, 
     1            AH,BH,NPL+1,FPCOL,DFDLNP ,DLNP )
C
      IBUF(2)=IMO
      IBUF(3)=KOX
      DO 420 K=1,NSL
         IBUF(4)=LH(K)
         CALL PUTFLD2(NF2,  X(1,K),IBUF,MAXX)
         WRITE(6,6026) (IBUF(IND),IND=1,8)
  420 CONTINUE
C---------------------------------------------------------------------------
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
 6026 FORMAT(' ',60X,A4,I6,1X,A4,5I6) 
      RETURN
      END
