      SUBROUTINE BPFT(PEE,T,PS,PHIS,LA,ILEV,TMEAN,RGAS,SF)
C 
C     * AVR 12/85 - B.DUGAS. (VECTORISER...)
C     * JUN 26/79 - J.D.HENDERSON 
C     * CALCULATES PEE(LA,ILEV) FROM T,PS,PHIS BY INTEGRATING 
C     *  UP FROM THE SURFACE. 
C     * PEE AND T MAY BE EQUIVALENCED.
C 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX PEE(LA,1),T(LA,1),PS(1),PHIS(1) 
      REAL TMEAN(1),SF(1) 
C-------------------------------------------------------------------- 
      ILEVM=ILEV-1
C 
      PEE(1,ILEV)= RGAS*(T(1,ILEV)*SF(ILEV) + TMEAN(ILEV)*PS(1))
C 
      DO 110 MN=2,LA
         PEE(MN,ILEV) = RGAS*(T(MN,ILEV)*SF(ILEV) + TMEAN(ILEV)*PS(MN)) 
     1                + PHIS(MN)
  110 CONTINUE
C 
      DO 130 IH=ILEVM,1,-1
         DO 130 MN=1,LA 
            PEE(MN,IH)=PEE(MN,IH+1)+RGAS*(TMEAN(IH)-TMEAN(IH+1))*PS(MN) 
     1                + RGAS*T(MN,IH)*SF(IH)
  130 CONTINUE
C 
      RETURN
      END 
