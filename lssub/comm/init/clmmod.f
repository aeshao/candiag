      SUBROUTINE CLMMOD(GT,SICN,SIC,GC,TIME,
     1                  NFDM,MMD,NYYMM,NM,ILG1,ILAT,
     2                  AA,BB,CC,DD,A1,B1,C1,D1,A2,B2,C2,D2,
     3                  SICN_MIN,SICN_CRT,SST_FRZ,SIC_MIN)

      IMPLICIT NONE
C
C     JAN 08/04 L.SOLHEIM  - REMOVED UNUSED ARRAYS GT1,SIC1,SICN1
C                          - ADDED IMPLICIT NONE
C     * DEC 18/97 - J.SHENG, M.LAZARE.
C     * PROCESSES GT,SIC AND SICN TO CONSERVE LONG-TERM AVERAGE
C     * OBSERVED MONTHLY MEANS UPON INTERPOLATION LINEARLY WITH TIME.

      INTEGER NM,ILG1,ILAT
      INTEGER NFDM(NM),MMD(NM),NYYMM(NM)
      REAL SICN_MIN,SICN_CRT,SST_FRZ,SIC_MIN
      REAL AA(NM,NM),BB(NM),CC(NM,NM),DD(NM)
      REAL A1(NM,NM),B1(NM),C1(NM,NM),D1(NM)
      REAL A2(NM,NM),B2(NM),C2(NM,NM),D2(NM)
      REAL GC(ILG1,ILAT,NM)
      REAL GT(ILG1,ILAT,NM)
      REAL SIC(ILG1,ILAT,NM)
      REAL SICN(ILG1,ILAT,NM)
      REAL TIME(ILG1,ILAT,NM)

C     * LOCAL

      INTEGER I,J,KM,KM1,KM2,KMM,KMP,MMD_KMM,MMD_KMP,NFDM_KMP
      INTEGER IA1,IA2,II
      REAL T1,T2,T3,AMN,AMX,DT1,DT2,DT3,DT4,DT5,DT6,TIME_KMM

C
C     SICN_MIN: SPECIFIED VALUE OF SICN WHEN ICE STARTS TO FORM OR STOPS
C               TO EXIST AT THE MIDDLE OF A MONTH
C     SICN_CRT: CRITICAL VALUE FOR A GRID TO TO BE CONSIDERED AS GC=1.0
C     SST_FRZ:  SST AT THE BEGINNING OR THE END OF A GC=1.0 PERIOD
C     SIC_MIN:  SPECIFIED MINIMUM SIC WHEN GC=1.0
C
C     LOOP FOR PRE-PROCESSING SICN FIRST
C
      DO 2100 J=1,ILAT
       DO 2100 I=1,ILG1
C
C     SPECIFY MATRIX AA FOR SICN
C
        DO 2010 KM1=1,NM
         DO 2010 KM2=1,NM
          AA(KM1,KM2)=0.0E0
 2010    CONTINUE
         DO 2020 KM=1,NM
          KMM=KM-1
          IF(KM.EQ.1)KMM=NM
          KMP=KM+1
          IF(KM.EQ.NM)KMP=1
          MMD_KMM=MMD(KMM)
          IF(KM.EQ.1)THEN
           MMD_KMM=MMD_KMM-365
          ENDIF
          NFDM_KMP=NFDM(KMP)
          MMD_KMP=MMD(KMP)
          IF(KM.EQ.NM)THEN
           NFDM_KMP=NFDM_KMP+365
           MMD_KMP=MMD_KMP+365
          ENDIF
          DT1=REAL(NFDM(KM)-MMD_KMM)
          DT2=REAL(MMD(KM)-NFDM(KM))
          DT3=REAL(NFDM_KMP-MMD(KM))
          DT4=REAL(MMD_KMP-NFDM_KMP)
          IF(DT1.LE.0.0E0.OR.DT2.LE.0.0E0.OR.
     &     DT3.LE.0.0E0.OR.DT4.LE.0.0E0)THEN
           WRITE(6,*)' DT1,DT2,DT3,DT4=',DT1,DT2,DT3,DT4
           CALL                                   XIT('CLMMOD',-1)
          ENDIF
C
C     CASE 1, WATER OR LAND POINTS
C
          IF(SICN(I,J,KM).LE.0.0E0)THEN
           AA(KM,KMM)=0.0E0
           AA(KM,KM)=1.0E0
           AA(KM,KMP)=0.0E0
           DD(KM)=0.0E0
C
C     CASE 2, ICE POINTS
C
C     CASE 2.1, FIRST MONTH ICE
C
          ELSEIF(SICN(I,J,KMM).LE.0.0E0.AND.SICN(I,J,KMP).GT.0.0E0)THEN
           AA(KM,KMM)=0.0E0
           AA(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           AA(KM,KMP)=0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
           DD(KM)=SICN(I,J,KM)
     &           -SICN_MIN*0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
C
C     CASE 2.2, CONTINUOUS ICE
C
          ELSEIF(SICN(I,J,KMM).GT.0.0E0.AND.SICN(I,J,KMP).GT.0.0E0)THEN
           AA(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
           AA(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           AA(KM,KMP)=0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
           DD(KM)=SICN(I,J,KM)
C
C     CASE 2.3, LAST MONTH ICE
C
          ELSEIF(SICN(I,J,KMM).GT.0.0E0.AND.SICN(I,J,KMP).LE.0.0E0)THEN
           AA(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
           AA(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           AA(KM,KMP)=0.0E0
           DD(KM)=SICN(I,J,KM)
     &           -SICN_MIN*0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
C
C     CASE 2.4, SINGLE MONTH ICE
C 
          ELSEIF(SICN(I,J,KMM).LE.0.0E0.AND.SICN(I,J,KMP).LE.0.0E0)THEN
           AA(KM,KMM)=0.0E0
           AA(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           AA(KM,KMP)=0.0E0
           DD(KM)=SICN(I,J,KM)
     &           -SICN_MIN*0.5E0*(DT2*DT2/(DT1+DT2)
     &                         +DT3*DT3/(DT3+DT4))/(DT2+DT3)
          ELSE
           CALL                                   XIT('CLMMOD',-2)
          ENDIF
 2020    CONTINUE
C
C     INVERT THE MATRIX FOR SICN
C
         DO 2030 KM2=1,NM
          DO 2030 KM1=1,NM
           CC(KM1,KM2)=AA(KM1,KM2)
 2030    CONTINUE
         DO 2040 KM=1,NM
          BB(KM)=0.0E0
 2040    CONTINUE
         CALL RGAUSSJ(CC,NM,NM,BB,1,1)
C
         DO 2050 KM1=1,NM
          BB(KM1)=0.0E0
          DO 2050 KM2=1,NM
           BB(KM1)=BB(KM1)+CC(KM1,KM2)*DD(KM2)
 2050    CONTINUE
C
C     SIGN NOW CONTAINS THE TARGET VALUES FOR SEA-ICE CONCENTRATION
C
        DO 2100 KM=1,NM
         SICN(I,J,KM)=BB(KM)
 2100 CONTINUE
C
C
C     SEARCH FOR POINTS WHER SICN .GT. 1.0 OR SICN .LT. 0.0
C
      DO 2450 KM=1,NM
       IA1=0
       IA2=0
       AMX=0.0E0
       AMN=1.0E0
       DO 2410 J=1,ILAT
        DO 2410 I=1,ILG1
         IF(SICN(I,J,KM).GT.1.0E0)IA1=IA1+1
         IF(SICN(I,J,KM).LT.0.0E0)IA2=IA2+1
         IF(SICN(I,J,KM).GT.AMX)AMX=SICN(I,J,KM)
         IF(SICN(I,J,KM).LT.AMN)AMN=SICN(I,J,KM)
 2410  CONTINUE
       WRITE(6,*)" KM,IA1,IA2,AMX,AMX=",KM,IA1,IA2,AMX,AMN
 2450 CONTINUE
C
      DO 3900 J=1,ILAT
       DO 3900 I=1,ILG1
C
C     DETERMINE THE JULIAN DAY GC SWITCHES BETWEEN 0.0 AND 1.0
C
        DO 3100 KM=1,NM
         KMP=KM+1
         IF(KM.EQ.NM)KMP=1
         T1=REAL(MMD(KM))
         T2=REAL(MMD(KMP))
         T3=REAL(NFDM(KMP))
         IF(KM.EQ.NM)T2=T2+365.0E0
         IF(KM.EQ.NM)T3=T3+365.0E0
         DT1=REAL(MMD(KMP)-MMD(KM))
         IF(DT1.LE.0.0E0)DT1=DT1+365.0E0
C     
C     GC SWITCHING FROM 0.0 TO 1.0
C          
         IF(SICN(I,J,KM).LE.SICN_CRT.AND.
     &     SICN(I,J,KMP).GT.SICN_CRT)THEN
          TIME(I,J,KM)=T1
     &    +(SICN_CRT-SICN(I,J,KM))*DT1/(SICN(I,J,KMP)-SICN(I,J,KM))
          IF(GC(I,J,KM).GT.0.5E0.AND.TIME(I,J,KM).LT.T1)THEN
           TIME(I,J,KM)=T3
     &     +(SICN_CRT-SICN_MIN)*DT2/(SICN(I,J,KMP)-SICN_MIN)
          ENDIF
C
C     GC SWITCHING FROM 1.0 TO 0.0
C
         ELSEIF(SICN(I,J,KM).GT.SICN_CRT.AND.
     &         SICN(I,J,KMP).LE.SICN_CRT)THEN
          TIME(I,J,KM)=T1
     &    +(SICN_CRT-SICN(I,J,KM))*DT1/(SICN(I,J,KMP)-SICN(I,J,KM))
          IF(GC(I,J,KMP).GT.0.5E0.AND.TIME(I,J,KM).GT.T2)THEN
           TIME(I,J,KM)=T1
     &     +(SICN_CRT-SICN(I,J,KM))*DT1/(SICN_MIN-SICN(I,J,KM))
          ENDIF
C
C     NO CHANGE IN GC
C
         ELSE
          TIME(I,J,KM)=-999.0E0
         ENDIF
 3100   CONTINUE
C
C     SPECIFY THE MATRICES FOR GT AND SIC
C     A1, D1: MATRIX AND RIGHT-HAND-SIDE TERM FOR GT
C     A2, D2: MATRIX AND RIGHT-HAND-SIDE TERM FOR SIC
C
        DO 3200 KM2=1,NM
        DO 3200 KM1=1,NM
         A1(KM1,KM2)=0.0E0
         A2(KM1,KM2)=0.0E0
 3200   CONTINUE
        DO 3300 KM=1,NM
         KMM=KM-1
         IF(KM.EQ.1)KMM=NM
         KMP=KM+1
         IF(KM.EQ.NM)KMP=1
         TIME_KMM=TIME(I,J,KMM)
         MMD_KMM=MMD(KMM)
         IF(KM.EQ.1)THEN
          TIME_KMM=TIME_KMM-365.0E0
          MMD_KMM=MMD_KMM-365
         ENDIF
         NFDM_KMP=NFDM(KMP)
         MMD_KMP=MMD(KMP)
         IF(KM.EQ.NM)THEN
          NFDM_KMP=NFDM_KMP+365
          MMD_KMP=MMD_KMP+365
         ENDIF
         DT1=REAL(NFDM(KM)-MMD_KMM)
         DT2=REAL(MMD(KM)-NFDM(KM))
         DT3=REAL(NFDM_KMP-MMD(KM))
         DT4=REAL(MMD_KMP-NFDM_KMP)
         IF(DT1.LE.0.0E0.OR.DT1.GT.16.0E0.OR.
     &      DT2.LE.0.0E0.OR.DT2.GT.16.0E0.OR.
     &      DT3.LE.0.0E0.OR.DT3.GT.16.0E0.OR.
     &      DT4.LE.0.0E0.OR.DT4.GT.16.0E0)THEN
          WRITE(6,*)' DT1,DT2,DT3,DT4=',DT1,DT2,DT3,DT4
          CALL                                    XIT('CLMMOD',-3)
         ENDIF
C
C     CASE 1, LANAD POINTS
C     
         IF(GC(I,J,KM).LT.-0.5E0)THEN
          A1(KM,KMM)=0.0E0
          A1(KM,KM)=1.0E0
          A1(KM,KMP)=0.0E0
          D1(KM)=GT(I,J,KM)
          A2(KM,KMM)=0.0E0
          A2(KM,KM)=1.0E0
          A2(KM,KMP)=0.0E0
          D2(KM)=0.0E0
C
C     CASE 2, WATER POINTS
C
          ELSEIF(SICN(I,J,KM).LE.SICN_CRT)THEN
C
C     CASE 2.1, CONTINUOUS WATER
C
          IF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &       SICN(I,J,KMP).LE.SICN_CRT)THEN
           A1(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
           A1(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           A1(KM,KMP)=0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
           D1(KM)=GT(I,J,KM)
C
C     CASE 2.2, ICE STARTS IN THE FIRST HALF OF NEXT MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME(I,J,KM).GT.REAL(NFDM_KMP))THEN
           DT5=TIME(I,J,KM)-REAL(NFDM_KMP)
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-4)
           ENDIF
           A1(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
           A1(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT5/(DT3+DT5))*DT3)/(DT2+DT3)
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)-SST_FRZ*0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT5))
C
C     CASE 2.3, ICE ENDS IN THE SECOND HALF OF PREVIOUS MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME_KMM.LE.REAL(NFDM(KM)))THEN
           DT5=REAL(NFDM(KM))-TIME_KMM
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-5)
           ENDIF
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=0.5E0*((1.0E0+DT5/(DT5+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           A1(KM,KMP)=0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
           D1(KM)=GT(I,J,KM)-SST_FRZ*0.5E0*DT2*DT2/((DT2+DT3)*(DT5+DT2))
C
C     CASE 2.4, ICE STARTS IN THE SECOND HALF OF CURRENT MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME(I,J,KM).LE.REAL(NFDM_KMP))THEN
           DT5=TIME(I,J,KM)-REAL(MMD(KM))
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-6)
           ENDIF
           A1(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT5)*(DT1+DT2))
           A1(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2+DT5)/(DT2+DT5)
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)-SST_FRZ*0.5E0*DT5/(DT2+DT5)
C
C     CASE 2.5, ICE ENDS IN THE FIRST HALF OF CURRENT MONTH
C      
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME_KMM.GT.REAL(NFDM(KM)))THEN
           DT5=REAL(MMD(KM))-TIME_KMM
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-7)
           ENDIF
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=0.5E0*(DT5+(1.0E0+DT4/(DT3+DT4))*DT3)/(DT5+DT3)
           A1(KM,KMP)=0.5E0*DT3*DT3/((DT5+DT3)*(DT3+DT4))
           D1(KM)=GT(I,J,KM)-SST_FRZ*0.5E0*DT5/(DT5+DT3)
C
C     CASE 2.6, ICE ENDS IN THE SECOND HALF OF PREVIOUS MONTH
C     AND RESTARTS IN THE SECOND HALF OF CURRENT MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME_KMM.LE.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).LE.REAL(NFDM_KMP))THEN
           DT5=REAL(NFDM(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(MMD(KM))
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-8)
           ENDIF
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=0.5E0*((1.0E0+DT5/(DT2+DT5))*DT2+DT6)/(DT2+DT6)
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)
     &     -SST_FRZ*0.5E0*(DT2*DT2/(DT2+DT5)+DT6)/(DT2+DT6)
C
C     CASE 2.7, ICE ENDS IN THE FIRST HALF OF CURRENT MONTH
C     AND RESTARTS IN THE FIRST HALF OF NEXT MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME_KMM.GT.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).GT.REAL(NFDM_KMP))THEN
           DT5=REAL(MMD(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(NFDM_KMP)
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-9)
           ENDIF
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=0.5E0*(DT5+(1.0E0+DT6/(DT3+DT6))*DT3)/(DT3+DT5)
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)
     &     -SST_FRZ*0.5E0*(DT5+DT3*DT3/(DT3+DT6))/(DT3+DT5)
C
C     CASE 2.8, ICE ENDS IN THE FIRST HALF AND RESTARTS IN THE SECOND
C     HALF OF CURRENT MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME_KMM.GT.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).LE.REAL(NFDM_KMP))THEN
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=0.5E0
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)-SST_FRZ*0.5E0
C
C     CASE 2.9, ICE ENDS IN THE SECOND HALF OF PREVIOUS MONTH
C     AND RESTARTS IN THE FIRST HALF OF NEXT MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME_KMM.LE.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).GT.REAL(NFDM_KMP))THEN
           DT5=REAL(NFDM(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(NFDM_KMP)
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-10)
           ENDIF
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=0.5E0*((1.0E0+DT5/(DT5+DT2))*DT2
     &                   +(1.0E0+DT6/(DT3+DT6))*DT3)/(DT2+DT3)
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)-SST_FRZ*0.5E0*(DT2*DT2/(DT5+DT2)
     &                                   +DT3*DT3/(DT3+DT6))/(DT2+DT3)
C
C     NO MORE CASES ON WATER POINTS
C
          ELSE
           WRITE(6,*)' GC=',GC(I,J,KM)
           WRITE(6,*)' SICN=',SICN(I,J,KMM),SICN(I,J,KM),SICN(I,J,KMP)
           WRITE(6,*)' TIME=',TIME_KMM,TIME(I,J,KM)
           CALL                                   XIT('CLMMOD',-11)
          ENDIF
          A2(KM,KMM)=0.0E0
          A2(KM,KM)=1.0E0
          A2(KM,KMP)=0.0E0
          D2(KM)=0.0E0
C
C     CASE 3, ICE POINTS
C
          ELSEIF(SICN(I,J,KM).GT.SICN_CRT)THEN
           A1(KM,KMM)=0.0E0
           A1(KM,KM)=1.0E0
           A1(KM,KMP)=0.0E0
           D1(KM)=GT(I,J,KM)
C
C     CASE 3.1, CONTINUOUS ICE
C
          IF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &       SICN(I,J,KMP).GT.SICN_CRT)THEN
           A2(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
           A2(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           A2(KM,KMP)=0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
           D2(KM)=SIC(I,J,KM)
C
C     CASE 3.2, WATER STARTS IN THE FIRST HALF OF NEXT MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME(I,J,KM).GT.REAL(NFDM_KMP))THEN
           DT5=TIME(I,J,KM)-REAL(NFDM_KMP)
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-12)
           ENDIF
           A2(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT3)*(DT1+DT2))
           A2(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2
     &                   +(1.0E0+DT5/(DT3+DT5))*DT3)/(DT2+DT3)
           A2(KM,KMP)=0.0E0
           D2(KM)=SIC(I,J,KM)-SIC_MIN*0.5E0*DT3*DT3/
     &       ((DT2+DT3)*(DT3+DT5))
C
C     CASE 3.3, WATER ENDS IN THE SECOND HALF OF PREVIOUS MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME_KMM.LE.REAL(NFDM(KM)))THEN
           DT5=REAL(NFDM(KM))-TIME_KMM
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-13)
           ENDIF
           A2(KM,KMM)=0.0E0
           A2(KM,KM)=0.5E0*((1.0E0+DT5/(DT5+DT2))*DT2
     &                   +(1.0E0+DT4/(DT3+DT4))*DT3)/(DT2+DT3)
           A2(KM,KMP)=0.5E0*DT3*DT3/((DT2+DT3)*(DT3+DT4))
           D2(KM)=SIC(I,J,KM)-SIC_MIN*0.5E0*DT2*DT2/
     &       ((DT2+DT3)*(DT5+DT2))
C
C     CASE 3.4, WATER STARTS IN THE SECOND HALF OF CURRENT MONTH
C
          ELSEIF(SICN(I,J,KMM).GT.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME(I,J,KM).LE.REAL(NFDM_KMP))THEN
           DT5=TIME(I,J,KM)-REAL(MMD(KM))
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-14)
           ENDIF
           A2(KM,KMM)=0.5E0*DT2*DT2/((DT2+DT5)*(DT1+DT2))
           A2(KM,KM)=0.5E0*((1.0E0+DT1/(DT1+DT2))*DT2+DT5)/(DT2+DT5)
           A2(KM,KMP)=0.0E0
           D2(KM)=SIC(I,J,KM)
     &     -SIC_MIN*0.5E0*DT5/(DT2+DT5)
C
C     CASE 3.5, WATER ENDS IN THE FIRST HALF OF CURRENT MONTH
C      
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).GT.SICN_CRT.AND.
     &           TIME_KMM.GT.REAL(NFDM(KM)))THEN
           DT5=REAL(MMD(KM))-TIME_KMM
           IF(DT5.LT.0.0E0)THEN
            WRITE(6,*)' DT5=',DT5
            CALL                                  XIT('CLMMOD',-15)
           ENDIF
           A2(KM,KMM)=0.0E0
           A2(KM,KM)=0.5E0*(DT5+(1.0E0+DT4/(DT3+DT4))*DT3)/(DT5+DT3)
           A2(KM,KMP)=0.5E0*DT3*DT3/((DT5+DT3)*(DT3+DT4))
           D2(KM)=SIC(I,J,KM)
     &     -SIC_MIN*0.5E0*DT5/(DT5+DT3)
C
C     CASE 3.6, WATER ENDS IN THE SECOND HALF OF PREVIOUS MONTH
C     AND RESTARTS IN THE SECOND HALF OF CURRENT MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME_KMM.LE.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).LE.REAL(NFDM_KMP))THEN
           DT5=REAL(NFDM(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(MMD(KM))
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-16)
           ENDIF
           A2(KM,KMM)=0.0E0
           A2(KM,KM)=0.5E0*((1.0E0+DT5/(DT2+DT5))*DT2+DT6)/(DT2+DT6)
           A2(KM,KMP)=0.0E0
           D2(KM)=SIC(I,J,KM)
     &     -SIC_MIN*0.5E0*(DT2*DT2/(DT2+DT5)+DT6)/(DT2+DT6)
C
C     CASE 3.7, WATER ENDS IN THE FIRST HALF OF CURRENT MONTH
C     AND RESTARTS IN THE FIRST HALF OF NEXT MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME_KMM.GT.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).GT.REAL(NFDM_KMP))THEN
           DT5=REAL(MMD(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(NFDM_KMP)
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-17)
           ENDIF
           A2(KM,KMM)=0.0E0
           A2(KM,KM)=0.5E0*(DT5+(1.0E0+DT6/(DT3+DT6))*DT3)/(DT3+DT5)
           A2(KM,KMP)=0.0E0
           D2(KM)=SIC(I,J,KM)
     &     -SIC_MIN*0.5E0*(DT5+DT3*DT3/(DT3+DT6))/(DT3+DT5)
C
C     CASE 3.8, WATER ENDS IN THE FIRST HALF AND RESTARTS IN THE SECOND
C     HALF OF CURRENT MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME_KMM.GT.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).LE.REAL(NFDM_KMP))THEN
           DT5=REAL(MMD(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(MMD(KM))
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-18)
           ENDIF
           A2(KM,KMM)=0.0E0
           A2(KM,KM)=0.5E0
           A2(KM,KMP)=0.0E0
           D2(KM)=SIC(I,J,KM)
     &     -SIC_MIN*0.5E0
C
C     CASE 3.9, WATER ENDS IN THE SECOND HALF OF PREVIOUS MONTH
C     AND RESTARTS IN THE FIRST HALF OF NEXT MONTH
C
          ELSEIF(SICN(I,J,KMM).LE.SICN_CRT.AND.
     &           SICN(I,J,KMP).LE.SICN_CRT.AND.
     &           TIME_KMM.LE.REAL(NFDM(KM)).AND.
     &           TIME(I,J,KM).GT.REAL(NFDM_KMP))THEN
           DT5=REAL(NFDM(KM))-TIME_KMM
           DT6=TIME(I,J,KM)-REAL(NFDM_KMP)
           IF(DT5.LT.0.0E0.OR.DT6.LT.0.0E0)THEN
            WRITE(6,*)' DT5,DT6=',DT5,DT6
            CALL                                  XIT('CLMMOD',-19)
           ENDIF
           A2(KM,KMM)=0.0E0
           A2(KM,KM)=0.5E0*((1.0E0+DT5/(DT5+DT2))*DT2
     &                   +(1.0E0+DT6/(DT3+DT6))*DT3)/(DT2+DT3)
           A2(KM,KMP)=0.0E0
           D2(KM)=SIC(I,J,KM)-SIC_MIN*0.5E0*(DT2*DT2/(DT5+DT2)
     &                                    +DT3*DT3/(DT3+DT6))/(DT2+DT3)
C
C     NO MORE CASES ON ICE POINTS
C
          ELSE
           WRITE(6,*)' GC=',GC(I,J,KM)
           WRITE(6,*)' SICN=',SICN(I,J,KMM),SICN(I,J,KM),SICN(I,J,KMP)
           WRITE(6,*)' TIME=',TIME_KMM,TIME(I,J,KM)
           CALL                                   XIT('CLMMOD',-20)
          ENDIF
         ELSE
C
C     NO MORE CASES
C
          WRITE(6,*)' GC,SICN=',GC(I,J,KM),SICN(I,J,KM)
          CALL                                    XIT('CLMMOD',-21)
         ENDIF
 3300   CONTINUE
C
C     INVERT THE MATRICES FOR GT AND SIC
C
        DO 3400 KM2=1,NM
        DO 3400 KM1=1,NM
         C1(KM1,KM2)=A1(KM1,KM2)
         C2(KM1,KM2)=A2(KM1,KM2)
 3400   CONTINUE
        DO 3500 KM=1,NM
         B1(KM)=0.0E0
         B2(KM)=0.0E0
 3500   CONTINUE
        CALL RGAUSSJ(C1,NM,NM,B1,1,1)
        CALL RGAUSSJ(C2,NM,NM,B2,1,1)
C
        DO 3600 KM1=1,NM
         B1(KM1)=0.0E0
         B2(KM1)=0.0E0
        DO 3600 KM2=1,NM
         B1(KM1)=B1(KM1)+C1(KM1,KM2)*D1(KM2)
         B2(KM1)=B2(KM1)+C2(KM1,KM2)*D2(KM2)
 3600   CONTINUE
C
C     GT AND SIC NOW CONTAIN THE TARGET VALUES
C     FOR GROUND TEPERATURE AND SEA-ICE MASS
C
        DO 3900 KM=1,NM
         GT(I,J,KM)=B1(KM)
         SIC(I,J,KM)=B2(KM)
 3900 CONTINUE
C
 6025 FORMAT(' ',A4,I10,2X,A4,I10,4I6)
 6035 FORMAT(1X,I2,12F7.4)
      RETURN
      END
