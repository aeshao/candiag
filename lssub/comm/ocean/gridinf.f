      SUBROUTINE GRIDINF(RADIUS,NLON,NLAT,LATF,LATH,LONH,DA,DX,DY)
C
C     * APR 04/2002 - BM  - MODIFIED FOR NEW DATA DESCRIPTION FORMAT AND
C     *                     NEW VALUE OF PI
C     * JUN 08/2001 - B. MIVILLE
C     
C     * NOTE ABOUT THE RADIUS OF THE EARTH:
C     
C     * DEPENDING ON WHICH VERSION OF OGCM YOU ARE USING AND WHICH GRID 
C     * RESOLUTION THE RADIUS WILL BE DIFFERENT.
C     *
C---------------------------------------------------------------------
C
C     * INPUT PARAMETERS...
C    
C     * RADIUS = RADIUS OF THE EARTH (METERS)
C     * NLON   = NUMBER OF LONGITUDES
C     * NLAT   = NUMBER OF LATITUDES
C     * LATF   = LATITUDES OF FULL INDEX (J) GRID CELL
C     * LATH   = LATITUDES OF HALF INDEX (J+1/2) GRID CELL
C     * LONH   = LONGITUDES OF HALF INDEX (I+1/2) GRID CELL
C
C     * OUTPUT PARAMETERS...
C
C     * DA = AREA OF FULL INDEX (J) GRID CELL
C     * DX = EAST-WEST EXTENT OF FULL INDEX (J) GRID CELL
C     * DY = NORTH-SOUTH EXTENT OF FULL INDEX (J) GRID CELL
C  
C---------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER NLON, NLAT
      REAL    LATF(NLON,NLAT), LATH(NLON,NLAT), LATHM
      REAL    LONH(NLON,NLAT), LONHM
      REAL    DX(NLON,NLAT), DY(NLON,NLAT)
      REAL    DA(NLON,NLAT), RADIUS
      DATA    PI/3.1415926535898D0/
C-----------------------------------------------------------------------
C
C     * CONSTANT
C
      TWOPI  = 2.0D0 * PI  
      DTOR   = PI / 180.0D0
C
C     *
C     * DX(J) IN METERS TAKEN AT PHI(J) OF FULL INDEX (TRACER) GRID CELL
C     *
C     * DY(J) IN METERS IS MEASURED BETWEEN PHI(J+1/2) AND PHI(J-1/2)
C     *
C     *
C     *  ---   x---------x---- PHI(J+1/2) = LATH(J)             
C     *   |    |         |                     
C     *   |    |         |                   
C     *  DY(J) |    X----|---- PHI(J) = LATF(J)   
C     *   |    |         |                   
C     *   |    |         |                     
C     *  ---   x---------x---- PHI(J-1/2)              
C     *
C     *        |         |
C     *        |--DX(J)--|   
C     *        |         |
C     *
C     *     DA(I,J) = DX(J) * DY(J)
C     *
C
C     * ASSUMING A SYMMETRIC GRID ABOUT THE EQUATOR FOR THE TRACERS
C     * THE FIRST BOTTOM GRID BOX LATITUDE OF THE GRID 
C     * (E.G.AT J=1, PHI(1/2)) IS:
C
      LATHM = -LATH(1,NLAT)
C
C     * DA AND DX,DY FROM SOUTH TO NORTH, WEST TO EAST
C
      DO 110 J = 1,NLAT
C
C        * THE FIRST LEFT SIDE LONGITUDE OF A GRID BOX ASSUMING A GLOBAL 
C        * CYCLIC FIELD IS:
C
         LONHM = LONH(NLON-1,1)-360.0E0
C
         DO 100 I = 1,NLON
            DX(I,J) = RADIUS * COS(LATF(I,J)*DTOR) * (LONH(I,J)-LONHM) 
     1                * DTOR
            DY(I,J) = RADIUS * DTOR * (LATH(I,J) - LATHM) 
            DA(I,J) = DX(I,J) * DY(I,J)
            LONHM = LONH(I,J)
 100     CONTINUE
         LATHM = LATH(1,J)
 110  CONTINUE
C
      RETURN
      END
