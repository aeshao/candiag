      SUBROUTINE LIMITAO(EFIELDA,AFIELD,NLONO,NLATO,NLONA,NLATA,DAO,
     1                   BETAO,DAA,INDII,INDJJ,RMIN,RMAX,MMTYPE)
C
C     * APR 04/02 - B. MIVILLE - REVISED FOR NEW DATA DESCRIPTION FORMAT
C     * JUN 08/01 - B. MIVILLE
C
C     * ADJUST INTERPOLATED OCEAN VALUE TO CONSERVE THE ORIGINAL 
C     * ATMOSPHERE VALUE
C
C     * THIS IS DONE ASSUMING AN INTEGRAL NUMBER OF OCEAN POINT INSIDE 
C     * THE OVERLYING ATMOSPHERE GRID CELL. THE AREA WEIGHTED AVERAGE 
C     * OF THE OCEAN POINTS NEED TO EQUAL THE ATMOSPHERE POINT.
C
C     * INPUT PARAMETERS...
C
C     * EFIELDA= EXPANDED ATMOSPHERE FIELD
C     * AFIELD = ADJUSTED OCEAN FIELD
C     * NLONO  = NUMBER OF OCEAN LONGITUDES INCLUDING CYCLIC
C     * NLATO  = NUMBER OF OCEAN LATITUDES
C     * NLONA  = NUMBER OF ATMOSPHERE LONGITUDES INCLUDING CYCLIC
C     * NLATA  = NUMBER OF ATMOSPHERE LATITUDES 
C     * DAO    = AREA OF OCEAN GRID CELL
C     * BETAO  = HEAVISIDE FUNCTION FOR OCEAN GRID CELL
C     * DAA    = AREA OF ATMOSPHERE GRID CELL
C     * INDII  = INDEX THE OCEAN I POINT TO ATMOSPHERE II POINT FOR 
C     *          ADJUSTMENT
C     * INDJJ  = INDEX THE OCEAN J POINT TO ATMOSPHERE JJ POINT FOR 
C     *          ADJUSTMENT
C     * RMIN   = MINIMUM VALUE ALLOWED FOR OCEAN FIELD
C     * RMAX   = MAXIMUM VALUE ALLOWED FOR OCEAN FIELD
C     * MMTYPE = TYPE OF CONSTRAIN TO APPLY
C     *          1 - MINIMUM ONLY (E.G. SST)
C     *          2 - MAXIMUM ONLY
C     *          3 - MINIMUM AND MAXIMUM (E.G. SEA ICE COVERAGE)
C
C     * OUTPUT PARAMETERS...
C
C     * AFIELD = MODIFIED ADJUSTED OCEAN FIELD      
C
C-------------------------------------------------------------------------------
C
C     * ARRAY SIZE FOR MM(IMX,JMX) MADE BIG ENOUGH TO ENSURE MOST
C     * FUTURE GRID SIZE
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      PARAMETER(IMX=769,JMX=384)
      INTEGER NLONA,NLATA,NLONO,NLATO
      INTEGER INDII(NLONO,NLATO),INDJJ(NLONO,NLATO),MMTYPE
      REAL AFIELD(NLONO,NLATO),EFIELDA(0:NLONA,0:NLATA+1)
      REAL RMIN,RMAX,AVG,AVGA
      REAL DAO(NLONO,NLATO),BETAO(NLONO,NLATO)
      REAL DAA(0:NLONA,0:NLATA+1)
      REAL MM(IMX,JMX),AVGB(IMX,JMX),THRESH
C-----------------------------------------------------------------------
C
C     * VERIFY THAT NLATA,NLONA DOES NOT EXCEED MAXIMUM GRID SIZE
C
      IF((NLATA.GT.JMX).OR.(NLONA.GT.IMX)) THEN
         WRITE(6,'(A)') 'NUMBER OF LON/LAT OVER MAXIMUM'
         WRITE(6,'(A,I8,A,I8)') 'NLONA: ',NLONA,'  MAXIMUM: ',IMX
         WRITE(6,'(A,I8,A,I8)') 'NLATA: ',NLATA,'  MAXIMUM: ',JMX
         CALL                                      XIT('LIMITAO',-24)
      ENDIF
C
C     * INITIALIZED ARRAY
C
      DO 412 J=1,NLATA
         DO 410 I=1,NLONA
            AVGB(I,J)=0.0E0
            MM(I,J)=2.0E0
 410     CONTINUE
 412  CONTINUE         
C
C     * VERIFY THRESHOLD
C
C
C     * MINIMUM ONLY
C     
      IF(MMTYPE.EQ.1)THEN
         DO 416 J=1,NLATO
            DO 414 I=1,NLONO-1
               II=INDII(I,J)
               JJ=INDJJ(I,J)
               IF (AFIELD(I,J).LT.RMIN) THEN
                  AVGB(II,JJ)=AVGB(II,JJ)+DAO(I,J)*(AFIELD(I,J)-RMIN)
                  AFIELD(I,J)=BETAO(I,J)*RMIN
               ENDIF
 414        CONTINUE
            AFIELD(NLONO,J)=AFIELD(1,J)
 416     CONTINUE
C
C        * RE-ADJUST FOR CONSERVATION
C
         DO 632 J=1,NLATO
            DO 630 I=1,NLONO-1
               II=INDII(I,J)
               JJ=INDJJ(I,J)
               IF (AFIELD(I,J).GT.RMIN) THEN
                  AVG=(EFIELDA(II,JJ)-RMIN)*DAA(II,JJ)
               ENDIF
               AVGA=AVG-AVGB(II,JJ)
               IF (AVGA.EQ.0) THEN
                  ADJM=0.E0
               ELSE
                  ADJM=AVG/AVGA
               ENDIF
               IF (AFIELD(I,J).GT.RMIN) THEN
                  AFIELD(I,J)=BETAO(I,J)*((AFIELD(I,J)-RMIN)*ADJM+RMIN)
               ENDIF
 630        CONTINUE
            AFIELD(NLONO,J)=AFIELD(1,J)
 632     CONTINUE
C
C     * MAXIMUM ONLY
C
      ELSEIF(MMTYPE.EQ.2)THEN
C
         DO 642 J=1,NLATO
            DO 640 I=1,NLONO-1
               II=INDII(I,J)
               JJ=INDJJ(I,J)
               IF (AFIELD(I,J).GT.RMAX) THEN
                  AVGB(II,JJ)=AVGB(II,JJ)+DAO(I,J)*(AFIELD(I,J)-RMAX)
                  AFIELD(I,J)=BETAO(I,J)*RMAX
               ENDIF
 640        CONTINUE
            AFIELD(NLONO,J)=AFIELD(1,J)
 642     CONTINUE
C
C        * RE-ADJUST FOR CONSERVATION
C
         DO 652 J=1,NLATO
            DO 650 I=1,NLONO-1
               II=INDII(I,J)
               JJ=INDJJ(I,J)
               IF (AFIELD(I,J).LT.RMAX) THEN
                  AVG=(EFIELDA(II,JJ)-RMAX)*DAA(II,JJ)
               ENDIF          
               AVGA=AVG-AVGB(II,JJ)
               IF (AVGA.EQ.0) THEN
                  ADJM=0.E0
               ELSE
                  ADJM=AVG/AVGA
               ENDIF
               IF (AFIELD(I,J).LT.RMAX) THEN
                  AFIELD(I,J)=BETAO(I,J)*((AFIELD(I,J)-RMAX)*ADJM+RMAX)
               ENDIF
 650        CONTINUE
            AFIELD(NLONO,J)=AFIELD(1,J)
 652     CONTINUE
C
C     * MINIMUM AND MAXIMUM
C

      ELSEIF(MMTYPE.EQ.3)THEN
C
         DO 662 J=1,NLATO
            DO 660 I=1,NLONO-1
               II=INDII(I,J)
               JJ=INDJJ(I,J)
               IF (AFIELD(I,J).LT.RMIN) THEN
                  AVGB(II,JJ)=AVGB(II,JJ)+DAO(I,J)*(AFIELD(I,J)-RMIN)
                  AFIELD(I,J)=BETAO(I,J)*RMIN
                  MM(II,JJ)=0.E0
               ENDIF
               IF (AFIELD(I,J).GT.RMAX) THEN
                  AVGB(II,JJ)=AVGB(II,JJ)+DAO(I,J)*(AFIELD(I,J)-RMAX)
                  AFIELD(I,J)=BETAO(I,J)*RMAX
                  MM(II,JJ)=1.E0
               ENDIF
 660        CONTINUE
            AFIELD(NLONO,J)=AFIELD(1,J)
 662     CONTINUE
C
C        * RE-ADJUST FOR CONSERVATION
C
         DO 672 J=1,NLATO
            DO 670 I=1,NLONO-1
               II=INDII(I,J)
               JJ=INDJJ(I,J)
               IF(MM(II,JJ).NE.2.E0)THEN
                  THRESH=((1.E0-MM(II,JJ))*RMIN)+MM(II,JJ)*RMAX
                  AVG=(EFIELDA(II,JJ)-THRESH)*DAA(II,JJ)
                  AVGA=AVG-AVGB(II,JJ)
                  IF (AVGA.EQ.0) THEN
                     ADJM=0.E0
                  ELSE
                     ADJM=AVG/AVGA
                  ENDIF
                  AFIELD(I,J)=BETAO(I,J)*((AFIELD(I,J)-THRESH)*ADJM
     1                 +THRESH)
               ENDIF
               AVG=0.0E0
 670        CONTINUE
            AFIELD(NLONO,J)=AFIELD(1,J)
 672     CONTINUE
      ENDIF
C
      RETURN
      END
