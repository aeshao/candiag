      SUBROUTINE FAST (S,F,LSR,LM,LA,ILH,ILEV,WLP)
C
C     * OCT 04/93 - M. LAZARE. DECLARE "WP" TO BE REAL*8 AS WELL. 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * JUL 21/83 - R.LAPRISE.

C     * MULTIPLE LEGENDRE TRANSFORMS FROM FOURIER TO SPECTRAL.
C     * ACCUMULATE THE CONTRIBUTIONS OF FOURIER COEFFICIENTS
C     * ON ONE LATITUDE CIRCLE INTO THE LATITUDE INTEGRAL 
C     * TO OBTAIN SPHERICAL HARMONIC COEFFICIENTS.
C     * S   = ACCUMULATED SPHERICAL HARMONIC COEFFICIENTS,
C     * F   = FOURIER COEFFICIENTS AT ONE LATITUDE, 
C     * WLP = LEGENDRE POLYNOMIALS NORMALIZED BY GAUSSIAN WEIGHT, 
C     * LM  = NUMBER OF E-W WAVE NUMBERS (M=1,LM),
C     * ILH = FIRST DIMENSION OF COMPLEX F, 
C     * LA  = FIRST DIMENSION OF COMPLEX S, 
C     * ILEV=NUMBER OF LEVELS FOR WHICH TRANSFORM IS PERFORMED. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL S(2,LA,ILEV),F(2,ILH,ILEV) 
      REAL*8 WLP(1) 
      REAL*8 WP
      INTEGER LSR(2,1)
C-----------------------------------------------------------------------
      DO 500 M=1,LM 
      NA=LSR(2,M) 
      NL=LSR(1,M) 
      NR=LSR(1,M+1)-1 
C 
      IF(ILEV.GT.NR-NL+1) THEN
C 
         DO 200 N=NL,NR 
         WP=WLP(NA) 
            DO 100 L=1,ILEV 
            S(1,N,L)=S(1,N,L)+WP*F(1,M,L) 
            S(2,N,L)=S(2,N,L)+WP*F(2,M,L) 
  100       CONTINUE
         NA=NA+1
  200    CONTINUE 
C 
      ELSE
C 
         DO 400 L=1,ILEV
         NB=NA
         F1=F(1,M,L)
         F2=F(2,M,L)
            DO 300 N=NL,NR
            S(1,N,L)=S(1,N,L) +WLP(NB)*F1 
            S(2,N,L)=S(2,N,L) +WLP(NB)*F2 
            NB=NB+1 
  300    CONTINUE 
  400    CONTINUE 
C 
      ENDIF 
C 
  500 CONTINUE
      RETURN
C-----------------------------------------------------------------------
      END
