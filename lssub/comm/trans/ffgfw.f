      SUBROUTINE FFGFW  (GD, ILG,FC,ILH,IR,LON,WRK,ILEV,IFAX,TRIGS) 
C
C     * JUL 22/94 - F.MAJAESS(ABORT IF ILG<2*(IR+1) AND ILEV > 1)
C     * APR 24/92 - M.LAZARE. - CHANGE PREFERABLE VECTOR LENGTH TO 256
C     *                         FOR SX3.  
C     * JAN 05/84 - B.DUGAS.
C     * JUL 19/83 - R.LAPRISE.
C     * DRIVING ROUTINE FOR THE FOURIER TRANSFORM,
C     * COEFFICIENTS TO GRID. 
C     * ====================
C     * FC    = FOURIER COEFFICIENTS, 
C     * ILH   = FIRST DIMENSION OF COMPLEX FC,
C     * GD    = GRID DATA,
C     * ILG   = FIRST DIMENSION OF REAL GD, NOTE THAT IT IS 
C     *         ASSUMED THAT GD AND FC ARE EQUIVALENCED IN MAIN,
C     *         SO OBVIOUSLY ILG MUST EQUAL 2*ILH,
C     *         ACTUALLY FC IS DECLARED REAL FOR CONVENIENCE. 
C     * IR    = MAXIMUM EAST-WEST WAVE NUMBER (M=0,IR), 
C     * LON   = NUMBER OF DISTINCT LONGITUDES,
C     * WRK   = WORK SPACE, 
C     * ILEV  = NUMBER OF LEVELS. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GD(ILG,ILEV),FC(ILG,ILEV),WRK(1) 
      DIMENSION IFAX(1),TRIGS(LON) 
C-----------------------------------------------------------------
      IF ( ILG.LT.2*(IR+1) .AND. ILEV.GT.1) THEN
       WRITE(6,6000) ILEV,ILG,2*(IR+1)
       CALL                 XIT('FFGFW',-1)
      ENDIF
      ISIGN = +1
      INC   =  1
      IR121 =  (IR+1)*2 +1
      IF(ILG.LT.LON+2) CALL XIT('FFGFW',-2) 
C 
C     * SET TO ZERO FOURIER COEFFICIENTS BEYOND TRUNCATION. 
C 

      IF(IR121.LE.LON+2) THEN
       DO 100 I=IR121,LON+2
       DO 100 L=1,ILEV 
  100  FC(I,L) =0.E0 
      ENDIF
C 
C     * AS MANY AS 256 ARE DONE AT ONCE FOR VECTORIZATION. 
C 
      NSTART=1
      NTIMES=ILEV/256
      NREST =ILEV-NTIMES*256 
C 
      IF(NREST.NE.0) THEN 
         LENGTH=NREST 
         NTIMES=NTIMES+1
      ELSE
         LENGTH=256
      ENDIF 
C 
C     * DO THE FOURIER TRANSFORMS.
C 
      DO 300 N=1,NTIMES 
         CALL VFFT(FC(1,NSTART),WRK,TRIGS,IFAX,INC,ILG,LON,LENGTH,ISIGN)
         NSTART=NSTART+LENGTH 
         LENGTH=256
  300 CONTINUE
C 
      RETURN
C-------------------------------------------------------------------- 
 6000 FORMAT(' FOURIER TRANSFORM CODE REQUIRES ILEV=',I3,
     1        ', BE 1 WHENEVER ILG=,',I3,' < 2*(IR+1)=',I3)
C-------------------------------------------------------------------- 
      END
