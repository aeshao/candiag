      SUBROUTINE FTSETUP (TRIGS,IFAX,N) 
C 
C     * AUG 13/90 - M.LAZARE.    CHANGE JFAX DIMENSION FROM 10 TO 9 
C     *                          AND ABORT IF NFAX>9, SINCE IFAX
C     *                          DIMENSION LIMITED TO 10 IN GCM.
C     * JUL 13/83 - R.LAPRISE.   ORIGINAL SET99 FROM ECMWF. 
C     * COMPUTES FACTORS OF N & TRIGONOMETRIC 
C     * FUNCTIONS REQUIRED BY VFFT. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      DIMENSION TRIGS(N),IFAX(1),JFAX(9),LFAX(7)
      DATA LFAX/6,8,5,4,3,2,1/
C------------------------------------------------------------------ 
      DEL=4.0E0*ASIN(1.0E0)/FLOAT(N)
      NIL=0 
      NHL=(N/2)-1 
      DO 10 K=NIL,NHL 
      ANGLE=FLOAT(K)*DEL
      TRIGS(2*K+1)=COS(ANGLE) 
      TRIGS(2*K+2)=SIN(ANGLE) 
   10 CONTINUE
C 
C     * FIND FACTORS OF N (8,6,5,4,3,2; ONLY ONE 8 ALLOWED) 
C     * LOOK FOR SIXES FIRST, STORE FACTORS IN DESCENDING ORDER 
C 
      NU=N
      IFAC=6
      K=0 
      L=1 
   20 CONTINUE
      IF (MOD(NU,IFAC).NE.0) GO TO 30 
      K=K+1 
      JFAX(K)=IFAC
      IF (IFAC.NE.8) GO TO 25 
      IF (K.EQ.1) GO TO 25
      JFAX(1)=8 
      JFAX(K)=6 
   25 CONTINUE
      NU=NU/IFAC
      IF (NU.EQ.1) GO TO 50 
      IF (IFAC.NE.8) GO TO 20 
   30 CONTINUE
      L=L+1 
      IFAC=LFAX(L)
      IF (IFAC.GT.1) GO TO 20 
C 
      WRITE(6,40) N 
   40 FORMAT('1N =',I4,' - CONTAINS ILLEGAL FACTORS')
      CALL XIT('FTSETUP',-1)
      RETURN
C-----------------------------------------------------------------
C     * NOW REVERSE ORDER OF FACTORS
C 
   50 CONTINUE
      NFAX=K
      IF(NFAX.GT.9) CALL XIT('FTSETUP',-2)
      IFAX(1)=NFAX
      DO 60 I=1,NFAX
      IFAX(NFAX+2-I)=JFAX(I)
   60 CONTINUE
      RETURN
C-------------------------------------------------------------------
      END
