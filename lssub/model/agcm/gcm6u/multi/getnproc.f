      SUBROUTINE GETNPROC(NPROC)
C
C     * APRIL 27/98 - B.DENIS.
C
C     * READ THE NUMBER OF PHYSICAL PROCESSOR SET BY THE USER.
C     * THE CALL TO GETENV RETURNS THE VALUE OF THE ENVIRONMENT
C     * VARIABLE 'NPROC' AS A CHARACTER VARIABLE. CONVERSION TO INTEGER
C     * IS DONE BY A INTERNAL READ STATEMENT.
C  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      CHARACTER*8 NPROC_SW,MULTI_SW
C
C---------------------------------------------------------------------- 

C     * FIRST, GET THE MULTI SWITCH. IT SHOULD BE TURNED ON.
C       
      MULTI_SW='off'
C       
      CALL GETENV('multi', MULTI_SW)
C       
      IF (.NOT.((MULTI_SW.EQ.'on').OR. (MULTI_SW.EQ.'ON'))) THEN
      WRITE(6,610)                        
                                               CALL XIT ('GETNPROC', -1)
      ENDIF                               
C       
C     * SECOND, GET NPROC.
C       
      NPROC=0
C       
      CALL GETENV('nproc', NPROC_SW)
      READ(NPROC_SW,600) NPROC
 600  FORMAT(I8)
C       
      IF(NPROC.EQ.0) THEN
        WRITE(6,620)
                                               CALL XIT ('GETNPROC', -2)
      ENDIF                               
C
C---------------------------------------------------------------------  
 610  FORMAT('0 PROBLEM WITH MULTI-TASKING:',/,
     1    '0 THIS MODEL CODE HAS BEEN COMPILED FOR MULTI-TASKING.',/,
     2    '0 PLEASE SET multi=on OR RECOMPILE FOR SEQUENTIAL MODE',/,
     3    '0 (USING samerun=off AND multi=off).')
C       
 620  FORMAT('0 PROBLEM WITH MULTI-TASKING:',/,
     1    '0 THE NUMBER OF PROCESSOR HAS NOT BEEN SET PROPERLY !!')
C
      RETURN
      END
