      SUBROUTINE GETRAC2(NF,TRAC,LA,ILEV,LH,ITRAC,NTRAC)
C
C     * SEP 27/92 - M.LAZARE. - MAXX NOW INCLUDES "MACHINE" FACTOR. 
C     * JUN 14/90 - R.LAPRISE/J DE GRANDPRE.
  
C     * AS IN GETRAC, EXCEPT FORMULATION FOR "N" TRACERS IMPLEMENTED. 
C 
C     * GET THE SPECTRAL TRACER FIELD, IF ITRAC.NE.0. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX TRAC(LA,ILEV,NTRAC) 
      INTEGER LH(ILEV)
C 
      LOGICAL OK
C 
      COMMON /ICOM/ IBUF(8),IDAT(1) 
      COMMON /MACHTYP/ MACHINE, INTSIZE
C-------------------------------------------------------------------------- 
      IF(ITRAC.EQ.0)RETURN
C 
      MAXX=( 2*LA + 8 )*MACHINE
      DO 200 N=1,NTRAC
      DO 200 L=1,ILEV 
          CALL NOM(NAM,'X',N) 
          CALL GETFLD2(NF,TRAC(1,L,N),NC4TO8("SPEC"),0,NAM,LH(L),IBUF,
     1                                                        MAXX,OK)
      IF(.NOT.OK)CALL XIT('GETRAC2',-1) 
      WRITE(6,6025)IBUF 
  200 CONTINUE
      RETURN
C-------------------------------------------------------------------------- 
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
      END 
