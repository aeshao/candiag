      SUBROUTINE GETZON6(OZZX,ILAT,IDAY,NF,LEVOZ)
C
C     * OCT 10/92 - M.LAZARE. - LIKE PREVIOUS GETZON5 EXCEPT NO TACZX,
C     *                         CLDPAK.
C
C     * GCM INPUT SUBROUTINE. 
C     * GETS ZONAL CROSS-SECTIONS OF OZONE FROM FILE NF.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL OZZX(ILAT,1)
C 
      LOGICAL OK
      COMMON /ICOM/ IBUF(8),IDAT(1) 
      COMMON /MACHTYP/ MACHINE, INTSIZE
      DATA NP /37/
C---------------------------------------------------------------------
C     * FIND AND READ OZONE.
C
      MAXX=(ILAT + 8 )*MACHINE 
      REWIND NF 
      CALL FIND(NF,NC4TO8("ZONL"),IDAY,NC4TO8("  OZ"),1,OK)
      IF(.NOT.OK) CALL XIT('GETZON6',-1)
      LEVOZ=NP
      DO 410 L=1,LEVOZ
      CALL GETFLD2(NF,OZZX(1,L),NC4TO8("ZONL"),IDAY,NC4TO8("  OZ"),L,
     1                                                  IBUF,MAXX,OK)
  410 WRITE(6,6025) IBUF
      RETURN
C---------------------------------------------------------------------
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
      END
