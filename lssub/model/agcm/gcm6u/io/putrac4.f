      SUBROUTINE PUTRAC4(NF,TRAC,LA,LRLMT,ILEV,LH,KOUNT,LSSP,ITRAC, 
     1                   NTRAC,IBUF3) 

C     * MAY 29/95 - M.LAZARE. INCLUDE OPTION "ISAVDTS" TO SAVE
C     *                       GCM OUTPUT IN "yyyymmddhh" FORMAT
C     *                       (FORMAT STATEMENT ALSO MODIFIED TO
C     *                       HANDLE POSSIBLE 10-CHARACTERS).
C     * FEB 08/95 - M.LAZARE. PREVIOUS VERSION PUTRAC3.
  
C     * SAVE TRACER FIELDS ONTO MODEL SPECTRAL HISTORY FILE, 
C     * IF ITRAC.NE.0.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      LOGICAL LSSP,OK
C 
      INTEGER LH(ILEV),IBUF(8)
C 
      COMPLEX TRAC(LA,ILEV,NTRAC) 

      COMMON /MACHTYP/ MACHINE,INTSIZE
      COMMON /KEEPTIM/ IYEAR,IYMDH,MYRSSTI,ISAVDTS
C-------------------------------------------------------------------------
      IF(ITRAC.EQ.0 .OR. .NOT.LSSP)RETURN 
C
C     * DETERMINE PROPER IBUF(2) TO USE FOR SAVED FIELDS, BASED ON
C     * VALUE OF OPTION SWITCH "ISAVDTS".
C
      IF(ISAVDTS.NE.0)                   THEN
         IBUF2=IYMDH
      ELSE
         IBUF2=KOUNT
      ENDIF
C
C     * GET DATA RECORD LENGTH.
C                        
      LEN=LA*2*INTSIZE
      CALL SETLAB(IBUF,NC4TO8("SPEC"),IBUF2,NC4TO8("TRAC"),0,LA,1,
     1                                                    LRLMT,0)
C
      DO 300 N=1,NTRAC
         CALL NOM(IBUF(3),IBUF3,N)
         DO 200 L=1,ILEV 
            IBUF(4)=LH(L)
            CALL FBUFOUT(NF,IBUF,-1,K)  
            CALL WRTBUF(NF,TRAC(1,L,N),LEN,OK)
C           WRITE(6,6026) IBUF
  200    CONTINUE
  300 CONTINUE
C-----------------------------------------------------------------------
 6026 FORMAT(' ',60X,A4,1X,I10,2X,A4,5I6)
      RETURN
      END 
