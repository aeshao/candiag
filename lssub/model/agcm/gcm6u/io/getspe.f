      SUBROUTINE GETSPE(NF,PHIS,PS,T,P,C,ES,LA,ILEV,LEVS,LS,LH,IDIV)
C
C     * SEP 27/92 - M.LAZARE. - MAXX NOW INCLUDES "MACHINE" FACTOR. 
C     * APR 07/92 - E.CHAN.   - REMOVE REWINDS AND GET FIELDS IN THE
C     *                         SAME ORDER AS THEY ARE SAVED.
C     * SEP 21/88 - M.LAZARE. - MAX DEFINED EXPLICITLY IN SUBROUTINE. 
C     * NOV 10/83 - R.LAPRISE, J.D.HENDERSON. 
C     * GETS INITIAL SPECTRAL VARIABLES FOR THE G.C.M. FROM FILE NF.
C     * LS,LH = LABEL SIGMA VALUES FOR FULL,HALF LEVELS.
C 
C     * INPUT FILE NF1 MUST CONTAIN SPECTRAL FIELDS ....
C     * MOUNTAINS            - 1 LEVEL
C     * LN(SURFACE PRESSURE) - 1 LEVEL
C     * TEMPERATURE          - HALF LEVELS 1 TO ILEV. 
C     * VORTICITY,DIVERGENCE - FULL LEVELS 1 TO ILEV (IN PAIRS) 
C     * MOISTURE VARIABLE    - HALF LEVELS (ILEV-LEVS+1) TO ILEV
C 
C     * NOTE - IF ANY FIELD IS MISSING, THE PROGRAM STOPS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      LOGICAL OK,IDIV 
C     * LEVEL 2,PHIS,PS,T,P,C,ES
      COMPLEX PHIS(1),PS(1) 
      COMPLEX T(LA,1),P(LA,1),C(LA,1),ES(LA,1)
      INTEGER LS(1),LH(1) 
C 
C     * ICOM IS A SHARED SCM I/O AREA. IT MUST BE LARGE ENOUGH
C     * FOR AN 8 WORD LABEL FOLLOWED BY A PACKED GAUSSIAN GRID. 
C 
      COMMON /ICOM/ IBUF(8),IDAT(1) 
      COMMON /MACHTYP/ MACHINE, INTSIZE
C------------------------------------------------------------------ 
      MAXX=( 2*LA + 8 )*MACHINE
C 
C     * LOG OF SURFACE PRESSURE (NEWTONS /M**2) CONVERTED FROM MB.
C 
      CALL GETFLD2(NF,      PS,NC4TO8("SPEC"),0,NC4TO8("LNSP"),1,IBUF,
     1                                                        MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETSPE',-1) 
      WRITE(6,6025) (IBUF(I),I=1,8) 
      PS(1)=PS(1)+LOG(100.)*SQRT(2.0)
C 
C     * MOUNTAINS (M/SEC)**2. 
C 
      CALL GETFLD2(NF,    PHIS,NC4TO8("SPEC"),0,NC4TO8("PHIS"),1,IBUF,
     1                                                        MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETSPE',-2) 
      WRITE(6,6025) (IBUF(I),I=1,8) 
C 
C     * TEMPERATURE  (DEG K). 
C 
      DO 250 L=1,ILEV 
      CALL GETFLD2(NF,T(1,L),NC4TO8("SPEC"),0,NC4TO8("TEMP"),LH(L),IBUF,
     1                                                          MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETSPE',-3) 
      WRITE(6,6025) (IBUF(I),I=1,8) 
  250 CONTINUE
C 
C     * VORTICITY AND DIVERGENCE (1./SEC).
C     * IF IDIV IS FALSE SET DIVERGENCE TO ZERO.
C 
      DO 350 L=1,ILEV 
      CALL GETFLD2(NF,  P(1,L),NC4TO8("SPEC"),0,NC4TO8("VORT"),LS(L),
     1                                                  IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETSPE',-4) 
      WRITE(6,6025) (IBUF(I),I=1,8) 
      CALL GETFLD2(NF,  C(1,L),NC4TO8("SPEC"),0,NC4TO8(" DIV"),LS(L),
     1                                                  IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETSPE',-5) 
      WRITE(6,6025) (IBUF(I),I=1,8) 
      IF(.NOT.IDIV) CALL SCOF2(C(1,L),LA,1,0) 
  350 CONTINUE
C 
C     * MOISTURE VARIABLE.
C 
      IF(LEVS.EQ.0) RETURN
      DO 450 N=1,LEVS 
      L=(ILEV-LEVS) + N 
      CALL GETFLD2(NF, ES(1,N),NC4TO8("SPEC"),0,NC4TO8("  ES"),LH(L),
     1                                                  IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETSPE',-6) 
      WRITE(6,6025) (IBUF(I),I=1,8) 
  450 CONTINUE
      RETURN
C------------------------------------------------------------------ 
 6010 FORMAT('0 INPUT AREA IS TOO SMALL')
 6025 FORMAT(' ',A4,I10,1X,A4,5I6)
      END 
