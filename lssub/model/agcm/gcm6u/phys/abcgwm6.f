      SUBROUTINE ABCGWM6 (A,B,C,DGW,GRAV,IL1,IL2,ILG,ILEV,
     1                   SGJ,SGBJ,DSGJ,GROP,TODT) 
  
C     * JUL 15/88 - M.LAZARE : REVERSE ORDER OF LOCAL SIGMA ARRAYS. 
C     * JAN 29/88 - R.LAPRISE: PREVIOUS HYBRID VERSION FOR GCM3H. 
  
C     * MODIFICATION OF VABCGWM FOR HYBRID VERSION OF MODEL.
C     * CALCULATES THE THREE VECTORS FORMING THE TRI-DIAGONAL 
C     * MATRIX FOR THE IMPLICIT GRAVITY WAVE DRAG OF MOMENTUM 
C     * ON FULL LEVELS.  A=LOWER DIAG.,  B=MAIN DIAG. AND C=UPPER DIAG. 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL SGJ (ILG,ILEV),SGBJ(ILG,ILEV),GROP(ILG),DSGJ(ILG,ILEV) 
      REAL A   (ILG,ILEV),B   (ILG,ILEV),C   (ILG,ILEV),DGW (ILG,ILEV)
C-----------------------------------------------------------------------
      DO 100 I=IL1,IL2
         X     =-GROP(I)*DGW(I,1)/SGBJ(I,1) 
         B(I,1)=B(I,1) +X*(SGJ (I,2)-SGBJ(I,1))/(SGJ(I,2)-SGJ(I,1)) 
         C(I,1)=C(I,1) +X*(SGBJ(I,1)-SGJ (I,1))/(SGJ(I,2)-SGJ(I,1)) 
  100 CONTINUE
  
      ILEVM=ILEV-1
      DO 200 L=2,ILEVM
         DO 200 I=IL1,IL2 
            AM=(SGJ (I,  L)-SGBJ(I,L-1)) / (SGJ (I,  L)-SGJ (I,L-1))
            BM=(SGBJ(I,L-1)-SGJ (I,L-1)) / (SGJ (I,  L)-SGJ (I,L-1))
            AP=(SGJ (I,L+1)-SGBJ(I,  L)) / (SGJ (I,L+1)-SGJ (I,  L))
            BP=(SGBJ(I,  L)-SGJ (I,  L)) / (SGJ (I,L+1)-SGJ (I,  L))
            XM=GROP(I)*DGW(I,L-1)        / (SGBJ(I,  L)-SGBJ(I,L-1))
            XP=GROP(I)*DGW(I,L  )        / (SGBJ(I,  L)-SGBJ(I,L-1))
            A(I,L)=A(I,L) + XM*AM 
            C(I,L)=C(I,L) - XP*BP 
            B(I,L)=B(I,L) + XM*BM-XP*AP 
  200 CONTINUE
  
      L=ILEV
      DO 300 I=IL1,IL2
         X     =GROP(I)*DGW(I,L-1)/DSGJ(I,  L)
         A(I,L)=A(I,L)
     1      +X*(SGJ (I,  L)-SGBJ(I,L-1)) / (SGJ (I,  L)-SGJ (I,L-1))
         B(I,L)=B(I,L)
     1      +X*(SGBJ(I,L-1)-SGJ (I,L-1)) / (SGJ (I,  L)-SGJ (I,L-1))
     1      -GROP(I)*DGW(I,L) / DSGJ(I,  L) 
  300 CONTINUE
  
C     * DEFINE MATRIX TO INVERT = I-2*DT*MAT(A,B,C).
  
      DO 500 L=1,ILEVM
         DO 500 I=IL1,IL2 
            A(I,L+1)=-TODT*A(I,L+1) 
            C(I,L)=  -TODT*C(I,L) 
  500 CONTINUE
      DO 550 L=1,ILEV 
         DO 550 I=IL1,IL2 
            B(I,L)=1.-TODT*B(I,L) 
  550 CONTINUE
  
      RETURN
      END 
