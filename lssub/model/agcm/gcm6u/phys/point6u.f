      SUBROUTINE POINT6U (ILEV,LEV,LEVOZ,ILG,IVA,IRL,IRS) 

C     * MAY 30/95 - M.LAZARE. ADD TWO NEW 1-LEVEL WORK ARRAYS FOR
C     *                       NEW RADIATION ROUTINE "RADNEWO", FOR
C     *                       THE ARRAYS "FCLRTOA" AND "FCLRSFC" TO
C     *                       SUPPORT THE CALCULATION OF CLEAR-SKY 
C     *                       FLUXES.  
C     * NOV 30/88 - M.LAZARE. PREVIOUS ROUTINE POINTP6.
C 
C     * THIS ROUTINE CALCULATES THE VALUES OF THE POINTERS USED 
C     * IN THE PHYSIC SUBROUTINE FOR GW DRAG, VERTICAL FLUXES, CLOUDS,
C     * CONVECTION, OZONE AND RADIATION.
  
C     * CARE IS TAKEN TO ENSURE THAT THE RADIATION OCCUPIES THE LARGEST 
C     * PORTION OF THE "WRK" WORK SPACE ARRAY AND THAT THE SHORWAVE 
C     * RADIATION WORK SPACE IS HIDDEN WITHIN THE LONGWAVE, ALSO THAT 
C     * THE SPACE ASSIGNED IN GCMPAR6 ($IVA$) IS AT LEAST AS LARGE AS 
C     * THE SIZE REQUIRED BY THE LONGWAVE RADIATION POINTERS. 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * THE POINTERS THEMSELVES ARE IN COMMON BLOCKS. 
  
      COMMON /GWPOINT/  IG1, IG2, IG3, IG4, IG5, IG6, IG7, IG8, IG9,
     1                  IG10,IG11,IG12,IG13,IG14,IG15,IG16,IG17,IG18, 
     2                  IG19,IG20,IG21,IG22,IG23,IG24,IG25,IG26,IG27
  
      COMMON /VRPOINT/  IV1, IV2, IV3, IV4, IV5, IV6, IV7, IV8, IV9,
     1                  IV10,IV11,IV12,IV13,IV14,IV15,IV16,IV17,IV18, 
     2                  IV19,IV20,IV21,IV22,IV23,IV24,IV25,IV26,IV27, 
     3                  IV28,IV29,IV30,IV31,IV32,IV33,IV34,IV35,IV36, 
     4                  IV37,IV38,IV39,IV40,IV41,IV42,IV43,IV44,IV45, 
     5                  IV46,IV47,IV48,IV49,IV50,IV51,IV52,IV53,IV54
  
      COMMON /CLPOINT/  IC1, IC2, IC3, IC4, IC5, IC6, IC7, IC8, IC9,
     1                  IC10,IC11,IC12,IC13,IC14,IC15,IC16,IC17,IC18, 
     2                  IC19,IC20,IC21,IC22,IC23,IC24,IC25,IC26,IC27
  
      COMMON /CVPOINT/  IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9,
     1                  IN10,IN11,IN12,IN13,IN14,IN15,IN16,IN17,IN18, 
     2                  IN19,IN20,IN21,IN22,IN23,IN24,IN25,IN26,IN27, 
     3                  IN28,IN29,IN30,IN31,IN32,IN33,IN34,IN35,IN36, 
     4                  IN37,IN38,IN39,IN40,IN41,IN42,IN43,IN44,IN45, 
     5                  IN46,IN47,IN48,IN49,IN50,IN51,IN52,IN53,IN54, 
     6                  IN55,IN56,IN57,IN58,IN59,IN60,IN61,IN62,IN63, 
     7                  IN64,IN65,IN66,IN67,IN68,IN69,IN70,IN71,IN72, 
     8                  IN73,IN74,IN75,IN76,IN77,IN78,IN79,IN80,IN81, 
     9                  IN82,IN83,IN84,IN85,IN86,IN87,IN88,IN89,IN90, 
     A                  IN91,IN92,IN93,IN94,IN95,IN96,IN97,IN98,IN99
  
      COMMON /OZPOINT/  IO1, IO2, IO3, IO4, IO5, IO6, IO7, IO8, IO9,
     1                  IO10,IO11,IO12,IO13,IO14,IO15,IO16,IO17,IO18, 
     2                  IO19,IO20,IO21,IO22,IO23,IO24,IO25,IO26,IO27
  
      COMMON /TRPOINT/  IR1, IR2, IR3, IR4, IR5, IR6, IR7, IR8, IR9,
     1                  IR10,IR11,IR12,IR13,IR14,IR15,IR16,IR17,IR18, 
     2                  IR19,IR20,IR21,IR22,IR23,IR24,IR25,IR26,IR27, 
     3                  IR28,IR29,IR30,IR31,IR32,IR33,IR34,IR35,IR36, 
     4                  IR37,IR38,IR39,IR40,IR41,IR42,IR43,IR44,IR45, 
     5                  IR46,IR47,IR48,IR49,IR50,IR51,IR52,IR53,IR54, 
     6                  IR55,IR56,IR57,IR58,IR59,IR60,IR61,IR62,IR63, 
     7                  IR64,IR65,IR66,IR67,IR68,IR69,IR70,IR71,IR72, 
     8                  IR73,IR74,IR75,IR76,IR77,IR78,IR79,IR80,IR81, 
     9                  IR82,IR83,IR84,IR85,IR86,IR87,IR88,IR89,IR90, 
     A                  IR91,IR92,IR93,IR94,IR95,IR96,IR97,IR98,IR99
  
      COMMON /RADSIZE/ KMX, LMX, KMXP, NGLP1, NGL 
C 
C     * IT ALSO CALCULATES THE RADIATION POINTER ARRAYS USED SEPARATELY 
C     * IN THE LONGWAVE AND SHORTWAVE ROUTINES, CALLED IRL AND IRS, 
C     * RESPECTIVELY. THESE ARRAYS ARE PASSED BACK TO PHYSICS AND 
C     * EVENTUALLY ON TO THE RADIATION. 
C 
      INTEGER IRL(1), IRS(1)
C---------------------------------------------------------------------- 
C     * CONVEC6 POINTERS. 
  
      IN1  = 1
      IN2  = IN1  + ILG*ILEV
      IN3  = IN2  + ILG*ILEV
      IN4  = IN3  + ILG*ILEV
      IN5  = IN4  + ILG*(ILEV-1)*6
      IN6  = IN5  + ILG 
      IN7  = IN6  + ILG 
      IN8  = IN7  + ILG 
      IN9  = IN8  + ILG 
      IN10 = IN9  + ILG 
      IN11 = IN10 + ILG 
      IN12 = IN11 + ILG 
      IN13 = IN12 + ILG 
      IN14 = IN13 + ILG 
      IN15 = IN14 + ILG 
      IN16 = IN15 + ILG 
      IN17 = IN16 + ILG 
      IN18 = IN17 + ILG 
      IN19 = IN18 + ILG 
      IN20 = IN19 + ILG 
      IN21 = IN20 + ILG 
      IN22 = IN21 + ILG 
      IN23 = IN22 + ILG 
      IN24 = IN23 + ILG 
      IN25 = IN24 + ILG 
      IN26 = IN25 + ILG 
      IN27 = IN26 + ILG 
      IN28 = IN27 + ILG 
      IN29 = IN28 + ILG 
      IN30 = IN29 + ILG 
      IN31 = IN30 + ILG 
      IN32 = IN31 + ILG 
      IN33 = IN32 + ILG 
      IN34 = IN33 + ILG 
      IN35 = IN34 + ILG 
      IN36 = IN35 + ILG 
      IN37 = IN36 + ILG 
      IN38 = IN37 + ILG 
      INSZ = IN38 + ILG 
  
C     * GRAVITY WAVE POINTERS.
  
      IG1  = 1
      IG2  = IG1  + ILG*ILEV
      IG3  = IG2  + ILG*ILEV
      IG4  = IG3  + ILG*ILEV
      IG5  = IG4  + ILG 
      IG6  = IG5  + ILG 
      IG7  = IG6  + ILG 
      IG8  = IG7  + ILG 
      IG9  = IG8  + ILG 
      IG10 = IG9  + ILG 
      IG11 = IG10 + ILG*ILEV
      IG12 = IG11 + ILG 
      IG13 = IG12 + ILG 
      IG14 = IG13 + ILG 
      IG15 = IG14 + ILG 
      IG16 = IG15 + ILG 
      IG17 = IG16 + ILG*ILEV
      IG18 = IG17 + ILG*ILEV
      IG19 = IG18 + ILG*ILEV
      IG20 = IG19 + ILG*ILEV
      IG21 = IG20 + ILG*ILEV
      IGSZ = IG21 + ILG 
  
C     * VRTDFS6 POINTERS. 
  
      IV1  = 1
      IV2  = IV1  + ILG 
      IV3  = IV2  + ILG 
      IV4  = IV3  + ILG 
      IV5  = IV4  + ILG 
      IV6  = IV5  + ILG 
      IV7  = IV6  + ILG 
      IV8  = IV7  + ILG 
      IV9  = IV8  + ILG 
      IV10 = IV9  + ILG 
      IV11 = IV10 + ILG 
      IV12 = IV11 + ILG 
      IV13 = IV12 + ILG*ILEV
      IV14 = IV13 + ILG*ILEV
      IV15 = IV14 + ILG 
      IV16 = IV15 + ILG 
      IV17 = IV16 + ILG 
      IV18 = IV17 + ILG 
      IV19 = IV18 + ILG 
      IV20 = IV19 + ILG*ILEV
      IV21 = IV20 + ILG*ILEV
      IV22 = IV21 + ILG*ILEV
      IV23 = IV22 + ILG*ILEV
      IV24 = IV23 + ILG 
      IV25 = IV24 + ILG 
      IV26 = IV25 + ILG*ILEV
      IV27 = IV26 + ILG*ILEV
      IV28 = IV27 + ILG*ILEV
      IV29 = IV28 + ILG*ILEV
      IV30 = IV29 + ILG*ILEV
      IV31 = IV30 + ILG*ILEV
      IV32 = IV31 + ILG*ILEV
      IV33 = IV32 + ILG*ILEV
      IV34 = IV33 + ILG 
      IV35 = IV34 + ILG*ILEV
      IV36 = IV35 + ILG*ILEV
      IV37 = IV36 + ILG*ILEV
      IV38 = IV37 + ILG 
      IV39 = IV38 + ILG 
      IV40 = IV39 + ILG 
      IV41 = IV40 + ILG*ILEV
      IV42 = IV41 + ILG 
      IV43 = IV42 + ILG 
      IV44 = IV43 + ILG 
      IV45 = IV44 + ILG 
      IVSZ = IV45 + ILG 
  
C     * CLOUDS POINTERS.
  
      IC1  =   1
      IC2  = IC1  + ILG*LEV 
      IC3  = IC2  + ILG*LEV 
      IC4  = IC3  + ILG*LEV 
      IC5  = IC4  + ILG*LEV 
      IC6  = IC5  + ILG 
      IC7  = IC6  + ILG 
      IC8  = IC7  + ILG 
      IC9  = IC8  + ILG 
      IC10 = IC9  + LEV 
      IC11 = IC10 + LEV 
      IC12 = IC11 + (LEV+1) 
      IC13 = IC12 + ILG 
      IC14 = IC13 + ILG 
      IC15 = IC14 + ILG 
      IC16 = IC15 + ILG 
      IC17 = IC16 + ILG 
      IC18 = IC17 + ILG 
      IC19 = IC18 + ILG 
      IC20 = IC19 + ILG 
      ICSZ = IC20 + ILG 
  
C     * OZONE POINTERS. 
  
      IO1  =   1
      IO2  = IO1  + ILG 
      IO3  = IO2  + ILG 
      IO4  = IO3  + ILG 
      IO5  = IO4  + ILG 
      IO6  = IO5  + ILG 
      IO7  = IO6  + ILG 
      IOSZ = IO7  + LEVOZ 
  
C     * GENERAL RADIATION POINTERS. 
C     * DEFINED RADIATION PARAMETERS USED IN THE POINTER CALCULATION. 
C     * NOTE THAT THESE ARE EQUIVALENT TO THOSE DEFINED IN THE
C     * PROGRAM GCMPARM, AS WELL AS SUBROUTINES POINTPH, SCADDR AND 
C     * RADIATN ITSELF. 
C 
      NINT=5
      NG1=2 
      NTR=11
      NG1P1=NG1+1 
      NUA=3*NINT+3
      MP=NINT+3 
      MP2=2*MP
      NTRA=NUA+1
      LN6=MP2*NTR*6 
      KMX=ILEV+1
      LMX=ILG 
      KMXP=KMX+1
      NGLP1=KMX*NG1P1+1 
      NGL=NGLP1-1 
  
      IR1  =   1
      IR2  = IR1  + ILG 
      IR3  = IR2  + ILG 
      IR4  = IR3  + ILG*KMX 
      IR5  = IR4  + ILG*KMX 
      IR6  = IR5  + ILG*KMX*5 
      IR7  = IR6  + ILG*2 
      IR8  = IR7  + ILG 
      IR9  = IR8  + ILG*KMXP
      IR10 = IR9  + ILG*KMXP
      IR11 = IR10 + ILG*KMXP
      IR12 = IR11 + ILG*KMX 
      IR13 = IR12 + ILG 
      IR14 = IR13 + ILG 
      IR15 = IR14 + ILG*KMX 
      IR16 = IR15 + ILG*2*KMX 
      IR17 = IR16 + ILG*2*KMX 
      IR18 = IR17 + ILG*2*KMX 
      IR19 = IR18 + ILG*KMXP
      IR20 = IR19 + ILG*KMXP
      IR21 = IR20 + ILG*KMX 
      IR22 = IR21 + ILG*2*KMXP
      IR23 = IR22 + ILG 
      IR24 = IR23 + ILG 
      IR25 = IR24 + ILG*NGLP1 
      IR26 = IR25 + ILG*KMXP
      IR27 = IR26 + ILG*KMXP
      IR28 = IR27 + ILG*KMXP
      IR29 = IR28 + ILG*KMX 
      IR30 = IR29 + ILG*KMX 
      IR31 = IR30 + ILG*KMX 
      IR32 = IR31 + ILG*KMX
      IR33 = IR32 + ILG
      IRSZ = IR33 + ILG 
C 
C     * SHORTWAVE RADIATION POINTERS. 
C 
      IRS( 1) = 1 
      IRS( 2) = IRS( 1) + ILG*3*KMXP
      IRS( 3) = IRS( 2) + ILG*KMXP
      IRS( 4) = IRS( 3) + ILG*2*KMXP
      IRS( 5) = IRS( 4) + ILG*6*KMXP
      IRS( 6) = IRS( 5) + ILG*6*KMXP
      IRS( 7) = IRS( 6) + ILG*2*KMXP
      IRS( 8) = IRS( 7) + ILG*KMXP
      IRS( 9) = IRS( 8) + ILG*8 
      IRS(10) = IRS( 9) + ILG*KMXP
      IRS(11) = IRS(10) + ILG*KMXP
      IRS(12) = IRS(11) + ILG*8 
      IRS(13) = IRS(12) + ILG 
      IRS(14) = IRS(13) + ILG 
      IRS(15) = IRS(14) + ILG 
      IRS(16) = IRS(15) + ILG 
      IRS(17) = IRS(16) + ILG 
      IRS(18) = IRS(17) + ILG 
      IRS(19) = IRS(18) + ILG 
      IRS(20) = IRS(19) + ILG 
      IRS(21) = IRS(20) + ILG 
      IRS(22) = IRS(21) + ILG 
      IRS(23) = IRS(22) + ILG 
      IRS(24) = IRS(23) + ILG 
      IRS(25) = IRS(24) + ILG 
      IRS(26) = IRS(25) + ILG 
      IRS(27) = IRS(26) + ILG 
      IRS(28) = IRS(27) + ILG 
      IRS(29) = IRS(28) + ILG*KMX 
      IRS(30) = IRS(29) + ILG*KMXP
      IRS(31) = IRS(30) + ILG*KMX 
      IRS(32) = IRS(31) + ILG*KMX 
      IRS(33) = IRS(32) + ILG*KMX 
      IRS(34) = IRS(33) + ILG*KMXP
      IRS(35) = IRS(34) + ILG*KMXP
      IRS(36) = IRS(35) + ILG*KMXP
      IRS(37) = IRS(36) + ILG 
      IRS(38) = IRS(37) + ILG 
      IRS(39) = IRS(38) + ILG*2 
      IRS(40) = IRS(39) + ILG 
      IRS(41) = IRS(40) + ILG 
      IRS(42) = IRS(41) + ILG*KMXP
      IRS(43) = IRS(42) + ILG*KMXP
      IRS(44) = IRS(43) + ILG 
      IRS(45) = IRS(44) + ILG 
      IRS(46) = IRS(45) + ILG 
      IRS(47) = IRS(46) + ILG 
      IRS(48) = IRS(47) + ILG 
      IRS(49) = IRS(48) + ILG 
      IRS(50) = IRS(49) + ILG 
      IRS(51) = IRS(50) + ILG 
      IRS(52) = IRS(51) + ILG 
      IRS(53) = IRS(52) + ILG 
      IRS(54) = IRS(53) + ILG*KMX 
      IRS(55) = IRS(54) + ILG*KMX 
      IRSLAST = IRS(55) + ILG 
  
C     * LONGWAVE RADIATION POINTERS.
  
      IRL( 1) = 1 
      IRL( 2) = IRL( 1) + ILG*8*NGLP1 
      IRL( 3) = IRL( 2) + ILG*NUA*NGLP1 
      IRL( 4) = IRL( 3) + ILG*KMXP
      IRL( 5) = IRL( 4) + ILG*KMXP*KMXP 
      IRL( 6) = IRL( 5) + ILG*KMXP
      IRL( 7) = IRL( 6) + ILG*NINT
      IRL( 8) = IRL( 7) + ILG 
      IRL( 9) = IRL( 8) + ILG 
      IRL(10) = IRL( 9) + ILG*NGLP1 
      IRL(11) = IRL(10) + ILG*NUA 
      IRL(12) = IRL(11) + ILG*NTRA
      IRL(13) = IRL(12) + ILG*NTRA
      IRL(14) = IRL(13) + ILG*NTRA
      IRL(15) = IRL(14) + ILG*NINT
      IRL(16) = IRL(15) + ILG 
      IRL(17) = IRL(16) + ILG 
      IRL(18) = IRL(17) + ILG 
      IRL(19) = IRL(18) + ILG 
      IRL(20) = IRL(19) + ILG 
      IRL(21) = IRL(20) + ILG 
      IRL(22) = IRL(21) + ILG 
      IRL(23) = IRL(22) + ILG 
      IRL(24) = IRL(23) + ILG 
      IRL(25) = IRL(24) + ILG 
      IRL(26) = IRL(25) + ILG 
      IRL(27) = IRL(26) + ILG 
      IRL(28) = IRL(27) + ILG 
      IRL(29) = IRL(28) + ILG 
      IRL(30) = IRL(29) + ILG 
      IRL(31) = IRL(30) + ILG 
      IRL(32) = IRL(31) + ILG 
      IRL(33) = IRL(32) + ILG 
      IRL(34) = IRL(33) + ILG 
      IRL(35) = IRL(34) + ILG 
      IRL(36) = IRL(35) + ILG 
      IRL(37) = IRL(36) + ILG 
      IRL(38) = IRL(37) + ILG 
      IRL(39) = IRL(38) + ILG 
      IRL(40) = IRL(39) + ILG*KMXP
      IRL(41) = IRL(40) + ILG 
      IRL(42) = IRL(41) + ILG 
      IRL(43) = IRL(42) + ILG 
      IRLLAST = IRL(43) + ILG 
  
C 
C     * ERROR EXITS (TO BE CONSISTENT WITH SPACE RESERVED IN GCMPAR6).
C     * ENSURE RADIATION REQUIRES MOST OF "WRK" WORK SPACE. 
C     * ENSURE THAT SHORTWAVE RADIATION WORK SPACE HIDDEN INSIDE
C     * LONGWAVE RADIATION AND THAT THE LATTER REQUIRES NO MORE SPACE 
C     * THAN RESERVED BY $IVA$ GCMPR6U DEFINITION.
C 
      IF(IRSZ.LT.INSZ)                           CALL XIT('POINT6U',-1) 
      IF(IRSZ.LT.IGSZ)                           CALL XIT('POINT6U',-2) 
      IF(IRSZ.LT.IVSZ)                           CALL XIT('POINT6U',-3) 
      IF(IRSZ.LT.ICSZ)                           CALL XIT('POINT6U',-4) 
      IF(IRSZ.LT.IOSZ)                           CALL XIT('POINT6U',-5) 
C 
      IF(IRLLAST.LT.IRSLAST)                     CALL XIT('POINT6U',-6) 
      IF(IVA    .LT.IRLLAST)                     CALL XIT('POINT6U',-7) 
  
      RETURN
      END 
