      SUBROUTINE SIGXK2 (SHTXKJ,SHXKJ, SHJ,SHTJ, NK,NK1,ILG,LON,RGOCP)
  
C     * JUL 15/88 - M.LAZARE. - CHANGE (NK,I) ARRAYS TO PROPER (I,NK).
C     * JAN 22/88 - R.LAPRISE.
C     * DEFINE SIGMA**(RGAS/CPRES) AT LAYERS CENTRES AND TOPS.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL SHTXKJ (ILG,NK ), SHXKJ (ILG,NK )
      REAL SHTJ   (ILG,NK1), SHJ   (ILG,NK )
C-----------------------------------------------------------------------
      DO 500  K=1,NK
         DO 100 I=1,LON 
            SHXKJ (I,K)=SHJ (I,K)**RGOCP
            SHTXKJ(I,K)=SHTJ(I,K)**RGOCP
  100    CONTINUE 
  500 CONTINUE
  
      RETURN
C-----------------------------------------------------------------------
      END 
