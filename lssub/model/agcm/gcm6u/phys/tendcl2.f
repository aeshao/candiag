      SUBROUTINE TENDCL2 (TG,ESG, THROW,RMROW, TTG,ESTG,
     1                     PRESPA,AH,BH, ILG,IL1,IL2, ILEV,LEVS,LEV,
     2                     DELT,RMOON,MOIST)
  
C     * AUG 25/89 - M.LAZARE. - ADD ADDITIONAL MOISTURE VARIABLE 4H LNQ.
C     * JAN 30/89 - M.LAZARE. - REMOVE LOWER BOUND ON Q FOR RADIATION 
C     *                         CALCULATION (NOT NEEDED NOW SINCE THIS
C     *                         ROUTINE CALLED AT THE END OF PHYSIC6).
C     * DEC 02/88 - M.LAZARE. - SAME AS TENDCAL EXCEPT NO CONVERSION
C     *                         FROM SPECIFIC HUMIDITY TO MIXING
C     *                         RATIO AT END, SINCE FORMER USED 
C     *                         THROUGHOUT PHYSICS NOW. 
C     *                         ALSO TG,ESG NOT UPDATED, WHICH IS 
C     *                         CONSISTENT WITH WINDS.
C     * MAY 17/88 - R.LAPRISE.- ORIGINAL VERSION TENDCAL. 
C     * CALCULATE TENDENCIES (T,ES)-TG FROM ADJUSTED (TH,RM)-ROW
C     * AND ORIGINAL VALUES (T,ES)-G OF TEMPERATURE AND MODEL MOISTURE
C     * VARIABLE. 
C     * ALSO UPDATE ORIGINAL COPIES.
C     * MOIST CONTROLS THE TYPE OF MOISTURE VARIABLE. 
C     * RMROW CORRESPONDS TO SPECIFIC HUMIDITY. 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL  TG(ILG,ILEV ), THROW(ILG, LEV ),  TTG(ILG,ILEV )
      REAL ESG(ILG, LEVS), RMROW(ILG, LEV ), ESTG(ILG, LEVS)
      REAL PRESPA(ILG),AH(ILEV),BH(ILEV)
  
      COMMON /EPS/ AA,BB,EPS1,EPS2
C-----------------------------------------------------------------------
      IF(LEV.NE.ILEV+1 .OR. LEVS.GT.ILEV)        CALL XIT('TENDCL2',-1) 
  
C     * PROCESS TEMPERATURE.
  
      DO 200 L=1,ILEV 
        DO 100 I=IL1,IL2
          TOLD     = TG   (I,L) 
          TNEW     = THROW(I,L+1) 
          TTG(I,L) = TTG(I,L) + (TNEW-TOLD)*(1./(2.*DELT))
  100   CONTINUE
  200 CONTINUE
  
C     * PROCESS MOISTURE UNDER CONTROL OF MOIST.
  
      DO 700 L=1,LEVS 
        M = L+ILEV+1-LEVS 
        MD= M-1 
  
        IF(MOIST.EQ.NC4TO8("RLNQ"))THEN
          DO 300 I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            ESNEW     = -1./LOG(HUMSP) 
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*(1./(2.*DELT))
  300     CONTINUE
  
        ELSEIF(MOIST.EQ.NC4TO8("SQRT"))THEN
          DO 350 I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            ESNEW     = HUMSP**0.5
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*(1./(2.*DELT))
  350     CONTINUE
  
        ELSEIF(MOIST.EQ.NC4TO8("   Q"))THEN
          DO 400 I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            ESNEW     = HUMSP 
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*(1./(2.*DELT))
  400     CONTINUE
  
        ELSEIF(MOIST.EQ.NC4TO8("T-TD"))THEN
          DO 450 I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            PRESMB    = 0.01*(AH(MD)+BH(MD)*PRESPA(I))
            PVAP      = HUMSP*PRESMB/(EPS1+EPS2*HUMSP)
            TD        = BB/(AA-LOG(PVAP))
            ESNEW     = THROW(I,M) - TD 
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*(1./(2.*DELT))
  450     CONTINUE
  
        ELSEIF(MOIST.EQ.NC4TO8("  TD"))THEN
          DO 500 I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            PRESMB    = 0.01*(AH(MD)+BH(MD)*PRESPA(I))
            PVAP      = HUMSP*PRESMB/(EPS1+EPS2*HUMSP)
            TD        = BB/(AA-LOG(PVAP))
            ESNEW     = TD
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*(1./(2.*DELT))
  500     CONTINUE
  
        ELSEIF(MOIST.EQ.NC4TO8(" LNQ"))THEN
          DO 475 I=IL1,IL2
            ESOLD     = ESG(I,L)
            HUMSP     = RMROW(I,M)
            ESNEW     = LOG(HUMSP) 
            ESTG(I,L) = ESTG(I,L) + (ESNEW-ESOLD)*(1./(2.*DELT))
  475     CONTINUE
  
        ELSE
                                                 CALL XIT('TENDCL2',-2) 
        ENDIF 
  
  700 CONTINUE
  
      RETURN
      END 
