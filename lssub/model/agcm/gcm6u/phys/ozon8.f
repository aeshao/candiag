      SUBROUTINE OZON8 (OZ, OZZXJ, PRESSG, SHTJ,
     1                  LEVOZ, LEV, IL1, IL2, ILG, ILAT, JL,
     2                  X, XM, Y, YM, 
     3                  PLOC, RIS, OREF)
  
C     THIS ROUTINE INTERPOLATES THE GIVEN CUMULATIVE OZONE FOR THE
C     CURRENT LATITUDE (OZZXJ CORRESPONDING TO PREF GRID) TO THE
C     LOCAL PRESSURE GRID. THE RESULT IS RETURNED IN UNITS OF 
C     'CM.ATM STP' IN EACH LAYERS.

C     * JAN 05/94. - M.LAZARE. - LIKE PREVIOUS VERSION OZON7 EXCEPT
C     *                          OPTIMIZED BY CUTTING DOWN ON 
C     *                          INDIRECT-ADDRESSING, MAINLY BY
C     *                          PASSING THE "ROW" OZONE DATA FIELD
C     *                          "OZZXJ" WHICH IS PRE-CALCULATED
C     *                          IN THE GCM DRIVER. ALSO, REALS
C     *                          REPLACE INTEGERS IN WORK ARRAYS USED
C     *                          FOR LEVEL POINTERS, TO AVOID FUTURE
C     *                          PROBLEMS WITH 64 BIT REALS/32 BIT
C     *                          INTEGERS.
C     * OCT 15/92. - M.LAZARE. - PREVIOUS VERSION OZON7.
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * OUTPUT ARRAY...

      REAL OZ(ILG,LEV)
  
C     * INPUT ARRAYS... 
  
      REAL OZZXJ(ILG,LEVOZ), PRESSG(ILG), SHTJ(ILG,LEV) 
      INTEGER JL(ILG)

C     * LOCAL ARRAY... 
  
      REAL PREF(37) 
  
C     * WORK SPACE PASSED FROM PHYSIC......
  
      REAL X(ILG), XM(ILG), Y(ILG), YM(ILG), PLOC(ILG), RIS(ILG),
     1     OREF(LEVOZ)
  
      DATA P REF /.3, .4, .5, .7, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 
     1     7.0, 10., 15., 20., 30., 40., 50., 70., 100., 150.,
     2     200., 300., 400., 500., 700., 1000., 1500., 2000., 
     3     3100., 4400., 8800., 17600., 26000., 37800., 53700., 
     4     75000., 101325./ 
  
C----------------------------------------------------------------------
C........ LOCATE INDICES FOR INTERPOLATION AT FULL PRESSURE LEVELS. 
      L START = 2 
      DO 180 L = 1, LEV 
          DO 120 I = IL1, IL2 
              RIS(I) = 2. 
  120     CONTINUE
  
          DO 140 K = L START, LEVOZ 
              DO 130 I = IL1, IL2 
                  PLOC(I) = SHTJ(I,L) * PRESSG(I)
                  IF( PREF(K) .GE. PLOC(I) .AND. RIS(I) .LE. 2.)    THEN
                      RIS(I)=REAL(K) 
                      X  (I)=PREF(K)
                      XM (I)=PREF(K-1)
                      Y  (I)=OZZXJ(I,K)
                      YM (I)=OZZXJ(I,K-1)
                  ENDIF
                  IF( PLOC(I) .GT. PREF(LEVOZ) )                    THEN
                      RIS(I)=REAL(LEVOZ)
                      X  (I)=P REF(LEVOZ)
                      XM (I)=P REF(LEVOZ-1)
                      Y  (I)=OZZXJ(I,LEVOZ)
                      YM (I)=OZZXJ(I,LEVOZ-1)
                  ENDIF
  130         CONTINUE
  140     CONTINUE
  
C........ LINEAR INTERPOLATION OF LEVEL CUMULATIVE OZONE (CM ATM STP) 

          L START=LEVOZ 
          DO 160 I=IL1,IL2
              L START = MIN(L START, NINT(RIS(I)))
              SLOPE   = (Y(I) - YM(I)) / (X(I) - XM(I)) 
              TERCEP  = YM(I) - SLOPE * XM(I) 
              OZ(I,L) = SLOPE * PLOC(I) + TERCEP 
  160     CONTINUE
  180 CONTINUE

C........ FIND AMOUNT OF OZONE (CM ATM STP) IN EACH LAYER 
  
      DO 200 L = LEV, 2, -1 
          L M 1 = L - 1 
          DO 190 I = IL1, IL2 
              OZ(I,L) = OZ(I,L) - OZ(I,LM1) 
  190     CONTINUE
  200 CONTINUE
C---------------------------------------------------------------------- 
      RETURN
      END
