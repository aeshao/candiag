      SUBROUTINE TRSPOSE(A,M,N,B)

C     * NOV 12/92. -  A.J.Stacey.

C     * TRANSPOSTE THE ORDER OF ARRAY "A" INTO NEW ARRAY "B".
     
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real a(m,n),b(n,m)
C-----------------------------------------------------------------------
      do 10 i=1,m
      do 10 j=1,n
        b(j,i) = a(i,j)
   10 continue
C
      return
      end
