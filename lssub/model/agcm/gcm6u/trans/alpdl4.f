      SUBROUTINE ALPDL4(DELALP,ALP,LSR,LM,NLATJ,IRAM)
C
C     * OCT 12/93 - M. LAZARE. MULTIPLE LATITUDE FORMULATION REVISED 
C     *             SO THAT DONE IN S-N PAIRS, REDUCING CALCULATIONS 
C     *             DONE IN THIS ROUTINE BY A FACTOR OF 2 (TAKING
C     *             INTO ACCOUNT SYMMETRY CONSIDERATIONS FOR ALP'S.).
C     * OCT 15/92 - M. LAZARE. PREVIOUS VERSION ALPDL3.
C 
C     * COMPUTES LAPLACIAN OF LEGENDRE POLYNOMIALS. 
C     * LSR CONTAINS ROW LENGTH INFO. 
C 
C     * LEVEL 2,DELALP,ALP
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DELALP(IRAM,NLATJ), ALP(IRAM,NLATJ) 
      INTEGER LSR(2,1)
C-------------------------------------------------------------------- 
C     * EXTERNAL LOOP OVER MULTIPLE LATITUDES.
C
      DO 300 NJ=1,NLATJ,2
        DO 210 M=1,LM 
          MS=M-1
          FM=FLOAT(MS) 
          KL=LSR(2,M) 
          KR=LSR(2,M+1)-1 
          DO 210 K=KL,KR
            NS=MS+(K-KL) 
            FNS=FLOAT(NS) 
            IF(MOD(NS-MS,2).EQ.0)                THEN
               FACT=1.
            ELSE
               FACT=-1.
            ENDIF
            DELALP(K,NJ)=-FNS*(FNS+1.)*ALP(K,NJ)
            DELALP(K,NJ+1)=FACT*DELALP(K,NJ)
  210   CONTINUE
  300 CONTINUE    
C 
      RETURN
      END 
