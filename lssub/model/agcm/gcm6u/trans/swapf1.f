      SUBROUTINE SWAPF1(F,WRKS,ILEV,NLAT,ILH)

C     * NOV 12/92. -  A.J.Stacey.
C
C     * Transpose F(2,ilev,nlat,ilh) to F(ilev,nlat,2,ilh)
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real f(2*ilev*nlat,ilh),wrks(*)
C-----------------------------------------------------------------------
      do 10 i=1,ilh
        k1 = 1
        k2 = ilev*nlat+1
        do 4 j=1,2*ilev*nlat-1,2
          wrks(k1) = f(j,i)
          k1 = k1 + 1
    4   continue
        do 5 j=2,2*ilev*nlat,2
          wrks(k2) = f(j,i)
          k2 = k2 + 1
    5   continue
        do 8 j=1,2*ilev*nlat
          f(j,i) = wrks(j)
    8   continue
   10 continue
C
      return
      end
