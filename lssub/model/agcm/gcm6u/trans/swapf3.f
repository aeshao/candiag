      SUBROUTINE SWAPF3(G,WRKS,ILH,NLAT,ILEV,LON,F)
C
C     * NOV 12/92. -  A.J.Stacey.
C     *
C     * Transpose G(2*ILH*NLAT,ILEV) to F(ILEV,NLAT,2,ILH).
C     *
C     * NOTE: For multiple latitude use, The actual transpose operation
C     * is G(2*ILH*NLAT,ILEV) to F(ILEV,NLAT,2,ILH).  What this means
C     * is that the 2 extra memory locations for each transform are unused
C     * by the physics, but are inserted here before the data is passed to
C     * the transforms (note that LON+2 = 2*ILH = ILG).
C     *
C     * Note that it is very important that the input array be dimensioned
C     * with the unused memory locations explicitly allocated, or else 
C     * the starting addresses according to level and variable will not be
C     * maintained. To avoid memory bank conflicts, the inner dimension of
C     * "g" is augmented by one over what is used.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      dimension f(ilev,nlat,2*ilh)
      dimension wrks(2*ilh,ilev,nlat)
      dimension g(2*ilh*nlat+1,ilev)
C-----------------------------------------------------------------------
C
C     * Transpose G(2*ILH*NLAT,ILEV) to WRKS(2,ILH,ILEV,NLAT).
C
      do 200 lev=1,ilev
        k = 1
        do 100 lat=1,nlat
          do 100 j=1,lon
            wrks(j,lev,lat) = g(k,lev)
            k = k + 1
  100   continue
  200 continue
C
C     * 2) Transpose WRKS(2,ILH,ILEV,NLAT) to F(ILEV,NLAT,2,ILH).
C
      call trspose(wrks,2*ilh,ilev*nlat,f)
C
      return
      end
