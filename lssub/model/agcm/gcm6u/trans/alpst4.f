      SUBROUTINE ALPST4(ALP,LSR,LM,SINLAT,EPSI,NLATJ,IRAM) 
C
C     * OCT 12/93 - M. LAZARE. MULTIPLE LATITUDE FORMULATION REVISED 
C     *             SO THAT DONE IN S-N PAIRS, REDUCING CALCULATIONS 
C     *             DONE IN THIS ROUTINE BY A FACTOR OF 2 (TAKING
C     *             INTO ACCOUNT SYMMETRY CONSIDERATIONS FOR ALP'S.).
C     * OCT 15/92 - M. LAZARE. PREVIOUS VERSION ALPST3.
C 
C     * PUTS LEGENDRE POLYNOMIALS INTO ALP, FOR MULTIPLE-LATITUDE
C     * FORMULATION WITH ORDERED S-N PAIRS.
C
C     * SINLAT IS THE SINE OF THE LATITUDE(S). 
C     * EPSI IS A FIELD OF PRECOMPUTED CONSTANTS. 
C     * LSR CONTAINS ROW LENGTH INFO FOR ALP,EPSI.
C 
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER LSR (2,1) 
C     * LEVEL 2,ALP,EPSI
      REAL*8 ALP(IRAM,NLATJ), EPSI(IRAM), SINLAT(NLATJ)
C-------------------------------------------------------------------- 
C     * EXTERNAL LOOP OVER MULTIPLE-LATITUDES.
C     * ABORT IF NLATJ NOT FACTOR OF 2.
C
      IF(MOD(NLATJ,2).NE.0)                    CALL XIT('ALPST4',-1)
C
      DO 300 NJ=1,NLATJ,2
        COS2=1.-SINLAT(NJ)**2 
        PROD=1. 
        A=1.
        B=0.
C 
C       * LOOP OVER LONGITUDINAL WAVE NUMBERS.
C 
        DO 230 M=1,LM 
          MS=M-1
          FM=FLOAT(MS) 
          IF(M.EQ.1) GO TO 210
          A=A+2.
          B=B+2.
          PROD=PROD*COS2*A/B
C 
C         * COMPUTE THE FIRST VALUE IN THE ROW. 
C         * COMPUTE THE SECOND ELEMENT ONLY IF NEEDED.
C 
  210     KL=LSR(2,M) 
          KR=LSR(2,M+1)-1 
          ALP(KL,NJ)=SQRT(.5*PROD) 
          ALP(KL,NJ+1)=ALP(KL,NJ)
          IF(KR.GT.KL)                               THEN
            ALP(KL+1,NJ)=SQRT(2.*FM+3.)*SINLAT(NJ)*ALP(KL,NJ)
            ALP(KL+1,NJ+1)=-ALP(KL+1,NJ)
          ENDIF
C 
C         * COMPUTE THE REST OF THE VALUES IN THE ROW IF NEEDED.
C 
          KL2=KL+2
          IF(KL2.GT.KR) GO TO 230 
          DO 220 K=KL2,KR 
            NS=MS+(K-KL) 
            IF(MOD(NS-MS,2).EQ.0)                THEN
               FACT=1.
            ELSE
               FACT=-1.
            ENDIF
            ALP(K,NJ)=(SINLAT(NJ)*ALP(K-1,NJ)-EPSI(K-1)*ALP(K-2,NJ))/
     1                EPSI(K) 
            ALP(K,NJ+1)=FACT*ALP(K,NJ)
  220     CONTINUE
  230   CONTINUE
  300 CONTINUE
C    
      RETURN
      END 
