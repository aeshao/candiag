      SUBROUTINE SWAPS(S,WRKS,LA,ILEV)

C     * NOV 12/92. -  A.J.Stacey.
C
C     * Transpose S(2,LA,ILEV) to S(2,ILEV,LA)
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real s(2,la,ilev),wrks(2,ilev,la)
C-----------------------------------------------------------------------
      do 10 j=1,la
        do 10 i=1,ilev
          wrks(1,i,j) = s(1,j,i)
          wrks(2,i,j) = s(2,j,i)
   10 continue
C
      return
      end
