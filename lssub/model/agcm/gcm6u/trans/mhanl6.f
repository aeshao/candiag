      SUBROUTINE MHANL6(PT,CT,PEET,PST,EST,PRESS,LA,IRAM,NLAT,
     1                  PUTF,PVTF,TUTF,TVTF,PEETF,PSTF,EF,SUSF,SVSF,
     2                  ESTF,PRESSF,ILH,ILG,LSR,LM,ALP,DALP,DELALP, 
     3                  WL,WOCSL,LON,ILEV,LEVS,IFAX,
     4                  TRIGS,TRACT,TRACTF,XUTF,XVTF,ITRAC,NTRAC,LVECT,
     5                  four1,four2,four3,four4,spec1,spec2,wrks)
C 
C     * OCT 30/92 - J. STACEY (MHANL6 CREATED FROM MHANL5.  EXTENSIVELY
C     *                        MODIFIED TO IMPLEMENT MULTIPLE LATITUDES
C     *                        AND UNIT STRIDES THROUGH DATA)
C     * JUL 14/92 - E. CHAN. - PREVIOUS VERSION MHANL5.
C     *  
C     * AS IN MHANL5, EXCEPT FORMULATION FOR MULTIPLE LATITUDES 
C     * IMPLEMENTED. 
C     *  
C     * TRANSFORM GRID VALUES TO FOURIER COEFFICIENTS, THEN CALCULATES
C     * CONTRIBUTIONS TO SPECTRAL TENDENCIES FOR THIS GAUSSIAN LATITUDE.
C     * TRACER VARIABLE TRANSFORMS DONE ONLY IF ITRAC.NE.0. 
C     *
C     *    ALP = LEGENDRE POLYNOMIALS *GAUSSIAN WEIGHT
C     *   DALP = N-S DERIVATIVE OF LEG.POLY. *GAUSS.WEIGHT/SIN(LAT) 
C     * DELALP = LAPLACIAN OF LEG.POLY. *(-.5*GAUSS.WEIGHT/SIN(LAT))
C     * WRKS IS AN SCM WORK ARRAY USED BY THE FAST FOURIER TRANSFORM. 
C     * ILH = FIRST DIMENSION OF COMPLEX ARRAYS.
C     * ILG = FIRST DIMENSION OF REAL ARRAYS (GRID SLICES), NOTE
C     *       THAT ILG MUST EQUAL 2*ILH.
C     * LON = NUMBER OF DISTINCT LONGITUDES.
C     *
C     * INPUT FIELDS
C     * ------------
C     *
C     * AS IN MHEXP6, THIS ROUTINE DEPENDS ON DATA IN /GR/ BEING ALIGNED
C     * APPROPRIATELY (TO INCREASE THE EFFECTIVE VECTOR LENGTH).  THE FOLLOWING
C     * LIST IS THE RECOMMENDED DATA STRUCTURE FOR THE GRID FIELDS.
C     *
C     *    COMMON /GR/    TTG(ILH,NLAT,ILEV)
C     *    COMMON /GR/   ESTG(ILH,NLAT,LEVS)
C     *    COMMON /GR/ TRACTG(ILH,NLAT,ILEV,NTRAC)
C     *    COMMON /GR/    VTG(ILH,NLAT,ILEV)
C     *    COMMON /GR/    UTG(ILH,NLAT,ILEV)
C     *    COMMON /GR/   PSTG(ILH,NLAT)
C     *    COMMON /GR/ PRESSG(ILH,NLAT)
C     *
C     * AND
C     *
C     *    COMMON /GR/   TVTG(ILH,NLAT,ILEV)
C     *    COMMON /GR/   SVTG(ILH,NLAT,LEVS)
C     *    COMMON /GR/   XVTG(ILH,NLAT,ILEV,NTRAC)
C     *    COMMON /GR/   UTMP(ILH,NLAT,ILEV)           *** WORK SPACE ***
C     *    COMMON /GR/   VTMP(ILH,NLAT,ILEV)           *** WORK SPACE ***
C     *
C     * AND
C     *
C     *    COMMON /GR/   TUTG(ILH,NLAT,ILEV)
C     *    COMMON /GR/   SUTG(ILH,NLAT,LEVS)
C     *    COMMON /GR/   XUTG(ILH,NLAT,ILEV,NTRAC)
C     *    COMMON /GR/     EG(ILH,NLAT,ILEV)
C     *
C     * THIS ARRANGEMENT OF DATA SATISFIES THE FOLLOWING CONSTRAINTS.
C     *   1) UTG,VTG,TTG,ESTG,TRACTG CONTIGUOUS FOR I/O.
C     *   2) TTG,ESTG,TRACG CONTIGUOUS FOR E-W DERIVATIVE CALCULATION.
C     *   3) TUTG,SUTG,XUTG CONTIGUOUS FOR E-W DERIVATIVE CALCULATION.
C     *   4) TTG,ESTG,TRACTG,VTG,UTG,PSTG,PRESSG CONTIGUOUS FOR LEGENDRE
C     *      TRANSFORM CONTRIBUTIONS TO PEET,EST,TRACT,PT,CT,PST,PRESS 
C     *      TENDENCIES.
C     *   5) TVTG,SVTG,XVTG,UTMP,VTMP CONTIGUOUS FOR LEGENDRE
C     *      TRANSFORM CONTRIBUTIONS TO PEET,EST,TRACT,PT,CT TENDENCIES.
C     * SPECIAL NOTE: THE TWO ARRAYS "UTMP" AND "VTMP" ARE TWO WORK ARRAYS
C     * THAT HAVE BEEN INSERTED INTO THE GRID FIELDS TO STORE COPIES OF
C     * "UTG" AND "VTG" AFTER THE FFTS.  THE ORDERING OF "UTMP" AND "VTMP"
C     * MUST BE REVERSED RELATIVE TO "UTG" AND "VTG" TO REFLECT THEIR
C     * "CROSS-PRODUCT" CONTRIBUTIONS TO THE "PT" AND "CT" TENDENCIES.
C     *
C     * THE DATA IN THE ABOVE GRID FIELDS IS NOT IN THE OPTIMAL ORGANIZATION
C     * FOR THE PRESENT ROUTINE.  ON ENTRY INTO THIS ROUTINE, THE DATA IN
C     * THE ABOVE COMMON BLOCK IS TRANSPOSED INTO WORKING ARRAYS "FOUR1",
C     * "FOUR2", AND "FOUR3" AS FOLLOWS:
C     *
C     * TTG,ESTG,TRACTG,VTG,UTG,PSTG,PRESSG --> FOUR1(NLEV1,NLAT,2*ILH)
C     * TVTG,SVTG,XVTG,UTMP,VTMP            --> FOUR2(NLEV2,NLAT,2*ILH)
C     * TUTG,SUTG,XUTG,EG                   --> FOUR3(NLEV3,NLAT,2*ILH)
C     *
C     * THE DATA IN THIS "ROW-WISE" FORMAT IS THEN PASSED TO THE FFT ROUTINES TO
C     * TRANSFORM FROM GRID TO FOURIER SPACE.  THE "ROW-WISE" TRANSFORM IS
C     * A FACTOR OF TWO FASTER THAN THE COLUMN-WISE TRANSFORM ON THE SX-3.
C     *
C     * TO CONFUSE THINGS FURTHER, AFTER THE FFTS ARE PERFORMED THESE
C     * FOURIER WORKING ARRAYS ARE TRANSPOSED INTO THE FORMAT THAT IS 
C     * OPTIMAL FOR THE LEGENDRE TRANSFORM ROUTINE "VFAST", I.E.
C     *
C     *      FOUR1(NLEV1,NLAT,2,ILH) --> FOUR1(2,NLEV1,NLAT,ILH)
C     *      FOUR2(NLEV2,NLAT,2,ILH) --> FOUR2(2,NLEV2,NLAT,ILH)
C     *      FOUR3(NLEV3,NLAT,2,ILH) --> FOUR3(2,NLEV3,NLAT,ILH)
C     *
C     * IT IS IN THIS SECOND FORMAT THAT THESE WORKING ARRAYS ARE DECLARED
C     * HERE IN "MHANL".  THIS ALLOWS CONVENIENT CALCULATION OF THE DERIVATIVE
C     * TERMS THAT PRECEDE THE LEGENDRE TRANSFORM.  THE OVERHEAD OF 
C     * TRANSPOSING THE DATA IS OFFSET BY THE FACTOR OF TWO SPEEDUP IN USING
C     * UNIT-STRIDE ACCESSING OF THE DATA.
C     *
C     * OUTPUT FIELDS
C     * -------------
C     *
C     * THE OUTPUT DATA FIELDS ARE THE SPECTRAL TENDENCIES, WHICH ARE
C     * ACCUMULATED BY "VFAST" INTO THE FOLLOWING DATA STRUCTURE:
C     *
C     *    COMMON /SP/    TT(LA,ILEV)
C     *    COMMON /SP/   EST(LA,LEVS)
C     *    COMMON /SP/ TRACT(LA,ILEV,NTRAC)
C     *    COMMON /SP/    PT(LA,ILEV)
C     *    COMMON /SP/    CT(LA,ILEV)
C     *    COMMON /SP/   PST(LA)
C     *    COMMON /SP/ PRESS(LA)
C     *
C     * THIS ARRANGEMENT OF DATA MAXIMIZES THE VECTOR LENGTHS FOR THE
C     * LEGENDRE TRANSFORM AND GAUSSIAN INTEGRATION.
C     *
C     * IN PRACTISE, THESE TENDENCIES ARE ACCUMULATED INTO A WORKING
C     * SPECTRAL ARRAY "SPEC1" WHICH MUST FIRST BE INITIALIZED WITH THE
C     * CONTENTS OF THE TENDENCIES IN /SP/.
C     *
C     * TT,EST,TRACT,PT,CT,PST,PRESS --> SPEC1(2,NLEVS,LA)
C     *
C     * THE SPECTRAL TENDENCIES ARE THEN ACCUMULATED BY THE LEGENDRE
C     * TRANSFORM ROUTINE "VFAST" INTO "SPEC1".  BEFORE RETURNING TO
C     * THE CALLING PROGRAM THE "SPEC1" ARRAY IS TRANSPOSED INTO /SP/.
C     *
C     * THE "SPEC2" TENDENCY ARRAY IS USED TO ACCUMULATE THE LAPLACIAN
C     * CONTRIBUTION TO THE "CT" TENDENCY, WHICH COULDN'T BE ACCOMMODATED
C     * IN THE ABOVE DATA STRUCTURES.
C
C     * ROUTINES CALLED
C     * ---------------
C     *
C     * VFAST  - VECTORIZED "FAST" ROUTINE.
C     *          THIS ROUTINE PERFORMS THE LEGENDRE TRANSFORM FROM
C     *          FOURIER TO SPECTRA{.
C     * FFWFG3 - GRID TO FOURIER TRANSFORMS.
C     * SWAPS
C     * SWAPF3 - INTERNAL DATA RE-ORDERING ROUTINES TO ENHANCE EFFICIENCY.
C     * SWAPF4
C     *

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX PT(LA,1),CT(LA,1),PEET(LA,1),PST(LA)
      COMPLEX EST(LA,1),PRESS(1)
      COMPLEX TRACT(LA,ILEV,NTRAC)
C 
      DIMENSION PUTF(*),PVTF(*),TUTF(*),TVTF(*) 
      DIMENSION PEETF(*),PSTF(*),EF(*)
      DIMENSION SUSF(*),SVSF(*),ESTF(*) 
      DIMENSION PRESSF(*) 
      DIMENSION TRACTF(*)
      DIMENSION XUTF(*),XVTF(*) 
  
      DIMENSION LSR(2,1)

      REAL*8 ALP(iram,nlat),DALP(iram,nlat),DELALP(iram,nlat)
      REAL*8 WL(nlat),WOCSL(nlat)

      DIMENSION WRKS(1),IFAX(1),TRIGS(LON)
C
C     * DECLARE THE FOURIER AND SPECTRAL WORKING ARRAYS.
C
      dimension four1(2,3*ilev+levs+itrac*ntrac*ilev+2,nlat,ilh)
      dimension four2(2,3*ilev+levs+itrac*ntrac*ilev,  nlat,ilh)
      dimension four3(2,2*ilev+levs+itrac*ntrac*ilev,  nlat,ilh)
      dimension four4(2,ilev,                    nlat,ilh)
      dimension spec1(2,3*ilev+levs+itrac*ntrac*ilev+2,la)
      dimension spec2(2,ilev,                          la)
C 
C      * SWITCH TO CONTROL VECTORIZATION ACROSS VARIABLES.
C 
      LOGICAL LVECT 
C-----------------------------------------------------------------------
C     * PART 0 - INITIALIZATION.
C 
C     * QUICK ABORT IF LVECT IS .FALSE.
C     * 92/11/11: Quick abort if ITRAC .ne. 0/1.  This is essential
C     * for the correct dimensioning of the FOUR arrays. [ajs]
C
      IF (.NOT. LVECT) CALL XIT('MHANL6',-1)
      if (itrac .ne. 0 .and. itrac .ne. 1) call xit('MHANL6',-2)

      IR    = LM - 1 
      nilev = 0
      if (itrac .ne. 0) NILEV=ILEV*NTRAC
C
C     * CALCULATE VARIOUS CONSTANTS AND POINTERS.
C
      NLEV1 = 3*ILEV + LEVS + NILEV + 2
      NLEV2 = 3*ILEV + LEVS + NILEV
      NLEV22= NLEV2 - 2*ILEV           !NLEV2 without UTMP, VTMP fields.
      NLEV3 = 2*ILEV + LEVS + NILEV
      NLEV4 = ILEV
      NLEVS = NLEV1


      ittf      = 0
      iestf     = ittf  + ilev
      itractf   = iestf + levs
      ivtf      = itractf
      if (itrac .ne. 0) then
        ivtf    = ivtf  + nilev
      end if
      iutf      = ivtf  + ilev
      ipstf     = iutf  + ilev
      ipressf   = ipstf + 1

      itvtf     = 0
      isvtf     = itvtf + ilev
      ixvtf     = isvtf + levs
      iutmp     = ixvtf
      if (itrac .ne. 0) then
        iutmp   = iutmp + nilev
      end if
      ivtmp     = iutmp + ilev

      itutf     = 0
      isutf     = itutf + ilev
      ixutf     = isutf + levs
      ief       = ixutf
      if (itrac .ne. 0) then
        ief     = ief   + nilev
      end if
      
C
C     * Transpose the grid and spectral input fields into their respective
C     * "FOURx" and "SPECx" working arrays.
C     * I.E.  GRID(2,ILH,NLAT,ILEV) to FOUR(ILEV,NLAT,2,ILH).
C     * This puts the grid data into row-wise format for the FFTs.
C
      call swapf3(peetf, wrks,  ilh, nlat, nlev1, lon, four1)
      call swapf3(tvtf,  wrks,  ilh, nlat, nlev2, lon, four2)
      call swapf3(tutf,  wrks,  ilh, nlat, nlev3, lon, four3)
      call swaps (peet,  spec1, la,  nlev1)
C
C-----------------------------------------------------------------------
C     * PART 1 - FOURIER TRANSFORMS FROM GRID TO FOURIER COEFFICIENTS.
C-----------------------------------------------------------------------
C     * WHEN LVECT IS .TRUE., ALL THE FFT ARE DONE AT ONCE. 
C     * THIS PRESUMES THAT THE GRID SLICES ARE LINED UP PROPERLY
C     * WITH ARRAY  EF  AS THE FIRST ONE OF THE SERIES. 
C     * NUMBER = ILEV*6 (FOR EF,TUTF,TVTF,PUTF,PVTF,PEETF)
C     *         +LEVS*3 (FOR SUSF,SVSF,ESTF)
C     *         +  1 *2 (FOR PSTF,PRESSF),
C     *         +ILEV*3 (FOR TRAC,XUTF,XVTF, IF ITRAC.NE.0).
C 
C     * In MHANL6, the FFTs are done in 3 stages.
C
      nlev1n = nlev1*nlat
      nlev2n = nlev2*nlat
      nlev3n = nlev3*nlat
      call ffwfg3(four1,ilh,four1,ilg,ir,lon,wrks,nlev1n, ifax,trigs)
      call ffwfg3(four2,ilh,four2,ilg,ir,lon,wrks,nlev2n, ifax,trigs)
      call ffwfg3(four3,ilh,four3,ilg,ir,lon,wrks,nlev3n, ifax,trigs)

C
C     * After the FFTs, the data must be again transformed into the
C     * format expected by the Legendre Transform routine.
C
      call swapf4(four1,wrks,nlev1,nlat,ilh)
      call swapf4(four2,wrks,nlev2,nlat,ilh)
      call swapf4(four3,wrks,nlev3,nlat,ilh)
C
C     * At this point, the "FOUR" arrays are in the format as declared
C     * in the declaration statements above.  Now we can copy the fourier
C     * "VTF" and "UTF" fields from "FOUR1" into "FOUR2".  Note that
C     * the ordering of "UTMP" and "VTMP" in "FOUR2" is reversed relative to the
C     * ordering of "VTF" and "UTF" in "FOUR1"---this is the ordering 
C     * required for calculating the cross-product contributions to the 
C     * "PT" and "CT" tendencies.

      do 100 m=1,ilh
      do 100 lat=1,nlat
      do 100 i=1,2*ilev
        four2(i,iutmp+1,lat,m) = four1(i,iutf+1,lat,m)
        four2(i,ivtmp+1,lat,m) = four1(i,ivtf+1,lat,m)
  100 continue
C 
C-----------------------------------------------------------------------
C     * PART 2 - LEGENDRE TRANSFORMS. 
C     * ACCUMULATE CONTRIBUTION OF THIS LATITUDE TO SPECTRAL TENDENCIES.
C-----------------------------------------------------------------------
C     * CALCULATION OF NORTH-SOUTH DERIVATIVE TERMS.
C     * FIRST NEGATE TVTF TERM. 
C 
      do 210 m=1,lm
      do 210 lat=1,nlat
      do 210 i=1,2*ilev
        four2(i,itvtf+1,lat,m) = -four2(i,itvtf+1,lat,m)
  210 continue
C 
C     * WHEN LVECT IS .TRUE., AS MANY TRANSFORMS AS POSSIBLE ARE DONE 
C     * AT ONCE.  THIS PRESUMES THAT GRID SLICES AND SPECTRAL ARRAYS
C     * ARE LINED UP IN MEMORY,  AND THAT TVTG-PEET ARE THE FIRST 
C     * OF THE SERIES.
C
C     * The use of "FOUR2" allows all the Legendre transforms using "DALP"
C     * to be done at once.
C
      call vfast(four2,nlev2,nlat,ilh,
     1            spec1,nlev1,la,
     2            dalp,iram,
     3            nlev2,LSR,LM)
C---------------------------------------------------------------------- 
C     * EAST - WEST DERIVATIVE OF FOURIER COMPONENTS. 
C 
C     DO 330 L=1,ILEV 
C     FSQ=WOCSL/WL
C 
C     DO 320 M=1,LM 
C     BI=FSQ*FLOAT(M-1) 
C      PEETF(M,L)=
C    1 PEETF(M,L)+CMPLX(-BI*AIMAG(TUTF(M,L)), BI*REAL(TUTF(M,L))) 
C      PUTF (M,L)=CMPLX( BI*AIMAG(PUTF(M,L)),-BI*REAL(PUTF(M,L))) 
C      PVTF (M,L)=CMPLX(-BI*AIMAG(PVTF(M,L)), BI*REAL(PVTF(M,L))) 
C 320 CONTINUE
C 330 CONTINUE
C
C     * SPECIAL NOTE:  Note that "PUTF" and "PVTF" map to "VTG" and "UTG"
C     * respectively in the call to "MHANL6".  This mapping is expressed
C     * using the "iutf" and "ivtf" pointers below.
C
      do 330 m=1,lm
        do 320 lat=1,nlat
        fsq   = wocsl(lat)/wl(lat)
        bi    = fsq*float(m-1)
        do 320 lev=1,ilev
          four1(1,ittf+lev,lat,m) =      four1(1,ittf +lev,lat,m)
     1                              - bi*four3(2,itutf+lev,lat,m)
          four1(2,ittf+lev,lat,m) =      four1(2,ittf +lev,lat,m)
     1                              + bi*four3(1,itutf+lev,lat,m)
  320   continue
        do 324 lat=1,nlat
        fsq   = wocsl(lat)/wl(lat)
        bi    = fsq*float(m-1)
        do 324 lev=ivtf+1,ivtf+ilev
          f1tmp                   =      four1(1, lev, lat, m)
          four1(1, lev, lat, m)   =   bi*four1(2, lev, lat, m)
          four1(2, lev, lat, m)   =  -bi*f1tmp
  324   continue
        do 326 lat=1,nlat
        fsq   = wocsl(lat)/wl(lat)
        bi    = fsq*float(m-1)
        do 326 lev=iutf+1,iutf+ilev
          f1tmp                   =      four1(1, lev, lat, m)
          four1(1, lev, lat, m)   =  -bi*four1(2, lev, lat, m)
          four1(2, lev, lat, m)   =   bi*f1tmp
  326   continue
  330 continue
C 
C     IF(ITRAC.NE.0)THEN
C     DO 340 N=1,NTRAC
C     DO 340 L=1,ILEV 
C     DO 340 M=1,LM 
C     BI=FSQ*FLOAT(M-1) 
C         TRACTF(M,L,N)=TRACTF(M,L,N)+
C    1    CMPLX(BI*AIMAG(XUTF(M,L,N)),-BI*REAL(XUTF(M,L,N)))
C 340 CONTINUE
C     ENDIF 

      if (itrac .ne. 0) then
        do 340 m=1,lm
        do 340 lat=1,nlat
        fsq   = wocsl(lat)/wl(lat)
        bi    = fsq*float(m-1)
        do 340 lev=1,itrac*ntrac*ilev
          four1(1,itractf+lev,lat,m) =  four1(1,itractf+lev,lat,m)
     1                             + bi*four3(2,ixutf  +lev,lat,m)
          four1(2,itractf+lev,lat,m) =  four1(2,itractf+lev,lat,m)
     1                             - bi*four3(1,ixutf  +lev,lat,m)

  340   continue
      end if
C 
C     DO 380 L=1,LEVS 
C     FSQ=WOCSL/WL
C 
C     DO 360 M=1,LM 
C     BI = FLOAT(M-1)*FSQ 
C      ESTF (M,L)=
C    1 ESTF (M,L)+CMPLX( BI*AIMAG(SUSF(M,L)),-BI*REAL(SUSF(M,L))) 
C 360 CONTINUE
C 380 CONTINUE

      do 380 m=1,lm
        do 360 lat=1,nlat
        fsq   = wocsl(lat)/wl(lat)
        bi    = fsq*float(m-1)
        do 360 lev=1,levs
          four1(1,iestf+lev,lat,m) =  four1(1,iestf+lev,lat,m)
     1                           + bi*four3(2,isutf+lev,lat,m)
          four1(2,iestf+lev,lat,m) =  four1(2,iestf+lev,lat,m)
     1                           - bi*four3(1,isutf+lev,lat,m)
  360   continue
  380 continue


C-----------------------------------------------------------------------
C     * DO THE DIRECT LEGENDRE TRANSFORMS.
C     * WHEN LVECT IS .TRUE., ALL THE TRANSFORMS ARE DONE AT ONCE.
C     * THIS PRESUMES THAT THE GRID SLICES AND SPECTRAL ARRAYS
C     * ARE LINED UP IN MEMORY,  AND THAT PSTG-PST ARE THE
C     * FIRST OF THE SERIES.
C     * NUMBER = ILEV*3 (FOR PUTF,PVTF,PEETF),
C     *         +LEVS*1 (FOR ESTF), 
C     *         +  1 *2 (FOR PSTF,PRESSF),
C     *         +ILEV*NTRAC (FOR TRACTF, IF ITRAC.NE.0) 
C 
      call vfast(four1,nlev1,nlat,ilh,
     1            spec1,nlev1,la,
     2            alp,iram,
     3            nlev1,LSR,LM)
C 
C-----------------------------------------------------------------------
C     * SUBSTRACT HALF THE LAPLACIAN OF K.E. FROM DIVERGENCE TENDENCY.
C
C     * Copy EF (Fourier transform of EG) into working array "FOUR4"
C
      do 480 m=1,ilh
      do 480 lat=1,nlat
      do 480 i=1,2*ilev
        four4(i,1,lat,m) = four3(i,ief+1,lat,m)
  480 continue
C
C     * Calculate the Laplacian contribution to the CT tendency. 
C     * Note that "SPEC2" *must* be initialized to zero to avoid
C     * accumulating spurious data.
C
      call rzero(spec2,2*la*ilev)
      call vfast(four4,nlev4,nlat,ilh,
     1            spec2,ilev,la,
     2            delalp,iram,
     3            ilev,LSR,LM)
C 
C     * Accumulate contribution for CT from working array "SPEC2".
C     * The "IUTF" pointer corresponds to the same range of levels
C     * as for the "CT" spectral tendency.
C
      do 500 m=1,la
        do 500 i=1,2*ilev
          spec1(i,iutf+1,m) = spec1(i,iutf+1,m) + spec2(i,1,m)
  500 continue
C
C     * Now that the "SPEC1" array contains all the updated data for
C     * the spectral tendency fields, transpose the data back into the /SP/
C     * common block.
C
      call swaps(spec1,  peet, nlev1, la)
C
      RETURN
      END
