      SUBROUTINE ALPDY4(DALP,ALP,LSR,LM,EPSI,NLATJ,IRAM) 
C
C     * OCT 12/93 - M. LAZARE. MULTIPLE LATITUDE FORMULATION REVISED 
C     *             SO THAT DONE IN S-N PAIRS, REDUCING CALCULATIONS 
C     *             DONE IN THIS ROUTINE BY A FACTOR OF 2 (TAKING
C     *             INTO ACCOUNT SYMMETRY CONSIDERATIONS FOR ALP'S.).
C     * OCT 15/92 - M. LAZARE. PREVIOUS VERSION ALPDY3.
C 
C     * SETS DALP TO N-S DERIVATIVE OF LEGENDRE POLYNOMIALS IN ALP. 
C     * EPSI CONTAINS PRECOMPUTED CONSTANTS.
C     * LSR CONTAINS ROW LENGTH ONFO. 
C 
C     * LEVEL 2,DALP,ALP,EPSI 
      IMPLICIT REAL*8 (A-H,O-Z),
     +INTEGER (I-N)
      REAL*8 DALP(IRAM,NLATJ), ALP(IRAM,NLATJ), EPSI(IRAM) 
      INTEGER LSR(2,1)
C-------------------------------------------------------------------- 
C     * EXTERNAL LOOP OVER MULTIPLE LATITUDES.
C
      DO 300 NJ=1,NLATJ,2
        DO 230 M=1,LM 
          MS=M-1
          KL=LSR(2,M) 
          KR=LSR(2,M+1)-2 
C 
          DO 220 K=KL,KR
            NS=MS+(K-KL)
            FNS=FLOAT(NS)
            ALPILM=0. 
            IF(K.GT.KL) ALPILM=ALP(K-1,NJ) 
            DALP(K,NJ)=(FNS+1)*EPSI(K)*ALPILM-FNS*EPSI(K+1)*ALP(K+1,NJ) 
            IF(K.EQ.KL)                                 THEN
              DALP(K,NJ+1)=-DALP(K,NJ)
            ELSE
              IF(MOD(NS-MS,2).EQ.0)                THEN
                 FACT=1.
              ELSE
                 FACT=-1.
              ENDIF
              DALP(K,NJ+1)=-1.*FACT*DALP(K,NJ)
            ENDIF
  220     CONTINUE
          DALP(KR+1,NJ)=0.
          DALP(KR+1,NJ+1)=0. 
  230   CONTINUE      
  300 CONTINUE     
C 
      RETURN
      END 
