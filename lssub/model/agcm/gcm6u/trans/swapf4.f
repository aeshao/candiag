      SUBROUTINE SWAPF4(F,WRKS,ILEV,NLAT,ILH)

C     * NOV 12/92. -  A.J.Stacey.
C
C     * Transpose F(ilev,nlat,2,ilh) to F(2,ilev,nlat,ilh).
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      dimension f(ilev*nlat,2,ilh),wrks(2,ilev*nlat)
C-----------------------------------------------------------------------
      do 10 i=1,ilh
        do 2 j=1,ilev*nlat
          wrks(1,j) = f(j,1,i)
    2   continue
        do 3 j=1,ilev*nlat
          wrks(2,j) = f(j,2,i)
    3   continue
        do 4 j=1,2*ilev*nlat
          f(j,1,i) = wrks(j,1)
    4   continue
   10 continue
C
      return
      end
