      SUBROUTINE SWAPF2(F,WRKS,ILEV,NLAT,ILH,LON,G)
C
C     * NOV 12/92 A.J. STACEY.
C     *
C     * Transpose F(ILEV,NLAT,2,ILH) to G(2,ILH,NLAT,ILEV).
C     *
C     * NOTE: For multiple latitude use, The actual transpose operation
C     * is F(ILEV,NLAT,2,ILH) to G(2,ILH,NLAT,ILEV).  What this means
C     * is that the 2 extra memory locations for each transform, unused
C     * by the physics, are extracted here before the data is passed to
C     * the physics (note that LON+2 = 2*ILH = ILG).
C     *
C     * Note that it is very important that the output array be dimensioned
C     * with the unused memory locations explicitly allocated, or else 
C     * the starting addresses according to level and variable will not be
C     * maintained. To avoid memory bank conflicts, the inner dimension of
C     * "g" is augmented by one over what is used.
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      real f(ilev*nlat,2*ilh)
      real wrks(2*ilh,ilev,nlat)
      real g(2*ilh*nlat+1,ilev)
C--------------------------------------------------------------------------
C
C     * 1) Transpose F(ILEV,NLAT,2,ILH) to WRKS(2,ILH,ILEV,NLAT)
C
      call trspose(f,ilev*nlat,2*ilh,wrks)
C
C     * 2) Transpose WRKS(2,ILH,ILEV,NLAT) to G(2*ILH*NLAT,ILEV).
C
      do 200 lev=1,ilev
        k = 1
        do 100 lat=1,nlat
          do 100 j=1,lon
            g(k,lev) = wrks(j,lev,lat)
            k = k + 1
  100   continue
  200 continue
C
      return
      end
