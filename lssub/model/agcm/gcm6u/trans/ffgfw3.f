      SUBROUTINE FFGFW3 (GD, ILG,FC,ILH,IR,LON,WRK,ILEV,IFAX,TRIGS) 
C   
C     * NOV 01/92 - J.STACEY. CALLS NEW OPTIMIZED FFT DRIVER "VFFT2"
C     *                       FOR SX-3 (ROW-WISE!).
C     * APR 24/92 - M.LAZARE. PREVIOUS VERSION FFGFW.
C
C     * DRIVING ROUTINE FOR THE FOURIER TRANSFORM,   
C     * COEFFICIENTS TO GRID.      
C     * ====================      
C     * FC    = FOURIER COEFFICIENTS,     
C     * ILH   = FIRST DIMENSION OF COMPLEX FC,    
C     * GD    = GRID DATA,      
C     * ILG   = FIRST DIMENSION OF REAL GD, NOTE THAT IT IS  
C     *         ASSUMED THAT GD AND FC ARE EQUIVALENCED IN MAIN, 
C     *         SO OBVIOUSLY ILG MUST EQUAL 2*ILH,   
C     *         ACTUALLY FC IS DECLARED REAL FOR CONVENIENCE.  
C     * IR    = MAXIMUM EAST-WEST WAVE NUMBER (M=0,IR),   
C     * LON   = NUMBER OF DISTINCT LONGITUDES,    
C     * WRK   = WORK SPACE,      
C     * ILEV  = NUMBER OF LEVELS.     
C         
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL FC(ILEV,ILG),GD(ILEV,ILG),WRK(ILEV,1)   
      DIMENSION IFAX(1),TRIGS(LON)     
C----------------------------------------------------------------- 
      ISIGN = +1       
      INC   =  1       
      IR121 =  (IR+1)*2 +1      
      IF(ILG.LT.LON+2) CALL XIT('FFGFW3',-1)    
C         
C     * SET TO ZERO FOURIER COEFFICIENTS BEYOND TRUNCATION.  
C         
      DO 100 I=IR121,LON+2      
      DO 100 L=1,ILEV       
  100 FC(L,I) =0.       
C         
C     * AS MANY AS 256 ARE DONE AT ONCE FOR VECTORIZATION.  
C         
      NSTART=1        
      NTIMES=ILEV/256       
      NREST =ILEV-NTIMES*256      
C         
      IF(NREST.NE.0) THEN      
         LENGTH=NREST       
         NTIMES=NTIMES+1      
      ELSE        
         LENGTH=256       
      ENDIF        
C         
C     * DO THE FOURIER TRANSFORMS.     
C         
      DO 300 N=1,NTIMES       
       CALL VFFT2(FC(NSTART,1),WRK,TRIGS,IFAX,ILEV,INC,LON,LENGTH,ISIGN)
       NSTART=NSTART+LENGTH      
       LENGTH=256       
  300 CONTINUE        
C         
      RETURN        
C-------------------------------------------------------------------- 
      END        
