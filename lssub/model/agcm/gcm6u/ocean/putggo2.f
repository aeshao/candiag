      SUBROUTINE PUTGGO2(A,IBUF,ID,MAXX,NAME,NF,NPACK) 
C 
C     * OCT 31,1984 T.CARRIERES 
C     * SAVES FIELD A ON FILE NF. 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL A(LON,LY)
      INTEGER IBUF(8) 
      COMMON/DIM/ LON,LX,LY 
C---------------------------------------------------------------------
C 
      DO 50 J=1,LY
   50 A(LON,J)=A(1,J) 
      CALL SETLAB(IBUF,NC4TO8("GRID"),ID,NAME,1,LON,LY,0,NPACK)
      CALL PUTFLD2(NF,A,IBUF,MAXX) 
      WRITE(6,6000) IBUF
C---------------------------------------------------------------------
 6000 FORMAT('0',A4,I10,2X,A4,5I6)
      RETURN
      END
