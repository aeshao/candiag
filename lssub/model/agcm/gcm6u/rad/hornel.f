      SUBROUTINE HORNEL(UU,TT,XN,XD,IND,IT,IL1,IL2) 
  
C     * FEB 06/95 - M.LAZARE. - INSERT PADA HILL CONDITION, FOR REDUCED
C     *                         CO2 SIMULATIONS ONLY. 
C     * JAN 30/89 - M.LAZARE. - SLIGHTLY REDUCED CONTINUUM COOLING
C     *                         ( MODIFIED TT(9) - TT(13) ).
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C........ CALCULATES TRANSMISSIVITIES 
  
      PARAMETER ( NG1=2, NINT=5, NTR=11,
     A            NG1P1=NG1+1, NUA=3*NINT+3, MP=NINT+3, 
     B            MP2=2*MP, NTRA=NUA+1, LN6=MP2*NTR*6 ) 
  
      COMMON /RADSIZE/ KMX, LMX, KMXP, NGLP1, NGL 
  
      COMMON /PARAD1/ RT1(NG1),WG1(NG1),MPAD,AT(MP,5),BT(MP,5),CNTNU(5),
     1                O1H,O2H,OCT(4),TREF,XP(2,6,NINT),TSTAND,
     2                GA(MP2,NTR,6),GB(MP2,NTR,6),NPAD, 
     3                TINTP(NTR),TSTP,MXIXT 
      COMMON /ABSOR/  IABS,IAER
      COMMON /SWCONS/ CSEC,   CH2O,  CCAR,  CCO2, SOLARC,RAYLG
  
      DIMENSION UU(LMX,NUA),TT(LMX,NTRA),IT(LMX),XN(LMX),XD(LMX)
C-----------------------------------------------------------------------
  
C........ TRANSMISSION BY H2O AND CO2 (FITTED BY PADE APPROXIMANTS) 
  
      DO 190 IA = 1, MP 
          L=2*IA-IND
          DO 150 I=IL1,IL2

C  ..... CHECK FOR PADA HILL CONDITION. THIS IS ONLY DONE FOR
C        CO2 CONCENTRATIONS LESS THAN VALUE USED IN EARLIER RUNS,
C        TO MAINTAIN UPWARD COMPATABILITY FOR CLIMATE.

              ZZ=SQRT (UU(I,IA))
              IF( CCO2.LT.3.3E-4 .AND. IA.EQ.6 .AND.
     1            ((ZZ.GT.7.E-3).AND.(ZZ.LT.9.E-3)) )        THEN
                 ZZ=7.E-3
              ENDIF
C
              XN(I) = (((((   GA(L,IT(I),6))*ZZ+GA(L,IT(I),5))*ZZ+
     1                        GA(L,IT(I),4))*ZZ+GA(L,IT(I),3))*ZZ+
     2                        GA(L,IT(I),2))*ZZ+GA(L,IT(I),1) 
              XD(I) = (((((ZZ+GB(L,IT(I),6))*ZZ+GB(L,IT(I),5))*ZZ+
     1                        GB(L,IT(I),4))*ZZ+GB(L,IT(I),3))*ZZ+
     2                        GB(L,IT(I),2))*ZZ+GB(L,IT(I),1) 
              TT(I,IA) = MAX (0., MIN (1., XN(I)/XD(I)))
  150     CONTINUE
  190 CONTINUE
  
C........ TRANSMISSION BY H2O CONTINUUM (FROM ROBERTS ET AL., 1976) 
  
      DO 200 I=IL1,IL2
              TT(I, 9) = 1.0
              TT(I,10) = EXP (-12.0 * UU(I,11) - 0.224 * UU(I,9)) 
              TT(I,11) = EXP (- 5.0 * UU(I,11) - 0.010 * UU(I,9)) 
              TT(I,12) = EXP (- 3.0 * UU(I,11) - 0.006 * UU(I,9)) 
              TT(I,13) = 1.0
  200 CONTINUE
  
C........ TRANSMISSION BY OZONE (MALKMUS, 1967) 
  
      DO 230 I=IL1,IL2
          X=MAX( 1.E-20, UU(I,12))
          Y=MAX( 1.E-20, UU(I,13))
          SQ=SQRT(1. + 4. * O1H * X*X / (O2H * Y) ) - 1.
  
          TT(I,14) = EXP(- O2H * Y * SQ / (2. * X)) 
  230 CONTINUE
  
C........ TRANSMISSION BY AEROSOLS. 
  
      IF (IAER .NE. 0) THEN 
          DO 260 JAER=14,NUA
              JAER P 1 = JAER + 1 
              DO 250 I=IL1,IL2
                  TT(I,JAERP1)=EXP(-UU(I,JAER)) 
  250         CONTINUE
  260     CONTINUE
      END IF
  
C-----------------------------------------------------------------------
  
      RETURN
      END 
