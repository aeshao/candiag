      SUBROUTINE RADNEWO (FSG, FSTOT, FDL, HRS, HRL, EMD,
     1                    EMM, PSOL, SHJ, SHTJ, TGND, THALF,
     2                    TFULL, Q, O3, C MTX, TAC, RMU0, 
     3                    ALSW, ALLW, CST, CSB, CLT, CLB,
     4                    LAY, LEV, LEVP1, IL1, IL2, ILG, LON, IFIXCLD, 
     5                    JLAT, LCSW, LCLW, LCSWCLR, VA, IVA, IRS, IRL,
     6                    MX, MXP, WV, TAVE, TAUAE, ALBSUR, 
     7                    FRACT, FSDOWN, FSUP, FSNET, HEAT, ALBPLA, 
     8                    FSSUR, QOF, TAU, OMEGA, CG, T,
     9                    O, RESULT, F, BINT, BSOLIN, DT0,
     A                    EM0, SSIG, XADJD, XADJM, XDESC, XMONT,
     B                    S1, SIG, Z, DSIG, DSH, DSC, 
     C                    DZ,FCLRTOA,FCLRSFC                           )

C     * MAY  17/95 - M.LAZARE. - ADD CALCULATION OF CLEAR-SKY FLUX TERMS
C     *                          (OPTIONAL CALL TO NEW ROUTINE SWVCLR
C     *                          TO OBTAIN ARRAYS "CST" AND "CSB")
C     *                          AND ADDITIONAL ARRAYS "CLT" AND "CLB"
C     *                          NOW CALCULATED AND PASSED OUT OF NEW
C     *                          ROUTINE LONGW2O. ALSO CALCULATE AND
C     *                          PASS OUT PHYSICS WORK ARRAY "FSTOT"
C     *                          TO BE USED TO CALCULATE ACCUMULATED
C     *                          INCIDENT SOLAR FLUX "FSSROW".
C     * DEC  05/88 - M.LAZARE. - PREVIOUS VERSION RADNEW.
  
C***********************************************************************
C                                                                      C
C   DEVELOPED, 'AEROSOLIZED' AND VECTORIZED  (ORDER ILX) BY            C
C                                                                      C
C    JEAN-JACQUES MORCRETTE                                            C
C    LABORATOIRE D'OPTIQUE ATMOSPHERIQUE                               C
C    UNIVERSITE DES SCIENCES ET TECHNIQUES DE LILLE                    C
C    59655 - VILLENEUVE D'ASCQ CEDEX, FRANCE                           C
C                                                                      C
C    THESE ETAT, 3E CHAPITRE, JUIN 1984                                C
C    SUBMITTED TO QUART.J.ROY.METEOR.SOC. (PAPER 84/64)                C
C                                                                      C
C-----------------------------------------------------------------------
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C........ PARAMETERS FOR THE RADIATION ROUTINES:  
C........ NG1  = NUMBER OF GAUSSIAN QUADRATURE POINTS.
C........ NINT = NUMBER OF SPECTRAL INTERVALS OF SCHEME.
C........ NTR  = NUMBER OF TEMPERATURE CLASSES IN DATA COEFFICIENTS.
  
      PARAMETER ( NG1=2, NINT=5, NTR=11,
     A            NG1P1=NG1+1, NUA=3*NINT+3, MP=NINT+3, 
     B            MP2=2*MP, NTRA=NUA+1, LN6=MP2*NTR*6 ) 
  
C........ VARIABLES INDEPENDENT OF THE ORDER OF VECTORIZATION 
  
      COMMON /RADSIZE/ KMX, LMX, KMXP, NGLP1, NGL 
  
      COMMON /AERO/ TAUA(2,5),SSC ALB(2,5),CGA(2,5),CAER(5,5) 
      COMMON /ABSOR/  IABS,IAER 
      COMMON /PARAD1/ RT1(NG1),WG1(NG1),MPAD,AT(MP,5),BT(MP,5),CNTNU(5),
     1                O1H,O2H,OCT(4),TREF,XP(2,6,NINT),TSTAND,
     2                GA(MP2,NTR,6),GB(MP2,NTR,6),NPAD, 
     3                TINTP(NTR),TSTP,MXIXT 
      COMMON /CONSTN/ DAY, C DYN MB, ERG W M2, PI,
     2                STFN, DIFFU
  
C........ MISCELLANEOUS RADIATION CONSTANTS FOR LONGWAVE AND SHORTWAVE
C         ROUTINES
  
      COMMON /LWCONS/ MAXC, CO2, SIG1 
      COMMON /SWCONS/ CSEC, CH2O, CCAR, CCO2, FCST, RAYLG 
  
C........ ARRAYS LOCAL TO THIS ROUTINE ONLY 
  
      INTEGER MX(LMX),MXP(LMX)
      REAL S1(LMX,KMXP), SIG(LMX,KMXP), Z(LMX,KMXP) 
  
C........ ARRAYS SHARED WITH LONGW AND SHORTW 
  
      DIMENSION WV(LMX,KMX),TAVE(LMX,KMX),TAUAE(LMX,KMX,5),DSIG(LMX,KMX)
  
C........ ARRAYS SHARED WITH SHORTW ONLY
  
      DIMENSION ALBSUR(LMX,2),FRACT(LMX)
      DIMENSION FSDOWN(LMX,KMXP),FSUP(LMX,KMXP),FSNET(LMX,KMXP),
     A          HEAT(LMX,KMX),ALBPLA(LMX),FSSUR(LMX),QOF(LMX,KMX),
     B          TAU(LMX,2,KMX),OMEGA(LMX,2,KMX),CG(LMX,2,KMX),
     C          DSH(LMX,KMX),DSC(LMX,KMX) 
  
C........ ARRAYS SHARED WITH LONGW ROUTINE ONLY 
  
      DIMENSION T(LMX,KMXP),O(LMX,KMXP),RESULT(LMX,KMX),F(LMX,2,KMXP),
     A BINT(LMX,KMXP),BSOLIN(LMX),DT0(LMX),EM0(LMX),SSIG(LMX,NGLP1),
     B XADJD(LMX,KMXP),XADJM(LMX,KMXP),XDESC(LMX,KMXP),XMONT(LMX,KMXP), 
     C DZ(LMX,KMX)

C........ ARRAYS IN CLEAR-SHORTWAVE ROUTINE ONLY

      DIMENSION FCLRTOA(LMX),FCLRSFC(LMX)

  
C........ WORK SPACE USED SEPARATELY FOR LONGWAVE AND SHORTWAVE ROUTINES
C         IRS AND IRL ARE RADIATION POINTERS FOR THE SHORTWAVE AND
C         LONGWAVE ROUTINES, RESPECTIVELY 
  
       DIMENSION VA(IVA)
       INTEGER IRS(1), IRL(1) 
  
C........ ARRAYS CONNECTED WITH PHYSICS.
  
      REAL HRS(ILG,LAY), HRL(ILG,LAY), RMU0(ILG), PSOL(ILG),
     1     SHJ(ILG,LAY), SHTJ(ILG,LEV), TGND(ILG),
     2     THALF(ILG,LEV), TFULL(ILG,LEV),
     3     Q(ILG,LEV), O3(ILG,LEV), C MTX(ILG,LEVP1,LEVP1), 
     4     TAC(ILG,LEV), FSG(ILG), FSTOT(ILG), FDL(ILG), EMD(ILG,LEV), 
     5     EMM(ILG,LEV), ALSW(ILG), ALLW(ILG), CST(ILG), CSB(ILG),
     6     CLT(ILG), CLB(ILG) 
  
      LOGICAL LCSW,LCLW,LCSWCLR 
  
C........ THE FOLLOWING ARRAYS ARE EQUIVALENCED IMPLICITLY IN THE 
C         CALLING SEQUENCE. THIS CAN BE DONE SINCE THESE SOLAR ARRAYS 
C         ARE USED UP BEFORE THEIR EQUIVALENCED LONGWAVE COUNTERPARTS 
C         ARE DEFINED.
  
C         EQUIVALENCE (FSDOWN(1,1),BINT(1,1)),
C    1                (FSUP(1,1),XADJD(1,1)), 
C    2                (ALBPLA(1),BSOLIN(1)),
C    3                (TAU(1,1,1),XADJM(1,1)),
C    4                (OMEGA(1,1,1),XDESC(1,1)),
C    5                (CG(1,1,1),XMONT(1,1))
  
C-----------------------------------------------------------------------
  
C........ OBTAIN COMMON BLOCK DATA. 
  
      CALL DATWBM 
      CALL AEROS
  
C........ THRESHOLDS FOR EFFECTIVE CLOUDINESS, OZONE AND MIXING RATIO.
  
      ZEP SC  = 1.E-02
      ZEP SCO = 1.E-10
      ZEP SCQ = 1.E-10
  
C........ SOME USEFUL CONSTANTS 
  
      DAY = 86400.
      PI = 3.1415926535898
      STFN = 5.67E-08 
      C SEC = 9.80665 / 1004.64 
      C DYN MB = 1.E-03 
      ERG W M2 = 1.E+03 
      C H2O = 5.3669274E-03 
      C CAR = C CO2 * 0.0088509 
C . . . . 0.00885 = 1.519 / (9.81 * 1.75 * 10.) 
      C O2 = 1.519 * C CO2
      DIFFU = 1.66
C
      RAYLG = 0.06
C 
C-- CSEC    GIVES HEATING RATES (K/SEC) FROM FLUX (W/M2) / PRESS (PA) 
C   CDYNMB  CONVERTS DYNES/CM2 IN MB
C   ERGWM2  CONVERTS WM2  IN ERG
C   FCST    SOLAR CONSTANT (W/M2) 
C   CH2O    1./(9.80665 * 1.9 * 10) CF. SHORTW
C   CCAR    CO2 VOL.MIX.RATIO * ATM.SCALE HEIGHT / 1.75  CF. SHORTW 
C   C123    CO2 VOL.MIX.RATIO ;  MCO2=44 / MAIR=28.97 
C   STFN    STEFAN'S CONSTANT (W.M-2.K-4) 
C   DIFFU   DIFFUSIVITY FACTOR
C   
C   RAYLG   RAYLEIGH OPTICAL THICKNESS FOR THE  WHOLE ATMOSPHERE
  
C     * CALCULATE EXTENDED SIGMA ARRAY INCLUDING MOON LAYER AND GROUND. 
  
      DO 1 I=IL1,IL2
         S1(I,1) = SHTJ(I,1)/10.
    1 CONTINUE
  
      DO 2 L=2,LEVP1
         LM1 = L-1
         DO 2 I=IL1,IL2 
            S1(I,L) = SHTJ(I,LM1) 
    2 CONTINUE
  
C     * CALCULATE SCALE HEIGHT OVER LEVELS. 
C     * NOTE THAT THE ORDERING OF THE LEVELS IS REVERSED FROM USUAL 
C     * (I.E. GROUND UP INSTEAD OF TOP DOWN). 
  
      DO 3 L=1,LEVP1
          LR = LEVP1 - L + 1
          DO 3 I=IL1,IL2
             SIG(I,L) = S1(I,LR)
             Z(I,L) = -LOG(SIG(I,L)) 
    3 CONTINUE
  
      DO 4 L=1,LEV
          LP1 = L+1 
          DO 4 I=IL1,IL2
             D SIG(I,L) = SIG(I,L) - SIG(I,LP1) 
             D SH(I,L) = SIG(I,L) ** 1.9 - SIG(I,LP1) ** 1.9
             D SC(I,L) = SIG(I,L) ** 1.75 - SIG(I,LP1) ** 1.75
             DZ(I,L) = (Z(I,LP1) - Z(I,L)) * 0.5
    4 CONTINUE
  
C........  PUTS CCC GCM FIELDS INTO LILLE FORMAT (THE INTERFACE STAGE)
  
      DO 8 I=IL1,IL2
          O(I,KMXP)=0.
          FSUP(I,KMXP)=0. 
          FSDOWN(I,KMXP)=1.E-10 
          FSNET(I,KMXP)=0.
          F(I,1,KMXP)=0.
          F(I,2,KMXP)=0.
          FSSUR(I)=0. 
          FCLRTOA(I)=0.
          FCLRSFC(I)=0.
          ALBPLA(I)=1.0 
          DT0(I) = T GND(I) - T FULL(I,LEV) 
          EM0(I)=1.0
          FRACT(I)=1.0
C........ DISTINGUISHING BETWEEN DIRECT AND DIFFUSE ALBEDOS, 0.25-0.68  AND 
C         0.68-4.0 MICRONS, AND BARE SOIL / VEGETATION. 
          ALBSUR(I,1) = ALSW(I) 
          ALBSUR(I,2) = ALLW(I) 
    8 CONTINUE
  
      DO 10 I=IL1,IL2 
          T(I,KMXP) = THALF(I,1)
 10   CONTINUE
  
      DO 23 K=1,KMX 
          K P 1 = K + 1 
          K R = K M X - K + 1 
          K R P 1 = K R + 1 
  
          DO 22 I=IL1,IL2 
              T(I,K) = TFULL(I,KR)
              TAVE(I,K) = THALF(I,KR) 
              WV(I,K) = MAX (Q(I,KR)/(1.-Q(I,KR)),ZEPSCQ) 
              IF(LCSW) THEN 
                  QOF(I,K) = MAX (O3(I,KR), ZEPSCO) 
C.............    CLOUD OPTICAL PROPERTIES A LA FOUQUART, 1980. 
                  TAU(I,1,K) = TAC(I,KR)
                  TAU(I,2,K) = TAU (I,1,K)
                  IF(IFIXCLD.EQ.0) THEN 
      Y = MIN (TAU(I,2,K), 316.)
      A1 = ((-8.261722E2*Y+1.337753E4)*Y+8.850529E5)*Y+1.761285E7 
      A2 = ((-2.149306E3*Y+2.658816E5)*Y-8.679865E6)*Y-6.882959E6 
      B1 = (((1.*Y-6.431155E2)*Y+2.408285E4)*Y+2.834550E5)*Y+2.694510E6 
      B2 = (((1.*Y-3.039226E3)*Y-9.175774E4)*Y-1.919094E6)*Y-1.396139E6 
      OMEG1 = 0.99999 
      OMEG = MIN (1. - 0.001 * (A1/B1 + A2/B2 * RMU0(I)), 0.9980) 
      OMEG = MERGE(0.9980, OMEG, Y .GE. 36. .AND. Y .LE. 85.)
  
                  OMEGA(I,1,K) = OMEG1
                  OMEGA(I,2,K) = OMEG1
                  IF(Y.GT.1.E-6.AND.T(I,K).GT.253.) OMEGA(I,2,K) = OMEG 
                  ELSE
                  IF(TAU(I,1,K).LE.3.15) THEN 
                     OMEG = 0.9989 - 4.E-03 * EXP(-0.15 * TAU(I,1,K)) 
                  ELSE
                     OMEG = 0.99905+1.32E-3*LOG(LOG(LOG(TAU(I,1,K))))
                  ENDIF 
                  OMEGA(I,1,K) = OMEG 
                  OMEGA(I,2,K) = OMEG 
                  ENDIF 
  
                  CG(I,1,K) = 0.8511
                  CG(I,2,K) = 0.8511
              ENDIF 
              IF(LCLW) THEN 
                  DP = PSOL(I) * (S1(I,KRP1) - S1(I,KR))
                  O(I,K) = QOF(I,K) * 9.80665 / (46.6968 * DP)
              ENDIF 
  
C............ OUTPUT
              RESULT(I,K) = 0.
              HEAT(I,K) = 0.
              FS UP(I,K) = 0. 
              FS DOWN(I,K) = 0. 
              FS NET(I,K) = 0.
              F(I,1,K) = 0. 
              F(I,2,K) = 0. 
   22     CONTINUE
   23 CONTINUE
  
      DO 26 K=1,KMX 
          DO 25 IAE=1,5 
              DO 24 I=IL1,IL2 
                  TAU AE(I,K,IAE) = 1.E-10
   24         CONTINUE
   25     CONTINUE
   26 CONTINUE
  
C........ CALCULATE SCALE HEIGHT OVER GAUSS SUB-LEVELS. 
  
      DO 40 I=IL1,IL2 
         DO 36 K = 1, KMX 
             K P 1 = K + 1
             K J = (K - 1) * NG1 P 1 + 1
             S SIG(I,KJ) = SIG(I,K) 
             DO 34 IG1 = 1, NG1 
                 KJ = KJ + 1
                 Z AVE = 0.5 * (Z(I,K) + Z(I,KP1))
                 S SIG(I,KJ) = EXP(-(RT1(IG1) * DZ(I,K) + Z AVE)) 
   34        CONTINUE 
   36    CONTINUE 
         S SIG(I,NGLP1) = SIG(I,KMXP) 
   40 CONTINUE
  
C........ CALLING THE SHORTWAVE RADIATION ROUTINE IF AT LEAST ONE POINT 
C         OVER THE 'ILX' LONGITUDES GETS SOME SOLAR RADIATION.
  
      RMUZ = 0. 
      DO 84 I=IL1,IL2 
          RMUZ = MAX (RMUZ, RMU0(I))
   84 CONTINUE
  
      IF (LCSW.AND.RMUZ.GT.0.) THEN 

        IF(LCSWCLR)  THEN

C........ CALL SW FOR CLEAR SKY ONLY (DIAGNOSTIC MODE - TO SAVE IN FLUXES)

          CALL SWVCLR (C MTX, RMU0, IL1, IL2, LON, IFIXCLD, 
     1                 DSIG, PSOL, WV, TAVE, TAUAE, 
     2                 DSH, DSC, ALBSUR, FRACT, FSDOWN, FSUP, 
     3                 FSNET, HEAT, ALBPLA, FSSUR, QOF, TAU,
     4                 OMEGA, CG, FCLRTOA, FCLRSFC,
     5    VA( IRS(1)), VA( IRS(2)), VA( IRS(3)), VA( IRS(4)), 
     6    VA( IRS(5)), VA( IRS(6)), VA( IRS(7)), VA( IRS(8)), 
     7    VA( IRS(9)), VA(IRS(10)), VA(IRS(11)), VA(IRS(12)), 
     8    VA(IRS(13)), VA(IRS(14)), VA(IRS(15)), VA(IRS(16)), 
     9    VA(IRS(17)), VA(IRS(18)), VA(IRS(19)), VA(IRS(20)), 
     A    VA(IRS(21)), VA(IRS(22)), VA(IRS(23)), VA(IRS(24)), 
     B    VA(IRS(25)), VA(IRS(26)), VA(IRS(27)), VA(IRS(28)), 
     C    VA(IRS(29)), VA(IRS(30)), VA(IRS(31)), VA(IRS(32)), 
     D    VA(IRS(33)), VA(IRS(34)), VA(IRS(35)), VA(IRS(36)), 
     E    VA(IRS(37)), VA(IRS(38)), VA(IRS(39)), VA(IRS(40)), 
     F    VA(IRS(41)), VA(IRS(42)), VA(IRS(43)), VA(IRS(44)), 
     G    VA(IRS(45)), VA(IRS(46)), VA(IRS(47)), VA(IRS(48)), 
     H    VA(IRS(49)), VA(IRS(50)), VA(IRS(51)), VA(IRS(52)), 
     I    VA(IRS(53)), VA(IRS(54)), VA(IRS(55)))

        ENDIF
  
        CALL SHORTW2(C MTX, RMU0, IL1, IL2, LON, IFIXCLD, 
     1               DSIG, PSOL, WV, TAVE, TAUAE, 
     2               DSH, DSC, ALBSUR, FRACT, FSDOWN, FSUP, 
     3               FSNET, HEAT, ALBPLA, FSSUR, QOF, TAU,
     4               OMEGA, CG, 
     5  VA( IRS(1)), VA( IRS(2)), VA( IRS(3)), VA( IRS(4)), 
     6  VA( IRS(5)), VA( IRS(6)), VA( IRS(7)), VA( IRS(8)), 
     7  VA( IRS(9)), VA(IRS(10)), VA(IRS(11)), VA(IRS(12)), 
     8  VA(IRS(13)), VA(IRS(14)), VA(IRS(15)), VA(IRS(16)), 
     9  VA(IRS(17)), VA(IRS(18)), VA(IRS(19)), VA(IRS(20)), 
     A  VA(IRS(21)), VA(IRS(22)), VA(IRS(23)), VA(IRS(24)), 
     B  VA(IRS(25)), VA(IRS(26)), VA(IRS(27)), VA(IRS(28)), 
     C  VA(IRS(29)), VA(IRS(30)), VA(IRS(31)), VA(IRS(32)), 
     D  VA(IRS(33)), VA(IRS(34)), VA(IRS(35)), VA(IRS(36)), 
     E  VA(IRS(37)), VA(IRS(38)), VA(IRS(39)), VA(IRS(40)), 
     F  VA(IRS(41)), VA(IRS(42)), VA(IRS(43)), VA(IRS(44)), 
     G  VA(IRS(45)), VA(IRS(46)), VA(IRS(47)), VA(IRS(48)), 
     H  VA(IRS(49)), VA(IRS(50)), VA(IRS(51)), VA(IRS(52)), 
     I  VA(IRS(53)), VA(IRS(54)), VA(IRS(55)))
  
      ENDIF
  
C     DO 98 I=IL1,IL2 
C     I = 1 
C     WRITE(6,891)(T  (I,K),  K=1,KMXP) 
C     WRITE(6,889)(WV (I,K),  K=1,KMX)
C     WRITE(6,889)(QOF(I,K),  K=1,KMX)
C     WRITE(6,889)(TAVE(I,K), K=1,KMX)
C     WRITE(6,889)(TAU(I,K),  K=1,KMX)
C     WRITE(6,889)(OMEGA(I,K),K=1,KMX)
C     DO 9898 K1=1,KMXP 
C         WRITE(6,887) (CMTX(I,K1,K),K=1,KMXP)
C9898 CONTINUE
C  98 CONTINUE
  
C........ INITIALISATION FOR LONG-WAVE.
C         ALSO, STORE INCIDENT SOLAR INTO OUTPUT ARRAY SINCE
C         "FSDOWN" IS EQUIVILENCED TO "BINT" AND WILL BE
C         OVERWRITTEN BY FOLLOWING INITIALIZATION OF BINT TO
C         ZERO AND SUBSEQUENT CALCULATION IN LONGWAVE.
C........ NOTE THAT THE SHORTWAVE RESULTS ARE SAVED ONLY AT FULL SHORTWAVE
C         CALCULATION TIME, SINCE OTHERWISE THEY ARE ZEROED OUT AT THE
C         BEGINNING OF RADNEW AND NOT UPDATED.
  
  100 MAX C = 0 
      DO 101 I=IL1,IL2
          IF(LCSW)                                      THEN
              FSTOT(I) = FSDOWN(I,1)/RMU0(I)
              IF(LCSWCLR)                        THEN
                  CST(I)   = FCLRTOA(I)/RMU0(I)
                  CSB(I)   = FCLRSFC(I)/RMU0(I)
              ENDIF
          ENDIF
          BSOLIN(I) = 0.
          MX(I) = 0 
          MXP(I) = 0
  101 CONTINUE

      DO 103 K = 1, KMX P 
          DO 102 I=IL1,IL2
              BINT(I,K)=0.
              XADJD(I,K)=0. 
              XADJM(I,K)=0. 
              XDESC(I,K)=0. 
              XMONT(I,K)=0. 
  102     CONTINUE
  103 CONTINUE
  
C........ SEARCHING FOR THE INDEX OF THE HIGHEST LAYER CONTAINING A 
C         CLOUD AMONG THE 'ILX' ATMOSPHERIC COLUMNS TAKEN INTO ACCOUNT
C         IN ONE CALCULATION  (THIS LOOP CANNOT BE VECTORIZED.....) 
  
      DO 111 K = 1, KMX 
          KR = KMXP - K 
          KRP= KR + 1 
          DO 110 I=IL1,IL2
C***          MXP(I) = CVMGT (K, MX(I), C MTX(I,KRP,KR) .GT. ZEPSC) 
              IF(C MTX(I,KRP,KR) .GT. ZEPSC)           THEN
                  MXP(I) = K
              ELSE
                  MXP(I) = MX(I)
              ENDIF   
              MAXC = MAX (MXP(I), MAXC)
              MX(I) = MXP(I)
  110     CONTINUE
  111 CONTINUE
  
C........ CALL LONGWAVE RADIATION 
  
      CALL LONGW2O(C MTX, EMD, EMM, IL1, IL2, LON, LAY, LCLW,
     1            DSIG, PSOL, WV, TAVE, TAUAE, CLT, CLB,
     2            T, O, RESULT, F, BINT, BSOLIN, DT0, EM0, DZ,
     3            SSIG, XADJD, XADJM, XDESC, XMONT, 
     4            VA(IRL( 1)), VA(IRL( 2)), VA(IRL( 1)),
     5            VA(IRL( 2)), VA(IRL( 1)), 
     6            VA(IRL( 3)), VA(IRL( 4)), VA(IRL( 9)), VA(IRL( 5)), 
     7            VA(IRL( 6)), VA(IRL( 7)), VA(IRL( 8)), VA(IRL( 9)), 
     8            VA(IRL(10)), VA(IRL(11)), VA(IRL(12)), VA(IRL(13)), 
     9            VA(IRL(14)), VA(IRL(15)), VA(IRL(16)), VA(IRL(17)), 
     A            VA(IRL(18)), VA(IRL(19)), VA(IRL(20)), VA(IRL(21)), 
     B            VA(IRL(22)), VA(IRL(23)), VA(IRL(24)), VA(IRL(25)), 
     C            VA(IRL( 4)), VA(IRL(14)), VA(IRL(26)), VA(IRL(27)), 
     D            VA(IRL(28)), VA(IRL(29)), VA(IRL(30)), VA(IRL(31)), 
     E            VA(IRL(32)), VA(IRL(33)), VA(IRL(34)), VA(IRL(35)), 
     F            VA(IRL(36)), VA(IRL(37)), VA(IRL(38)), VA(IRL(39)), 
     G            VA(IRL(40)), VA(IRL(41)), VA(IRL(42)), VA(IRL( 5)), 
     H            VA(IRL(43)))
  
C........ SAVE RESULTS NEEDED TO PASS TO THE GCM
  
      DO 185 I=IL1,IL2 
          IF(LCSW)        FSG(I)   = FSSUR(I)/RMU0(I)
          FDL(I) = - F(I,2,1) 
  185 CONTINUE
  
C........ ONLY UPDATE LONGWAVE HEATING RATE BELOW HIGHEST VERTICAL LEVEL
C         OF CLOUDINESS AROUND A LATITUDE CIRCLE. 
  
      DO 187 K = 2, KMX
          KM1 = K - 1 
          KR = KMX - K + 1
          DO 186 I=IL1,IL2 
              IF(LCSW)                HRS(I,KR) = HEAT(I,KM1)/RMU0(I) 
              IF(LCLW.OR.KM1.LE.MAXC) HRL(I,KR) = - RESULT(I,KM1) 
  186     CONTINUE
  187 CONTINUE
  
C---------------------------------------------------------------------
  
C     DO 804 I=IL1,IL2
C     I=1 
C     WRITE(6,891) (F(I,1,K),K=1,KMXP)
C     WRITE(6,891) (F(I,2,K),K=1,KMXP)
C     WRITE(6,889) (RESULT(I,K),K=1,KMX)
C     WRITE(6,891) (FSUP(I,K),K=1,KMXP) 
C     WRITE(6,891) (FSDOWN(I,K),K=1,KMXP) 
C     WRITE(6,889) (HEAT(I,K),K=1,KMX)
C 804 CONTINUE
  
      RETURN
  
 886  FORMAT(16I5)
 887  FORMAT(1X,12F10.4)
 889  FORMAT(6X,11E10.4)
 891  FORMAT(1X,12F10.2)
  
      END 
