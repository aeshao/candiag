      SUBROUTINE TRFCTN ( INT, JJ, W, R1 ,IL1, IL2, LON, R2) 

C     * M. LAZARE. JULY 20/93. MOVE INVARIANT SCALAR TEMPORARIES
C     *                        OUT OF LONGITUDE LOOPS TO
C     *                        OPTIMIZE.
C     * H. BARKER. JULY 10/92.
C  
C     CALCULATES TRANSMISSION FUNCTIONS BY PADE  APPROXIMANTS 
C      USING HORNER'S ALGORITHM (VECTORIZED VERSION)
C 
C      2 SPECTRAL INTERVALS (0.25-0.68 UM ; 0.68-4.00 UM) 
C 
C--INPUT
C       INT     INDEX OF SPECTRAL INTERVAL  (1-2) 
C       JJ        "   "  ABSORBER           (1-3) 
C       W       ABSORBER AMOUNT             (SEE BELOW) 
C--OUTPUT 
C       R1      TRANSMISSION FUNCTION       (0-1.0) 
C 
C         JJ=1......  H2O FOR U BETWEEN 1.E-05 AND 100 G.CM-2 
C         JJ=2......  CO2 FOR U BETWEEN 0.3    AND  10 G.CM-2 
C         JJ=3......   O3 FOR U BETWEEN 0.01   AND  10 G.CM-2 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON /RADSIZE/ KMX, LMX, KMXP, NGLP1, NGL 
  
      COMMON/PADE/APAD(2,3,7),BPAD(2,3,7),D(2,3),CRAY(2,6),SUN(2) 
  
      DIMENSION W(LMX),R1(LMX),R2(LMX)
  
C---------------------------------------------------------------------- 
  
      DO 1 I=IL1,IL2
          R1(I)=APAD(INT,JJ,7)
          R2(I)=BPAD(INT,JJ,7)
    1 CONTINUE
  
      DO 3 II=1,6 
          KI=7-II 
          AP=APAD(INT,JJ,KI)
          BP=BPAD(INT,JJ,KI)
          DO 2 I=IL1,IL2
              R1(I)=R1(I)*W(I)+AP
              R2(I)=R2(I)*W(I)+BP
    2     CONTINUE
    3 CONTINUE

      DP=D(INT,JJ)  
      DO 4 I=IL1,IL2
          R1(I)=(R1(I)/R2(I))*(1.-DP)+DP
    4 CONTINUE
  
      RETURN
      END 
