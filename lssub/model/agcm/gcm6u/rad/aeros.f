      SUBROUTINE AEROS
  
C........ OPTICAL PARAMETERS FOR THE STANDARD AEROSOLS OF THE RADIATION 
C   COMMISSION  (WCP-55, WILLIAMSBURG, VA. 28-30 MAR. 1983, WMO, GENEVA)
C 
C        FROM DR. D. TANRE, L.O.A. UNIV. LILLE, FRANCE. 
C 
C   MODEL 1= CONTINENTAL, 2=MARITIME, 3=URBAN, 4=VOLCANIC,
C         5=STRATOSPHERIC 
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMMON /RADSIZE/ KMX, LMX, KMXP, NGLP1, NGL 
  
      COMMON /AERO/ TAUA(2,5), SSC ALB(2,5), CGA(2,5), CAER(5,5)
  
C---------------------------------------------------------------------- 
  
C........ SHORTWAVE (ADAPTED TO THE L.O.A. SHORTWAVE SCHEME)
C           AS OF AUG. 1985, SAME VALUES IN BOTH INTERVALS. 
  
      DATA ((TAUA(I,J),J=1,5),I=1,2) /
     1                .730719, .912819, .725059, .745405, .682188,
     2                .730719, .912819, .725059, .745405, .682188 / 
  
      DATA ((SSCALB(I,J),J=1,5),I=1,2) /
     1                .872212, .982545, .623143, .944887, .997975,
     2                .872212, .982545, .623143, .944887, .997975 / 
  
      DATA ((CGA(I,J),J=1,5),I=1,2)  /
     1                .647596, .739002, .580845, .662657, .624246,
     2                .647596, .739002, .580845, .662657, .624246 / 
  
C....... LONGWAVE (ADAPTED TO THE L.O.A. LONGWAVE SCHEME SPECTRALS
C        INTERVALS) AS OF AUG. 1985, SAME VALUES IN BOTH INTERVALS. 
  
      DATA CAER / .038520, .037196, .040532, .054934, .038520,
     1            .12613 , .18313 , .10357 , .064106, .12613, 
     2            .012579, .013649, .018652, .025181, .012579,
     3            .011890, .016142, .021105, .028908, .011890,
     4            .013792, .026810, .052203, .066338, .013792 / 
  
C---------------------------------------------------------------------- 
  
      RETURN
      END 
