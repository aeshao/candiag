      SUBROUTINE SCADR6U (ID,IP,IR,IS,IE,IT,IX,IA,NLATJ,NENDJ,
     1                    ILG,ILEV,LEVS,LA,NTRAC,LENGTH) 

C     * MAY 17/95 - M.LAZARE. NEW VERSION FOR GCM6U"
C     *                       - REMOVE WORK SPACE FOR STAWS,SHTJ10,
C     *                         S,SH FROM THAT USED IN SCADDR6 FOR
C     *                         MODEL VERSION GCM6 (NO LONGER USED
C     *                         IN GCM6O), IN CONJUNCTION WITH
C     *                         CHANGE MADE IN GCMPR6O.
C     *                       - WORK SPACE ADDED FOR "BWGO", "BEGO" 
C     *                         AND "GCO" ADDED FOR PHYSICS.
C     *                       - ADD EXTRA WORK ARRAYS FOR MHEXP6/
C     *                         MHANL6 IN CONJUNCTION WITH NEW
C     *                         CHAINED LATITUDES, AS IN SCADDR7.
C     *                       - ADD EXTRA WORK ARRAY FOR ENERSV3 IN
C     *                         CONJUNCTION WITH IMPLEMENTATION OF
C     *                         "N-TRACERS", AS IN SCADDR7.  
C     *                       - ADD SPACE FOR "N-TRACERS" IN RESTART
C     *                         WORK SPACE, AS IN SCADDR7.
C     *                       - ADD 4 NEW PHYSICS POINTERS FOR 4
C     *                         NEW WORK ARRAYS (FLTCROL,FLGCROL,
C     *                         PCPNROL,QFNROL)
C     *                       - ADD 2 NEW POINTERS FOR RADIATION, TO
C     *                         SUPPORT CLEAR-SKY CALCULATION IN
C     *                         REVISED ROUTINE "RADNEWO" (ARRAYS
C     *                         "FCLRTOA" AND "FCLRSFC").
C     * OCT 20/88 - M.LAZARE. EARLIER VERSION SCADDR6.
C 
C     * RETURNS THE STARTING ADDRESSES OF SCRATCH ARRAYS USED 
C     * TO VECTORIZE THE HYBRID MODEL.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      INTEGER ID(1), IP(1), IR(1), IS(1), IE(1), IT(1), IX(1), IA(1)
C-------------------------------------------------------------------- 
C     * THE FOLLOWING RADIATION PARAMETERS ARE USED IN THE CALCULATION. 
C 
      NINT=5
      NG1=2 
      NTR=11
      NG1P1=NG1+1 
      NUA=3*NINT+3
      MP=NINT+3 
      MP2=2*MP
      NTRA=NUA+1
      KMX=ILEV+1
      NGLP1=KMX*NG1P1+1 
C 
C     * ADDRESSES FOR PHYSICS WORK SPACE. 
C 
      IP(1 )=   1 
      IP(2 )=IP(1 )+ ILG*(ILEV+1) 
      IP(3 )=IP(2 )+ ILG*(ILEV+1) 
      IP(4 )=IP(3 )+ ILG*(ILEV+1) 
      IP(5 )=IP(4 )+ ILG*(ILEV+1) 
      IP(6 )=IP(5 )+ ILG* ILEV
      IP(7 )=IP(6 )+ ILG* ILEV
      IP(8 )=IP(7 )+ ILG
      IP(9 )=IP(8 )+ ILG
      IP(10)=IP(9 )+ ILG
      IP(11)=IP(10)+ ILG
      IP(12)=IP(11)+ ILG
      IP(13)=IP(12)+ ILG
      IP(14)=IP(13)+ ILG
      IP(15)=IP(14)+ ILG
      IP(16)=IP(15)+ ILG
      IP(17)=IP(16)+ ILG
      IP(18)=IP(17)+ ILG
      IP(19)=IP(18)+ ILG
      IP(20)=IP(19)+ ILG
      IP(21)=IP(20)+ ILG
      IP(22)=IP(21)+ ILG
      IP(23)=IP(22)+ ILG
      IP(24)=IP(23)+ ILG
      IP(25)=IP(24)+ ILG
      IP(26)=IP(25)+ ILG*(ILEV+1) 
      IP(27)=IP(26)+ ILG*(ILEV+2)*(ILEV+2)
      IP(28)=IP(27)+ ILG
      IP(29)=IP(28)+ ILG
      IP(30)=IP(29)+ ILG
      IP(31)=IP(30)+ ILG
      IP(32)=IP(31)+ ILG
      IP(33)=IP(32)+ ILG
      IP(34)=IP(33)+ ILG
      IP(35)=IP(34)+ ILG
      IP(36)=IP(35)+ ILG*(ILEV+1) 
      IP(37)=IP(36)+ ILG*ILEV 
      IP(38)=IP(37)+ ILG*ILEV 
      IP(39)=IP(38)+ ILG*ILEV 
      IP(40)=IP(39)+ ILG*(ILEV+1) 
      IP(41)=IP(40)+ ILG*ILEV 
      IP(42)=IP(41)+ ILG*ILEV 
      IP(43)=IP(42)+ ILG*ILEV 
      IP(44)=IP(43)+ ILG*ILEV 
      IP(45)=IP(44)+ ILG*ILEV 
      IP(46)=IP(45)+ ILG*ILEV 
      IP(47)=IP(46)+ ILG*ILEV 
      IP(48)=IP(47)+ ILG*ILEV 
      IP(49)=IP(48)+ ILG
      IP(50)=IP(49)+ ILG
      IP(51)=IP(50)+ ILG
      IP(52)=IP(51)+ ILG
      IP(53)=IP(52)+ ILG
      IP(54)=IP(53)+ ILG
      IP(55)=IP(54)+ ILG
      LAST  =IP(55)+ ILG*(30*ILEV+51+NGLP1) 
  
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-1) 
      ENDIF 
C 
C     * ADDRESSES FOR DYNAMICS WORK SPACE.
C 
      ID(1 )= 1 
      ID(2 )=ID(1 )+ ILG*ILEV 
      ID(3 )=ID(2 )+ ILG
      ID(4 )=ID(3 )+ ILG
      ID(5 )=ID(4 )+ ILG
      ID(6 )=ID(5 )+ ILG*ILEV 
      ID(7 )=ID(6 )+ ILEV**2
      ID(8 )=ID(7 )+ ILEV**2
      ID(9 )=ID(8 )+ ILEV 
      ID(10)=ID(9 )+ ILEV 
      ID(11)=ID(10)+ ILEV 
      ID(12)=ID(11)+ ILEV 
      ID(13)=ID(12)+ LEVS 
      ID(14)=ID(13)+ 2*LA*ILEV
      ID(15)=ID(14)+ 2*LA*ILEV
      ID(16)=ID(15)+ 2*LA 
      LAST  =ID(16)+ ILG*ILEV 
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-2) 
      ENDIF 
C 
C     * ADDRESSES FOR RESTART WORK SPACE. 
C 
      IR(1 )= 1 
      IR(2 )=IR(1 ) + ILEV
      IR(3 )=IR(2 ) + ILEV
      IR(4 )=IR(3 ) + 8 
      LAST  =IR(4 ) + MAX( 2*LA, ILG*NENDJ )
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-3) 
      ENDIF 
C 
C     * ADDRESSES FOR PUTST_ WORK SPACE.
C 
      IS(1 )= 1 
      IS(2 )=IS(1 ) + ILEV
      LAST  =IS(2 ) + ILEV
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-4) 
      ENDIF 
C 
C     * ADDRESSES FOR ENERSV_ WORK SPACE. 
C 
      IE(1 )= 1 
      IE(2 )=IE(1 ) + ILEV
      IE(3 )=IE(2 ) + ILEV
      IE(4 )=IE(3 ) + (7+NTRAC) 
      LAST  =IE(4 ) + ILEV
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-5) 
      ENDIF 
C 
C     * ADDRESSES FOR TRACD_ WORK SPACE.
C 
      IT(1 )= 1 
      LAST  =IT(1 ) + ILEV
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-6) 
      ENDIF 
C
C     * THE FOLLOWING ARE REQUIRED FOR MULTIPLE-LATITUDES METHODOLOGY.
C
      ILGSL=ILG/NLATJ
      NILH=ILGSL/2
      NLEV1E=3*ILEV+LEVS+NTRAC*ILEV+1
      NLEV2E=2*ILEV+1
      NLEV3E=2*ILEV
      NLEVE=NLEV1E+NLEV2E+1
      NLEV1A=3*ILEV+LEVS+NTRAC*ILEV+2
      NLEV2A=3*ILEV+LEVS+NTRAC*ILEV          
      NLEV3A=2*ILEV+LEVS+NTRAC*ILEV
      NLEV4A=ILEV 
C
C     * ADDRESSES FOR MHEXP6 WORK SPACE.
C
      IX(1 )= 1 
      IX(2 )=IX(1 ) + 2*NLEVE *NLATJ*NILH
      IX(3 )=IX(2 ) + 2*NLEV2E*NLATJ*NILH
      IX(4 )=IX(3 ) + 2*NLEV3E*NLATJ*NILH
      IX(5 )=IX(4 ) + 2*NLEV1E*LA  
      IX(6 )=IX(5 ) + 2*NLEV3E*LA  
      LAST  =IX(6 ) + 2*NLEVE *NLATJ*NILH
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-7) 
      ENDIF 
C
C     * ADDRESSES FOR MHANL6 WORK SPACE.
C
      IA(1 )= 1 
      IA(2 )=IA(1 ) + 2*NLEV1A*NLATJ*NILH
      IA(3 )=IA(2 ) + 2*NLEV2A*NLATJ*NILH
      IA(4 )=IA(3 ) + 2*NLEV3A*NLATJ*NILH
      IA(5 )=IA(4 ) + 2*NLEV4A*NLATJ*NILH
      IA(6 )=IA(5 ) + 2*NLEV1A*LA  
      IA(7 )=IA(6 ) + 2*NLEV4A*LA  
      LAST  =IA(7 ) + 2*NLEVE *NLATJ*NILH
C 
      IF(LAST  .GT.LENGTH) THEN 
         WRITE(6,6000)LAST,LENGTH 
         CALL XIT('SCADR6U',-8) 
      ENDIF 
C 
      RETURN
C-----------------------------------------------------------------------
 6000 FORMAT('0SIZE NEEDED=',I10,' DIMENSION=',I10) 
      END 
