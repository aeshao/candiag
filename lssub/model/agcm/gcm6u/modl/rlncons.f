      SUBROUTINE RLNCONS(TENER,TFICQ,QSRCRAT,QSRCRM1,WPP,WP0,WPM,TFICQM,
     1                   DELT,FS,KOUNT) 
  
C     * APRIL 26/90 - J. DE GRANDPRE (U.Q.A.M.) 
C                   - EXTRACTION DU CODE DE LA SUBROUTINE ENERSV3 DE FACON
C                   - A POUVOIR L'UTILISER DANS LE CAS DU TRACEUR.
C     * SUBROUTINE QUI CALCULE L'EXCES OU LE DEFICIT DE MASSE DE CHACUN DES 
C     * TRACEURS A CHAQUE PAS DE TEMPS
  
C     *   TENER    MASSE GLOBALE DE LA QUANTITE X AU TEMPS T AVANT CORRECTION 
C     *   TFICQ    MASSE Y AJOUTEE AU SYSTEME DANS L'INTERVALLE DE TEMPS DT 
C     *            PRECEDENT, DUE A LA PRESENCE DES TERMES DE SOURCE. 
C     *     WP0    MASSE GLOBALE DE LA QUANTITE X AU TEMPS T TELLE QUE PREDIT 
C     *            AU TEMPS T-DT. 
C     *     WPP    ESTIMATION DE LA MASSE DE X AU TEMPS T+DT
C     * QSRCRM1    CORRECTION APPORTEE A LA MASSE GLOBALE AU TEMPS T
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
C---------------------------------------------------------------------- 
      IF(KOUNT.EQ.0) THEN 
         WP0=TENER
         WPM=TENER
         TFICQM=TFICQ 
         QSRCRM1=QSRCRAT
         WPP=WP0 + DELT*TFICQM
      ELSE
         TEMP=WPP 
         WPM=WP0
         WP0=TEMP 
         WPP=WPM + 2.*DELT*TFICQM 
         TFICQM=TFICQ 
         QSRCRM1=QSRCRAT
         QSRCRAT=WP0/TENER
C 
C        * FOLLOWING LINE IS TO BE INCLUDED IF DESIRE TIME
C        * FILTER.
C 
         WP0=FS*WPP + (1.-2.*FS)*TEMP + FS*WPM
C 
      ENDIF 
  
      RETURN
      END 
