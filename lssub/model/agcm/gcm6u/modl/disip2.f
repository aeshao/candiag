      SUBROUTINE DISIP2(SP,LSR,LM,DISSIP,DT,KTR)
C 
C     * AUG 30/79 - J.D.HENDERSON 
C     * PERFORMS DISSIPATION ON GLOBAL SPECTRAL FIELD SP. 
C     * THE FORMULA IS FROM G. BOER (SEPT 78).
C 
C     * DISSIP = DISSIPATION CONSTANT 
C     * DT = MODEL TIMESTEP IN SECONDS
C     * LSR CONTAINS ROW LENGTH INFO. 
C     * KTR = TRUNCATION TYPE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * LEVEL 2,SP
      COMPLEX SP(1) 
      INTEGER LSR(2,1)
C---------------------------------------------------------------------
      IF(DISSIP.EQ.0.) RETURN 
      DISDT=2.*DT*DISSIP*4. 
      FR=FLOAT(LM-1)
      IF(KTR.EQ.2) FR=FR/SQRT(2.) 
      FNSTAR=SQRT(2.)*FR
      NSMIN=INT(.55*FNSTAR+.5)
C 
      DO 210 M=1,LM 
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-1 
      DO 210 K=KL,KR
      NS=(M-1)+(K-KL) 
      IF(NS.LT.NSMIN) GO TO 210 
      FNS=FLOAT(NS) 
      C=DISDT*(FNS/FNSTAR-.55)**2 
      SP(K)=SP(K)/(1.+C)
  210 CONTINUE
      RETURN
      END 
