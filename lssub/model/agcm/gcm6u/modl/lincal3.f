      SUBROUTINE LINCAL3
     1                 ( RZ, RD, RT, RQ, RPS, RX, 
     3                   LA, LM, LSR, NK, NM, NMAX, 
     4                   ZM, DM, TM, QM, LPSM, XM,
     5                   Z, D, T, Q, LPS, X, ITRAC, 
     6                   NTRAC, SSDK,  PHIK,
     7                   DEL2LA, AM, BM, CM,
     8                   R, TREF, R2DT )

C     * FEB 02/94 - M.LAZARE. USE TRIANGULAR-FILLED "DEL2LA" INSTEAD
C     *                       OF "DEL2" TO OPTIMIZE LOOPS 4001-4003. 
C     * AUG 14/90 - J-D.GRANDPRE. PREVIOUS VERSION LINCAL2.
C 
C     * MODELE SPECTRAL,SEMI-IMPLICITE,ELEMENTS FINIS CONSTANT
C     * CALCUL DE LA CONTRIBUTION DES TERMES LINEAIRES
C     * AUX COTES DROITS DES EQUATIONS
C     * -IL Y A CONTRIBUTION DES VARIABLES A T-DT 
C     * -IL Y A CONTRIBUTION DE TERMES IMPLIQUES DANS LE
C     * SCHEME SEMI-IMPLICITE ET CONCERNANT LES VARIABLES 
C     * A T ET A T-DT 
C 
C     * ON CALCULE LES QUANTITES
C 
C S          RZ(MN,K)    : PARTIE DU COTE DROIT, EQN DU TOURBILLON .
C S          RD(MN,K)    : PARTIE DU COTE DROIT, EQN DE LA DIVERGENCE . 
C S          RT(MN,K)    : PARTIE DU COTE DROIT, EQN THERMODYNAMIQUE. 
C S          RQ(MN,KM)   : PARTIE DU COTE DROIT, EQN DE L'HUMIDITE. 
C S          RPS(MN)     : COTE DROIT DE L'EQUATION DE CONTINUITE.
C S          RX(MN,K)    : PARTIE DU COTE DROIT, EQN DU TRACEUR.
C 
C      POUR CHAQUE NOMBRE D'ONDE MN ET CHAQUE COUCHE K
C 
C E          LA          : NOMBRE DE COEFFICIENTS SPECTRAUX.
C E          LM          :  
C E          LSR(2,*)    :  
C E          NK          : NOMBRE DE COUCHES. 
C E          NM          : NOMBRE DE COUCHES D'HUMIDITE.
C 
C     A PARTIR DES DONNEES D'ENTREE 
C 
C E          ZM(MN,K)    : TOURBILLON RELATIF , A T-DT. 
C E          DM(MN,K)    : DIVERGENCE , A T-DT. 
C E          TM(MN,K)    : TEMPERATURE , A T-DT.
C E          QM(MN,KM)   : VARIABLE HUMIDITE , A T-DT.
C E          LPSM(MN)    : LOG DE LA PRESSION DE SURFACE
C E          XM(MN,K)    : F(TRACEUR) , A T-DT. 
C E          X(MN,K)     : TRACEUR. 
C E          Z(MN,K)     : TOURBILLON . 
C E          D(MN,K)     : DIVERGENCE . 
C E          T(MN,K)     : TEMPERATURE .
C E          Q(MN,KM)    : HUMIDITIE .
C E          LPS(MN)     : LOG DE LA PRESSION DE SURFACE
C 
C     ON SE SERT DES CHAMPS DE TRAVAIL SUIVANTS 
C     SSDK ET PHIK PEUVENT ETRE EQUIVALENCES DANS L'APPEL.
C 
C E          SSDK(MN)    : SOMME DES COUCHES 1 A K DE (D-D-/ 2)*DSIGMA. 
C E          PHIK(MN)    : SOMME DES COUCHES K+1 A NK DE R*(T-T-/ 2)*DL 
C 
C     ON SE SERT AUSSI DES PARAMETRES SUIVANTS
C 
C E          DSG(K)      : DSIGMA, EPAISSEUR DE LA COUCHE K.
C E          DSH(K)      : DSIGMA, EPAISSEUR DE LA COUCHE K.
C E          DLNSG(K)    : DLNSIGMA.
C E          A1SG(K)     : (1.-SIGMAM*DLNSIGMA/DSIGMA). 
C E          A2SG(K)     : (1.-SIGMAM*DLNSIGMA/DSIGMA). 
C E          R           : CONSTANTE DES GAZ PARFAITS.
C E          TREF        : TREF, UNE CONSTANTE..
C E          R2DT        : RECIPROQUE DE 2*DT.
C 

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

C     * SORTIES 
  
      COMPLEX RZ(LA,NK),RD(LA,NK),RT(LA,NK),RQ(LA,NM),RPS(LA) 
      COMPLEX RX(LA,NK,NTRAC) 
  
C     * ENTREES 
C 
      COMPLEX ZM(LA,NK),DM(LA,NK),TM(LA,NK),QM(LA,NM),LPSM(LA)
      COMPLEX Z (LA,NK),D (LA,NK),T (LA,NK),Q (LA,NM),LPS (LA)
      COMPLEX XM(LA,NK,NTRAC),X(LA,NK,NTRAC)
C 
C     * CHAMPS DE TRAVAIL 
C 
      COMPLEX SSDK(LA), PHIK(LA)
C 
C     * PARAMETRES DE LA DISCRETISATION VERTICALE 
C 
      REAL    DEL2LA(LA)
      REAL    AM(NK,NK),BM(NK,NK),CM(NK,NK) 
C 
      INTEGER LSR(2,*)
C 
C     ----------------------------------------------------------------- 
C            ETAPE 1 :  1.CALCUL DE PARAMETRES INTERNES, S IL Y A LIEU
C                       2.INITIALISATION DE DIVERS CHAMPS, S IL Y A LIEU
C     ----------------------------------------------------------------- 
C 
            DO 1001 MN = 1,LA 
            SSDK(MN) = (0.,0.)
 1001       CONTINUE
C 
C     ----------------------------------------------------------------- 
C            ETAPE 2 :   CONTRIBUTIONS DE Z-, D-, T-, Q-, LPS-, 
C                                      ET X-
C                        RESPECTIVEMENT,DANS RZ, RD, RT, RQ, RPS. 
C                                            ET RX. 
C     ----------------------------------------------------------------- 
C 
      DO 2001 K = 1,NK
            DO 2001 MN = 1,LA 
C 
            RZ(MN,K) = ZM(MN,K)*R2DT
C 
 2001 CONTINUE
C 
      DO 2002 K = 1,NM
            DO 2002 MN = 1,LA 
C 
            RQ(MN,K) = QM(MN,K)*R2DT
C 
 2002 CONTINUE
C 
      IF(ITRAC.NE.0) THEN 
  
      DO 2003 N=1,NTRAC 
      DO 2003 K = 1,NK
            DO 2003 MN = 1,LA 
C 
            RX(MN,K,N)=XM(MN,K,N)*R2DT
C 
 2003 CONTINUE
  
      ENDIF 
C 
C 
C     ----------------------------------------------------------------- 
C            ETAPE 3 :    SOLUTION DU SCHEME SEMI-IMPLICITE 
C                     A)  CONTRIBUTION DES TERMES SEMI-IMPLICITES 
C                                      DANS RT ET RPS.
C     ----------------------------------------------------------------- 
C 
      K=1 
            DO 3001 MN=1,LA 
C 
            RT(MN,K) = TM(MN,K)*R2DT
     1               + BM(K,K)*(D(MN,K)-0.5*DM(MN,K)) 
     2               + BM(K+1,K)*(D(MN,K+1)-0.5*DM(MN,K+1)) 
C 
 3001       CONTINUE
C 
      DO 3002 K=2,NK-1
            DO 3002 MN=1,LA 
C 
            SSDK(MN) = SSDK(MN) + (D(MN,K-1)-0.5*DM(MN,K-1))*CM(K-1,K)
C 
            RT(MN,K) = TM(MN,K)*R2DT
     1               + (BM(K-1,K)/CM(K-1,K))*SSDK(MN) 
     2               + BM(K,K)*(D(MN,K)-0.5*DM(MN,K)) 
     3               + BM(K+1,K)*(D(MN,K+1)-0.5*DM(MN,K+1)) 
C 
 3002 CONTINUE
C 
      K=NK
            DO 3012 MN=1,LA 
C 
            SSDK(MN) = SSDK(MN) + (D(MN,K-1)-0.5*DM(MN,K-1))*CM(K-1,K)
C 
            RT(MN,K) = TM(MN,K)*R2DT
     1               + (BM(K-1,K)/CM(K-1,K))*SSDK(MN) 
     2               + BM(K,K)*(D(MN,K)-0.5*DM(MN,K)) 
C 
 3012 CONTINUE
C 
            DO 3003 MN=1,LA 
C 
            SSDK(MN)  =SSDK(MN)+(D(MN,K)-0.5*DM(MN,K))*CM(K,K)
C 
            RPS(MN)    = LPSM(MN)*R2DT + SSDK(MN) 
C 
 3003       CONTINUE
C 
C     ----------------------------------------------------------------- 
C            ETAPE 4 :    SOLUTION DU SCHEME SEMI-IMPLICITE 
C                     B)  CONTRIBUTION DES TERMES SEMI-IMPLICITES DANS
C     ----------------------------------------------------------------- 
C 
      K = NK
      RT0 = R*TREF
      DO 4001 MN=1,LA
         PHIK(MN) = RT0*(LPS(MN)-0.5*LPSM(MN))
         RD(MN,K) = DM(MN,K)*R2DT + DEL2LA(MN)* ( PHIK(MN)
     1             + AM(K,K)*(T(MN,K)-0.5*TM(MN,K)) 
     2             + AM(K-1,K)*(T(MN,K-1)-0.5*TM(MN,K-1))  ) 
 4001 CONTINUE
C 
      DO 4002 K=NK-1,2,-1 
      DO 4002 MN=1,LA
         PHIK(MN) = PHIK(MN)+AM(K+1,K)*(T(MN,K+1)-0.5*TM(MN,K+1))
         RD(MN,K) = DM(MN,K)*R2DT + DEL2LA(MN)* ( PHIK(MN)
     1             + AM(K,K)*(T(MN,K)-0.5*TM(MN,K)) 
     2             + AM(K-1,K)*(T(MN,K-1)-0.5*TM(MN,K-1))  ) 
 4002 CONTINUE
C 
      K=1 
      DO 4003 MN=1,LA
         PHIK(MN) = PHIK(MN)+AM(K+1,K)*(T(MN,K+1)-0.5*TM(MN,K+1))
         RD(MN,K) = DM(MN,K)*R2DT + DEL2LA(MN)* ( PHIK(MN)
     1             + AM(K,K)*(T(MN,K)-0.5*TM(MN,K))  )
 4003 CONTINUE
C 
      RETURN
C-----------------------------------------------------------------------
      END 
