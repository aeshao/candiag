      SUBROUTINE UNDOTF1(X,XM,F,ISIZ) 
C 
C     * OCT 26/87 - R.LAPRISE.
C     * UNDO THE EFFECT OF TF1. 
C     * F  IS THE TIME FILTER COEFFICIENT.
C     * X  IS X' (K  ) ON INPUT AND X (K) ON OUTPUT,
C     * XM IS X''(K-1). 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX X(ISIZ),XM(ISIZ)
C-----------------------------------------------------------------------
      DO 100 I=1,ISIZ 
         X(I) = (X(I)-F*XM(I)) / (1.-2.*F)
  100 CONTINUE
C 
      RETURN
      END 
