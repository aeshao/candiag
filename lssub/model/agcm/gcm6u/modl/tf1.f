      SUBROUTINE TF1 (X,XM,F,ISIZ)
C 
C     * OCT 26/87 - R.LAPRISE.
C     * APPLY FIRST PART OF TIME FILTER TO SPECTRAL VARIABLE X(ISIZ). 
C     * F  IS THE TIME FILTER COEFFICIENT.
C     * X  IS X  (K)  ON INPUT AND X'(K) ON OUTPUT. 
C     * XM IS X''(K-1). 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      COMPLEX X(ISIZ),XM(ISIZ)
C-----------------------------------------------------------------------
      DO 100 I=1,ISIZ 
         X(I) = (1.-2.*F)*X(I) + F*XM(I)
  100 CONTINUE
C 
      RETURN
      END 
