      SUBROUTINE FIZDTND(  RDESODQ,  HSJ,  PRESSG,
     1                     MOIST, AH, BH, ILEV, LEVS, ILG, LON, SHUMREF)

C     * DEC 17/96 - B.DENIS. CORRECTIONS FOR "RDESODQ" FOR 4HLNQ AND
C     *                      4HQHYB CASES.   
C     * JUN 15/90 - M.LAZARE. 
  
C     * CALCULATES FIELD FOR CONVERSION FROM MOISTURE TENDENCY TO 
C     * SPECIFIC HUMIDITY TENDENCY (RECIPROCAL OF D(ES)/DQ).
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      REAL RDESODQ(ILG,ILEV)
  
      REAL HSJ(ILG,ILEV),  PRESSG(ILG),     AH(ILEV),       BH(ILEV)
  
      COMMON /EPS/ AA,BB,EPS1,EPS2
C-----------------------------------------------------------------------
  
      DO 300 L=1,LEVS 
          MD=L+(ILEV-LEVS)
          DO 200 I=1,LON
              IF(MOIST.EQ.NC4TO8("   Q")) THEN
                  RDESODQ(I,MD) = 1.
              ELSEIF(MOIST.EQ.NC4TO8(" LNQ")) THEN
                  RDESODQ(I,MD) = HSJ(I,MD) 
              ELSEIF(MOIST.EQ.NC4TO8("RLNQ")) THEN
                  RDESODQ(I,MD) = HSJ(I,MD)*LOG(HSJ(I,MD))**2
              ELSEIF(MOIST.EQ.NC4TO8("SQRT")) THEN
                  RDESODQ(I,MD) = 2.*SQRT(HSJ(I,MD))
              ELSEIF(MOIST.EQ.NC4TO8("  TD")) THEN
                  PRESMB  = 0.01*(AH(MD)+BH(MD)*PRESSG(I))
                  PVAP    = HSJ(I,MD)*PRESMB/(EPS1+EPS2*HSJ(I,MD))
                  TD      = BB/(AA-LOG(PVAP))
                  RDESODQ(I,MD) = +1.*BB*PRESMB*(HSJ(I,MD)**2)/ 
     1                      (EPS1*PVAP*TD**2) 
              ELSEIF(MOIST.EQ.NC4TO8("T-TD")) THEN
                  PRESMB  = 0.01*(AH(MD)+BH(MD)*PRESSG(I))
                  PVAP    = HSJ(I,MD)*PRESMB/(EPS1+EPS2*HSJ(I,MD))
                  TD      = BB/(AA-LOG(PVAP))
                  RDESODQ(I,MD) = -1.*BB*PRESMB*(HSJ(I,MD)**2)/ 
     1                      (EPS1*PVAP*TD**2) 
              ELSEIF(MOIST.EQ.NC4TO8("QHYB")) THEN
                 IF(HSJ(I,MD).GE.SHUMREF) THEN       
                   RDESODQ(I,MD) = 1.0
                 ELSE
                   Q0OQ = SHUMREF/HSJ(I,MD)
                   RDESODQ(I,MD) = Q0OQ/((1.+LOG(Q0OQ))**2)
                 ENDIF
              ELSE
                  CALL XIT('FIZDTND',-1)
              ENDIF 
  200     CONTINUE
  300 CONTINUE
C 
      IF(ILEV-LEVS.GT.0) THEN 
          DO 400  MD=1,ILEV-LEVS
          DO 400 I=1,LON
              RDESODQ(I,MD) = 0.
  400     CONTINUE
      ENDIF 
C-------------------------------------------------------------------
      RETURN
      END 
