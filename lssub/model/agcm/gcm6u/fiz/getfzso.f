      SUBROUTINE GETFZSO(NF,C,ES,P,PHI,PHIS,PS,LA,KOUNT,ILEV,LEVS,LS, 
     1                   LH,LSPHI,IDIV) 
C 
C     * JUN 15/89 - S.GOYETTE.
C     * GET SPECTRAL VARIABLES FOR THE FIZ RUN. NF SHOULD BE LUSP 
C     * AND KOUNT SHOULD HAVE BEEN SETTLED TO THE FIRST TIMESTEP SAVED
C     * IN A PREVIOUS G.C.M. RUN. 
C     * LS AND LH ARE LABEL SIGMA VALUES FOR FULL AND HALF LEVELS.
C     * LSPHI 
C 
C     * NOTE - IF ANY FIELD IS MISSING, THE PROGRAM WILL STOP.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)

      LOGICAL OK,IDIV 
      COMPLEX   C(LA,1) 
      COMPLEX  ES(LA,1) 
      COMPLEX   P(LA,1),  PHI(LA,1),PHIS(1),  PS(1) 
      INTEGER  LS(1),   LSPHI(1),     LH(1) 
C 
C 
      COMMON/ICOM/  IBUF(8),IDAT(1) 
C --------------------------------------------------------------------- 
      WRITE(6,6010) 
      MAXX = 2*LA
C 
C     * MOUNTAINS (M/SEC)**2. 
C 
      CALL GETFLD2(NF,PHIS,NC4TO8("SPEC"),KOUNT,NC4TO8("PHIS"),1,
     1                                              IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETFZSO',-1)
      WRITE(6,6015) (IBUF(I),I=1,8) 
  
C 
C     * PHI.
C 
      DO 100 L=1,ILEV 
      CALL GETFLD2(NF,PHI(1,L),NC4TO8("SPEC"),KOUNT,NC4TO8(" PHI"),
     1                                       LSPHI(L),IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETFZSO',-2)
      WRITE(6,6015) (IBUF(I),I=1,8) 
  100 CONTINUE
C 
C     * LOG OF SURFACE PRESSURE (NEWTONS / M**2) CONVERTED FROM MB. 
C 
      CALL GETFLD2(NF,PS,NC4TO8("SPEC"),KOUNT,NC4TO8("LNSP"),1,
     1                                            IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETFZSO',-3)
      WRITE(6,6015) (IBUF(I),I=1,8) 
      PS(1)=PS(1)+LOG(100.)*SQRT(2.0)
C 
C     * VORTICITY AND DIVERGENCE (1./SEC).
C     * IF IDIV IS FALSE SET DIVERGENCE TO ZERO.
C 
      DO 200 L=1,ILEV 
      CALL GETFLD2(NF,P(1,L),NC4TO8("SPEC"),KOUNT,NC4TO8("VORT"),
     1                                        LS(L),IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETFZSO',-4)
      WRITE(6,6015) (IBUF(I),I=1,8) 
      CALL GETFLD2(NF,C(1,L),NC4TO8("SPEC"),KOUNT,NC4TO8(" DIV"),
     1                                        LS(L),IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETFZSO',-5)
      WRITE(6,6015) (IBUF(I),I=1,8) 
      IF(.NOT.IDIV) CALL SCOF2(C(1,L),LA,1,0) 
  200 CONTINUE
C 
C     * DEW POINT DEPRESSION
C 
      IF(LEVS.EQ.0) RETURN
      DO 300 L=1,LEVS 
      N=(ILEV-LEVS)+L 
      CALL GETFLD2(NF,ES(1,L),NC4TO8("SPEC"),KOUNT,NC4TO8("  ES"),
     1                                         LH(N),IBUF,MAXX,OK)
      IF(.NOT.OK) CALL XIT('GETFZSO',-6)
      WRITE(6,6015) (IBUF(I),I=1,8) 
  300 CONTINUE
C 
      WRITE(6,6020) 
      RETURN
C --------------------------------------------------------------------- 
 6010 FORMAT(1X,20('-'),'>  GETFZSO BEGIN <',20('-')) 
 6015 FORMAT(' ',A4,I10,1X,A4,5I6)
 6020 FORMAT(1X,20('-'),'>  GETFZSO END   <',20('-')) 
      END 
