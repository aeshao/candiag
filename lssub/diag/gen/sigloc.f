      SUBROUTINE SIGLOC (ACH,BCH,AH,BH,SH,SHB,NK,LAY,ICOORD,PTOIT)
  
C     * OCT 25/88 - M.LAZARE. BASED ON MODEL SUBROUTINE BASCAL, 
C     *                       CALCULATES LOCAL SIGMA INFORMATION ARRAYS 
C     *                       ACH,BCH,AH,BH,SHB BASED ON INPUT ARRAY SH.
C     *                       ONLY HAS THERMODYNAMIC LEVEL INFORMATION
C     *                       FROM GCM HISTORY FILE, SO "SG" AND "SGB"
C     *                       ARRAYS FROM BASCAL NOT INCLUDED.
C     * 
C     * PARAMETERS: 
C     * 
C     * ACH,BCH(K)   : INFO. SUR POSITION DES MILIEUX DE COUCHES. 
C     * AH ,BH (K)   : INFO. SUR POSITION DES BASES DE COUCHES. 
C     * SH(K)        : MILIEU DES COUCHES DE TEMPERATURE. 
C     * SHB(K)       : BASES DES COUCHES DE TEMPERATURE.
C     * NK           : NOMBRE DE COUCHES. 
C     * 
C     * LAY = TYPE D'ARRANGEMENT DES INTERFACES PAR RAPPORT 
C     *       AUX MILIEUX DE COUCHES. 
C     *     = 0, COMME LAY=2 POUR V ET LAY=4 POUR T,
C     *          POUR COMPATIBILITE AVEC VERSION ANTERIEURE DU GCM. 
C     *     = 1 / 2, INTERFACES DES COUCHES PLACEES A DISTANCE EGALE
C     *              SIGMA / GEOMETRIQUE DU MILIEU DES COUCHES
C     *              ADJACENTES.
C     *     = 3 / 4, BASES DES COUCHES EXTRAPOLEES EN MONTANT 
C     *              DE SORTE QUE LES MILIEUX DES COUCHES SOIENT
C     *              AU CENTRE SIGMA / GEOMETRIQUE DES COUCHES. 
C     * DECOUPLAGE DE L'EPAISSEUR DES COUCHES PEUT RESULTER 
C     * DE L'APPLICATION DE LAY=3 OU 4 (AUSSI =0 POUR T) SI 
C     * LES MILIEUX DE COUCHES NE SONT PAS DEFINIS DE FACON 
C     * APPROPRIES. 
C 
C     * ICOORD       : COORDINATE VERTICALE (4H ETA OU 4H SIG). 
C     * PTOIT        : LID OF MODEL (PASCALS).
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL ACH(NK),BCH(NK),AH(NK),BH(NK),SH(NK),SHB(NK) 
C-------------------------------------------------------------------- 
C     * DEFINITION DES BASES DES COUCHES. 
C 
      IF(LAY.EQ.0) THEN 
         SHB(NK)=1.E0 
         DO 1 K=NK-1,1,-1 
            SHB(K) = SH(K+1)**2/SHB(K+1)
    1    CONTINUE 
C 
      ELSEIF(LAY.EQ.1) THEN 
         DO 3 K=1,NK-1
            SHB(K)=0.5E0*(SH(K)+SH(K+1))
    3    CONTINUE 
         SHB(NK)=1.0E0
C 
      ELSEIF(LAY.EQ.2) THEN 
         DO 4 K=1,NK-1
            SHB(K)=SQRT(SH(K)*SH(K+1))
    4    CONTINUE 
         SHB(NK)=1.0E0
C 
      ELSEIF(LAY.EQ.3) THEN 
         SHB(NK)=1.0E0
         DO 5 K=NK-1,1,-1 
            SHB(K)=2.E0*SH(K+1)-SHB(K+1)
    5    CONTINUE 
C 
      ELSEIF(LAY.EQ.4) THEN 
         SHB(NK)=1.0E0
         DO 6 K=NK-1,1,-1 
            SHB(K)=SH(K+1)**2/SHB(K+1)
    6    CONTINUE 
C 
      ELSE
         CALL XIT('SIGLOC',-1) 
      ENDIF 
C 
      WRITE(6,6000) 'SH =',SH
      WRITE(6,6000) 'SHB=',SHB 
C 
C     * NOW CALCULATE "A" AND "B" ARRAYS. 
C 
      CALL COORDAB (ACH,BCH, NK,SH ,ICOORD,PTOIT) 
      CALL COORDAB (AH ,BH , NK,SHB,ICOORD,PTOIT) 
C 
      RETURN
C-------------------------------------------------------------- 
 6000 FORMAT(4X,A4,/,(8X,10E10.3)) 
      END 
