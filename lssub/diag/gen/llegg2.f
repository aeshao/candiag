      SUBROUTINE LLEGG2(GG,ILG1,ILAT,DLAT,GLL,NLG1,NLAT)
C 
C     * DEC  3/79 - J.D.HENDERSON 
C     * EXTRACTS A GLOBAL GAUSSIAN GRID GG(ILG1,ILAT) FROM A
C     *  GLOBAL LAT-LONG GRID GLL(NLG1,NLAT). 
C     * EACH GG POINT TAKES THE VALUE OF THE CLOSEST POINT IN GLL.
C 
C     * GG HAS ILG EQUALLY SPACED LONGITUDES. ILAT IS EVEN AND THE
C     *  EQUATOR AND POLES ARE NOT ONE OF THE LATITUDE CIRCLES. 
C     * DLAT(ILAT) CONTAINS GG LATITUDES IN DEGREES S. TO N.
C 
C     * THE LAT-LONG GRID GLL HAS NLG EQUALLY SPACED LONGITUDES 
C     *  AND NLAT EQUALLY SPACED LATITUDES. 
C     * THE POLES ARE INCLUDED AS THE TOP AND BOTTOM ROWS.
C 
C     * FOR BOTH GRIDS THE LEFT SIDE IS REPEATED ON THE RIGHT SIDE. 
C 
C     * LEVEL 2,GG,GLL
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL GG(ILG1,ILAT),GLL(NLG1,NLAT) 
      REAL DLAT(ILAT) 
C-----------------------------------------------------------------------
      ILG=ILG1-1
      DEGX=360.E0/FLOAT(NLG1-1) 
      DEGY=180.E0/FLOAT(NLAT-1) 
C 
C     * THE ROWS ARE DONE FROM SOUTH TO NORTH.
C     * DROW = DEGREES OF ROW J FROM THE SOUTH POLE.
C 
      DO 240 J=1,ILAT 
      DROW=DLAT(J)+90.E0
      Y=DROW/DEGY+1.E0
      JJ=INT(Y) 
      DY=Y-FLOAT(JJ)
      IF(DY.GT.0.5E0) JJ=JJ+1 
C 
C     * EXTRACT THE POINTS FROM LEFT TO RIGHT IN EACH ROW.
C     * DLON = DEGREES OF POINT I EASTWARD FROM GREENWICH.
C 
      DO 220 I=1,ILG
      DLON=FLOAT(I-1)/FLOAT(ILG)*360.E0 
      X=DLON/DEGX+1.E0
      II=INT(X) 
      DX=X-FLOAT(II)
      IF(DX.GT.0.5E0) II=II+1 
      GG(I,J)=GLL(II,JJ)
  220 CONTINUE
C 
      GG(ILG1,J)=GG(1,J)
  240 CONTINUE
C 
      RETURN
      END 
