      SUBROUTINE GAINSQ(L0,K,W,LAMBDA,B,NFREQ) 

C     * JAN 05/98 - F. ZWIERS
C     * AOUT 02/85 - F. ZWIERS, B.DUGAS.
  
C     * COMPUTE AND DISPLAY THE SQUARED GAIN OF THE FILTER
C     * 
C     * L0 IS THE CUTOFF FREQUENCY OF THE FILTER
C     * K IS THE TRUNCATION POINT OF THE FILTER WEIGHTS 
C     * 
C     * LAMBDA RETURNS THE FREQUENCIES AT WHICH THE SQUARED GAIN
C     * IS EVALUATED
C     * B RETURNS THE CORRESPONDING VALUES OF THE SQUARED GAIN
C     * 
C     * NOTE
C     *------------------------------------------------------------------ 
C     * 
C     * IMSL ROUTINE USPLO IS USED TO DISPLAY THE COMPUTED TRANSFER 
C     * FUNCTION. 
C     * 
C     *-----------------------------------------------------------------
  
  
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL L0,LAMBDA(1),B(1),W(1),RANGE(4)
      DATA PI/3.1415927E0/,TWOPI/6.2831853E0/

      NFREQ=99 
  
      IF (L0.LT.PI/2.0E0)                                        THEN 
          D = 2.0E0*L0/FLOAT(NFREQ-1) 
      ELSE
          D = 2.0E0*(PI-L0)/FLOAT(NFREQ-1)
      ENDIF 
  
      NF2 = (NFREQ-1)/2 
      DO 10 I=-NF2,NF2
          LAMBDA(I+NF2+1) = L0+FLOAT(I)*D 
   10 CONTINUE
  
      DO 20 I=1,NFREQ 
         GAIN=W(K+1)
         DO J=1,K
            GAIN = GAIN + 2.0E0*W(K+1+J)*COS(LAMBDA(I)*J)
         ENDDO
         B(I)=GAIN**2
         LAMBDA(I) = LAMBDA(I)/TWOPI 
   20 CONTINUE
  
      WRITE(6,6030) 
      WRITE(6,6040) (LAMBDA(I),B(I),I=1,NFREQ)
  
      RANGE(1) = LAMBDA(1)
      RANGE(2) = LAMBDA(NFREQ)
      RANGE(3) = -0.4E0 
      RANGE(4) =  1.6E0 
      CALL USPLO(LAMBDA,B,NFREQ,NFREQ,1,1,
     1           'SQUARED GAIN OF THE LOW PASS FILTER',35,
     2           'FREQUENCY',9,' ',0,RANGE,'@',1,IER) 
  
      RETURN
C---------------------------------------------------------------------- 
 6030 FORMAT('0SQUARED GAIN OF THE LOW PASS FILTER:'/ 
     1       7X,'F',5X,'ABS(B(F))**2'/' ------------------------')
 6040 FORMAT(1X,2E12.4) 
      END 
