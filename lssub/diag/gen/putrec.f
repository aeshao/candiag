      SUBROUTINE PUTREC(UNIT,TIME,T1,T3,V,ORDER,PUT)
C 
C     * M. LAZARE  -- AUG 13/90. REMOVE HARD-COAT ON "F", SINCE PASSED. 
C     *                          ALSO REMOVE "MAX", SINCE NOT USED. 
C     * F. ZWIERS -- JAN 14/85
C 
C     * UTILITY ROUTINE FOR SELECT2 USED TO WRITE A FIELD TO UNIT 
C     * 'UNIT' AFTER DOING SOME TIME CHECKING.
C 
C     * UNIT=OUTPUT UNIT
C     * TIME=TIME OF OBSERVATION
C     * T1=FIRST TIME TO BE SELECTED
C     * T3=SAMPLING INTERVAL -- T3> 0 ==> DO TIME CHECKING. RETURN
C     *                                   ORDER=.FALSE. IF A 'HOLE' OR
C     *                                   A CHANGE IN TIME ORDER IS 
C     *                                   DETECTED. 
C     *                         T3<=0 ==> OUTPUT ALL RECORDS. RETURN
C     *                                   ORDER=.FALSE. IF A CHANGE IN
C     *                                   TIME ORDER IS DETECTED. 
C     * V=VARIABLE DESIGNATOR IN THE RANGE 1 TO 4 -- THE ROUTINE
C     *   CAN KEEP TRACK OF THE TIME-SEQUENCING OF UPTO FOUR
C     *   VARIALBES.  HISTORICAL INFORMATION FROM PREVIOUS CALLS
C     *   TO THE ROUTINE NECESSARY FOR DETERMINING WHETHER RECORDS
C     *   ARE BEING PRESENTED TO THE ROUTINE IN TIME SEQUENCE IS
C     *   KEPT IN COMMON 'PREC'.
C     * ORDER IS RETURNED LOGICAL .TRUE. OR .FALSE. AS DESCRIBED ABOVE.
C     * PUT IS RETURNED LOGICAL .FALSE. IF A RECORD WAS NOT OUTPUTED. 
C     *    (.NOT.PUT = T3.GT.0 .AND. .NOT.ORDER.) 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER TIME,T1,T3,T,UNIT,TOLD(2),TSTEP(2),NSET(2),ISET(2),V
      LOGICAL PUT,ORDER,FIRST(2),SECOND(2)
      COMMON/ICOM/IBUF(8),F(10000)
      COMMON/PREC/TOLD,TSTEP,NSET,ISET,FIRST,SECOND 
      DATA MAXV/2/
C 
C-----------------------------------------------------------------------------
C 
      ORDER=.TRUE. 
      PUT=.TRUE. 
      IF(V.LT.1 .OR. V.GT.MAXV)THEN 
         PUT=.FALSE.
         RETURN 
      ENDIF 
      IF(ISET(V).EQ.0)THEN
C 
C        * FIRST RECORD IN 'SET'
C 
         IF(T3.GT.0)THEN
C 
C           * OUTPUT RECORD ONLY IF TIME-T1=0 (MOD T3) AND THE
C           * PRESENT TIME IS TIME OF THE PREVIOUS SET PLUS T3. 
C 
            T=MOD(TIME-T1,T3) 
            IF(T.EQ.0)THEN
               IF(FIRST(V))THEN 
                  CALL RECPUT(UNIT,IBUF)
                  FIRST(V)=.FALSE.
                  TOLD(V)=TIME
                  ISET(V)=ISET(V)+1 
               ELSEIF(TIME.EQ.TOLD(V)+T3)THEN 
                  CALL RECPUT(UNIT,IBUF)
                  TOLD(V)=TIME
                  ISET(V)=ISET(V)+1 
               ELSE 
                  PUT=.FALSE. 
                  ORDER=.FALSE. 
               ENDIF
            ELSE
               PUT=.FALSE.
            ENDIF 
         ELSE 
C 
C           * OUTPUT EVERY RECORD BUT NOTE ANY CHANGE IN TIMESTEP 
C 
            IF(FIRST(V))THEN
               FIRST(V)=.FALSE. 
               SECOND(V)=.TRUE.
               TOLD(V)=TIME 
            ELSEIF(SECOND(V))THEN 
               SECOND(V)=.FALSE.
               TSTEP(V)=TIME-TOLD(V)
               TOLD(V)=TIME 
            ELSE
               T=TIME-TOLD(V) 
               TOLD(V)=TIME 
               IF(T.NE.TSTEP(V))THEN
                  ORDER=.FALSE. 
                  TSTEP(V)=T
               ENDIF
            ENDIF 
            CALL RECPUT(UNIT,IBUF)
            ISET(V)=ISET(V)+1 
         ENDIF
      ELSE
C 
C         * 2ND AND SUBSEQUENT RECORDS IN A 'SET' 
C 
          IF(T3.GT.0)THEN 
             IF(TIME.EQ.TOLD(V))THEN
                CALL RECPUT(UNIT,IBUF)
                ISET(V)=ISET(V)+1 
             ELSE 
                PUT=.FALSE. 
                ORDER=.FALSE. 
             ENDIF
          ELSE
             IF(TIME.EQ.TOLD(V))THEN
                ISET(V)=ISET(V)+1 
             ELSE 
                ORDER=.FALSE. 
                ISET(V)=1 
                TOLD(V)=TIME
             ENDIF
             CALL RECPUT(UNIT,IBUF) 
          ENDIF 
      ENDIF 
C 
C     * SET 'SET' COUNTERS TO ZERO IF A SET HAS BEEN COMPLETED
C 
      ISET(V)=MOD(ISET(V),NSET(V))
C 
C-----------------------------------------------------------------------------
C 
      RETURN
      END 
