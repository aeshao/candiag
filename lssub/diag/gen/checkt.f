      SUBROUTINE CHECKT(TIME,YY,MM,DD,HH,MINN,OBS,OK)
C 
C     * F. ZWIERS  --  DEC 27/84
C 
C     * DO CONSISTENCY CHECKING ON A TIME GIVEN IN FORMAT YY,DD,MM,HH,MIN.
C     * OK IS RETURNED .TRUE. OR .FALSE. DEPENDING UPON WHETHER OR NOT ALL 
C     * VALUES FOUND TO BE CONSISTENT.
C     * TIME IS THE PACKED VERSION OF YY,MM,DD,HH,MIN 
C     * OBS = .TRUE.  INDICATES THAT THE YY RANGE IS [1,199]
C     * OBS = .FALSE. INDICATES THAT THE YY RANGE IS [0,99] 
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER TIME,YY,MM,DD,HH,MINN,LMONTH(12),YLOW,YHIGH
      LOGICAL OBS,OK
      DATA LMONTH/31,28,31,30,31,30,31,31,30,31,30,31/
C 
C---------------------------------------------------------------------------
C 
      OK=.TRUE.
      IF(OBS)THEN 
         IF(YY.EQ.(YY/4)*4 .AND. YY.NE.100)THEN 
            LMONTH(2)=29
         ELSE 
            LMONTH(2)=28
         ENDIF
         YLOW=1 
         YHIGH=199
      ELSE
         LMONTH(2)=28 
         YLOW=0 
         YHIGH=99 
      ENDIF 
      IF(YY.LT.YLOW .OR. YY.GT.YHIGH)THEN 
         OK=.FALSE. 
         WRITE(6,6010) TIME 
         RETURN 
      ENDIF 
      IF(MM.LT.1 .OR. MM.GT.12) THEN
         OK=.FALSE. 
         WRITE(6,6010) TIME 
         RETURN 
      ENDIF 
      IF(DD.LT.1 .OR. DD.GT.LMONTH(MM))THEN 
         OK=.FALSE. 
         WRITE(6,6010) TIME 
         RETURN 
      ENDIF 
      IF(HH.LT.0 .OR. HH.GT.23)THEN 
         OK=.FALSE. 
         WRITE(6,6010) TIME 
         RETURN 
      ENDIF 
      IF(MINN.LT.0 .OR. MINN.GT.59)THEN 
         OK=.FALSE. 
         WRITE(6,6010) TIME 
         RETURN 
      ENDIF 
C 
C-------------------------------------------------------------------------- 
C 
      RETURN
 6010 FORMAT('0********** TIME ',I12,' HAS CONSTITUENTS OUT OF RANGE.') 
      END 
