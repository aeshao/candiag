      SUBROUTINE LBL2T (IBUF8W, IBUFT)

C     * FEB 22/96 - DAMIN LIU

C     * THIS SUBROUTINE CONVERTS AN 8-WORD LABEL IBUF8W
C     * INTO A STANDARD TIME SERIES LABEL IBUFT.
C
C     * STANDARD TIME SERIES LABEL IS DEFINED AS FOLLOWS:
C               IBUFT(1) = TIME
C               IBUFT(2) = LOCATION (IN LATLON FORMAT)
C               IBUFT(3) = NAME       
C               IBUFT(4) = LEVEL                   
C               IBUFT(5) = LENGTH OF SERIES
C               IBUFT(6) = 1
C               IBUFT(7) = DIMENSION OF ARRAY, KHEM AND DATA TYPE
C                          (IN THE FORMAT CCCRRRKQ  OR  LRLMTQ)
C               IBUFT(8) = PACKING DENSITY
C
C     WHEN CONVERTED, IBUFT RETAINS THE INFORMATION OF NAME, LEVEL, AND
C     THE PACKING DENSITY OF IBUF8W. IBUFT(1) IS SET TO 'TIME' AND IBUFT(6)
C     IS DEFINED AS 1 FOR THE 1-D TIME SERIES. IBUFT(7) CONTAINS CODED
C     INFORMATION. IBUF8W DOES NOT CONTAIN INFORMATION FOR IBUFT(2) AND
C     IBUFT(5). THIS SUBROUTINE SIMPLY ASSIGNS IBUF8W(2) AND IBUF8W(5)
C     TO IBUFT(2) AND IBUFT(5), RESPECTIVELY. THEY SHOULD BE RESET OUTSIDE
C     THE SUBROUTINE.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER IBUFT(8), IBUF8W(8)
      LOGICAL SPEC

C     DATA NGRID/4HGRID/,NZONL/4HZONL/,
C    1     NSPEC/4HSPEC/,NSUBA/4HSUBA/
C--------------------------------------------------------------------
C
      NGRID=NC4TO8("GRID")
      NZONL=NC4TO8("ZONL")
      NSPEC=NC4TO8("SPEC")
      NSUBA=NC4TO8("SUBA")
      SPEC=.FALSE.
      KIND=IBUF8W(1)
      IF (KIND.EQ.NGRID)                                       THEN
          IKIND=1
      ELSE IF (KIND.EQ.NC4TO8("ZONL"))                         THEN
          IKIND=2
      ELSE IF (KIND.EQ.NC4TO8("SUBA"))                         THEN
          IKIND=9
      ELSE IF (KIND.EQ.NC4TO8("SPEC"))                         THEN
          WRITE(6,6010)
          SPEC = .TRUE.
          IKIND=3
      ELSE
         CALL                                      XIT('LBL2T',-1)
      ENDIF

C     * SET UP TIME SERIES LABEL
C     * IBUFT(2)=LOCATION OF THE POINT SHOULD BE SET OUTSIDE THE SUBROUTINE

      IBUFT(2)      = IBUF8W(2)          

C     * IBUFT(5)=LENGTH OF TIME SERIES SHOULD BE SET OUTSIDE THE SUBROUTINE
                                        
      IBUFT(5)      = IBUF8W(5)          
                                        
C     * NAME,LEVEL AND PACKING DENSITY UNCHANGED.

      IBUFT(3)      = IBUF8W(3)          
      IBUFT(4)      = IBUF8W(4)         
      IBUFT(8)      = IBUF8W(8)        


      IBUFT(1)      = NC4TO8("TIME")
      IBUFT(6)      = 1                 
      IBUFT(7)      = 100000*IBUF8W(5)+100*IBUF8W(6)+10*IBUF8W(7)+IKIND
      IF (SPEC) IBUFT(7)  = 10*IBUF8W(7)+IKIND
C--------------------------------------------------------------------
 6010 FORMAT('0RECORD KIND IS NOT GRID OR ZONL.')

      RETURN
      END
