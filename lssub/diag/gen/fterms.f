      SUBROUTINE FTERMS(F,U,V,Q,D,EPSI,LSR,LM)
C 
C     * JUL 14/92 - E. CHAN (ADD REAL*8 DECLARATIONS)
C     * FEB  1980 - S. LAMBERT 
C 
C     * COMPUTES THE CORIOLIS TERMS IN THE KINETIC ENERGY BUDGET
C 
C     * LEVEL 2, U,V,Q,D,EPSI,F 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      COMPLEX U(1),V(1),Q(1),D(1),F(1)
      REAL*8 EPSI(1)
      INTEGER LSR(2,1)
      COMPLEX A,B 
C 
C-----------------------------------------------------------------------
C 
      FMAX=1.458E-4 
C 
C     * FIRST LRS-1 COEFFICIENTS IN EACH ROW
C     * K INDEXES Q,D.  IUV INDEXES U,V,EPSI. 
C 
      DO 40 M=1,LM
      MS=M-1
      KL=LSR(1,M) 
      KR=LSR(1,M+1)-2 
      IF(KR.LT.KL) GO TO 50 
      JL=LSR(2,M) 
      DO 20 K=KL,KR 
      NS=MS+(K-KL)
      IF(NS.EQ.0) GO TO 20
      IUV=JL+(K-KL) 
      A=FMAX*(EPSI(IUV+1)*Q(K+1)+EPSI(IUV)*Q(K-1)-U(IUV)) 
      B=-FMAX*(EPSI(IUV+1)*D(K+1)+EPSI(IUV)*D(K-1)+V(IUV))
      C=REAL(A)*REAL(D(K))+ IMAG(A)* IMAG(D(K)) 
      E=REAL(B)*REAL(Q(K))+ IMAG(B)* IMAG(Q(K)) 
      F(K)=CMPLX(C+E,0.E0)
20    CONTINUE
C 
C     * LAST COEFFICIENT IN EACH ROW
C 
      A=FMAX*(EPSI(IUV+1)*Q(KR)-U(IUV+1)) 
      B=-FMAX*(EPSI(IUV+1)*D(KR)+V(IUV+1))
      C=REAL(A)*REAL(D(KR+1))+ IMAG(A)* IMAG(D(KR+1)) 
      E=REAL(B)*REAL(Q(KR+1))+ IMAG(B)* IMAG(Q(KR+1)) 
      F(KR+1)=CMPLX(C+E,0.E0) 
40    CONTINUE
      GO TO 60
C 
C     * TRIANGULAR CASE - LAST ROW HAS ONLY ONE COEFFICIENT 
C 
50    IUV=LSR(2,LM) 
      K=LSR(1,LM) 
      A=-FMAX*U(IUV)
      B=-FMAX*V(IUV)
      C=REAL(A)*REAL(D(K))+ IMAG(A)* IMAG(D(K)) 
      E=REAL(B)*REAL(Q(K))+ IMAG(B)* IMAG(Q(K)) 
      F(K)=CMPLX(C+E,0.E0)
C 
C     * SET THE MEAN VALUE
C 
60    F(1)=CMPLX(0.E0,0.E0) 
      RETURN
      END 
