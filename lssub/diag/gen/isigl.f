      SUBROUTINE ISIGL(LSF,LSH,S,NLEV)
C 
C     * FEB  5/80 - J.D.HENDERSON 
C     * COMPUTES INTEGER LABEL VALUES OF SIGMA LEVELS.
C     * S = SIGMA VALUES OF FULL LEVELS.
C     * LSF = 1000*SIGMA ON FULL LEVELS.
C     * LSH = 1000*SIGMA ON HALF LEVELS.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL S(1) 
      INTEGER LSF(1),LSH(1) 
C-------------------------------------------------------------------- 
C     * FULL LEVELS.
C 
      DO 210 L=1,NLEV 
      LSF(L)=INT(1000.E0*S(L) + 0.5E0) 
  210 CONTINUE
C 
C     * HALF LEVELS.
C 
      NLEVM=NLEV-1
      DO 310 L=1,NLEVM
      SH=SQRT(S(L)*S(L+1))
      LSH(L)=INT(1000.E0*SH + 0.5E0) 
  310 CONTINUE
      LSH(NLEV)=INT(1000.E0*SQRT(S(NLEV)) + 0.5E0) 
C 
      RETURN
      END 
