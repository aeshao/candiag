      SUBROUTINE T2LBL (IBUFT, IBUF8W, LSR)

C     * JAN 03/97 - D. LIU (IBUF(7) DECODING FOR 'SPEC' CASE REVISED; LSR
C                           PASSED AS AN ARGUMENT)
C     * APR 16/96 - F. MAJAESS (ENSURE "SPEC" IS INITIALIZED)
C     * FEB 22/96 - D. LIU

C     * THIS SUBROUTINE CONVERTS A STANDARD TIME SERIES LABEL IBUFT
C     * INTO AN 8-WORD LABEL IBUF8W.
C
C     * STANDARD TIME SERIES LABEL IS DEFINED AS FOLLOWS:
C               IBUFT(1) = TIME
C               IBUFT(2) = LOCATION (IN LATLON FORMAT)
C               IBUFT(3) = NAME       
C               IBUFT(4) = LEVEL                   
C               IBUFT(5) = LENGTH OF SERIES
C               IBUFT(6) = 1
C               IBUFT(7) = DIMENSION OF ARRAY, KHEM AND DATA TYPE
C                          (IN THE FORMAT CCCRRRKQ  OR  LRLMTQ)
C               IBUFT(8) = PACKING DENSITY
C
C     WHEN CONVERTED, IBUF8W RETAINS THE INFORMATION OF NAME, LEVEL, AND
C     THE PACKING DENSITY OF IBUFT. OTHER INFORMATION IS OBTAINED BY 
C     UNDECODING IBUFT(7). IBUFT, HOWEVER, DOES NOT CONTAIN INFORMATION 
C     FOR IBUF8W(2). THIS SUBROUTINE SIMPLY ASSIGNS IBUFT(2) TO IBUF8W(2). 
C     IBUF8W(2) SHOULD BE RESET APPROPRIATELY OUTSIDE THIS SUBROUTINE.

      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER IBUFT(8), IBUF8W(8), LSR(2,1)
      LOGICAL SPEC

C     DATA NGRID/4HGRID/,NZONL/4HZONL/,
C    1     NSPEC/4HSPEC/,NSUBA/4HSUBA/

C------------------------------------------------------------------------

      NGRID=NC4TO8("GRID")
      NZONL=NC4TO8("ZONL")
      NSPEC=NC4TO8("SPEC")
      NSUBA=NC4TO8("SUBA")

      IF(IBUFT(1).NE.NC4TO8("TIME").AND.IBUFT(1).NE.NC4TO8("COEF")) THEN
         CALL                                      XIT('T2LBL',-1)
      ENDIF

C     DECODE IBUFT(7)

      IKIND=IBUFT(7)-(IBUFT(7)/10)*10
      IF(IKIND.LT.1.OR.(IKIND.GT.3.AND.IKIND.NE.9)) THEN
         WRITE(6,6010) IKIND
         CALL                                      XIT('T2LBL',-2)
      ENDIF
      SPEC = .FALSE.
      IF (IKIND.EQ.3) SPEC = .TRUE.

      IF (SPEC) THEN
          IBUF8W(1) = NSPEC
          LRLMT    = IBUFT(7)/10
          IF(LRLMT.LT.100) CALL FXLRLMT (LRLMT,IBUF8W(5),IBUF8W(6),0)
          CALL DIMGT(LSR,LA,LR,LM,KTR,LRLMT)
          IBUF8W(5) = LA
          IBUF8W(6) = 1
          IBUF8W(7) = LRLMT
      ELSE
          IF(IKIND.EQ.1) IBUF8W(1)=NGRID
          IF(IKIND.EQ.2) IBUF8W(1)=NZONL
          IF(IKIND.EQ.9) IBUF8W(1)=NSUBA
          IB       = IBUFT(7)/10
          ICCC     = IB/10000
          IRRR     = (IB-ICCC*10000)/10
          IHEM     = IB-ICCC*10000-IRRR*10
          IBUF8W(5) = ICCC
          IBUF8W(6) = IRRR
          IBUF8W(7) = IHEM
      ENDIF
C     * IBUFT(2)=POSITION INDEX; SHOULD RECONSTRUCT AS TIME STEP OUTSIDE
C     * THIS SUBROUTINE.

      IBUF8W(2)     = IBUFT(2)            
                                          
C     * NAME,LEVEL AND PACKING DENSITY UNCHANGED
                                          
      IBUF8W(3)     = IBUFT(3)       
      IBUF8W(4)     = IBUFT(4)            
      IBUF8W(8)     = IBUFT(8)           

C--------------------------------------------------------------------
 6010 FORMAT('0DATA TYPE IS ERRONEOUS AND IS (IKIND) ',I6)
      RETURN
      END
