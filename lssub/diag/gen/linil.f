      SUBROUTINE LINIL(YIN,XC,NX,Y,S,NN,DL,DR)
C
C     * MAY 09/96 - W.G.LEE (REVISE TO CHECK FOR MISSING DATA)
C
C     * FEB 22/78 - J.D.HENDERSON 
C     * PERFORMS LINEAR INTERPOLATION AT POINTS IN XC(NX) 
C     * OF THE VARIABLE CONTAINED IN Y(NN), KNOWN AT THE 
C     * NN COORDINATES CONTAINED IN S(NN).
C     * COORDINATES IN S MUST INCREASE TO THE RIGHT (IE 1 TO NN). 
C     * DL,DR ARE EXTRAPOLATION DERIVATIVES FOR LEFT,RIGHT SIDES. 
C 
C     * IF EITHER ENDPOINT HAS THE MISSING DATA VALUE "1.E38", SET
C     * THE INTERPOLATION RESULT TO THE MISSING DATA VALUE. ALSO,
C     * IF EITHER POINT USED FOR EXTRAPOLATION HAS THE MISSING
C     * DATA VALUE, SET THE RESULT TO THE MISSING DATA VALUE.
C 
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      REAL YIN(NX),XC(NX) 
      REAL S(NN),Y(NN)
C-------------------------------------------------------------------- 
C     * LOOP OVER ALL POINTS TO BE INTERPOLATED.
C 
      DO 300 NV=1,NX
      X=XC(NV)
C 
C     * EXTRAPOLATE TO THE LEFT (X.LT.S(1)).
C 
      IF(X.GE.S(1)) GO TO 80
      IF(Y(1).GE.1.E37) THEN
        VAL=1.0E38
      ELSE 
        VAL=Y(1)+DL*(X-S(1))
      ENDIF
      GO TO 290 
C 
C     * EXTRAPOLATE TO THE RIGHT (X.GT.S(NN)).
C 
   80 IF(X.LT.S(NN)) GO TO 90 
      IF(Y(NN).GE.1.E37) THEN
        VAL=1.0E38
      ELSE
        VAL=Y(NN)+DR*(X-S(NN))
      ENDIF
      GO TO 290 
C 
C     * INTERPOLATION.
C     * FIRST FIND WHICH INTERVAL TO USE. 
C 
   90 CONTINUE
      DO 110 I=2,NN 
      K=I-1 
      IF(X.LT.S(I)) GO TO 120 
  110 CONTINUE
C 
C     * NOW PERFORM THE LINEAR INTERPOLATION. 
C     * IF EITHER POINT USED FOR INTERPOLATION IS
C     * MISSING DATA, GIVE THE INTERPOLATED VALUE
C     * A MISSING DATA FLAG AS WELL.
C
  120 IF (Y(K).GE.1.E37) THEN
         VAL=1.0E38
      ELSE IF (Y(K+1).GE.1.E37) THEN
         VAL=1.0E38
      ELSE
         SLOPE=(Y(K+1)-Y(K))/(S(K+1)-S(K)) 
         VAL=Y(K)+SLOPE*(X-S(K)) 
      ENDIF
C
  290 YIN(NV)=VAL 
  300 CONTINUE
C 
      RETURN
      END 
