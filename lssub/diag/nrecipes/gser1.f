      SUBROUTINE GSER1(GAMSER,A,X,GLN)
C
C     * THIS SUBROUTINE IS TAKEN FROM NUMERICAL RECIPES.
C     * PAUSE STATEMENT IS REPLACED BY A CALL TO "XIT"
C     * SUBROUTINE.
C     * THE ORIGINAL NAME "GSER" IS CHANGED TO "GSER1".
C
C     JUL 25/96 - SLAVA KHARIN
C
C  (C) COPR. 1986-92 NUMERICAL RECIPES SOFTWARE #=!E=#,)]UBCJ.
C--------------------------------------------------------------------
C
      IMPLICIT REAL (A-H,O-Z),
     +INTEGER (I-N)
      INTEGER ITMAX
      REAL A,GAMSER,GLN,X,EPS
      PARAMETER (ITMAX=10000,EPS=3.E-7)
CU    USES GAMMLN1
      INTEGER N
      REAL AP,DEL,SUM,GAMMLN1
C--------------------------------------------------------------------
C
      GLN=GAMMLN1(A)
      IF(X.LE.0.E0)THEN
        IF(X.LT.0.E0) THEN
           WRITE(6,'(A)') 
     1          ' *** ERROR IN GSER1: X < 0.'
           CALL                                    XIT('GSER1',-1)
        ENDIF
        GAMSER=0.E0
        RETURN
      ENDIF
      AP=A
      SUM=1.E0/A
      DEL=SUM
      DO 10 N=1,ITMAX
        AP=AP+1.E0
        DEL=DEL*X/AP
        SUM=SUM+DEL
        IF(ABS(DEL).LT.ABS(SUM)*EPS) GOTO 100
  10  CONTINUE
C
      WRITE (6,'(A)') 
     1     ' *** ERROR IN GSER1: A TOO LARGE, ITMAX TOO SMALL.'
      CALL                                         XIT('GSER1',-2)
C
 100  GAMSER=SUM*EXP(-X+A*LOG(X)-GLN)
      RETURN
      END
