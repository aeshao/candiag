#deck plotspec2
jobname=plotspec2 ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#                  plotspec2           bd,gjb - apr/96
#   ---------------------------------- based on plotspec - add div, div ke
#                                      spectra and moisture interaction/flux
#                                      but plot integrated values only.


#   ---------------------------------- spectra plots.

      access spfile ${flabel}sp

      libncar plunit=$plunit
.     plotid.cdk

#   ---------------------------------- Part 1: basic spectra.
#                                      N.B.:  spvort,spdiv,sptemp,sphum
#                                      produce the data needed.

#   ---------------------------------- enstrophy.
      xfind spfile txtx
      xfind spfile txp2

      vpint  txtx  itxtx
      vpint  txp2  itxp2
      xlin   itxtx igs
      xlin   itxp2 igt
      add    igs   igt  ig
      spcsum igs   gs
      spcsum igt   gt
      spcsum ig    g
      rm txtx txp2 itxtx itxp2

#   ---------------------------------- compute reference slopes.

      spzdev ig  xg
      spamp  xg  amp
      cp     amp ampx
      cdiv   amp ampx one
#
      spcsum one   plus1
      fpow   plus1 minus1
      fpow   plus1 minus3
      rm  ig xg amp ampx one

      xlin    minus1 gref
      joinup  gfile  g gs gt gref
      crvplot gfile
      rm gfile g gs gt gref

#   ---------------------------------- rotational kinetic energy.

      splinv  igs  ikrs
      splinv  igt  ikrt
      add     ikrs ikrt ikr
      spcsum  ikrs krs
      spcsum  ikrt krt
      spcsum  ikr  kr
      rm igs igt ikrs ikrt ikr

      xlin    minus3 kref
      joinup  krfile kr krs krt kref
      crvplot krfile
      rm krfile kr krs krt kref

#   ---------------------------------- divergence spectrum.

      xfind spfile  txtx
      xfind spfile  txp2
#
      vpint  txtx itxtx
      vpint  txp2 itxp2
      xlin  itxtx ids
      xlin  itxp2 idt
      add ids idt id
      spcsum ids ds
      spcsum idt dt
      spcsum id  d
      rm txtx txp2 itxtx itxp2 id

      xlin    plus1  dref
      joinup  dfile d ds dt dref
      crvplot dfile
      rm dfile d ds dt dref

#   ---------------------------------- divergent kinetic energy.

      splinv  ids  ikds
      splinv  idt  ikdt
      add     ikds ikdt ikd
      spcsum  ikds kds
      spcsum  ikdt kdt
      spcsum  ikd  kd
      rm ids idt ikds ikdt ikd

      xlin    minus1 kdref
      joinup  kdfile kd kds kdt kdref
      crvplot kdfile
      rm kdfile kd kds kdt kdref

#   ---------------------------------- available potential energy.

      xfind spfile  txtx
      xfind spfile  txp2
      xfind spfile  gam

      xlin    gam  afact
      spzxmlt txtx afact as
      spzxmlt txp2 afact at
      rm txtx txp2 gam afact

      vpint   as   ias
      vpint   at   iat
      add     ias  iat   ia
      spcsum  ias  as1
      spcsum  iat  at1
      spcsum  ia   a1
      rm as at ias iat ia

      xlin    minus3 aref
      joinup  afile a1 as1 at1 aref
      crvplot afile
      rm afile a1 as1 at1 aref

#   ---------------------------------- moisture spectrum.

      xfind spfile  txtx
      xfind spfile  txp2

      vpint txtx  itxtx
      vpint txp2  itxp2
      xlin itxtx  iqs
      xlin itxp2  iqt
      add iqs iqt iq
      spcsum iqs  qs
      spcsum iqt  qt
      spcsum iq   q
      rm txtx txp2 itxtx itxp2 iqs iqt iq

      xlin    minus1 qref
      joinup  qfile  q qs qt qref
      crvplot qfile
      rm qfile  q qs qt qref

#   ---------------------------------- Part 2: interaction terms.
#                                      N.B.:  Decks spj, spm, spl
#                                      produce the data needed and use
#                                      the rotational wind Vr only.

#   ---------------------------------- enstrophy term J and flux H.

      xfind   spfile jr
      vpint   jr  ijr
      spcsum  ijr j
      spflux  ijr h
      crvplot j
      crvplot h

#   ---------------------------------- rotational ke term I and flux F.

      splinv  ijr iir
      spcsum  iir i
      spflux  iir f
      crvplot i
      crvplot f
      rm  jr ijr j h iir i f

#   ---------------------------------- ape term M and flux  P.

      xfind   spfile mr
      vpint   mr  imr
      spcsum  imr m
      spflux  imr p
      crvplot m
      crvplot p
      rm mr imr m p

#   ---------------------------------- moisture term L and flux R

      xfind   spfile lr
      vpint   lr  ilr
      spcsum  ilr l
      spflux  ilr r
      crvplot l
      crvplot r
      rm lr ilr l r

.     plotid.cdk
.     plot.cdk

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
0 PLOTSPEC2----------------------------------------------------------- PLOTSPEC2
. headfram.cdk
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        PBPB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        P"P"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED STATIONARY ENSTROPHY. SEC-2 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED TRANSIENT  ENSTROPHY. SEC-2 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.             INTEGRATED ENSTROPHY. SEC-2 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.             SUM UP ONES.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FPOW.           -1.0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FPOW.           -3.0
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.E-6
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1    1.E-10     1.E-6    1  100    4    4    1    1
  WAVE NUMBER N.    ENSTROPHY (KG/M2.S2)  RUN $run  DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED STATIONARY  ROT KE. UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED TRANSIENT ROT  KE. UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED  ROT KE.  UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.E+8
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1      1.E1      1.E6    1  100    4    4    1    1
  WAVE NUMBER N.     ROT KE  (J/M2)        RUN $run  DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        DBDB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        D"D"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED STATIONARY DIVERGENCE SPECT SEC-2 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED TRANSIENT  DIVERGENCE SPECT  SEC-2 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.             INTEGRATED DIVERGENCE SPECT  SEC-2 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.E-9
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1    1.E-11     1.E-6    1  100    4    4    1    1
  WAVE NUMBER N.    DIVERGENCE (KG/M2S2)  RUN $run  DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED STATIONARY  DIV KE. UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED TRANSIENT DIV  KE. UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED  DIV KE.  UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.E+4
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1      1.E1      1.E6    1  100    4    4    1    1
  WAVE NUMBER N.     DIV KE  (J/M2)       RUN $run  DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        TBTB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        T"T"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        (GAM)
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.         5.02E2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED STATIONARY APE. UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED TRANSIENT  APE. UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED APE.  UNITS J/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.E+8
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1      1.E1      1.E7    1  100    4    4    1    1
  WAVE NUMBER N.     APE  (J/M2)        RUN $run  DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        QBQB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        Q"Q"
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.            0.5
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED STATIONARY MOISTURE SPECTRUM. (G/G).KG/M2
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED TRANSIENT  MOISTURE SPECTRUM. (G/G).KG/M2
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.             INTEGRATED MOISTURE SPECTRUM. (G/G).KG/M2
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XLIN.          1.E-2
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1    1.0E-6    1.0E-1    1  100    4    4    1    1
  WAVE NUMBER N.    MOISTURE (G/G)KG/M2  RUN $run  DAYS $days
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        J ND
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED ENSTROPHY TRANSFER J ND. SEC-3 KG/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPFLUX.       N
  RUN $run.  DAYS $days.  INTEGRATED ENSTROPHY ND FLUX TERM.   SEC-3 KG/M2.
SPFLUX.           -1 NEXT   -1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1   -1.E-12    1.E-12    0  100    1    1    1  1 1
  WAVE NUMBER N.     ENSTROPHY TERM J.   RUN $run  DAYS $days (SEC-3 KG/M2)
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1   -2.E-12   10.E-12    0  100    1    1    1  1 1
  WAVE NUMBER N.     ENSTR0PHY FLUX H    RUN $run  DAYS $days (SEC-3 KG/M2)
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED ROT. KE TRANSFER I ND.    W/M2
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPFLUX.       N
  RUN $run.  DAYS $days.  INTEGRATED ROT. KE ND FLUX TERM.     W/M2
SPFLUX.           -1 NEXT   -1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1      -.20       .50   0   100    1    1    1  1 1
  WAVE NUMBER N.     ROT KE TERM I.      RUN $run  DAYS $days (W/M2)
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1       -1.       0.4    0  100    1    1    1  1 1
  WAVE NUMBER N.     ROT KE FLUX F       RUN $run  DAYS $days (W/M2)
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        M ND
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       N
  RUN $run.  DAYS $days.  INTEGRATED ND APE TRANSFER TERM M.  W/M2.
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPFLUX.       N
  RUN $run.  DAYS $days.  INTEGRATED ND APE FLUX.  W/M2.
SPFLUX.           -1 NEXT   -1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1      -2.5        .5    0  100    1    1    1  1 1
  WAVE NUMBER N.     APE TERM M          RUN $run  DAYS $days (W/M2)
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1       -.5       2.5    0  100    1    1    1  1 1
  WAVE NUMBER N.     APE FLUX P          RUN $run  DAYS $days (W/M2
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XFIND.        L ND
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPCSUM.       L
 RUN $run. DAYS $days. INTEGRATED ND  MOISTROPHY TRANSFER TERM L. (G/G).KG/M2.S
SPCSUM.           -1 NEXT   -1    0    0    1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SPFLUX.       N
RUN $run. DAYS $days.  INTEGRATED ND  MOISTROPHY FLUX TERM R. (G/G).KG/M2.S
SPFLUX.           -1 NEXT   -1
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1   -2.5E-8     5.E-9    0  100    1    1    1  1 1
  WAVE NUMBER N.     MOISTURE TERM L  RUN $run  DAYS $days (G/G).KG/M2.S
                                       0    $lrt
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
CRVPLOT.          -1 NEXT   -1    -5.E-9     2.E-8    0  100    1    1    1  1 1
  WAVE NUMBER N.     MOISTROPHY FLUX R  RUN $run  DAYS $days (G/G).KG/M2.S
                                       0    $lrt
. tailfram.cdk
end_of_data

. endjcl.cdk
