#comdeck multisub
#  
# Revised to check for and apply gcmjcl "openmp" setting if not set in "gcmsub".
#                  multisub            BD,FM - Jun 07/06 - FM ----------multisub
#  ----------------------------------- Script used to handle multitasking 
#                                      dependent code preprocessing.
#
#  Setup "nproc" based on "nproc_a" and "oproc_o",
#    and "nnode" based on "nnode_a" and "nnode_o".

if [ -z "$openmp" -o -z "$float1" -o -z "$phys_node" ] ; then
 if [ -s condef.in ] ; then
  (\rm -f float1_openmp_captur || : )
  cat condef.in | sed -n -e '/^ *$/d' -e '/^ *#/d' -e '/openmp=/p' -e '/float1=/p' -e '/phys_node=/p' > float1_openmp_captur
  if [ -s float1_openmp_captur ] ; then
   . float1_openmp_captur
   (\rm -f float1_openmp_captur || : )
  fi
 fi
fi
nproc_a=${nproc_a:=1}
nproc_o=${nproc_o:=1}
nproc_c=${nproc_c:=1}
nnode_a=${nnode_a:=0}
nnode_o=${nnode_o:=0}
nnode_c=${nnode_c:=0}
if [ "$nproc_o" -gt "$nproc_a" ] ; then
 nproc=${nproc=$nproc_o}
else
 nproc=${nproc=$nproc_a}
fi
if [ "$nnode_a" -gt 0 -o "$nnode_o" -gt 0 ] ; then
 nnode_def=$nnode
 nnode=`expr $nnode_a + $nnode_o`
 nnode=`expr $nnode + $nnode_c`
 [ -n "$nnode_def" ] && nnode=$nnode_def
else
 nnode=${nnode:=0}
fi
if [ "${nnode}" -gt 1 ] ; then
 if [ -n "${phys_node}" -a "${phys_node}" -gt "${nnode}" ] ; then
  echo "Invalid setting of phys_node=${phys_node} and/or "
  echo "                   nnode_a=${nnode_a}, nnode_o=${nnode_o} !"
  touch haltit
  echo "Invalid setting of phys_node=${phys_node} and/or "           >> haltit
  echo "                   nnode_a=${nnode_a}, nnode_o=${nnode_o} !" >> haltit
  exit 1
 fi
fi

#  If "coupled" is undefined, check and honour "gcmjcl" condef section's 
#  settings if "coupled/slab" are defined.

if [ -z "$coupled" ] ; then
 if [ -s condef.in ] ; then
  (\rm -f coupled_slab_captur || : )
  cat condef.in | sed -n -e '/^ *$/d' -e '/^ *#/d' -e '/coupled=/p' -e '/slab=/p' > coupled_slab_captur
  if [ -s coupled_slab_captur ] ; then
   . coupled_slab_captur
   (\rm -f coupled_slab_captur || : )
  fi
 fi
fi

## if [ "$multi"  = on ] ; then
## # ------------------------------------ Multi tasking variables checks.
## #
##   echo ' ----------------------------------------------------- '
##   echo ' ---- Pre-processing for multi-tasking (multi=on). --- '
##   echo ' ----------------------------------------------------- '
##   echo
## #
##   nproc=`echo $nproc | sed -n -e '/^ *[0-9]*$/p'`
##   if [ -z "$nproc" ] ; then
##      echo ' ******************************************************** '
##      echo ' *  ERROR: The variable for the number of processor(s)  * '
##      echo ' *         (nproc) must be specified (with a valid      * '
##      echo ' *         number) when running in multi-tasking mode   * '
##      echo ' *         (multi=on)                                   * '
##      echo ' ******************************************************** '
##      echo
##      exit 1
##   fi
##   
##   #                                    - Ensure that "multi" and "nproc"  are
##   #                                      passed to the "gcmjcl" job.
##   
##   cat >> condef.in <<..end_of_cat
## multi=$multi
## nproc_a=$nproc_a
## nproc_o=$nproc_o
## nproc=$nproc
## ..end_of_cat
## 
##   #                                    - Set the condef parameter %DF MULTI "ON" at
##   #                                      the beginning of the update model section.
## 
##   cat >  update_model_tempo<<..end_of_cat
## %DF MULTI
## ..end_of_cat
## 
##   cat update_model.in >> update_model_tempo
##   rm  update_model.in
##   mv  update_model_tempo  update_model.in
## 
## 
##   #                                    - Set the condef parameter %DF MULTI "ON" at
##   #                                      the beginning of the update ocean section.
## 
##   if [ -s update_ocean.in ] ; then
## 
##    cat >  update_ocean_tempo<<..end_of_cat
## %DF MULTI
## ..end_of_cat
## 
##    cat update_ocean.in >> update_ocean_tempo
##    rm  update_ocean.in
##    mv  update_ocean_tempo  update_ocean.in
## 
##   fi
## elif [ "$openmp"  = on ] ; then
if [ "$openmp"  = on ] ; then
# ------------------------------------ Openmp and possibly MPI case
#
# ------------------------------------ Openmp variable check.
#
  echo ' ----------------------------------------------------- '
  if [ "$float1" = 'on' ] ; then
   echo ' ---- Pre-processing for openmp/float1 mode setup. --- '
  else
   echo ' ---- Pre-processing for openmp setup (openmp=on). --- '
  fi
  echo ' ----------------------------------------------------- '
  echo
#
  nproc=`echo $nproc | sed -n -e '/^ *[0-9]*$/p'`
  if [ -z "$nproc" ] ; then
     echo ' ******************************************************** '
     echo ' *  ERROR: The variable for the number of processor(s)  * '
     echo ' *         (nproc) must be specified (with a valid      * '
     echo ' *         number) when running in openmp mode          * '
     echo ' *         (openmp=on)                                  * '
     echo ' ******************************************************** '
     echo
     exit 1
  fi
  
  #                                    - Ensure that "openmp","nproc" and
  #                                      possibly "nnode" and "geometry"
  #                                      are passed to the "gcmjcl" job.
  
  touch condef.in
  echo "openmp=$openmp" >> condef.in
  if [ "$float1" = 'on' ] ; then
    echo "float1=$float1" >> condef.in
  fi
  echo "nproc_a=$nproc_a" >> condef.in
  echo "nproc_o=$nproc_o" >> condef.in
  echo "nproc=$nproc" >> condef.in
  if [ "$nnode" -gt 1 ] ; then
   echo "nnode_a=$nnode_a" >> condef.in
   echo "nnode_o=$nnode_o" >> condef.in
   echo "nnode_c=$nnode_c" >> condef.in
   echo "nnode=$nnode" >> condef.in
   if [ -n "$geometry" ] ; then
    echo "geometry='$geometry'" >> condef.in
   fi
   if [ -n "$phys_node" ] ; then
     echo "phys_node=$phys_node" >> condef.in
   fi
  fi

  #                                    - Set the condef parameter %DF OPENMP "ON" at
  #                                      the beginning of the update model section.

  touch update_model_tempo
  echo '%DF OPENMP' >> update_model_tempo
  if [ "$float1" = 'on' ] ; then
   echo '%DF FLOAT1' >> update_model_tempo
  fi
  if [ "$nnode_a" -gt 1 ] ; then
   echo '%DF MPI' >> update_model_tempo
   if [ "$parallel_io" = 'on' ] ; then
    echo '%DF PARALLEL_IO' >> update_model_tempo
    echo "parallel_io=on" >> condef.in
   fi
  fi
  cat update_model.in >> update_model_tempo
  rm  update_model.in
  mv  update_model_tempo  update_model.in


  #                                    - Set the condef parameter %DF OPENMP "ON" at
  #                                      the beginning of the update ocean section.

  if [ -s update_ocean.in ] ; then

   touch update_ocean_tempo
   echo '%DF OPENMP' >> update_ocean_tempo
   if [ "$float1" = 'on' ] ; then
    echo '%DF FLOAT1' >> update_ocean_tempo
   fi
   if [ "$nnode_o" -gt 1 ] ; then
    echo '%DF MPI' >> update_ocean_tempo
   fi

   cat update_ocean.in >> update_ocean_tempo
   rm  update_ocean.in
   mv  update_ocean_tempo  update_ocean.in

  fi

elif [ "$nnode" -gt 1 ] ; then
# ------------------------------------ MPI and No Openmp case
#
  echo ' ------------------------------------- '
  echo ' --- Pre-processing for MPI setup. --- '
  echo ' ------------------------------------- '
  echo
#
  nproc=`echo $nproc | sed -n -e '/^ *[0-9]*$/p'`
  if [ -z "$nproc" ] ; then
     echo ' ******************************************************** '
     echo ' *  ERROR: The variable for the number of processor(s)  * '
     echo ' *         (nproc) must be specified (with a valid      * '
     echo ' *         number) when running in MPI mode with        * '
     echo ' *         nnode=${nnode}.                              * '
     echo ' ******************************************************** '
     echo
     exit 1
  fi
  
  #                                    - Ensure that "nnode","nproc" and
  #                                      possibly "geometry" are passed 
  #                                      to the "gcmjcl" job.
  
  touch condef.in
  if [ "$float1" = 'on' ] ; then
    echo "float1=$float1" >> condef.in
  fi
  echo "nproc_a=$nproc_a" >> condef.in
  echo "nproc_o=$nproc_o" >> condef.in
  echo "nproc_c=$nproc_c" >> condef.in
  echo "nproc=$nproc" >> condef.in
  echo "nnode_a=$nnode_a" >> condef.in
  echo "nnode_o=$nnode_o" >> condef.in
  echo "nnode_c=$nnode_c" >> condef.in
  echo "nnode=$nnode" >> condef.in
  if [ -n "$geometry" ] ; then
   echo "geometry='$geometry'" >> condef.in
  fi
  if [ -n "$phys_node" ] ; then
   echo "phys_node=$phys_node" >> condef.in
  fi

  #                                    - Set the condef parameter %DF MPI "ON" at
  #                                      the beginning of the update model section.

  touch update_model_tempo
  if [ "$float1" = 'on' ] ; then
   echo '%DF FLOAT1' >> update_model_tempo
  fi
  if [ "$nnode_a" -gt 1 ] ; then
   echo '%DF MPI' >> update_model_tempo
   if [ "$parallel_io" = 'on' ] ; then
    echo '%DF PARALLEL_IO' >> update_model_tempo
    echo "parallel_io=on" >> condef.in
   fi
  fi
  cat update_model.in >> update_model_tempo
  rm  update_model.in
  mv  update_model_tempo  update_model.in


  #                                    - Set the condef parameter %DF MPI "ON" at
  #                                      the beginning of the update ocean section.

  if [ -s update_ocean.in ] ; then

   touch update_ocean_tempo
   if [ "$float1" = 'on' ] ; then
    echo '%DF FLOAT1' >> update_ocean_tempo
   fi
   if [ "$nnode_o" -gt 1 ] ; then
    echo '%DF MPI' >> update_ocean_tempo
   fi

   cat update_ocean.in >> update_ocean_tempo
   rm  update_ocean.in
   mv  update_ocean_tempo  update_ocean.in

  fi

elif [ "float1" = 'on' ] ; then

# ------------------------------------ FLOAT1 mode case.
#
  echo ' ---------------------------------------- '
  echo ' --- Pre-processing for FLOAT1 setup. --- '
  echo ' ---------------------------------------- '
  echo
#

  #                                    - Set the condef parameter %DF FLOAT1 "ON" at
  #                                      the beginning of the update model section.

  touch condef.in
  if [ "$float1" = 'on' ] ; then
    echo "float1=$float1" >> condef.in
  fi
  touch update_model_tempo
  if [ "$float1" = 'on' ] ; then
   echo '%DF FLOAT1' >> update_model_tempo
  fi
  cat update_model.in >> update_model_tempo
  rm  update_model.in
  mv  update_model_tempo  update_model.in


  #                                    - Set the condef parameter %DF FLOAT1 "ON" at
  #                                      the beginning of the update ocean section.

  if [ -s update_ocean.in ] ; then

   touch update_ocean_tempo
   if [ "$float1" = 'on' ] ; then
    echo '%DF FLOAT1' >> update_ocean_tempo
   fi

   cat update_ocean.in >> update_ocean_tempo
   rm  update_ocean.in
   mv  update_ocean_tempo  update_ocean.in

  fi

fi

touch condef.in

use_cancpl=${use_cancpl:=off}
use_nemo=${use_nemo:=off}
nemo_repo=${nemo_repo:=''}
nemo_ver=${nemo_ver:=''}
nemo_exec=${nemo_exec:=''}
[ "x$use_cancpl" = "xon" ] && echo "use_cancpl=$use_cancpl" >> condef.in
[ "x$use_nemo" = "xon" ] && echo "use_nemo=$use_nemo" >> condef.in
[ -n "$nemo_repo" ] && echo "nemo_repo=$nemo_repo" >> condef.in
[ -n "$nemo_ver" ]  && echo "nemo_ver=$nemo_ver" >> condef.in
[ -n "$nemo_exec" ] && echo "nemo_exec=$nemo_exec" >> condef.in

# if [ "$f90x4" = on ] ; then 
# echo 'f90x4=on' >> condef.in
# fi

#  Adjust condef.in/update_model.in/update_ocean.in 
#  for coupled/slab settings.

if [ "$coupled" = 'on' ] ; then
 echo 'coupled=on' >> condef.in
 touch update_model_tempo
 echo '%DF COUPLED' >> update_model_tempo
 if [ -s update_ocean.in ] ; then
  touch update_ocean_tempo 
  echo '%DF COUPLED' >> update_ocean_tempo 
 fi

 if [ "$slab" = 'on' ] ; then
  echo 'slab=on' >> condef.in
  echo '%DF SLAB' >> update_model_tempo
  if [ -s update_ocean.in ] ; then
   echo '%DF SLAB' >> update_ocean_tempo
  fi
 fi

 cat update_model.in >> update_model_tempo
 rm  update_model.in
 mv  update_model_tempo  update_model.in

 if [ -s update_ocean.in ] ; then
  cat update_ocean.in >> update_ocean_tempo
  rm  update_ocean.in
  mv  update_ocean_tempo  update_ocean.in
 fi

fi 
