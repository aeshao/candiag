#!/bin/bash
# Convert canesm timeseries data into 'cmorized' netcdf data

#=====================
# Define bail function
#=====================
  # bail is a simple error exit routine
  bail_prefix="NETCDF CONV"
  bail(){
    echo `date`" $this_host $runid --- ${bail_prefix}: $*"
    exit 1
  }

#==================================================
# setup working directory, and setup conversion env
#==================================================
  # activate pycmor environment
  source activate ${pycmor_env}

  # get NCCONV directory
  NCCONV_DIR=${CCRNSRC}/ncconv

  # If specified by the user, checkout a specific branch/commit of pycmor,
  # else leave it as the set commit
  [[ -n $ncconv_cmmt ]] && $(cd $NCCONV_DIR && git checkout $ncconv_cmmt)

  # setup directory
  ${NCCONV_DIR}/pycmor/config-pycmor 

  # source additional pycmor settings (version strings/table lists)
  . ${NCCONV_DIR}/pycmor/pycmor.cfg

  # navigate into working directory and setup additional path considerations
  WRK_DIR=$(pwd)/netcdfconv
  cd $WRK_DIR
  . path_export.sh

#===========================================================
# set entry in experiments table, and alter CVs, if needed 
#===========================================================
  ENTRY_EXISTS=0
  EXPTAB_DIR=$(dirname $ncconv_exptab)
  EXPTAB_FILENAME=$(basename $ncconv_exptab)

  # clone experiment table repo, if necessary
  [[ -d $EXPTAB_DIR ]] || git clone --branch master --single-branch $ncconv_exptab_repo $EXPTAB_DIR 
  cd $EXPTAB_DIR

  # check if there is an entry for this runid
  runids=$(awk '{print $2}' $EXPTAB_FILENAME)
  RUNID_PAT='\b'$runid'\b'
  [[ $runids =~ $RUNID_PAT ]] && ENTRY_EXISTS=1

  # if not, check that meta data jives with CVs, adding to them if necessary, 
  #     and add run entry into experiment table
  if (( ENTRY_EXISTS == 0 )) ; then
    #-------------------------------
    # check for necessary meta data
    #-------------------------------

    # set overrides
    [[ -n $override_activity_id ]]      && activity_id=$override_activity_id
    [[ -n $override_experiment_id ]]    && experiment_id=$override_experiment_id
    [[ -n $override_subexperiment_id ]] && subexperiment_id=$override_subexperiment_id

    # check that all necessary fields are set
    [[ -z $variant_label ]]         && bail "MODEL VARIANT LABEL (rip number) MUST BE SET!"
    [[ -z $source_id ]]             && bail "SOURCE ID MUST BE SET!"
    [[ -z $activity_id ]]           && bail "ACTIVITY ID MUST BE SET!"
    [[ -z $experiment_id ]]         && bail "EXPERIMENT ID MUST BE SET!"
    [[ -z $parent_runid ]]          && bail "PARENT RUNID MUST BE SET!"
    [[ -z $parent_branch_time ]]    && bail "PARENT BRANCH TIME MUST BE SET!"

    # if subexperiment isn't set, set it to none
    [[ -z $subexperiment_id ]] && subexperiment_id="none"

    #------------
    # handle CVs
    #------------
    # define location of the files containing CCCma variable, source ids, and experiment ids
    suppl_json_pth=${CCRNSRC}/CanESM/CONFIG/COMMON/ncconv_cccma_json_files
    CMOR_CV_FILE=${WRK_DIR}/cmip6-cmor-tables/Tables/CMIP6_CV.json 

    # check for source id and experiment id
    check_CVs -q --src $source_id --CV $CMOR_CV_FILE || source_not_present=1 
    check_CVs -q --exp $experiment_id --CV $CMOR_CV_FILE || experiment_not_present=1

    if (( CANESM_CMIP == 1 )) ; then
      # source id and experiment id must be present
      (( source_not_present == 1 ))        && bail 'CANESM_CMIP == 1: $source_id MUST BE INCLUDED IN THE OFFICIAL CMOR CVs!'
      (( experiment_not_present == 1 ))    && bail 'CANESM_CMIP == 1: $experiment_id MUST BE INCLUDED IN THE OFFICIAL CMOR CVs!'
    else
      # the combination of source id and experiment id must be unique
      if ! (( source_not_present == 1 || experiment_not_present == 1 )) ; then 
        bail_msg='CANESM_CMIP == 0: either $source_id or $experiment_id must NOT be an entry in the official CMOR CVs!'
        bail_msg="${bail_msg} This is necessary to create unique nc files names unique to those produced from OFFICIAL runs!"
        bail "$bail_msg"
      fi

      # switch to development CVs
      $(cd ${WRK_DIR}/cmip6-cmor-tables ; git checkout cccma >> /dev/null)

      # add CCCma source and experiment ids to CV
      src_id_file=${suppl_json_pth}/cccma_source_ids.json
      exp_id_file=${suppl_json_pth}/cccma_experiments.json
      alter_cmor_tabs add2cv --src $src_id_file --CV $CMOR_CV_FILE || 
        bail "Failed to add CCCma source information in $src_id_file to cmor CVs! Is the json syntax correct?"
      alter_cmor_tabs add2cv --exp $exp_id_file --CV $CMOR_CV_FILE || 
        bail "Failed to add CCCma experiment information in $exp_id_file to cmor CVs! Is the json syntax correct?"

      # confirm that users source id and experiment id are NOW both contained in the CVs
      if (( source_not_present == 1 )) ; then
        # check if the given source id has the runid appended to it, i.e. CanESM5-$runid
        if [[ "$source_id" == *"-${runid}" ]] ; then
          # check if the "true source id" is contained in the CVs, and copy its entry if so
          true_source_id=${source_id%-${runid}}
          check_CVs -q --src $true_source_id --CV $CMOR_CV_FILE && alter_cmor_tabs cpCVent -s $source_id $true_source_id --CV $CMOR_CV_FILE 
        fi

        # check the updated CVs
        check_CVs -q --src $source_id --CV $CMOR_CV_FILE || source_still_not_present=1 
        if (( source_still_not_present == 1 )) ; then
          bail_msg="Unsupported source_id ---> ${source_id} <--- ! source_id must be contained in $src_id_file" 
          bail_msg="${bail_msg} or be one of CanESM5-runid, or CanESM5-CanOE-runid. Note that if you provided"
          bail_msg="${bail_msg} a unique experiment_id, then CanESM5 or CanESM5-CanOE are also valid."
          bail "$bail_msg"
        fi 
      fi
      if (( experiment_not_present == 1 )) ; then
        # check the updated CVs
        check_CVs -q --exp $experiment_id --CV $CMOR_CV_FILE || experiment_still_not_present=1 
        if (( experiment_still_not_present == 1 )) ; then
          bail_msg="Your given experiment_id --> ${experiment_id} <--- is not contained in $exp_id_file!"
          bail_msg="${bail_msg} Add an entry to the json file to allow for netcdf conversion!"
          bail "$bail_msg"
        fi
      fi
    fi

    # bail if user didn't pick supported activity or subexperiment, regardless if this is a CMIP run or not
    check_CVs -q --act $activity_id --CV $CMOR_CV_FILE || bail "$activity_id is not a supported activity_id! See 'canesm.cfg'"
    check_CVs -q --subexp $subexperiment_id --CV $CMOR_CV_FILE || bail "$subexperiment_id is not a supported subexperiment_id! See 'canesm.cfg'"

    #----------------------------------
    # determine branch time information
    #----------------------------------
    # BTIP => branch time in parent
    # BTIC => branch time in child

    # branch_time_in_parent
    BTIP_yr=${parent_branch_time%_*}
    BTIP_mnth=${parent_branch_time#*_m}
    BTIP_mnth=${BTIP_mnth##+(0)} # remove leading zeros to allow for easy integer addition         
    BTIP_yr=${BTIP_yr##+(0)}
    if (( BTIP_mnth == 12 )) ; then
      # roll over to first day of next year
      tmp_yr=$((BTIP_yr+1))
      tmp_yr=$(printf '%04d' "$tmp_yr")       # pad to four digits
      branch_time_in_parent=${tmp_yr}:01:01:00
    else
      # roll over to next month
      tmp_yr=$(printf '%04d' "$BTIP_yr")      # pad to four digits
      tmp_mnth=$((BTIP_mnth+1))
      tmp_mnth=$(printf '%02d' "$tmp_mnth")   # pad to two digits
      branch_time_in_parent=${tmp_yr}:${tmp_mnth}:01:00
    fi

    # branch_time_in_child
    BTIC_yr=${run_start_time%:*}
    BTIC_yr=$(printf '%04d' "$BTIC_yr")         # pad to four digits
    # check if the user gave a month
    if [[ $run_start_time == *:* ]]; then
        BTIC_mnth=${run_start_time#*:}
        BTIC_mnth=$(printf '%02d' "$BTIC_mnth") # pad to two digits
    else
      # run mustve started from january
      BTIC_mnth=01
    fi
    branch_time_in_child=${BTIC_yr}:${BTIC_mnth}:01:00

    #------------------------------------------
    # build experiment table entry and enter it
    #------------------------------------------
    exptab_entry="| $runid | $activity_id | $source_id | $experiment_id | $subexperiment_id "
    exptab_entry=${exptab_entry}"| $variant_label | | $parent_runid | $branch_time_in_parent "
    exptab_entry=${exptab_entry}"| $branch_time_in_child | ${run_start_time/:/m}:${run_stop_time/:/m} | $USER | hall${HALLN} | | |"

    # create new branch
    git checkout -b $runid
      
    # insert entry into experiment table
    echo $exptab_entry >> $EXPTAB_FILENAME

    # add/commit
    git add $EXPTAB_FILENAME
    git commit -m "added experiment entry for $runid in $EXPTAB_FILENAME"

    #-----------------------------------------------------------------
    # if desired, add cccma specific variables to the variable tables
    #-----------------------------------------------------------------
    cccma_var_file="$suppl_json_pth/cccma_vars.json"
    if (( add_cccma_vars == 1 )) ; then
      # if the given source_id is just an official source id with the runid attached, alter the "true source"
      if [[ -n "$true_source_id" ]] ; then
        vartab_source_id=$true_source_id
      else
        vartab_source_id=$source_id
      fi
      alter_cmor_tabs add2vartabs $cccma_var_file $vartab_source_id -c $NCCONV_DIR/tables/variable_tables -C ${WRK_DIR}/cmip6-cmor-tables/Tables ||
        bail "Failed to add CCCma variables in $cccma_var_file to the CMOR tables! Is the json syntax correct?"

      # if this is an "official" run, commit changes in the variable table so pycmor can be ran with the '--prod' flag
      if (( CANESM_CMIP == 1 )) ; then
        # update cmor table submodule
        cd $NCCONV_DIR/cmor_tools/cmip6-cmor-tables
        git checkout -b prod-${runid}
        git add Tables/*
        git commit -m "added CCCma variables to variable tables"

        # update main repo
        cd $NCCONV_DIR
        git checkout -b prod-${runid}
        git add tables/variable_tables/*
        git add cmor_tools/cmip6-cmor-tables
        git commit -m "added CCCma variables to variable tables"
      fi
    fi
  fi
  cd $WRK_DIR

#=================
#   run pycmor
#=================
  # check if user provided a list of tables to convert
  if [[ -n $ncconv_tabs2conv ]]; then
    # use user defined list
    tabs2conv="$ncconv_tabs2conv"
  else
    # use default cmorlist from pycmor.cfg
    tabs2conv="$cmorlist"
  fi
  
  # build argument list
  if (( CANESM_CMIP == 1 )) ; then
    pycmor_opts="--prod"
  fi
  pycmor_opts="${pycmor_opts} -L --exptable $ncconv_exptab -r $runid"
  pycmor_opts="${pycmor_opts} -d $NCCONV_DIR/tables/variable_tables -k ${year}01_${year}12" 
  pycmor_opts="${pycmor_opts} -v $version -n $newversion"

  # call pycmor
  # Note: the table list must be passed as a space delimited list, and thus must be within ""
  time pycmor.py $pycmor_opts -t "$tabs2conv" >> conv.log 2>&1

  # confirm that at least some netcdf files were produced
  if [[ ! -d "output" ]] ; then
    bail_msg="pycmor failed to produce ANY output! Are you sure the timeseries files are on disk?"
    bail_msg="${bail_msg} Set debug=on and check the logs produced in the conversion working directory"
    bail_msg=" for additional information"
    bail $bail_msg
  fi

#====================
#   handle output
#====================

  #---------------------------------
  # dump output to tape, if desired
  #---------------------------------
  if (( with_dump_nc == 1 )) ; then
    # bundle driver logs 
    mkdir pycmor_driver_logs
    mv pycmor-driver-log_* pycmor_driver_logs/

    # build file list
    files2arch="conv.log output cmor_logs pycmor_logs pycmor_driver_logs"

    # set archive project
    if [[ $shortermdir_tser == "on" ]] ; then
      hpcarchive_project=crd_short_term
    else
      hpcarchive_project=crd_cccma
    fi

    # archive
    datestamp=$(date -u +%Y%j%H%M)
    hpcarchive -g -p $hpcarchive_project -a $files2arch -c "nc_${runid}_${year}01_${year}12_${datestamp}"
  fi

  #------------------------------------------------------
  # store output (or subset) to users RUNPATH, if desired
  #------------------------------------------------------
  if (( keep_nc_on_disk == 1 )) ; then
    if [[ -n $keep_vartab_pairs ]] ; then 
      # only store user defined subset of data, so delete those that aren't desired
      
      # create regex pattern for matching all the given var/table pairs
      for vartab in $keep_vartab_pairs; do
        file_prefix=${vartab/:/_}
        PATTERN="${PATTERN} .*${file_prefix}.*"
      done
      # remove leading whitespace
      PATTERN=$(echo $PATTERN | sed 's/^ *//')

      # replace spaces between pattern list with 'or' syntax '\|'
      PATTERN=${PATTERN/\* \./\*\\|\.}

      # delete files that don't match the given pattern
      find output/ -type f -not -regex "$PATTERN" -delete

      # delete the resulting empty directories
      find output/ -type d -empty -delete
    fi

    rsync -avz output/ ${RUNPATH}/nc_output
  fi
