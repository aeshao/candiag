#deck radiag3
jobname=radia ; time=$stime ; memory=$memory1
. comjcl.cdk

cat > Execute_Script <<end_of_script

#   remove use of condef parameter ggsave; remove use of zxlook.
#   Use 1365 W/M2 for solar constant, instead of 1370.
#                  radiag3             E. Chan., ec  oct 3/96 - dl.
#   ---------------------------------- Compute radiation balance in gcm.
#                                      Based on deck radiag2 except to be
#                                      used with gcm7 onward which saves
#                                      fss  fsd  fsv  flg  and cldt  total
#                                      accumulated overlapped cloud.
#                                      The calculation of the surface
#                                      albedo  salb  is now done using the
#                                      accumulated variables fss and fsg.
#
#
.     ggfiles.cdk
#
#   *******************************************************************
#   ************************* solar radiation *************************
#   *******************************************************************
#
#   ---------------------------------- compute solar flux incident
#                                      on top of atmosphere.
ccc   select  npakgg fsg
      solflx2 fsg    fsi
      timavg  fsi    tfsi

#   ---------------------------------- solar flux absorbed by atmosphere.
ccc   select  npakgg fsa
      timavg  fsa    tfsa

#   ---------------------------------- solar heat absorbed by ground.
      timavg  fsg    tfsg

#   ---------------------------------- heat absorbed by earth-atmosphere
#                                      system  i.e. net solar flux at
#                                      top of the atmosphere.
      add    fsa  fsg  fsag
      timavg fsag tfsag
      rm fsa

#   ---------------------------------- reflected flux at top of atmosphere.
#                                      limit fsr to non-negative values to
#                                      avoid numerical differences between
#                                      fsag calculated from model data  i.e.
#                                      fsa derived from vertical integration
#                                      of heating rates  and "precise"
#                                      calculation of fsi.
      sub    fsi  fsag fsri
      fmask  fsri msk
      mlt    fsri msk  fsr
      timavg fsr  tfsr
      rm fsri msk fsr fsi fsag

#   ---------------------------------- local planetary albedo.
      div tfsr tfsi tpal
      rm tfsr

#   ---------------------------------- flux received at the surface.
ccc   select npakgg fss
      timavg fss    tfss

#   ---------------------------------- ground albedo. limit fsrg to
#                                      non-negative values to avoid problems
#                                      with small numerical differences
#                                      when fss is near zero.
      sub    fss   fsg   fsrgi
      fmask  fsrgi msk
      mlt    fsrgi msk   fsrg
      timavg fsrg  tfsrg
      div    tfsrg tfss  tal
      rm fsrgi msk fss fsg fsrg tfsrg

#   ---------------------------------- visible flux received at the surface.
ccc   select npakgg fsv
      timavg fsv    tfsv
      rm fsv

#   ---------------------------------- direct flux received at the surface.
ccc   select npakgg fsd
      timavg fsd    tfsd
      rm fsd

#   ---------------------------------- clear-sky solar flux at toa.
ccc   select npakgg fstc
      timavg fstc   tfstc
      rm fstc

#   ---------------------------------- clear-sky solar flux at ground.
ccc   select npakgg fsgc
      timavg fsgc   tfsgc
      rm fsgc

#   ---------------------------------- solar cloud forcing at toa.
      sub tfsag tfstc tcfst
      rm cfst

#   ---------------------------------- solar cloud forcing at ground.
      sub tfsg  tfsgc tcfsb
      rm cfsb

#   *******************************************************************
#   ************************* terrestrial radiation *******************
#   *******************************************************************

#   ---------------------------------- downward atmospheric lw at sfc.
ccc   select npakgg fdl
      timavg fdl    tfdl
      rm fdl

#   ---------------------------------- net lw absorbed/emitted by atm.
ccc   select npakgg fla
      timavg fla    tfla
      rm fla

#   ---------------------------------- net lw at the ground.
ccc   select npakgg flg
      timavg flg    tflg
      rm flg

#   ---------------------------------- net lw emitted by earth-atmosphere
#                                      i.e. net lw at the top of atmosphere.
      add tfla tflg tflag

#   ---------------------------------- clear-sky lw flux at toa.
ccc   select npakgg fltc
      timavg fltc   tfltc
      rm fltc

#   ---------------------------------- clear-sky lw flux at ground.
ccc   select npakgg flgc
      timavg flgc   tflgc
      rm flgc

#   ---------------------------------- lw cloud forcing at toa.
      sub tflag tfltc tcflt
      rm cflt

#   ---------------------------------- lw cloud forcing at ground.
      sub tflg  tflgc tcflb
      rm cflb

#   *******************************************************************
#   ************************* total radiation *************************
#   *******************************************************************
#
#   ---------------------------------- radiation budget at the ground.
      add tfsg tflg tfg

#   ---------------------------------- radiation budget at top of atmosphere
#                                      i.e. at the highest full sigma level.
      add tfsag tflag tfag

#   ---------------------------------- total cloud forcing at toa.
      add tcfst tcflt tcft

#   ---------------------------------- total cloud forcing at ground.
      add tcfsb tcflb tcfb

#   *******************************************************************
#   **************************** total cloud **************************
#   *******************************************************************
#
#   ---------------------------------- total overlapped cloud amount
#                                      accumulated .
ccc   select npakgg cldt
      timavg cldt tcldt
      rm cldt

#   ---------------------------------- xsave gaussian grids.
      xsaveup new_gp tpal  tfg   tfag  tfss  tal    \
                     tfsa  tflg  tfla  tfsg  tfdl   \
                     tfsv  tfsd  tcldt tfsag tflag  \
                     tcft  tcfb  tfstc tfsgc tfltc  \
                     tflgc tcfst tcfsb tcflt tcflb

#   ---------------------------------- save results.
      access oldgp ${flabel}gp
      xjoin oldgp new_gp newgp
      save newgp ${flabel}gp
      delete oldgp

end_of_script

cat > Input_Cards <<end_of_data
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
         DAYS      = $days
         RUN       = $run
         FLABEL    = $flabel
         MODEL1    = $model1
         R1        = $r1
         R2        = $r2
         R3        =      $r3
0 RADIAG3 -------------------------------------------------------------- RADIAG3
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FSG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SOLFLX2   $iday $gmt $delt$incday     1365.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FSA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.            -1   -1   GT        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FSS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
FMASK.            -1   -1   GT        0.
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FSV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FSD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      FSTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      FSGC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FDL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1       FLG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      FLTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $r1 $r2 $r3         1    1      FLGC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
SELECT          $t1 $t2 $t3         1    1      CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        FSO
NEWNAM.     FSO
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        LPA
NEWNAM.     LPA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        BALG
NEWNAM.    BALG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        BALT
NEWNAM.    BALT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSS
NEWNAM.     FSS
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        SALB
NEWNAM.    SALB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        FSA
NEWNAM.     FSA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        FLG
NEWNAM.     FLG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE.        FLA
NEWNAM.     FLA
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSG
NEWNAM.     FSG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FDL
NEWNAM.     FDL
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSV
NEWNAM.     FSV
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSD
NEWNAM.     FSD
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CLDT
NEWNAM.    CLDT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSAG
NEWNAM.    FSAG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FLAG
NEWNAM.    FLAG
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CFT
NEWNAM.     CFT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CFB
NEWNAM.     CFB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSTC
NEWNAM.    FSTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FSGC
NEWNAM.    FSGC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FLTC
NEWNAM.    FLTC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         FLGC
NEWNAM.    FLGC
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CFST
NEWNAM.    CFST
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CFSB
NEWNAM.    CFSB
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CFLT
NEWNAM.    CFLT
+   .    :    .    :    .    :    .    :    .    :    .    :    .    :    .    :
XSAVE         CFLB
NEWNAM.    CFLB
end_of_data

. endjcl.cdk
